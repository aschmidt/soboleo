/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.webdocument;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class SetWebDocumentURL extends CommandEvent implements IsSerializable{

	private String docURI;
	private String url;
	
	public SetWebDocumentURL(String docURI, String url) {
		this.docURI = docURI;
		this.url = url;
	}
	
	@SuppressWarnings("unused")
	private SetWebDocumentURL(){}
	
	public String getDocURI() {
		return docURI;
	}
	
	public String getURL() {
		return url;
	}

	
}
