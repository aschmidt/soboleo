package de.fzi.ipe.soboleo.event.logging;

import de.fzi.ipe.soboleo.event.EventImpl;


public class StartAnnotateWebDocument extends EventImpl {
	
	private String url;
	
	public StartAnnotateWebDocument(String url){
		this.url = url;
	}
	
	public StartAnnotateWebDocument() {;}
	
	public String getURL() {
		return url;
	}

}
