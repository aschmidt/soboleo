/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.event.peopletagging;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;


/**
 * Returns an empty tagged person (with only a name and an email address) 
 */
public class AddTaggedPerson extends CommandEvent {
	private String email;
	private String name;
	
	public AddTaggedPerson(String email, String name) {
		this.email = email;
		this.name = name;
	}
	
	@SuppressWarnings("unused")
	private AddTaggedPerson() {; }
	
	public String getEmail() { return email; }
	public String getName() { return name; }
	
}
