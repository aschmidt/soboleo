package de.fzi.ipe.soboleo.event.editor;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;


/**
 * Event to indicate that the taxonomy has  been loaded. Gives components a 
 * chance to get a reference to the ClientSideTaxonomy
 */
public class TaxonomyLoadedEvent extends EditorOnlyEvent {

	private ClientSideTaxonomy tax;
	
	public TaxonomyLoadedEvent(ClientSideTaxonomy tax) {
		this.tax = tax;
	}
	
	public ClientSideTaxonomy getTaxonomy() {
		return tax;
	}
	
	
}
