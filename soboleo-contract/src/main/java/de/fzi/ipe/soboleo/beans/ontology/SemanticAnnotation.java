package de.fzi.ipe.soboleo.beans.ontology;

import java.io.Serializable;

public class SemanticAnnotation implements Serializable {

	private static final long serialVersionUID = 1L;
	// the uri of the concept / instance in an ontology 
	String uri;
	// the id of the user who created the semantic annotation
	String userId;
	// the long value of the date the annotation was created, if new: -1 
	long createdDate=-1;
	// the long value of the date the annotation has changed
	long dateChanged=-1;
	
	public SemanticAnnotation()
	{
		;
	}
	
	public SemanticAnnotation(String uri, String userId, long createdDate, long dateChanged)
	{
		this.uri=uri;
		this.userId=userId;
		this.createdDate=createdDate;
		this.dateChanged=dateChanged;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public long getDateChanged() {
		return dateChanged;
	}

	public void setDateChanged(long dateChanged) {
		this.dateChanged = dateChanged;
	}

	
}
