/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos.peopletag;

import java.util.Set;

public class TaggedExternalPerson extends TaggedPerson{

	public TaggedExternalPerson(String URI, String email, String name, Set<PersonTag> tags) {
		super(URI,email,name,tags);
	}
	
	@SuppressWarnings({ "deprecation", "unused" })
	private TaggedExternalPerson() {; }
	
}
