/*
 * FZI - Information Process Engineering 
 * Created on 27.09.2008 by zach
 */
package de.fzi.ipe.soboleo.event.user;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Given to the EventBusPermitters, they can use it to determine whether an event, command or query 
 * should be allowed. 
 */
public class EventSenderCredentials implements IsSerializable{
	
	public static final EventSenderCredentials SERVER = new EventSenderCredentials("http://www.soboleo.com/server","Server","");
	public static final EventSenderCredentials ANONYM = new EventSenderCredentials("","","");
	
	private String senderURI; 
	private String senderName;
	private String key;

	
	@SuppressWarnings("unused")
	private EventSenderCredentials() {;}
	
	public EventSenderCredentials(String senderURI, String senderName, String key) {
		this.senderURI = senderURI;
		this.senderName = senderName;
		this.key = key;
	}
	
	public String getSenderURI() {
		return senderURI;
	}
	
	public String getSenderName() {
		return senderName;
	}
	
	public String getKey() {
		return key; 
	}
	
	@Override
	public String toString(){
		return (senderURI + ", " +senderName + ", " +key);
	}
	
}
