/*
 * FZI - Information Process Engineering 
 * Created on 07.08.2008 by zach
 */
package de.fzi.ipe.soboleo.event;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface Event extends IsSerializable {

	public static final long EVENT_TYPE_EXCEPTION = 1;
	public static final long EVENT_TYPE_TAXONOMY_CHANGE = 1<<1;
	public static final long EVENT_TYPE_CHAT = 1<<2;
	
	public long getEventType();	

	/**
	 * Gives each event an id (unique within an Space), set by the EventBus as first thing when an event comes in. 
	 * This methods is meant to be only called by the event bus - calling it from anywhere else will later lead
	 * to an exception and will prevent the event from getting distributed. 
	 * The ids increase and establish an order within the event, query and commands. There is not necessarily an 
	 * event for every id (exceptions and stuff may happen)
	 */
	public void setId(long id);

	
	/**
	 * Sets the ID of the sender that send this event. This is set by the EventBus and is not supposed to be set by anyone else
	 * (in fact: setting it anywhere else will lead to an exception)
	 */
	public void setSenderURI(String actor) ;	

	/**
	 * Sets the name of the sender that send this event. This is set by the EventBus and is not supposed to be set by anyone else
	 * (in fact: setting it anywhere else will lead to an exception)
	 */
	public void setSenderName(String senderName); 
	
	public String getSenderURI();
	
	public String getSenderName();
	
	public long getCreationTime();
	

	/**
	 * gets the id of this event, or -1 if an id has not yet been set. 
	 */
	public long getId() ;
	
}
