package de.fzi.ipe.soboleo.beans.dialog;



import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;

public class ConceptDialog extends Dialog{

	private String conceptURI;
	
	/**
	 * @param uri
	 * @param title
	 * @param content
	 * @param aboutConceptURI
	 * @param initiator
	 * @param participants
	 * @param tags
	 */
	public ConceptDialog(String uri, String title, String content, String aboutConceptURI, String initiator, Set<String> participants, Set<Tag> tags) {
		super(uri, title, content, initiator, participants, tags);
		this.conceptURI = aboutConceptURI;
	}

	@Override
	public String getAbout() {
		return conceptURI;
	}

}
