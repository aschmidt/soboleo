/*
 * A string class with an language identifier. 
 * FZI - Information Process Engineering 
 * Created on 23.01.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology;

import com.google.gwt.user.client.rpc.IsSerializable;

public class LocalizedString implements IsSerializable{
	
	/** some language tags according to ISO 639-1 as summarized by http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes, 
	 * feel free to add (this will not break anything) but be careful about removing anything. 
	 */
	public enum Language implements IsSerializable{
		
		en("en","English"),
		es("es","Spanish"),
		de("de","German"),
		fr("fr","French"),
		it("it","Italian");
		
		private String acronym;
		private String name;
		
		Language(String acronym, String name) {
			this.acronym = acronym;
			this.name = name;
		}
		
		public String toString() {
			return acronym;
		}
		
		public String getAcronym() {
			return acronym;
		}
		
		public String getName() {
			return name;
		}
		
		public static Language getLanguage(String acronym) {
			for (Language lan: Language.values()) {
				if (lan.getAcronym().equals(acronym)) return lan;
			}
			return null;
		}
		
	}
	
	private static final Language DEFAULT_LANGUAGE = Language.en;
	
	private String string;
	private Language lan;
	
	public LocalizedString(String string, Language lan) {
		this.string = string.trim();
		this.lan = lan;
	}
	
	public LocalizedString(String string, String language) {
		this.string = string;
		this.lan = Language.valueOf(language);
	}
	
	public LocalizedString(String string) {
		this(string,DEFAULT_LANGUAGE);
	}
	
	/**
	 * Constructor exists only for GWT
	 */
	public LocalizedString() {
		;
	}
	
	public String getString() {
		return string;
	}
	
	public Language getLanguage() {
		return lan;
	}
	
	@Override
	public String toString() {
		return string+"@"+lan;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof LocalizedString) {
			LocalizedString other = (LocalizedString) o;
			return (lan.equals(other.lan) && string.equals(other.string));
		}
		else return false;
	}

	@Override
	public int hashCode() {
		return (lan+string).hashCode();
	}

}
