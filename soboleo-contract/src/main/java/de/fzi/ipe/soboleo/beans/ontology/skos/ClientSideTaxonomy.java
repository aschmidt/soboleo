/*
 * FZI - Information Process Engineering 
 * Created on 26.01.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.ontology.ComplexTaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveCreateConcept;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveConcept;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveTaxonomyChangeCommand;

public class ClientSideTaxonomy implements IsSerializable{

	
	private HashMap<String,ClientSideConcept> concepts = new HashMap<String, ClientSideConcept>();
	
	//the last event seen and processed by this ClientSideTaxonomy (or before 
	// this ClientSideTaxonomy was created)
	private long versionID = -1L;
	private transient SynonymsTrie trie = null;
	
	
	/**
	 * Constructor only for GWT
	 */
	public ClientSideTaxonomy() {
		;
	}
	
	public ClientSideTaxonomy(Set<ClientSideConcept> concepts, long versionID) {
		this.versionID = versionID;
		for (ClientSideConcept c:concepts) this.concepts.put(c.getURI(), c);
	}
	
	public ClientSideConcept get(String URI) {
		return concepts.get(URI);
	}
	
	public long getVersion() {
		return versionID;
	}
	
	public Collection<ClientSideConcept> getConcepts() {
		return concepts.values();
	}

	public ClientSideConcept getConceptForPrefLabel(LocalizedString prefLabel) {
		for (ClientSideConcept c: concepts.values()) {
			LocalizedString currentLabel = c.getText(SKOS.PREF_LABEL, prefLabel.getLanguage());
			if (currentLabel != null && currentLabel.equals(prefLabel)) return c;
		}
		return null;
	}
	
	public ClientSideConcept getConceptForPrefLabel(LocalizedString prefLabel, boolean isCaseSensitive){
		for (ClientSideConcept c: concepts.values()) {
			LocalizedString currentLabel = c.getText(SKOS.PREF_LABEL, prefLabel.getLanguage());
			if(isCaseSensitive){
				if (currentLabel != null && currentLabel.getString().equalsIgnoreCase(prefLabel.getString())) return c;
			}
			else
				if (currentLabel != null && currentLabel.equals(prefLabel)) return c;
		}
		return null;
	}
	
	public Set<ClientSideConcept> getConceptsForPrefLabel(String prefLabel) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> prefLabels = c.getTexts(SKOS.PREF_LABEL);
			for(LocalizedString currentLabel : prefLabels){
				if (currentLabel.getString().equals(prefLabel)){
					toReturn.add(c);
					break;
				}
			}
		}
		return toReturn;
	}
	
	/**
	 * Also includes descriptions
	 */
	public Set<ClientSideConcept> getConceptsForLabel(String label) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> texts = c.getTexts();
			for(LocalizedString currentText : texts){
				if (currentText.getString().equals(label)){
					toReturn.add(c); 
					break;
				}
			}
		}
		return toReturn;
	}
	
	public Set<ClientSideConcept> getConceptsForLabel(String label, boolean isCaseSensitive) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> texts = c.getTexts();
			for(LocalizedString currentText : texts){
				if(isCaseSensitive){
					if (currentText.getString().equalsIgnoreCase(label)){
						toReturn.add(c); 
						break;
					}
				}else{
					if (currentText.getString().equals(label)){
						toReturn.add(c); 
						break;
					}
				}
			}
		}
		return toReturn;
	}
	
	/**
	 * Also includes descriptions
	 */
	public Set<ClientSideConcept> getConceptsForLabel(String label, SKOS property) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> texts = c.getTexts(property);
			for(LocalizedString currentText : texts){
				if (currentText.getString().equals(label)){
					toReturn.add(c); 
					break;
				}
			}
		}
		return toReturn;
	}
	
	public Set<ClientSideConcept> getConceptsForLabel(String label, SKOS property, boolean isCaseSensitive) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> texts = c.getTexts(property);
			for(LocalizedString currentText : texts){
				if(isCaseSensitive){
					if (currentText.getString().equalsIgnoreCase(label)){
						toReturn.add(c); 
						break;
					}
				}else{
					if (currentText.getString().equals(label)){
						toReturn.add(c); 
						break;
					}
				}
			}
		}
		return toReturn;
	}
	
	
	
	/**
	 * Also includes descriptions
	 */
	public Set<ClientSideConcept> getConceptsForLabel(LocalizedString label) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> texts = c.getTexts();
			for(LocalizedString currentText : texts){
				if (currentText.equals(label)) {
					toReturn.add(c);
					break;
				}
			}
		}
		return toReturn;
	}
	
	public Set<ClientSideConcept> getConceptsForLabel(LocalizedString label, boolean isCaseSensitive) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> texts = c.getTexts();
			for(LocalizedString currentText : texts){
				if(isCaseSensitive){
					if(currentText.getString().equalsIgnoreCase(label.getString())){
						toReturn.add(c);
						break;
					}
				}else {
					if (currentText.equals(label)) {
						toReturn.add(c);
						break;
					}
				}
			}
		}
		return toReturn;
	}
	
	public Set<ClientSideConcept> getConceptsForLabel(LocalizedString label, SKOS property) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> labels = c.getTexts(property);
			for(LocalizedString currentLabel : labels){
				if (currentLabel.equals(label)){
					toReturn.add(c);
					break;
				}
			}
		}
		return toReturn;
	}
	
	public Set<ClientSideConcept> getConceptsForLabel(LocalizedString label, SKOS property, boolean isCaseSensitive) {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			Set<LocalizedString> labels = c.getTexts(property);
			for(LocalizedString currentLabel : labels){
				if(isCaseSensitive){
					if(currentLabel.getString().equalsIgnoreCase(label.getString())){
						toReturn.add(c);
						break;
					}
				}else {
					if (currentLabel.equals(label)){
						toReturn.add(c);
						break;
					}
				}
			}
		}
		return toReturn;
	}
	
	public Set<ClientSideConcept> getRootConcepts() {
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			if (c.getConnected(SKOS.HAS_BROADER).size()==0) toReturn.add(c);
		}
		return toReturn;
	}
	
	
	public List<ClientSideConcept> getSortedRootConcepts(String protoName, Language... languages) {
		ClientSideConcept prototypicalConcept = null;
		Set<ClientSideConcept> unsortedRoots = new HashSet<ClientSideConcept>();
		for (ClientSideConcept c: concepts.values()) {
			if(c.getText(SKOS.PREF_LABEL, Language.en) != null && c.getText(SKOS.PREF_LABEL, Language.en).getString().equals(protoName)) prototypicalConcept = c;
			else if (c.getConnected(SKOS.HAS_BROADER).size()==0) unsortedRoots.add(c);
		}
		List<ClientSideConcept>toReturn = ClientSideTaxonomy.sortConcepts(unsortedRoots, languages);
		if (prototypicalConcept!=null) toReturn.add(prototypicalConcept);
		return toReturn;
	}
	
	public boolean hasEqualValues(ClientSideTaxonomy other) {
		if (!concepts.keySet().equals(other.concepts.keySet())) return false;
		else {
			for (String key: concepts.keySet()) {
				if (!concepts.get(key).hasEqualValues(other.concepts.get(key))) return false;
			}
		}
		return true;
	}
	
	/**
	 * Takes an command event and applies it to this taxonomy (note: this happens without any checking!)
	 */
	public void apply(CommandEvent event) {
		if (event.getId()<=versionID) return; //have already seen this event. 
		else {
			versionID = event.getId();
			applyInternal(event);
		}
		
	}

	private void applyInternal(CommandEvent event) {
		if (event instanceof ComplexTaxonomyChangeCommand) {
			ComplexTaxonomyChangeCommand current = (ComplexTaxonomyChangeCommand) event;
			for (CommandEvent e: current.getImpliedCommands()) applyInternal(e);
		}
		else if (event instanceof PrimitiveTaxonomyChangeCommand) {
			if (event instanceof PrimitiveRemoveConcept) {
				trie = null;
				PrimitiveRemoveConcept current = (PrimitiveRemoveConcept) event;
				concepts.remove(current.getURI());
			}
			else if (event instanceof PrimitiveCreateConcept) {
				trie = null;
				PrimitiveCreateConcept current = (PrimitiveCreateConcept) event;
				ClientSideConcept newConcept = new ClientSideConcept(current.getURI());
				newConcept.apply(current, this);
				concepts.put(current.getURI(), newConcept);
			}
			else {
				trie = null;
				PrimitiveTaxonomyChangeCommand current = (PrimitiveTaxonomyChangeCommand) event;
				ClientSideConcept concept = concepts.get(current.getFromURI());
				concept.apply(current,this);	
			}
		}
	}
	
	public static List<ClientSideConcept> sortConcepts(Set<? extends ClientSideConcept> concepts,final Language... languages){
		List<ClientSideConcept> sortedConcepts = new ArrayList<ClientSideConcept>();					
		for (ClientSideConcept c:concepts) sortedConcepts.add(c);

		Collections.sort(sortedConcepts, new ClientSideConceptComparator(languages));
		return sortedConcepts;
	}
	
	public Set<String> getAllConnectedURIs(Set<String> conceptURIs,SKOS property) {
		Set<String> processed = new HashSet<String>();
		Stack<String> toDo = new Stack<String>();		
		Set<String> toReturn = new HashSet<String>();
		toDo.addAll(conceptURIs);
		while (toDo.size() > 0) {
			String currentURI = toDo.pop();
			if (!processed.contains(currentURI)) {
				processed.add(currentURI);
				ClientSideConcept c = get(currentURI);
				if(c != null){
					toReturn.add(currentURI);
					toDo.addAll(c.getConnected(property));
				}
			}
		}
		toReturn.removeAll(conceptURIs);
		return toReturn;
	}
	
	public Set<ClientSideConcept> getAllConnectedConcepts(Set<String> conceptURIs,SKOS property) {
		Set<String> processed = new HashSet<String>();
		Stack<String> toDo = new Stack<String>();		
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		toDo.addAll(conceptURIs);
		while (toDo.size() > 0) {
			String currentURI = toDo.pop();
			if (!processed.contains(currentURI)) {
				processed.add(currentURI);
				ClientSideConcept c = get(currentURI);
				if(c != null){
					toReturn.add(c);
					toDo.addAll(c.getConnected(property));
				}
			}
		}
		for(String currentURI : conceptURIs){
			ClientSideConcept c = get(currentURI);
			if(c != null) toReturn.remove(c);
		}
		return toReturn;
	}
	
	public Set<ClientSideConcept> getAllSubconcepts(String conceptURI){
		Set<String> conceptURIs = new HashSet<String>();
		conceptURIs.add(conceptURI);
		return getAllConnectedConcepts(conceptURIs, SKOS.HAS_NARROWER);
	}
	
	public Set<String> getAllSubconceptURIs(String conceptURI){
		Set<String> conceptURIs = new HashSet<String>();
		conceptURIs.add(conceptURI);
		return getAllConnectedURIs(conceptURIs,SKOS.HAS_NARROWER);
	}
	
	public Set<ClientSideConcept> getAllSuperconcepts(String conceptURI){
		Set<String> conceptURIs = new HashSet<String>();
		conceptURIs.add(conceptURI);
		return getAllConnectedConcepts(conceptURIs,SKOS.HAS_BROADER);
	}	
	
	public Set<String> getAllSuperconceptURIs(String conceptURI){
		Set<String> conceptURIs = new HashSet<String>();
		conceptURIs.add(conceptURI);
		return getAllConnectedURIs(conceptURIs,SKOS.HAS_BROADER);
	}
	
	public Set<ClientSideConcept> getExtractedConcepts(String searchString) {
		if (trie == null) {
			trie = new SynonymsTrie(this);
		}
		return trie.getConcepts(searchString);
	}
	
	public Set<String> getExtractedConceptURIs(String searchString){
		Set<String> extractedConceptURIs = new HashSet<String>();
		for(ClientSideConcept c : getExtractedConcepts(searchString)) extractedConceptURIs.add(c.getURI());
		return extractedConceptURIs;
	}
}
