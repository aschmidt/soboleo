/*
 * FZI - Information Process Engineering 
 * Created on 07.08.2008 by zach
 */
package de.fzi.ipe.soboleo.eventbus;

import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.common.ExceptionEvent;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;

/**
 * The EventBus is the main means of communication between components. There is one EventBus for each space and one for each server instance.
 * For more documentation please consult http://sites.google.com/a/soboleo.com/soboleo/Home/documentation-eventbus
 */
public class EventBus {

	private EventBusProcessor processor; 	
	private ReadWriteLock rwLock = new ReentrantReadWriteLock();
	 
	private boolean isProcessingCommand = false;
	
	private long eventID = 0; 
	private Object eventIDLock = new Object(); // just the lock used to synchronize access to the eventID variable
	
	private Queue<EventCredentialTuple<Event>> eventsToProcess = new ConcurrentLinkedQueue<EventCredentialTuple<Event>>();
	private Queue<EventCredentialTuple<CommandEvent>> commandsToProcess = new ConcurrentLinkedQueue<EventCredentialTuple<CommandEvent>>();
	
	private Set<EventBusPermitter> permitters = new HashSet<EventBusPermitter>();
	private Set<EventBusCommandProcessor> commandProcessors = new HashSet<EventBusCommandProcessor>();
	private Set<EventBusQueryProcessor> queryProcessors = new HashSet<EventBusQueryProcessor>();
	private Set<EventBusEventProcessor> eventListeners = new HashSet<EventBusEventProcessor>();
	
	//temporary storage for listeners to add /to remove - needed to allow non-blocking add/remove of listeners
	private List<EventBusListener> listenersToAdd = Collections.synchronizedList(new LinkedList<EventBusListener>());
	private List<EventBusListener> listenersToRemove = Collections.synchronizedList(new LinkedList<EventBusListener>());
	
	public EventBus() {
		processor = new EventBusProcessor();
		processor.start();
	}
	
	public void stop() {
		processor.stopped = true;
		processor.notifyProcessor();
		try {
			processor.join();
		} catch (InterruptedException e) {;}
	}
	
	public void sendEvent(Event event,EventSenderCredentials credentials) {
		event.setSenderURI(credentials.getSenderURI());
		event.setSenderName(credentials.getSenderName());
		eventsToProcess.add(new EventCredentialTuple<Event>(event,credentials));
		processor.notifyProcessor();
	}
	
	public void executeCommand(CommandEvent command,EventSenderCredentials credentials) {
		command.setSenderURI(credentials.getSenderURI());
		command.setSenderName(credentials.getSenderName());
		commandsToProcess.add(new EventCredentialTuple<CommandEvent>(command,credentials));
		processor.notifyProcessor();
	}
	
	public Object executeCommandNow(CommandEvent command, EventSenderCredentials credentials) throws EventPermissionDeniedException {
		command.setSenderURI(credentials.getSenderURI());
		command.setSenderName(credentials.getSenderName());
		return distributeCommand(command,credentials);
	}
	
	public Object executeQuery(QueryEvent query, EventSenderCredentials credentials) throws EventPermissionDeniedException{
		query.setSenderURI(credentials.getSenderURI());
		query.setSenderName(credentials.getSenderName());
		return distributeQuery(query,credentials);
	}
	
	private void giveIDForEvent(Event e) {
		synchronized (eventIDLock) {
			e.setId(++eventID);
		}
	}
	
	
	public void checkWritePermission(EventSenderCredentials creds) throws EventPermissionDeniedException{
		getPermissionCommand(null, creds);
	}
	
	public void checkReadPermission(EventSenderCredentials creds) throws EventPermissionDeniedException {
		getPermissionEvent(null, creds);
	}
	
	private void getPermissionEvent(Event event, EventSenderCredentials credentials) throws EventPermissionDeniedException {
		for (EventBusPermitter permitter:permitters) {
			try {
				String failureReason = permitter.permitEvent(event,credentials);
				if (failureReason != null) throw new EventPermissionDeniedException(failureReason,event);
			} catch (RuntimeException t) {
				throw new EventPermissionDeniedException(t.getMessage(),event);
			}
		}		
	}
	
	private void getPermissionCommand(CommandEvent event, EventSenderCredentials credentials) throws EventPermissionDeniedException {
		for (EventBusPermitter permitter:permitters) {
			try {
				String failureReason = permitter.permitCommand(event,credentials);
				if (failureReason != null) throw new EventPermissionDeniedException(failureReason,event);
			} catch (RuntimeException t) {
				throw new EventPermissionDeniedException(t.getMessage(),event);
			}
		}		
	}
	
	
	private Object distributeCommand(CommandEvent cmd, EventSenderCredentials credentials) throws EventPermissionDeniedException, IllegalStateException {
		giveIDForEvent(cmd);
		try {
			rwLock.writeLock().lock();
			manageListenerInternal(); 
			if (isProcessingCommand) throw new ConcurrentModificationException("You must not call executeCommandNow during command processing!");
			isProcessingCommand = true;
			
			//extend command, start again at the beginning on a changed command. 
			int changeCounter = 0;
			boolean changed;
			do {
				changed = false;
				if (changeCounter >20) throw new IllegalStateException("Command Preprocessing failed: changed to many times");
				
				for (EventBusCommandProcessor processor: commandProcessors) {
					CommandEvent newCommand = processor.prepare(cmd);
					if ((newCommand!=null) && (newCommand != cmd)){
						newCommand = cmd;
						changed=true;
						changeCounter++;
						break;
					}
				}
			} while (changed == true);
			
			getPermissionCommand(cmd,credentials);			
			
			//execute command
			Object result = null;
			for (EventBusCommandProcessor processor: commandProcessors) {
				try {
					Object newResult = processor.process(cmd);
					if (newResult != null) result = newResult;
				} catch(RuntimeException re) {
					sendEvent(new ExceptionEvent(re, ExceptionEvent.Severity.WillManage),EventSenderCredentials.SERVER);
				}
			}
			
			//send command as event & return
			distributeEvent(cmd,credentials); //sendEvent
			return result;			
		} finally {
			isProcessingCommand = false;
			rwLock.writeLock().unlock();
		}
	}
	
	private Object distributeQuery(QueryEvent query, EventSenderCredentials credentials) throws EventPermissionDeniedException, IllegalStateException{
		manageListenerInternal();
		giveIDForEvent(query);
		try {
			rwLock.readLock().lock();
			
			//extend query, start again at the beginning on a changed query. 
			boolean changed;
			int changeCounter = 0;
			do {
				changed = false;
				if (changeCounter >20) throw new IllegalStateException("Query Preprocessing failed: changed to many times");

				for (EventBusQueryProcessor processor: queryProcessors) {
					try {
						QueryEvent newQuery = processor.prepare(query);
						if ((newQuery != null) && (newQuery != query)) {
							query = newQuery;
							changed = true;
							changeCounter++;
							break;
						}
					} catch (Exception e) {
						sendEvent(new ExceptionEvent(e, ExceptionEvent.Severity.WillManage),EventSenderCredentials.SERVER);
					}
				}
			} while (changed == true);
			
			getPermissionEvent(query,credentials);
			
			//execute query
			for (EventBusQueryProcessor processor: queryProcessors) {
				Object result = processor.process(query);
				if (result != null) return result;
			}
			
			return null;
			
		} finally {
			rwLock.readLock().unlock();
		}
	}
	
	private void distributeEvent(Event event,EventSenderCredentials credentials) throws EventPermissionDeniedException {
		manageListenerInternal();
		giveIDForEvent(event);
		try {
			rwLock.readLock().lock();
			getPermissionEvent(event,credentials);
			
			for (EventBusEventProcessor listener:eventListeners) {
				try {
					listener.receiveEvent(event);
				} catch (RuntimeException t) {
					sendEvent(new ExceptionEvent(t, ExceptionEvent.Severity.WillManage),EventSenderCredentials.SERVER);
				}
			}
		} finally {
			rwLock.readLock().unlock();
		}
	}
	
	
	public void addListener(EventBusListener lis) {
		listenersToAdd.add(lis);
	}
	
	public void removeListener(EventBusListener lis) {
		listenersToRemove.add(lis);
	}


	private void manageListenerInternal() {
		if (listenersToAdd.size()>0) addListenerInternal();
		if (listenersToRemove.size()>0) removeListenerInternal();
	}
	
	private void addListenerInternal() {
		try {
			rwLock.writeLock().lock();
			while (listenersToAdd.size() >0) {
			EventBusListener lis = listenersToAdd.remove(0);
				boolean foundOne = false;
				if (lis instanceof EventBusCommandProcessor) {
					EventBusCommandProcessor commandprocessor = (EventBusCommandProcessor) lis;
					commandProcessors.add(commandprocessor);
					foundOne = true;
				}
				if (lis instanceof EventBusQueryProcessor) {
					EventBusQueryProcessor queryProcessor = (EventBusQueryProcessor) lis;
					queryProcessors.add(queryProcessor);
					foundOne = true;
				}
				if (lis instanceof EventBusEventProcessor) {
					EventBusEventProcessor eventProcessor = (EventBusEventProcessor) lis;
					eventListeners.add(eventProcessor);
					foundOne = true;
				}
				if (lis instanceof EventBusPermitter) {
					EventBusPermitter permitter = (EventBusPermitter) lis;
					permitters.add(permitter);
					foundOne = true;
				}
				if (!foundOne) {
					throw new IllegalArgumentException("Listener needs to implement at least one sub-interface of EventBusListener");
				}
			}
		} finally {
			rwLock.writeLock().unlock();
		}		
	}

	private void removeListenerInternal () {
		try {
			rwLock.writeLock().lock();
			while (listenersToRemove.size() >0) {
				EventBusListener lis = listenersToRemove.remove(0);
				if (lis instanceof EventBusCommandProcessor) {
					EventBusCommandProcessor commandprocessor = (EventBusCommandProcessor) lis;
					commandProcessors.remove(commandprocessor);
				}
				if (lis instanceof EventBusQueryProcessor) {
					EventBusQueryProcessor queryProcessor = (EventBusQueryProcessor) lis;
					queryProcessors.remove(queryProcessor);
				}
				if (lis instanceof EventBusEventProcessor) {
					EventBusEventProcessor eventProcessor = (EventBusEventProcessor) lis;
					eventListeners.remove(eventProcessor);
				}
				if (lis instanceof EventBusPermitter) {
					EventBusPermitter permitter = (EventBusPermitter) lis;
					permitters.remove(permitter);
				}
			}
		} finally {
			rwLock.writeLock().unlock();
		}		
	}

	private static class EventCredentialTuple<T extends Event>{
		
		T e;
		EventSenderCredentials c;
		
		public EventCredentialTuple(T e, EventSenderCredentials c) {
			this.e = e;
			this.c = c;
		}
		
	}
	
	private class EventBusProcessor extends Thread {
		
		volatile boolean stopped = false;
		
		public void run() {
			while (true) {
				try {
					if (!commandsToProcess.isEmpty()) {
						EventCredentialTuple<CommandEvent> cmd = commandsToProcess.poll();
						distributeCommand(cmd.e, cmd.c);
					}
					else if (!eventsToProcess.isEmpty()) {
						EventCredentialTuple<Event> event = eventsToProcess.poll();
						distributeEvent(event.e,event.c);
					}
					else {
						if (stopped) break;
						else waitProcessor();
					}
				} catch (Throwable t) {
					sendEvent(new ExceptionEvent(t,ExceptionEvent.Severity.WillManage),EventSenderCredentials.SERVER);
				}
			}
		}
		
		private synchronized void waitProcessor() {
			try {
				wait();
			} catch (InterruptedException e) {}
		}
		
		private synchronized void notifyProcessor() {
			notify();
		}
	}

	
}
