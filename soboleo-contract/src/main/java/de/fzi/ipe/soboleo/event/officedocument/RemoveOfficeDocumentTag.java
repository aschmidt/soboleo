/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.officedocument;

import de.fzi.ipe.soboleo.event.document.RemoveTag;

public class RemoveOfficeDocumentTag extends RemoveTag{

	
	public RemoveOfficeDocumentTag(String docURI, String tagURI, String conceptURI) {
		super(docURI, tagURI,conceptURI);
	}
	
	@SuppressWarnings("unused")
	private RemoveOfficeDocumentTag(){;}
}
