/*
 * FZI - Information Process Engineering 
 * Created on 07.08.2008 by zach
 */
package de.fzi.ipe.soboleo.eventbus;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;




public interface EventBusPermitter extends EventBusListener {

	
	public String permitEvent(Event event, EventSenderCredentials credentials);
	public String permitCommand(CommandEvent event, EventSenderCredentials credentials);
	
}
