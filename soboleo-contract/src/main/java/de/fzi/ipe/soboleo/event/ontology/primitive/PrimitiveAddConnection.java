/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology.primitive;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;

public class PrimitiveAddConnection extends PrimitiveTaxonomyChangeCommand{

	private String fromURI,toURI;
	private SKOS connection;
	
	public PrimitiveAddConnection(String fromURI, SKOS connection, String toURI) {
		this.fromURI = fromURI;
		this.connection = connection;
		this.toURI = toURI; 
	}

	@SuppressWarnings("unused") //only used for gwt serialization
	private PrimitiveAddConnection() {;}
	
	public String getFromURI() {
		return fromURI;
	}

	public String getToURI() {
		return toURI;
	}

	public SKOS getConnection() {
		return connection;
	}
	
	
}
