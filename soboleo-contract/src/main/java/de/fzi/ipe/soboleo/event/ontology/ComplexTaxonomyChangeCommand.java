/*
 * Superclass for all (non-primitive) changes to the SKOS taxonomy. All instances of this 
 * class represent high level changes that get extended with primitive events later on, e.g.
 * an instance of this class may represent the deletion of a concept, this would then be 
 * extended by primitive events like (remove related relation from a to b, remove related relation
 * from b to a etc). 
 * 
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ComplexTaxonomyChangeCommand extends TaxonomyChangeCommand {

	private List<TaxonomyChangeCommand> impliedChanges = new LinkedList<TaxonomyChangeCommand>();
	
	@Override
	public long getEventType() {
		return EVENT_TYPE_TAXONOMY_CHANGE; 
	}
	
	public void addImpliedCommand(TaxonomyChangeCommand cmd){
		this.impliedChanges.add(cmd);
		cmd.setParentEvent(this);
	}
	
	public void addImpliedCommands(Collection<TaxonomyChangeCommand> cmds) {
		this.impliedChanges.addAll(cmds);
		for (TaxonomyChangeCommand cmd:cmds) cmd.setParentEvent(this);
	}
	
	public List<TaxonomyChangeCommand> getImpliedCommands() {
		//in ideal world this would be immutable, but this is bound to lead to problems with the GWT compiler
		return impliedChanges; 
	}
	
	
}
