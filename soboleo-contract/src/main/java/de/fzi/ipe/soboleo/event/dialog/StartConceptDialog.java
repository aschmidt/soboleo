/*
 * FZI - Information Process Engineering 
 * Created on 25.06.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

/**
 * Represents a chat message, i.e. the content and the id of the sender.
 */
public class StartConceptDialog extends CommandEvent implements ReadableEditorEvent{
	
	private String aboutURI;
	private String title;
	private Language userLanguage;
	private Set<String> participants;
	private String uri;
	
	public StartConceptDialog(String aboutURI, String title, Language userLanguage) {
		this.aboutURI = aboutURI;
		this.title = title;
		this.userLanguage = userLanguage;
	}
	
	@SuppressWarnings("unused")
	private StartConceptDialog() {
		;
	}
	
	public String getAboutURI() {
		return aboutURI;
	}

	public String getTitle(){
		return title;
	}
	
	public Language getUserLanguage(){
		return userLanguage;
	}
	
	public Set<String> getParticipants(){
		return participants;
	}

	public void addParticipant(String userID){
		participants.add(userID);
	}
	
	public boolean hasParticipant(String userID){
		return participants.contains(userID);
	}
	
	public void setURI(String uri){
		this.uri = uri;
	}
	
	public String getURI(){
		return uri;
	}
	
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		String conceptName ="";
		ClientSideConcept concept = tax.get(getAboutURI());
		if (concept != null) conceptName = concept.getBestFitText(SKOS.PREF_LABEL, lan).getString();
		
		if (lan == Language.de) return getSenderName() + " hat einen neuen Dialog über das Konzept "+conceptName + " gestartet.";
		else if (lan == Language.es) return getSenderName() + " comenzaba un debate del tema "+conceptName + ".";
		else return getSenderName() + " has started a new dialog about the concept " +conceptName+".";
	}
}
