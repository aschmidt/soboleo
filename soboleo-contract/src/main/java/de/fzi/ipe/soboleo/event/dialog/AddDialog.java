/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;


import de.fzi.ipe.soboleo.event.document.AddDocument;

public abstract class AddDialog extends AddDocument{
	
	private String aboutURI;
	
	public AddDialog(String title, String aboutURI, String docOwner) {
		super(title,docOwner);
		this.aboutURI = aboutURI;
	}
	
	public String getAboutURI(){
		return aboutURI;
	}
	
	protected AddDialog(){;}
}
