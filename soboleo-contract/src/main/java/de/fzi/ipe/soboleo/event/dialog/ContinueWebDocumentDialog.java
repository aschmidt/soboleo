/*
 * FZI - Information Process Engineering 
 * Created on 25.06.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

/**
 * Represents a chat message, i.e. the content and the id of the sender.
 */
public class ContinueWebDocumentDialog extends ContinueDialog implements ReadableEditorEvent{

	public ContinueWebDocumentDialog(String dialogURI, Language userLanguage) {
		super(dialogURI, userLanguage);
	}
	
	@SuppressWarnings("unused")
	private ContinueWebDocumentDialog() {
		;
	}
	
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		if (lan == Language.de) return getSenderName() + " führt einen Dialog über ein Webdokument fort. ";
		else if(lan == Language.es) return getSenderName() + " 	continúa una discusión de una página web.";
		else return getSenderName() + " continues a dialog about a webdocument.";
	}
}
