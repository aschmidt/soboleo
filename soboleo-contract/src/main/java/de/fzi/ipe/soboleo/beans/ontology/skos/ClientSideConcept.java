/*
 * FZI - Information Process Engineering 
 * Created on 26.01.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveAddConnection;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveAddText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveChangeText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveCreateConcept;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveConnection;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveTaxonomyChangeCommand;

/**
 * A class whose objects can be used as cached representations of the taxonomy. Its
 * meant to be used both on server and in the browser. It can be updated through 
 * command objects.
 */
public class ClientSideConcept implements IsSerializable{
	
	private String uri;

	private VariedRelationStore<LocalizedString> texts = new VariedRelationStore<LocalizedString>();
	private VariedRelationStore<String> relations = new VariedRelationStore<String>();
	
	public ClientSideConcept(String uri) {
		this.uri = uri;
	}
	
	/**
	 * Constructor only for GWT
	 */
	public ClientSideConcept(){
		;
	}
	
	/** This method is only meant to be used in the initial creation of the concept */
	public void initializeText(SKOS property, LocalizedString text) {
		texts.addValue(property, text);
	}
	
	/** This method is only meant to be used in the initial creation of the concept */
	public void initializeRelation(SKOS property, String uri) {
		relations.addValue(property, uri);
	}
	
	public String getURI() {
		return uri;
	}
	
	public Set<LocalizedString> getTexts(SKOS... properties) {
		Set<LocalizedString> toReturn = new HashSet<LocalizedString>();
		for(SKOS prop : properties){
			toReturn.addAll(texts.getValues(prop));
		}
		return toReturn;
	}
	
	public Set<LocalizedString> getTexts(){
		return texts.getValues();
	}
	
	public Set<LocalizedString> getTexts(SKOS property, Language lan){
		Set<LocalizedString> languageTexts = new HashSet<LocalizedString>(); 
		for (LocalizedString string : getTexts(property)) {
			if (string.getLanguage().equals(lan)) languageTexts.add(string); 
		}
		return languageTexts;
	}
	
	public Set<LocalizedString> getTexts(Language lan){
		Set<LocalizedString> languageTexts = new HashSet<LocalizedString>();
		for (LocalizedString string : getTexts()) {
			if (string.getLanguage().equals(lan)) languageTexts.add(string); 
		}
		return languageTexts;
	}
	
	public LocalizedString getText(SKOS property, Language lan) {
		for (LocalizedString string: getTexts(property)) {
			if (string.getLanguage().equals(lan)) return string;
		}
		return null;
	}
	
	public LocalizedString getText(SKOS property) {
		return texts.getValue(property);
	}
	
	public Set<String> getConnected(SKOS property) {
		return relations.getValues(property);
	}
	
	public Set<ClientSideConcept> getConnectedAsConcepts(SKOS property, ClientSideTaxonomy taxonomy) {
		Set<String> uris = getConnected(property);
		Set<ClientSideConcept> toReturn = new HashSet<ClientSideConcept>();
		for (String s:uris) toReturn.add(taxonomy.get(s));
		return toReturn; 
	}

	public LocalizedString getBestFitText(SKOS property, Language... languages){
		for(Language lan : languages){
			for (LocalizedString string: getTexts(property)) {
				if (string.getLanguage().equals(lan)) return string;
			}
		}
		if (texts.getValue(property)!= null) return texts.getValue(property);
		return new LocalizedString(getURI());
	}
	
	/**
	 * Compares to ClientSideConcept objects, returns true if all values (included the uri) are equal to the other concept.
	 * This method is not named "equals", because it would be not consistent with the hashCode function.
  	 * @Override
	 */
	public boolean hasEqualValues(ClientSideConcept other) {
		if (!getURI().equals(other.getURI())) return false;
		else if (!texts.hasEqualValues(other.texts)) return false;
		else if (!relations.hasEqualValues(other.relations)) return false;
		else return true;
	}
	
	public void apply(PrimitiveTaxonomyChangeCommand cmd,ClientSideTaxonomy tax) {
		if (cmd instanceof PrimitiveAddConnection) {
			PrimitiveAddConnection current = (PrimitiveAddConnection) cmd;
			relations.addValue(current.getConnection(), current.getToURI());
		}
		else if (cmd instanceof PrimitiveRemoveConnection) {
			PrimitiveRemoveConnection current = (PrimitiveRemoveConnection) cmd;
			relations.removeValue(current.getConnection(), current.getToURI());
		}
		else if (cmd instanceof PrimitiveAddText) {
			PrimitiveAddText current = (PrimitiveAddText) cmd;
			texts.addValue(current.getConnection(), current.getText());
		}
		else if (cmd instanceof PrimitiveRemoveText) {
			PrimitiveRemoveText current = (PrimitiveRemoveText) cmd;
			texts.removeValue(current.getConnection(), current.getText());
		}
		else if (cmd instanceof PrimitiveChangeText) {
			PrimitiveChangeText current = (PrimitiveChangeText) cmd;
			texts.removeValue(current.getConnection(), current.getOldText());
			texts.addValue(current.getConnection(), current.getNewText());
		}
		else if (cmd instanceof PrimitiveCreateConcept) {
			PrimitiveCreateConcept current = (PrimitiveCreateConcept) cmd;
			texts.addValue(SKOS.PREF_LABEL,current.getInitialName());
		}
		else {
			throw new IllegalArgumentException(cmd.getClass() + " Unknown type of change event");
		}
	}
	
	
}
