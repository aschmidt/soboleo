/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.webdocument;

import de.fzi.ipe.soboleo.event.document.RemoveDocument;

public class RemoveWebDocument extends RemoveDocument {

	public RemoveWebDocument(String docURI) {
		super(docURI);
	}
	
	@SuppressWarnings("unused")
	private RemoveWebDocument(){;}
}
