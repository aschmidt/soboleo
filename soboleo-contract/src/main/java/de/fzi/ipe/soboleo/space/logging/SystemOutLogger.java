/*
 * FZI - Information Process Engineering 
 * Created on 02.07.2009 by zach
 */
package de.fzi.ipe.soboleo.space.logging;

import java.util.Properties;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.common.ExceptionEvent;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;

/** 
 * Listener to the event bus and - if configured that way - prints stuff to sys out. 
 */
public class SystemOutLogger implements EventBusEventProcessor, EventBusCommandProcessor, EventBusQueryProcessor {

    private int level = 0;
	
	public SystemOutLogger(Properties properties) {
		String sysoutLevel =  properties.getProperty("sysoutLogging");
		if (sysoutLevel == null) sysoutLevel = "exceptions";
		if (sysoutLevel.equals("nothing")) level = 0;
		else if (sysoutLevel.equals("exceptions")) level = 1;
		else if (sysoutLevel.equals("commands")) level = 2;
		else if (sysoutLevel.equals("events")) level = 3;
		else if (sysoutLevel.equals("query")) level = 4;
//		if (level >0) System.out.println("Started SystemOutLogger with level "+level);
	}
	
	@Override
	public void receiveEvent(Event event) {
		if (level >= 3) { 
			System.out.println("Event: "+event);			
		}
		else if (level >=1 && event instanceof ExceptionEvent) {
			System.out.println("ExceptionEvent: "+event);
		}
	}

	@Override
	public CommandEvent prepare(CommandEvent command) {
		if (level >=2) {
			System.out.println("PrepareCommand: "+command);
		}
		return null;
	}

	@Override
	public Object process(CommandEvent command) {
		if (level >=2) {
			System.out.println("ProcessCommand: "+command);
		}
		return null;
	}

	@Override
	public QueryEvent prepare(QueryEvent query) {
		if (level >=4) {
			System.out.println("PrepareQuery: "+query);
		}
		return null;
	}

	@Override
	public Object process(QueryEvent query) {
		if (level >=4) {
			System.out.println("PrepareQuery: "+query);
		}
		return null;
	}

}
