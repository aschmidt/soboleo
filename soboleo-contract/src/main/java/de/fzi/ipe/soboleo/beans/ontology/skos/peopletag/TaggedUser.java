/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos.peopletag;

import java.util.Set;

public class TaggedUser extends TaggedPerson{

	public TaggedUser(String URI, String email, String name, Set<PersonTag> tags) {
		super(URI, email, name, tags);
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings({ "deprecation", "unused" })
	private TaggedUser() {; }

	public String getUserID () {
		//TODO
		return null;
	}
	
}
