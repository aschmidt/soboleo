package de.fzi.ipe.soboleo.ontology.rdf;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;
import static de.fzi.ipe.soboleo.beans.ontology.SKOS.CONCEPT;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

/**
 * Caching decorator for the triple store that aids in retrieving and changing the taxonomy. This 
 * class is intended for the use of the SkosTaxonomyEventBusAdaptor - other classes should use
 * query and command events to interact with the taxonomy. 
 */
public class RDFTaxonomy {

	private TripleStore tripleStore; 
	private ValueFactory vf;
	private static final String METACHARS = "\\.?*+{}()[]";
	
	protected static Logger logger = Logger.getLogger(RDFTaxonomy.class);
	
	private Map<URI,RDFConcept> conceptCache = new HashMap<URI,RDFConcept>();
	
	/**
	 * @param tripleStore
	 */
	public RDFTaxonomy(TripleStore tripleStore){
		this.tripleStore  = tripleStore;
		this.vf = tripleStore.getValueFactory();
	}
	
	/**
	 * Returns a concept object with the given URI (or null if none exists)
	 * @param uri URI
	 * @return SKOS_RDFConcept
	 * @throws IOException 
	 * 
	 */
	public RDFConcept getConcept(URI uri) throws IOException {
		URI typeURI = vf.createURI(TYPE.toString());
		URI conceptURI = vf.createURI(CONCEPT.toString());
		if (this.tripleStore.hasStatement(uri, typeURI, conceptURI)) {
			RDFConcept toReturn = this.conceptCache.get(uri);
			if(toReturn == null) 
			{
				toReturn = new RDFConcept(uri,this,this.tripleStore);
				this.conceptCache.put(uri, toReturn);
			}
			return toReturn;
		}
		else return null;
	}
		
	/**
	 * Returns a concept object with the given URI (or null if none exists)
	 * @param uriString 
	 * @return SKOS_RDFConcept
	 * @throws IOException 
	 */
	public RDFConcept getConcept(String uriString) throws IOException {
		URI uri = vf.createURI(uriString);
		return getConcept(uri);
	}
	
	/**
	 * Returns a concept object with the given pref label (or null if none exists)
	 * @param prefLabel 
	 * @return SKOS_RDFConcept
	 * @throws IOException 
	 */
	public RDFConcept getConceptByPrefLabel(LocalizedString prefLabel) throws IOException {
		URI propertyURI = vf.createURI(SKOS.PREF_LABEL.toString());
		Value label = this.tripleStore.getValueFactory().createLiteral(prefLabel.getString(),prefLabel.getLanguage().toString());
		List<Statement> statements = this.tripleStore.getStatementList(null, propertyURI, label);
		if(!statements.isEmpty()){
			URI conceptURI = (URI) ((Statement)statements.get(0)).getSubject();
			return getConcept(conceptURI);
		}		
		else return null;
	}
	
	/**
	 * Creates a new concept. 
	 * @return SKOS_RDFConcept
	 * @throws IOException 
	*/
	public RDFConcept createConcept() throws IOException {
		RDFConcept newConcept = new RDFConcept(this.tripleStore.getUniqueURI(),this,this.tripleStore);
		this.conceptCache.put(newConcept.getURI(), newConcept);
		URI rdfType = vf.createURI(TYPE.toString());
		URI skosConcept = vf.createURI(CONCEPT.toString());
		this.tripleStore.addTriple(newConcept.getURI(), rdfType, skosConcept, null);
		return newConcept;
	}
	
	/**
	 * @param c SKOS_RDFConcept
	 * @throws IOException
	 */
	public void deleteConcept(RDFConcept c) throws IOException {
		c.delete();
		this.conceptCache.remove(c.getURI());
	}

	/**
	 * @return Set<SKOS_RDFConcept>
	 * @throws IOException
	 */
	public Set<RDFConcept> getAllConcepts() throws IOException {
		Set<RDFConcept> toReturn = new HashSet<RDFConcept>();
		URI skosConcept = vf.createURI(CONCEPT.toString());
		URI rdfType = vf.createURI(TYPE.toString());			
		List<Statement> statements = this.tripleStore.getStatementList(null, rdfType, skosConcept);
		for (Statement currentStatement:statements) {
			URI conceptURI = (URI) currentStatement.getSubject();
			toReturn.add(getConcept(conceptURI));
		}
		return toReturn;
	}

	/**
	 * @return Set<SKOS_RDFConcept>
	 * @throws IOException
	 */
	public Set<RDFConcept> getRootConcepts() throws IOException {
		Set<RDFConcept> toReturn = new HashSet<RDFConcept>();
		for(RDFConcept c: getAllConcepts()) {
			if (c.getConnected(SKOS.HAS_BROADER).size() == 0) toReturn.add(c);
		}
		return toReturn;
	}

	/**
	 * @param versionID
	 * @return Set<SKOS_RDFConcept>
	 * @throws IOException
	 */
	public ClientSideTaxonomy getClientSideTaxonomy(long versionID) throws IOException {
		logger.debug("Building ClientSideTaxonomy");
		Set<ClientSideConcept> concepts = new HashSet<ClientSideConcept>();
		for (RDFConcept currentConcept: getAllConcepts()) {
			concepts.add(new ClientSideConcept(currentConcept.getURI().toString()));
		}
		ClientSideTaxonomy tax = new ClientSideTaxonomy(concepts,versionID);
		loadTexts(tax, SKOS.NOTE);
		loadTexts(tax, SKOS.PREF_LABEL);
		loadTexts(tax, SKOS.ALT_LABEL);
		loadTexts(tax, SKOS.HIDDEN_LABEL);
		loadRelations(tax,SKOS.HAS_BROADER);
		loadRelations(tax,SKOS.HAS_NARROWER);
		loadRelations(tax,SKOS.RELATED);
		logger.debug("Returning client side taxonomy");
		return tax;
	}
	
	private void loadRelations(ClientSideTaxonomy clientSideTaxonomy, SKOS property) throws IOException {
		URI relationURI = tripleStore.getValueFactory().createURI(property.toString());
		List<Statement> statementList = tripleStore.getStatementList(null, relationURI, null);
		for (Statement currentStatement: statementList) {
			String uriC1 = ((URI)currentStatement.getSubject()).stringValue();
			String uriC2 = ((URI)currentStatement.getObject()).stringValue();
			ClientSideConcept c1 = clientSideTaxonomy.get(uriC1);
			c1.initializeRelation(property, uriC2);
		}
	}
	
	private void loadTexts(ClientSideTaxonomy clientSideTaxonomy, SKOS property) throws IOException {
		URI synonymURI = tripleStore.getValueFactory().createURI(property.toString());
		List<Statement> statementList = tripleStore.getStatementList(null, synonymURI, null);
		for (Statement currentStatement: statementList) {
			String uriC1 = ((URI)currentStatement.getSubject()).stringValue();
			Literal lit = (Literal) currentStatement.getObject();
			LocalizedString ls = new LocalizedString(lit.stringValue(),lit.getLanguage());
			ClientSideConcept c1 = clientSideTaxonomy.get(uriC1);
			c1.initializeText(property, ls);
		}
	}
	
	public Set<Value> checkPrefLabel(LocalizedString toCheck) throws IOException{
		
		String toMatch = regexEncode(toCheck.getString());
		toMatch = "^" +toMatch +"$";
//    	System.out.println("To Match" + toMatch);
		
		String bindingName = "conceptURI";
		String query = "SELECT ?" +bindingName +
        " WHERE { " +
            "?" + bindingName +" <" + SKOS.PREF_LABEL.toString() +"> ?name . " +
            "FILTER (regex(str(?name), \"" + toMatch +"\", \"i\") &&  lang(?name) = \"" +toCheck.getLanguage().toString() +"\")" +
        "}";
		Set<Value> result = this.tripleStore.executeSelectSPARQLQueryForParameter(query, bindingName);
		return result;
	}
	
	/**
	  * @param raw a regular expression (syntax see class comment) , never null
	  * @return an encoded regular expression
	  */
	 private static String regexEncode(String raw) {
	  if(raw == null)
	   throw new IllegalArgumentException("raw may not be null");
	  
	  String result = raw;
	  // escape all meta characters
	  for(int i = 0; i < METACHARS.length(); i++) {
	   char c = METACHARS.charAt(i);
	   result = result.replace("" + c, "" + "\\\\" + c);
	  }
	  return result;
	 }
	
}
