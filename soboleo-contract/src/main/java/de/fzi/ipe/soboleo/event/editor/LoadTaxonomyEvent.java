/*
 * Event to indicate that a new ontology should be loaded (which one is
 * stored in the context object). This is a
 * client side only event, it never passes to the server.
 * Created on 24.10.2006 by zach
 */
package de.fzi.ipe.soboleo.event.editor;


public class LoadTaxonomyEvent extends EditorOnlyEvent {

	public LoadTaxonomyEvent() {
		;
	}
	
	
}
