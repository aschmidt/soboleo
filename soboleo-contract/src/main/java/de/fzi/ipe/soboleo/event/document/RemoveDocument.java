/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.document;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class RemoveDocument extends CommandEvent implements IsSerializable {

	private String docURI;
	
	public RemoveDocument(String docURI) {
		this.docURI = docURI;
	}
	
	protected RemoveDocument() {}
	
	public String getDocURI() {
		return docURI;
	}
	
	
}
