/*
 * FZI - Information Process Engineering 
 * Created on 25.06.2009 by zach
 */
package de.fzi.ipe.soboleo.event.message;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;

/**
 * Represents a chat message, i.e. the content and the id of the sender.
 */
public class ChatCommand extends CommandEvent implements ReadableEditorEvent{
	
	private String message;
	private String user;
	
	
	/**
	 * Constructor which creates a ChatCommand with a message 
	 * and a String which represents the person who wrote this message.
	 * @param message
	 * @param user
	 */
	public ChatCommand(String message, String user) {
		this.message = message;
		this.user = user;
	}
	
	@SuppressWarnings("unused")
	private ChatCommand() {
		;
	}
	
	/**
	 * Returns the message as a String
	 * @return String
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Returns the User as a Stirng
	 * @return String
	 */
	public String getUser() {
		return user;
	}

	/*
	 * Hint @kluge
	 * the "   " + before getSenderName() is needed, that you can see the full name. 
	 * If it is not there the first two characters are missing and the third will be half missing.
	 * 
	 */
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		if (lan == Language.de) return "   " + getSenderName() + " sagt: " + getMessage();
		else if (lan == Language.es) return "   " + getSenderName() + " dice: " + getMessage();
		else return "   " + getSenderName() + " says: " + getMessage();
	}

}
