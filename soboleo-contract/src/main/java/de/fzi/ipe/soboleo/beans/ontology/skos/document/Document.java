package de.fzi.ipe.soboleo.beans.ontology.skos.document;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * A client side copy of a document representation in the tripleStore. Contains the information
 * about the document and all its annotations (tags). This is an immutable object - changes should
 * be performed using commands send over the event bus. 
 */
public class Document implements IsSerializable{

	private String uri,title;
	private Set<Tag> tags;
	
	public Document(String uri, String title, Set<Tag> tags) {
		this.uri = uri;
		this.title = title;
		//TODO taggedPersonURI should not be set here!!!
		for(Tag t : tags) t.setDocumentURI(uri);
		this.tags = tags;
	}
	
	protected Document() {;}	
	
	public String getURI() {
		return uri;
	}
	
	public String getTitle() {
		return title;
	}
	
	public Set<Tag> getAllTags() {
		return tags;
	}
	
	public Set<Tag> getUserTags(String userID) {
		Set<Tag> toReturn = new HashSet<Tag>();
		for (Tag t:tags) {
			if (t.getUserID().equals(userID)) toReturn.add(t);
		}
		return toReturn;
	}
	
	public Set<String> getTagConceptIDs() {
		Set<String> toReturn = new HashSet<String>();
		for (Tag t:tags) toReturn.add(t.getConceptID());
		return toReturn;
	}
	
	public Set<String> getUserIDs() {
		Set<String> toReturn = new HashSet<String>();
		for (Tag t:tags) toReturn.add(t.getUserID());
		return toReturn;
	}
	
	public Set<Tag> getConceptTags(String conceptID){
		Set<Tag> toReturn = new HashSet<Tag>();
		for(Tag t : tags){
			if(t.getConceptID() != null && t.getConceptID().equals(conceptID)) toReturn.add(t);
		}
		return toReturn;
	}
	
	public Tag hasAnnotation(String userID, String conceptID){
		for(Tag t : tags){
			if(t.getUserID() != null && t.getConceptID() != null && t.getUserID().equals(userID) && t.getConceptID().equals(conceptID)) 	return t;
		}
		return null;
	}
}
