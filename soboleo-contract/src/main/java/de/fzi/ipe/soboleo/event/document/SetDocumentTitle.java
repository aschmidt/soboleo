/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.document;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class SetDocumentTitle extends CommandEvent implements IsSerializable{
	
	private String docURI;
	private String docTitle;

	public SetDocumentTitle(String docURI, String docTitle) {
		this.docURI = docURI;
		this.docTitle = docTitle;
	}
	
	protected SetDocumentTitle() {;}
	
	public String getDocURI() {
		return docURI;
	}
	
	public String getTitle() {
		return docTitle;
	}

	

}
