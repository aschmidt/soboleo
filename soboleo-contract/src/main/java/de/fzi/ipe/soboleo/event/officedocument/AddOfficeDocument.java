/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.officedocument;


import java.io.InputStream;

import de.fzi.ipe.soboleo.beans.document.DocumentType;
import de.fzi.ipe.soboleo.event.document.AddDocument;

public class AddOfficeDocument extends AddDocument{
	
	// the title in an office document is the filename itself
	
	private DocumentType documentType;
	
	// the input stream of the document
	private transient InputStream inputStream;
	
	
	public AddOfficeDocument(DocumentType dt, InputStream is, String title, String docOwner) {
		super(title,docOwner);
		this.documentType = dt;
		this.inputStream=is;
		
	}
	

	
	public DocumentType getDocumentType() {
		return documentType;
	}



	public void setDocumentType(DocumentType documentType) {
		this.documentType = documentType;
	}



	public InputStream getInputStream() {
		return inputStream;
	}



	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}



	@SuppressWarnings("unused")
	private AddOfficeDocument(){;}
}
