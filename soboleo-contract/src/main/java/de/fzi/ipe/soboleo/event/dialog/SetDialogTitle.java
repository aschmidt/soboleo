/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import de.fzi.ipe.soboleo.event.document.SetDocumentTitle;

public class SetDialogTitle extends SetDocumentTitle{
	
	public SetDialogTitle(String docURI, String docTitle) {
		super(docURI,docTitle);
	}
	
	@SuppressWarnings("unused")
	private SetDialogTitle(){;}
}
