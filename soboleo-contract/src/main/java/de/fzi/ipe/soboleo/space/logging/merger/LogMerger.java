package de.fzi.ipe.soboleo.space.logging.merger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogMerger {

	private static final String LOG_ROOT_PATH = "C:\\soboleo\\spaces\\";
	private static final int LOG_FILE_MERGE_LENGTH = 500000;

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		
		File root = new File(LOG_ROOT_PATH);
		String[] spaceDirs = root.list();
		String[] spaceSubDirs = null;
		String currentSpaceLogPath = "";
		if (spaceDirs != null) {
			File currentSpaceLogDir = null;
			for(String arg : args){
				if(arg.equals("-a")) {
					for (String spaceName : spaceDirs) {
						currentSpaceLogPath = LOG_ROOT_PATH + spaceName + "\\log\\";
						currentSpaceLogDir = new File(currentSpaceLogPath);
						spaceSubDirs = currentSpaceLogDir.list();
						if (spaceSubDirs != null) {
							mergeFiles(currentSpaceLogDir, currentSpaceLogPath);
						}
					}
				}
				else {
					String spaceName = arg;
					currentSpaceLogPath = LOG_ROOT_PATH + spaceName + "\\log\\";
					currentSpaceLogDir = new File(currentSpaceLogPath);
					spaceSubDirs = currentSpaceLogDir.list();
					if (spaceSubDirs != null) {
						mergeFiles(currentSpaceLogDir, currentSpaceLogPath);
					}
				}
			}
		}
	}
	

	private static void mergeFiles(File currentSpaceLogDir, String currentSpaceLogPath) throws IOException {
		File logDir = currentSpaceLogDir;
		String[] logFiles = logDir.list();	
		int mergedFileCount = (logFiles.length / LOG_FILE_MERGE_LENGTH) + 1;
		String outputFilePath = currentSpaceLogPath.split("log")[0];
		FileOutputStream fstream = null;
		BufferedWriter out = null;
		try {
			if (logFiles != null) {
				for (int i = 0; i < mergedFileCount; i++) {
					fstream = new FileOutputStream(outputFilePath + "log" + (i*LOG_FILE_MERGE_LENGTH) + ".xml");
					out = new BufferedWriter(new OutputStreamWriter(fstream, "UTF8"));
					int boundary = Math.min (LOG_FILE_MERGE_LENGTH,(logFiles.length - (i*LOG_FILE_MERGE_LENGTH)));
					out.append("<logEvents>");
					for(int j=0 ; j<boundary ; j++){
						String logFileName = logFiles[(i * LOG_FILE_MERGE_LENGTH) + j];
						File logFile = new File(currentSpaceLogPath + logFileName);
						StringBuilder toMerge = new StringBuilder();
						BufferedReader reader = new BufferedReader(new FileReader(logFile));
						String line = null;
						while ((line = reader.readLine()) != null) {
							String utf8line = new String(line.getBytes(),"UTF-8");
							if(utf8line.contains("<creationTime>")){
								String epoch = utf8line.substring(utf8line.indexOf(">")+1, utf8line.indexOf("</"));
								String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(Long.parseLong(epoch)));
								utf8line = "<creationTime>" + date + "</creationTime>";
							}
							toMerge.append(utf8line);
							toMerge.append(System.getProperty("line.separator"));
						}
						if(toMerge.indexOf("<senderURI>http://soboleo.com/ns/1.0#users-db-gen2</senderURI>")==-1){
							out.append(toMerge);
							out.append("\n\n");
						}
						reader.close();
					}
					out.append("</logEvents>");
					out.flush();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			out.close();
		}
	}
}
