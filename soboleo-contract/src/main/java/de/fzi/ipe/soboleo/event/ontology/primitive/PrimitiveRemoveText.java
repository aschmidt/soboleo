/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology.primitive;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;

public class PrimitiveRemoveText extends PrimitiveTaxonomyChangeCommand {
	
	private String fromURI;
	private SKOS connection;
	private LocalizedString text;
	
	public PrimitiveRemoveText(String fromURI, SKOS connection, LocalizedString text) {
		this.fromURI = fromURI;
		this.connection = connection;
		this.text = text;
	}

	@SuppressWarnings("unused") //only used for gwt serialization
	private PrimitiveRemoveText() {;}
	
	public String getFromURI() {
		return fromURI;
	}

	public SKOS getConnection() {
		return connection;
	}

	public LocalizedString getText() {
		return text;
	}
	

}
