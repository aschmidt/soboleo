package de.fzi.ipe.soboleo.event.message;



/**
 * Simple interface for events that have some kind of readable 
 * representation
 */
public interface ReadableEditorEvent extends ReadableEvent{

}
