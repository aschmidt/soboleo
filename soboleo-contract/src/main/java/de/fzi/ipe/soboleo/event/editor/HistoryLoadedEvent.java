package de.fzi.ipe.soboleo.event.editor;

import java.util.List;

import de.fzi.ipe.soboleo.event.Event;


/**
 * Event to indicate that the recent history has  been loaded. Gives System Message Panel a 
 * chance to get the list of events
 */
public class HistoryLoadedEvent extends EditorOnlyEvent {

	private List<? extends Event> events;
	
	public HistoryLoadedEvent(List<? extends Event> events) {
		this.events = events;
	}
	
	public List<? extends Event> getHistory() {
		return events;
	}
	
	
}
