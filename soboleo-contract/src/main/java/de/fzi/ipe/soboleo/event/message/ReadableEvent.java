package de.fzi.ipe.soboleo.event.message;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;


/**
 * Simple interface for events that have some kind of readable 
 * representation
 */
public interface ReadableEvent extends Event {

	public String getReadable(Language lan, ClientSideTaxonomy taxonomy); 
	
	public String getSenderURI();
	
}
