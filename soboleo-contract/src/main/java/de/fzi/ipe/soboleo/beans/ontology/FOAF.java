/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum FOAF implements IsSerializable{

	/** A tag for a resource */
	MAKER("http://xmlns.com/foaf/0.1/#maker"); 
	
	private final String uri;
	
	FOAF(String uri) {
		this.uri = uri;
	}
	
	public String toString() {
		return uri;
	
	}
	
}
