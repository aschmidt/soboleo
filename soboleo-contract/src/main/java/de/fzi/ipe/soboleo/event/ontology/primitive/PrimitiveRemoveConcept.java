/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology.primitive;

public class PrimitiveRemoveConcept extends PrimitiveTaxonomyChangeCommand {
	
	private String uri;
	
	public PrimitiveRemoveConcept(String uri) {
		this.uri  = uri;
	}

	@SuppressWarnings("unused") //only used for gwt serialization
	private PrimitiveRemoveConcept() {;}
	
	public String getURI() {
		return uri;
	}
	
	public String getFromURI() {
		return uri;
	}
	

}
