/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class AddParticipant extends CommandEvent implements IsSerializable{
	
	private String dialogURI;
	private String userURI;
	
	public AddParticipant(String dialogURI, String userURI) {
		this.dialogURI = dialogURI;
		this.userURI = userURI;
	}
	
	@SuppressWarnings("unused")
	private AddParticipant() {}
	
	public String getDialogURI() {
		return dialogURI;
	}
	
	public String getParticipantURI() {
		return userURI;
	}
	
}
