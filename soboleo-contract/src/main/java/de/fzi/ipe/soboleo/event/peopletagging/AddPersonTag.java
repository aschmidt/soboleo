/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.event.peopletagging;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class AddPersonTag extends CommandEvent{

	private String conceptID;
	private String userID;
	private String url;
	private String taggedPersonURI;

	
	
	public String getConceptID() {
		return this.conceptID;
	}


	public String getUserID() {
		return this.userID;
	}


	public String getUrl() {
		return this.url;
	}


	public String getTaggedPersonURI() {
		return this.taggedPersonURI;
	}

	
	public AddPersonTag(String taggedPersonURI, String url, String conceptID, String userID) {
		this.taggedPersonURI = taggedPersonURI;
		this.url = url;
		this.conceptID = conceptID;
		this.userID = userID;
	}
	
	@SuppressWarnings("unused")
	private AddPersonTag() { ; }
	
	
	
}
