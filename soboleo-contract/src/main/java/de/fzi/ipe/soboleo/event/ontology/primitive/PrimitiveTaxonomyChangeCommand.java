/*
 * Superclass for all classes directly representing a change to the taxonomy. Examples for the supclasses
 * are "PrimitiveAddConnection" directly represents the adding of a single relations or "PrimitiveAddText". 
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology.primitive;

import de.fzi.ipe.soboleo.event.ontology.TaxonomyChangeCommand;

public abstract class PrimitiveTaxonomyChangeCommand extends TaxonomyChangeCommand {
	
	public abstract String getFromURI();

}
