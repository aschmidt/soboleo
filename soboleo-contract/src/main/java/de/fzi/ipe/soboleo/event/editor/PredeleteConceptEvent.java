package de.fzi.ipe.soboleo.event.editor;



/**
 * Client Side in the editor only. Used by the menu bar to notify the tree that the currently selected 
 * concept should be deleted 
 */
public class PredeleteConceptEvent extends EditorOnlyEvent{
	public PredeleteConceptEvent() {
		;
	}
}
