/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.dialog;

import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.document.Document;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;

/**
 * A client side copy of a document representation in the tripleStore. Contains the information
 * about the document and all its annotations (tags). This is an immutable object - changes should
 * be performed using commands send over the event bus. 
 */
public abstract class Dialog extends Document{

	private String content;
	private Set<String> participants;
	private String initiatorURI;
	
	public Dialog(String uri, String title, String content, String initiatorURI, Set<String> participants, Set<Tag> tags) {
		super(uri, title, tags);
		this.content = content;
		this.participants = participants;
		this.initiatorURI = initiatorURI;
	}
	
	protected Dialog(){;}
	
	public String getContent(){
		return content;
	}
	
	public Set<String> getParticipants(){
		return participants;
	}
	
	public void setContent(String content){
		this.content = content;
	}
	 
	public void addParticipant(String userID){
		participants.add(userID);
	}
	
	public boolean hasParticipant(String userID){
		return participants.contains(userID);
	}
	
	public void setInitiator(String userID){
		this.initiatorURI = userID;
	}
	
	public String getInitiator(){
		return initiatorURI;
	}
	public abstract Object getAbout();
}
