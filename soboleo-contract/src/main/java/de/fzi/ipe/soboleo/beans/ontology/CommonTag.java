/*
 * FZI - Information Process Engineering 
 * Created on 08.07.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology;

import com.google.gwt.user.client.rpc.IsSerializable;

public enum CommonTag implements IsSerializable{

	/** A tag for a resource */
	TAG("http://commontag.org/ns#Tag"), 
	
	/** Connects the tag with the resource resource-tagged-tag */
	IS_TAGGED_WITH("http://commontag.org/ns#tagged"), 
	
	/** Connects a tag to a concept tag-means-concept */
	TAG_MEANS("http://commontag.org/ns#means"),
	
	/** Tagging Date */
	HAS_TAGGINGDATE("http://commontag.org/ns#taggingDate"),
	
	/** Human readable Label for tag */
	HAS_LABEL("http://commontag.org/ns#label");
	
	
	private final String uri;
	
	CommonTag(String uri) {
		this.uri = uri;
	}
	
	public String toString() {
		return uri;
	
	}
	
}
