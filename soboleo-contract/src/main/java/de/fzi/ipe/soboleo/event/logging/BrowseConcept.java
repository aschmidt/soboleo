package de.fzi.ipe.soboleo.event.logging;

import de.fzi.ipe.soboleo.event.EventImpl;


public class BrowseConcept extends EventImpl {
	
	private String conceptURI;
	
	public BrowseConcept(String conceptURI){
		this.conceptURI = conceptURI;
	}
	
	@SuppressWarnings("unused")
	private BrowseConcept() {;}
	
	public String getConceptURI(){
		return conceptURI;
	}

}
