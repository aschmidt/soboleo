package de.fzi.ipe.soboleo.event.editor;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;


/**
 * This is a client side only event indicating that a different concept has been selected. 
 */
public class ConceptSelectionChangedEvent extends EditorOnlyEvent{
	
	private ClientSideConcept concept;
	
	public ConceptSelectionChangedEvent() {
		;
	}
	
	public ConceptSelectionChangedEvent(ClientSideConcept c) {
		concept = c;
	}

	public ClientSideConcept getConcept() {
		return concept;
	}
}
