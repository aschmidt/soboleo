package de.fzi.ipe.soboleo.event.logging;

import de.fzi.ipe.soboleo.event.EventImpl;


public class StartAnnotatePeople extends EventImpl {
	
	private String taggedPersonURI;
	
	public StartAnnotatePeople(String taggedPersonURI){
		this.taggedPersonURI = taggedPersonURI;
	}
	
	public StartAnnotatePeople() {;}
	
	public String getPersonURI() {
		return taggedPersonURI;
	}

}
