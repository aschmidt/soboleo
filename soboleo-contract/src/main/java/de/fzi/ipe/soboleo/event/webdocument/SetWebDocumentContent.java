/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.webdocument;

import de.fzi.ipe.soboleo.event.document.SetDocumentContent;

public class SetWebDocumentContent extends SetDocumentContent{
	
	public SetWebDocumentContent(String docURI, String docContent) {
		super(docURI,docContent);
	}
	
	@SuppressWarnings("unused")
	private SetWebDocumentContent(){;}
}
