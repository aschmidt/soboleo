/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.webdocument;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.event.document.AddTag;

public class AddWebDocumentTag extends AddTag{

	
	public AddWebDocumentTag(String documentURI, SemanticAnnotation sia) {
		super(documentURI,sia);
	}
	
	@SuppressWarnings("unused")
	private AddWebDocumentTag(){;}
}
