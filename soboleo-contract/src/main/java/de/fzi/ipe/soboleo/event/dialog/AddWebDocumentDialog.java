/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;



public abstract class AddWebDocumentDialog extends AddDialog{
	
	public AddWebDocumentDialog(String title, String aboutURI, String docOwner) {
		super(title, aboutURI, docOwner);
	}
	
	@SuppressWarnings("unused")
	private AddWebDocumentDialog(){;}
}
