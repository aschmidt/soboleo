package de.fzi.ipe.soboleo.eventbus;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;


/**
 * @author FZI - Information Process Engineering 
 * Created on 07.08.2008 by zach
 *
 */
public interface EventBusCommandProcessor extends EventBusListener{
	
	/**
	 * Gives command processors a chance to pre-process commands, e.g. to add children commands. 
	 * Command processors must return "null" if they haven't made a change to CommandEvent. Also note
	 * that a changed command is again send to all command processors for a further round of preprocessing - 
	 * hence care needs to be taken to avoid infinite loops (in fact the event processor will raise an
	 * exception after an event is changed twenty times during preprocessing). 
	 * 
	 * CommandProcessors can be sure that there is always only one command processed at any given time (in one Space)
	 * and that no other command B comes between a call of prepare and process for command A. However, it may 
	 * be the case that prepare is called for an event that then is not processed (e.g. because the EventBusPermitters
	 * prevented it). 
	 */
	public CommandEvent prepare(CommandEvent command);
	
	
	public Object process(CommandEvent command);
	

}
