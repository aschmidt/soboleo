/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.officedocument;

import de.fzi.ipe.soboleo.event.document.RemoveDocument;

public class RemoveOfficeDocument extends RemoveDocument {

	public RemoveOfficeDocument(String docURI) {
		super(docURI);
	}
	
	@SuppressWarnings("unused")
	private RemoveOfficeDocument(){;}
}
