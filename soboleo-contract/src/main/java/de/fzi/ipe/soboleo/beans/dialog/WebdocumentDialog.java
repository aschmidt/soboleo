package de.fzi.ipe.soboleo.beans.dialog;

import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;

public class WebdocumentDialog extends Dialog{

	private String documentURI;
	
	public WebdocumentDialog(String uri, String title, String content, String documentURI, String initiator, Set<String> participants, Set<Tag> tags) {
		super(uri, title, content, initiator, participants, tags);
		this.documentURI = documentURI;
	}

	@Override
	public String getAbout() {
		return documentURI;
	}

}
