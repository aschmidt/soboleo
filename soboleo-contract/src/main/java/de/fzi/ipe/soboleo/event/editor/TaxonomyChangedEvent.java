/*
 * FZI - Information Process Engineering 
 * Created on 01.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.editor;

import de.fzi.ipe.soboleo.event.ontology.ComplexTaxonomyChangeCommand;

/**
 * ClientSide only event used to indicate to the TaxonomyTree in the Taxonomy Panel 
 * that the ClientSideTaxonomy has changed
 */
public class TaxonomyChangedEvent extends EditorOnlyEvent{

	private ComplexTaxonomyChangeCommand cmd;
	
	public TaxonomyChangedEvent(ComplexTaxonomyChangeCommand cmd) {
		this.cmd = cmd;
	}
	
	public ComplexTaxonomyChangeCommand getCommand() {
		return cmd;
	}
	
}
