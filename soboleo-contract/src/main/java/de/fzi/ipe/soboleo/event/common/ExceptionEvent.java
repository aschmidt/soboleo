/*
 * Event representing an exception (or in fact throwable) that occurred somewhere
 * in the program
 * FZI - Information Process Engineering 
 * Created on 27.09.2008 by zach
 */
package de.fzi.ipe.soboleo.event.common;

import de.fzi.ipe.soboleo.event.EventImpl;



public class ExceptionEvent extends EventImpl {

	public enum Severity{NoProblem, WillManage, SeriousTrouble};
	
	private Throwable t;
	private Severity s;
	private String note;
	
	private static final String NEW_LINE = "\n";

	
	
	@Override
	public long getEventType() {
		return EVENT_TYPE_EXCEPTION;
	}
	
	public ExceptionEvent(Throwable t,Severity s) {
		this.t = t;
		this.s = s;
	}
	
	public ExceptionEvent(Throwable t, Severity s, String note) {
		this(t,s);
		this.note = note;
	}
	
	public Throwable getException() {
		return t;
	}
	
	public Severity getSeverity() {
		return s;
	}
	
	public String getNote() {
		return note;
	}
	
	private void addException(StringBuilder s, Throwable t) {
		if (t.getMessage() != null) s.append(t.getMessage()+NEW_LINE);
		StackTraceElement[] stackTrace = t.getStackTrace();
		for (StackTraceElement sT: stackTrace) {
			s.append(sT.toString());
			s.append(NEW_LINE);
		}
		if (t.getCause()!=null) {
			s.append("**** Caused by: ****"+NEW_LINE);
			addException(s, t.getCause());
		}
	}
	
	public String toString() {
		StringBuilder toReturn = new StringBuilder ();
		toReturn.append("Exception Event, severity: "+s+NEW_LINE);
		toReturn.append("Exception type "+t.getClass().toString());
		toReturn.append(NEW_LINE);
		if (note != null) toReturn.append(note);
		addException(toReturn, getException());
		return toReturn.toString();
	}
	
}
