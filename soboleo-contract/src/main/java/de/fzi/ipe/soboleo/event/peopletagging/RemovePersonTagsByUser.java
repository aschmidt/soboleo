/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.event.peopletagging;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class RemovePersonTagsByUser extends CommandEvent{

	private String personTagURI;
	private String userURI;
	
	public RemovePersonTagsByUser(String taggedPersonURI, String userURI) {
		this.personTagURI = taggedPersonURI;
		this.userURI = userURI;
	}
	
	@SuppressWarnings("unused")
	private RemovePersonTagsByUser() { ; }
	
	public String getTaggedPersonURI() {
		return personTagURI;
	}
	public String getUserURI(){
		return userURI;
	}
	
}
