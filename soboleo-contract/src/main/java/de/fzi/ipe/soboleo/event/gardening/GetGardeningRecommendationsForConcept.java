/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.event.gardening;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Returns gardening recommendations just for one concept
 */
public class GetGardeningRecommendationsForConcept extends QueryEvent implements IsSerializable{

	private String conceptURI; 
	private boolean refresh;
	
	/**
	 * @param refresh whether to return any cached recommendations (refresh = false)
	 * or to compute new recommendations. 
	 */
	public GetGardeningRecommendationsForConcept(String conceptURI, boolean refresh) {
		this.refresh = refresh;
		this.conceptURI = conceptURI;
	}
	
	public GetGardeningRecommendationsForConcept(String conceptURI) {
		this(conceptURI,false);
	}
	
	@SuppressWarnings("unused") //needed for GWT
	private GetGardeningRecommendationsForConcept() { ;}  

	/**
	 * whether to return any cached recommendations (refresh = false)
	 * or to compute new recommendations. 
	 */
	public boolean isRefresh() {
		return refresh;
	}
	
	public String getConceptURI() {
		return conceptURI;
	}
	

}
