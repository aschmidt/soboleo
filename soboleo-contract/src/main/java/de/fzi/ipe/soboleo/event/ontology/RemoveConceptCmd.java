/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

public class RemoveConceptCmd extends ComplexTaxonomyChangeCommand implements ReadableEditorEvent {

	private String conceptURI;
	
	public RemoveConceptCmd(String conceptURI) {
		this.conceptURI = conceptURI;
	}
	
	@SuppressWarnings("unused") //only used for gwt serialization
	private RemoveConceptCmd() {
		;
	}
	
	public String getConceptURI() {
		return conceptURI;
	}

	/**
	 * This method combines the output string for the log screen.
	 */
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		
		String conceptName ="";
		String spacesForName = "   "; // needed to shift the name to the right, if not the first two characters are missing
		ClientSideConcept concept = tax.get(getConceptURI());
		if (concept != null) conceptName = concept.getBestFitText(SKOS.PREF_LABEL, lan).getString();
		else if(this.getLogObject(conceptURI)!= null) conceptName = ((ClientSideConcept)this.getLogObject(conceptURI)).getBestFitText(SKOS.PREF_LABEL, lan).getString();
				
		if (lan == Language.de)	return spacesForName + getSenderName() + " hat das Konzept "+conceptName+" gelöscht.";
		if (lan == Language.es)	return spacesForName + getSenderName() + " borraba el tema "+conceptName+".";
		else return spacesForName + getSenderName() + " deleted topic " + conceptName+".";
	}
	
}
