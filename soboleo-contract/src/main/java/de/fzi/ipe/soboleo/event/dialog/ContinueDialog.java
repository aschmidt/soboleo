/*
 * FZI - Information Process Engineering 
 * Created on 25.06.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

/**
 * Represents a chat message, i.e. the content and the id of the sender.
 */
public class ContinueDialog extends CommandEvent implements ReadableEditorEvent{
	
	private Language userLanguage;
	private String uri;
	
	public ContinueDialog(String dialogURI, Language userLanguage) {
		this.uri = dialogURI;
		this.userLanguage = userLanguage;
	}
	
	protected ContinueDialog() {
		;
	}
	
	
	public Language getUserLanguage(){
		return userLanguage;
	}
	
	public String getURI(){
		return uri;
	}
	
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		if (lan == Language.de) return getSenderName() + " führt einen Dialog fort. ";
		else if(lan == Language.es) return getSenderName() + " 	continúa un debate.";
		else return getSenderName() + " continues a dialog.";
	}
}
