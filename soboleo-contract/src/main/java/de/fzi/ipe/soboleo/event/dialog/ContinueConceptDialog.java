/*
 * FZI - Information Process Engineering 
 * Created on 25.06.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

/**
 * Represents a chat message, i.e. the content and the id of the sender.
 */
public class ContinueConceptDialog extends ContinueDialog implements ReadableEditorEvent{

	public ContinueConceptDialog(String dialogURI, Language userLanguage) {
		super(dialogURI, userLanguage);
	}
	
	@SuppressWarnings("unused")
	private ContinueConceptDialog() {
		;
	}
	
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		String conceptName ="";
		if (lan == Language.de) return getSenderName() + " fuehrt einen Dialog über Konzept "+conceptName + " fort. ";
		else if(lan == Language.es) return getSenderName() + " 	continúa un debate del tema " +conceptName+".";
		else return getSenderName() + " continues a dialog about the concept " +conceptName+".";
	}
}
