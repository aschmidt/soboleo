package de.fzi.ipe.soboleo.beans.tagcloud;

import java.io.Serializable;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;

public class ConceptStatistic implements Serializable {
	private static final long serialVersionUID = 1L;
	private String conceptId;
	private ClientSideConcept concept;
	private int count;
	
	public ConceptStatistic()
	{
		
	}
	
	public ClientSideConcept getConcept() {
		return concept;
	}
	public void setConcept(ClientSideConcept concept) {
		this.concept = concept;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getConceptId() {
		return conceptId;
	}
	public void setConceptId(String conceptId) {
		this.conceptId = conceptId;
	}
	
	
	
}
