package de.fzi.ipe.soboleo.event.logging;

import de.fzi.ipe.soboleo.event.EventImpl;


public class SubscribeConcept extends EventImpl {
	
	private String conceptURI;
	
	public SubscribeConcept(String conceptURI){
		this.conceptURI = conceptURI;
	}
	
	@SuppressWarnings("unused")
	private SubscribeConcept() {;}
	
	public String getConceptURI(){
		return conceptURI;
	}

}
