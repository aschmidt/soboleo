/*
 * Superclass of all classes that represent changes to the taxonomy. 
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public abstract class TaxonomyChangeCommand extends CommandEvent {

	private CommandEvent parent = null;
	
	public CommandEvent getParentEvent() {
		return parent;
	}
		
	protected void setParentEvent(CommandEvent parent) {
		this.parent = parent;
	}
	
}
