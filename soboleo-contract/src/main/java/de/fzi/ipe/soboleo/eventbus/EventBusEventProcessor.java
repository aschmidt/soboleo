/*
 * FZI - Information Process Engineering 
 * Created on 07.08.2008 by zach
 */
package de.fzi.ipe.soboleo.eventbus;

import de.fzi.ipe.soboleo.event.Event;


public interface EventBusEventProcessor extends EventBusListener{

	public void receiveEvent(Event event);
	
}
