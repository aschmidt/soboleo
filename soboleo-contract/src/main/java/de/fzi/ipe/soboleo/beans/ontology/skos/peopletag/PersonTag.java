/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.beans.ontology.skos.peopletag;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public abstract class PersonTag implements IsSerializable{

	public abstract String getTagID();	
	public abstract String getConceptID();
	public abstract Date getDate();
	public abstract String getUserID();
	public abstract void setTaggedPersonURI(String taggedPersonURI);
	public abstract String getTaggedPersonURI();
}
