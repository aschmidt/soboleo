/*
 * FZI - Information Process Engineering 
 * Created on 07.08.2008 by zach
 */
package de.fzi.ipe.soboleo.eventbus;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

public interface EventBusQueryProcessor extends EventBusListener {

	/**
	 * This method gives a chance to change or extend the query. Processors that 
	 * do not wish to change the query should return null. 
	 * When the prepare method changes a query and returns an object not identical
	 * to the one handed in, this new query object is again given to all 
	 * EventBusQueryProcessors. 
	 */
	public QueryEvent prepare(QueryEvent query);

	
	public Object process(QueryEvent query);
	
}
