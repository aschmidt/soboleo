/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

public class AddTextCmd extends ComplexTaxonomyChangeCommand implements ReadableEditorEvent{

	private String fromConceptURI;
	private SKOS connection;
	private LocalizedString text;
	
	public AddTextCmd(String fromConceptURI, SKOS connection, LocalizedString text) {
		this.fromConceptURI = fromConceptURI;
		this.connection = connection;
		this.text = text;
	}

	@SuppressWarnings("unused") //only used for gwt serialization
	private AddTextCmd() {;}
	
	public String getFromConceptURI() {
		return fromConceptURI;
	}

	public SKOS getConnection() {
		return connection;
	}

	public LocalizedString getText() {
		return text;
	}

	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		String conceptName ="";
		String spacesForName = "   ";// added to display the first two characters of the name 
		ClientSideConcept concept = tax.get(getFromConceptURI());
		if (concept != null) conceptName = concept.getBestFitText(SKOS.PREF_LABEL, lan).getString();
		
		if (lan == Language.de) {
			String connectionLabel = null;
			if (connection == SKOS.ALT_LABEL) connectionLabel = " die alternative Bezeichnung ";
			else if (connection == SKOS.PREF_LABEL) connectionLabel = " die bevorzugte Bezeichnung ";
			else if (connection == SKOS.HIDDEN_LABEL) connectionLabel = " die fehlerhafte Bezeichnung "; 
			else if (connection == SKOS.NOTE) connectionLabel = " die Beschreibung ";
			else connectionLabel = " den Text ";
			return spacesForName + getSenderName() + " hat" +connectionLabel + "'" +getText().getString() + "' zum Konzept "+conceptName + " hinzugefügt. ";
		}
		else if (lan == Language.es) {
			String connectionLabel = null;
			if (connection == SKOS.ALT_LABEL) connectionLabel = " la etiqueta alternativa ";
			else if (connection == SKOS.PREF_LABEL) connectionLabel = " la etiqueta preferida ";
			else if (connection == SKOS.NOTE) connectionLabel = " la etiqueta preferida mala ";
			else connectionLabel = " el texto ";
			return spacesForName + getSenderName() + " añadía" +connectionLabel + "'" +getText().getString() + "' al tema "+conceptName + ". ";
		}
		else {
			String connectionLabel = null;
			if (connection == SKOS.ALT_LABEL) connectionLabel = " the alternative label ";
			else if (connection == SKOS.PREF_LABEL) connectionLabel = " the preferred label ";
			else if (connection == SKOS.HIDDEN_LABEL) connectionLabel = " the hidden label";
			else if (connection == SKOS.NOTE) connectionLabel = " the description ";
			else connectionLabel = " the text ";
			return spacesForName +  getSenderName() + " added" +connectionLabel + "'" + getText().getString() + "' to topic "+conceptName+".";
		}
	}
	
	
	
}
