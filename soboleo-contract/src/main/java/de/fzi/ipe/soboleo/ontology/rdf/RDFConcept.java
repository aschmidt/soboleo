package de.fzi.ipe.soboleo.ontology.rdf;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;


/**
 * Decorator for the triple store that aids in retrieving and changing the taxonomy and its concepts. This 
 * class is intended for the use of the SkosTaxonomyEventBusAdaptor - other classes should use
 * query and command events to interact with the taxonomy. Note that this is a really 'stupid' class,
 * it does not maintain the consistency of the SKOS taxonomy (e.g. when adding a relation "a related to b"
 * it will NOT add the relation "b related a"). 
 */
public class RDFConcept {
	
	private URI uri;
	private RDFTaxonomy tax;
	private TripleStore tripleStore;

		
	protected RDFConcept(URI uri, RDFTaxonomy tax,TripleStore tripleStore) {
		this.uri = uri;
		this.tax = tax;
		this.tripleStore = tripleStore; 
	}

	public URI getURI(){
		return this.uri;
	}
	
	protected void delete() throws IOException {
		this.tripleStore.removeTriples(getURI(), null, null, null);
	}	

	public String checkRemoveConnection(SKOS property, RDFConcept other) throws IOException {
		if (!getConnected(property).contains(other)) return "[error_100] No such relation to remove";
		else return null;
	}
	
	public void removeConnection(SKOS property, RDFConcept other) throws IOException {
		removeFromRepository(getURI(), property, other.getURI());
	}
	
	/**
	 * @param property
	 * @param other
	 * @param circlesAllowed
	 * @return String
	 * @throws IOException
	 */
	public String checkAddConnection(SKOS property, RDFConcept other, boolean circlesAllowed) throws IOException {
		if (this.getConnected(property).contains(other)) return "[error_101] The relation already exists";
		if (this.equals(other)) return "[error_102] Cannot relate a topic to itself";
		if (!circlesAllowed) {
			if (other.getAllConnected(property).contains(this)) return "[error_103] Adding such a relation would produce a circle";
		}
		return null;
	}
	
	/**
	 * Adds a connected concept. Can be used e.g. for related, broader or narrower.
	 * @param property 
	 * @param other 
	 * @throws IOException 
	 */
	public void addConnection(SKOS property, RDFConcept other) throws IOException {
		addToRepository(getURI(), property, other.getURI());
	}
	
	
	/**
	 * Returns all concepts connected to the current one through the triples of the form
	 * this property x, where 'this' is the current object, 'property' the property
	 * parameter and finally x are the values that are returned. Used e.g. to get 
	 * related-, super- and sub-concepts. 
	 * @param property 
	 * @return Set<SKOS_RDFConcept>
	 * @throws IOException 
	 */
	public Set<RDFConcept> getConnected(SKOS property) throws IOException {
		Set<RDFConcept> foundConcepts = new HashSet<RDFConcept>();
		URI pred = this.tripleStore.getValueFactory().createURI(property.toString());
		List<Statement> statements = this.tripleStore.getStatementList(getURI(), pred, null);
		for (Statement s: statements) {
			URI object = (URI) s.getObject();
			foundConcepts.add(this.tax.getConcept(object));
		}
		return foundConcepts;
	}
	
	/**
	 * Return all concepts connected to the current one through chains of triples of the form
	 * <this property x> <x property y>; i.e. 'property' is assumed to be transitive.  
	 * @param property 
	 * @return Set<SKOS_RDFConcept>
	 * @throws IOException 
	 */
	public Set<RDFConcept> getAllConnected(SKOS property) throws IOException {
		Set<RDFConcept> allConnectedObjects= new HashSet<RDFConcept>();
		List<RDFConcept> toProcess = new LinkedList<RDFConcept>();
		toProcess.addAll(getConnected(property));
		while (toProcess.size()>0) {
			RDFConcept current = toProcess.remove(0);
			if (allConnectedObjects.add(current)) {
				for (RDFConcept candidate:current.getConnected(property)) {
					if (!allConnectedObjects.contains(candidate)) toProcess.add(candidate);
				}
			}
		}
		return allConnectedObjects;
	}
	
	/**
	 * Method to retrieve textual properties such as alternative labels or descriptions
	 * @param propertyType 
	 * @return Set<LocalizedString>
	 * @throws IOException 
	 */
	public Set<LocalizedString> getTexts(SKOS propertyType) throws IOException {
		Set<LocalizedString> values = new HashSet<LocalizedString>();
		URI synonymURI = this.tripleStore.getValueFactory().createURI(propertyType.toString());
		List<Statement> statementList = this.tripleStore.getStatementList(this.uri, synonymURI, null);
		for (Statement currentStatement: statementList) {
			Literal lit = (Literal) currentStatement.getObject();
			LocalizedString ls = new LocalizedString(lit.stringValue(),lit.getLanguage());
			values.add(ls);
		}
		return values;		
	}
	
	/**
	 * Method to retrieve textual properties such as alternative labels or descriptions. This method
	 * returns the first property value in the given language (or null, if none is found). 
	 * @param propertyType 
	 * @param language 
	 * @return LocalizedString
	 * @throws IOException 
	 */
	public LocalizedString getText(SKOS propertyType, LocalizedString.Language language) throws IOException {
		Set<LocalizedString> values = this.getTexts(propertyType);
		for (LocalizedString ls: values) {
			if (ls.getLanguage().equals(language)) return ls;
		}
		return null;
	}
	
	/**
	 * Method to check before adding textual properties such as alternative labels or descriptions
	 * The unique property is used as information whether there can be only one value of this property type 
	 * per language. i.e. 'unique' must be true for prefLabels and description (there is only one per 
	 * language) and false for synonyms. 
	 * @param value 
	 * @param propertyType 
	 * @param uniquePerLanguage 
	 * @return String
	 * @throws IOException 
	 */
	public String checkAddText(LocalizedString value, SKOS propertyType, boolean uniquePerLanguage) throws IOException {
		String toInsert = "label";
		int code = 0;
		if (propertyType.equals(SKOS.NOTE)){
			toInsert = "description";
			code = 1;
		}
		if((value == null) || value.getString().trim().isEmpty()) return "[error_" + (104+code) +"] The given " +toInsert +" is blank";
		Set<LocalizedString> textualProperties = this.getTexts(propertyType);
		if (textualProperties.contains(value)) return "[error_" +(106+code) +"] The " +toInsert +" already exists";
		if (uniquePerLanguage) {
			for (LocalizedString ls: textualProperties) {
				if (ls.getLanguage().equals(value.getLanguage())){
					return "[error_" +(108+code) +"] Only one " +toInsert +" per language permitted";
				}
			}
		}
		if(!propertyType.equals(SKOS.NOTE))	return checkLabelPairwiseDisjointness(value, propertyType);
		else return null;
	}
	
	/**
	 * Method to check before changing textual properties (without language change) such as alternative labels or description.
	 * @param oldValue 
	 * @param value
	 * @param propertyType
	 * @return String
	 * @throws IOException
	 */
	public String checkChangeText(LocalizedString oldValue, LocalizedString value, SKOS propertyType) throws IOException {
		String toInsert = "label";
		int code = 0;
		if (propertyType.equals(SKOS.NOTE)){
			toInsert = "description";
			code = 1; 
		}
		if((value == null) || value.getString().trim().isEmpty()) return "[error_" + (104+code) +"] The given "+ toInsert +" is blank.";
		Set<LocalizedString> textualProperties = this.getTexts(propertyType);
		if (!oldValue.equals(value) && textualProperties.contains(value)) {
			
			return "[error_" +(106+code) +"] The "+ toInsert +" already exists";
		}
		return checkLabelPairwiseDisjointness(value, propertyType);		
	}
	
	/**
	 * Method to check pairwise disjointness between a new and existing labels. 
	 * @param value
	 * @param propertyType
	 * @return
	 * @throws IOException
	 */
	private String checkLabelPairwiseDisjointness(LocalizedString value, SKOS propertyType) throws IOException {
		String exceptionMsg = "[error_110] The label already exists for this language";
		if (propertyType.equals(SKOS.PREF_LABEL)){
			Set<LocalizedString> altLabels = this.getTexts(SKOS.ALT_LABEL);
			Set<LocalizedString> hiddenLabels = this.getTexts(SKOS.HIDDEN_LABEL);
			if(altLabels.contains(value) || hiddenLabels.contains(value)) return exceptionMsg;				
		}
		else if (propertyType.equals(SKOS.ALT_LABEL)){
			Set<LocalizedString> prefLabels = this.getTexts(SKOS.PREF_LABEL);
			Set<LocalizedString> hiddenLabels = this.getTexts(SKOS.HIDDEN_LABEL);
			if(prefLabels.contains(value) || hiddenLabels.contains(value)) return exceptionMsg;	
		}
		else if (propertyType.equals(SKOS.HIDDEN_LABEL)){
			Set<LocalizedString> prefLabels = this.getTexts(SKOS.PREF_LABEL);
			Set<LocalizedString> altLabels = this.getTexts(SKOS.ALT_LABEL);
			if(prefLabels.contains(value) || altLabels.contains(value)) return exceptionMsg;	
		}
		return null;		
	}
	
	
	/**
	 * Method to add a textual property such as an alternative label or a description
	 */
	public void addText(LocalizedString value, SKOS propertyType) throws IOException {
		Value syn = this.tripleStore.getValueFactory().createLiteral(value.getString(),value.getLanguage().toString());
		addToRepository(this.uri, propertyType, syn);
	}

	/**
	 * Method to check whether a textual property (such as a label or description) can be removed
	 */
	public String checkRemoveText(LocalizedString value, SKOS propertyType) throws IOException {
		String toInsert = "label";
		int code = 0;
		if (propertyType.equals(SKOS.NOTE)){
			toInsert = "description";
			code = 1;
		}
		if(value == null) return "[error_" + (104+code) +"] The given " +toInsert +" is blank";
		if(! this.getTexts(propertyType).contains(value)){
			return "[error_" + (111+code) +"] No such " +toInsert +" to remove";
		}
		else return null;
	}
	
	/**
	 * Method to remove a textual property such as a label or an description
	 */
	public void removeText(LocalizedString value, SKOS propertyType) throws IOException {
		Value syn = this.tripleStore.getValueFactory().createLiteral(value.getString(),value.getLanguage().toString());
		removeFromRepository(this.uri, propertyType, syn);		
	}

	private void addToRepository(URI subjectURI, SKOS relationship, Value objectURI) throws IOException {
		URI predicateURI = this.tripleStore.getValueFactory().createURI(relationship.toString());
		this.tripleStore.addTriple(subjectURI, predicateURI, objectURI, null);
	}
	
	private void removeFromRepository(URI subjectURI, SKOS relationship, Value objectURI) throws IOException  {
		URI predicateURI = this.tripleStore.getValueFactory().createURI(relationship.toString());
		this.tripleStore.removeTriples(subjectURI, predicateURI, objectURI,null);
	}

}
