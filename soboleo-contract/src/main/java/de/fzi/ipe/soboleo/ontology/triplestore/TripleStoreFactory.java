/*
 * FZI - Information Process Engineering 
 * Created on 15.01.2009 by zach
 */
package de.fzi.ipe.soboleo.ontology.triplestore;

import java.io.File;
import java.io.IOException;

public interface TripleStoreFactory {

	public TripleStore getTripleStore(File file, String datastoreName) throws IOException;
	
}
