/*
 * Thrown after some event (e.g. event, query or command) was denied the permission 
 * to execute or was denied the permission to be distributed.  
 * FZI - Information Process Engineering 
 * Created on 08.08.2008 by zach
 */
package de.fzi.ipe.soboleo.event.user;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.Event;


public class EventPermissionDeniedException extends Exception implements IsSerializable{
	
	private static final long serialVersionUID = 1L;

	private String reason;
	private Event event;
	
	
	public EventPermissionDeniedException(String reason, Event event) {
		this.reason = reason;
		this.event = event;
	}
	
	public EventPermissionDeniedException(Exception e,Event event) {
		this.reason = e.getMessage();
		this.event = event;
	}
	
	@SuppressWarnings("unused")
	private EventPermissionDeniedException() {
		;
	}
	
	public String getReason() {
		return reason;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public String toString() {
		return reason;//+" "+super.toString();
	}
	

}
