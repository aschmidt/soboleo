/*
 * FZI - Information Process Engineering 
 * Created on 08.08.2008 by zach
 */
package de.fzi.ipe.soboleo.eventbus;

/**
 * Common superclass for all eventBus listeners; not meant to be directly instantiated. 
 */
public interface EventBusListener {

}
