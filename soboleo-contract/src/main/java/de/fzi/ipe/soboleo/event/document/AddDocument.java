/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.document;


import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public abstract class AddDocument extends CommandEvent{
	
	private String title;
	private String uri = null;
	private String docOwner;
	
	public AddDocument(String title, String docOwner) {
		this.title = title;
		this.docOwner=docOwner;
	}
	
	protected AddDocument() {}
	
	public void setURI(String uri) {
		this.uri = uri;
	}
	
	public String getURI() {
		return uri;
	}
	
	public String getTitle() {
		return title;
	}

	public String getDocOwner() {
		return docOwner;
	}
	

	
}
