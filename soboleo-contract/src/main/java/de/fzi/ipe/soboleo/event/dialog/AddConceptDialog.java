/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;



public class AddConceptDialog extends AddDialog{
	
	public AddConceptDialog(String title, String aboutURI, String docOwner) {
		super(title, aboutURI, docOwner);
	}
	
	@SuppressWarnings("unused")
	private AddConceptDialog(){;}
}
