/*
 * FZI - Information Process Engineering 
 * Created on 02.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology.primitive;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;

public class PrimitiveChangeText  extends PrimitiveTaxonomyChangeCommand{

	private LocalizedString oldText, newText;
	private SKOS connection; 
	private String fromURI;
	
	
	public PrimitiveChangeText(String fromConceptURI, SKOS connection, LocalizedString oldText, LocalizedString newText) {
		this.oldText = oldText;
		this.newText = newText;
		this.connection = connection;
		this.fromURI = fromConceptURI;
	}
	
	@SuppressWarnings("unused")
	private PrimitiveChangeText() {
		;
	}
	
	@Override
	public String getFromURI() {
		return fromURI;
	} 
	
	public LocalizedString getOldText() {
		return oldText;
	}
	
	public LocalizedString getNewText() {
		return newText;
	}

	public SKOS getConnection() {
		return connection;
	}

}
