/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.dialog;

import de.fzi.ipe.soboleo.event.document.RemoveDocument;

public class RemoveDialog extends RemoveDocument {

	public RemoveDialog(String docURI) {
		super(docURI);
	}
	
	@SuppressWarnings("unused")
	private RemoveDialog(){;}
}
