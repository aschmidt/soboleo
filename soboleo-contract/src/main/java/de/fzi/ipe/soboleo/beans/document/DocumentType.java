package de.fzi.ipe.soboleo.beans.document;

/** stores a list of supported document types (binary) in Soboleo . 
 * 
 * @author awalter
 *
 */
public enum DocumentType {
	doc,
	xls,
	pdf,
	ppt
}
