/*
 * FZI - Information Process Engineering 
 * Created on 03.02.2010 by zach
 */
package de.fzi.ipe.soboleo.event.common;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

/**
 * Send by the space to indicate that the space is now closing. 
 */
public class ShutdownCommand extends CommandEvent {	

}
