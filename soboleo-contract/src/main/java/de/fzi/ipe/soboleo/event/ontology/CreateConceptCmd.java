/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;


public class CreateConceptCmd extends ComplexTaxonomyChangeCommand implements ReadableEditorEvent {

	private LocalizedString initialName;
	
	public CreateConceptCmd(LocalizedString initialName) {
		this.initialName = initialName;
	}
	
	@SuppressWarnings("unused") //only used for gwt serialization
	private CreateConceptCmd() {;}
	
	public LocalizedString getInitialName() {
		return initialName;
	}

	/**
	 * This combines the string output for the log-box.
	 * Added spacesForName to get the full name displayed.
	 * Before this, the first two characters were missing.
	 */
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy taxonomy)
	{
		String spacesForName = "   ";// added to adjust the border setting that the two characters of the name will be seen
		if (lan == Language.de) return spacesForName + getSenderName() + " hat das Konzept '"+initialName+"' neu angelegt.";
		else if (lan == Language.es) return spacesForName + getSenderName() + " creaba el tema '"+initialName+"'.";
		else return spacesForName + getSenderName() + " added the topic '"+initialName+"'.";
		
	}
	
	
	
	
}
