package de.fzi.ipe.soboleo.event;

import java.util.HashMap;

public abstract class EventImpl implements Event{

	private long id = -1;
	private String senderURI = null;
	private String senderName = null;
	protected HashMap<String, Object> logObjects = null;
	
	private long creationTime = System.currentTimeMillis();
	
	
	public long getEventType() {
		return 0;
	}
	
	/**
	 * Gives each event an id (unique within an Space), set by the EventBus as first thing when an event comes in. 
	 * This methods is meant to be only called by the event bus - calling it from anywhere else will later lead
	 * to an exception and will prevent the event from getting distributed. 
	 * The ids increase and establish an order within the event, query and commands. There is not necessarily an 
	 * event for every id (exceptions and stuff may happen)
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Sets the ID of the sender that send this event. This is set by the EventBus and is not supposed to be set by anyone else
	 * (in fact: setting it anywhere else will lead to an exception)
	 */
	public void setSenderURI(String actor) {
		if (this.senderURI == null) this.senderURI = actor;
		else if (this.senderURI.equals(actor)) return;
		else throw new IllegalStateException("The setSender method is supposed to be only used by the event bus .. ("+senderURI+","+actor+")");
	}
	

	/**
	 * Sets the name of the sender that send this event. This is set by the EventBus and is not supposed to be set by anyone else
	 * (in fact: setting it anywhere else will lead to an exception)
	 */
	public void setSenderName(String senderName) {
		if (this.senderName == null) this.senderName = senderName;
		else if (this.senderName.equals(senderName)) return;
		else throw new IllegalStateException("The setSenderName method is supposed to be only used by the event bus .. ("+senderName+","+this.senderName+")");
	}

	
	public String getSenderURI() {
		return this.senderURI;
	}
	
	public String getSenderName() {
		return this.senderName;
	}
	
	public long getCreationTime(){
		return this.creationTime;
	}
	
	
	public void setLogObjects(String key, Object value){
		if(this.logObjects == null) 
		{
		    this.logObjects = new HashMap<String, Object>();
		}
		this.logObjects.put(key, value);
	}
	
	public void resetLogObjects(){
		this.logObjects = null;
	}
	
	public Object getLogObject(String key){
		
		if(this.logObjects == null) return null;
		else return this.logObjects.get(key);
	}
	
	/**
	 * gets the id of this event, or -1 if an id has not yet been set. 
	 */
	public long getId() {
		return id;
	}
	
}
