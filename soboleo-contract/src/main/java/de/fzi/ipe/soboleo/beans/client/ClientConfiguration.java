package de.fzi.ipe.soboleo.beans.client;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;

/**
 * Simple class to hold and transport a bit of configuration data. 
 * @author Created on 30.11.2006 by zach
 */
public class ClientConfiguration implements IsSerializable{

	private String userName;
	private String userID;
	private String key;
	private String defaultSpace;
	private String prototypicalConceptsName;
	private Language defaultUserLanguage;
	private Language defaultSpaceLanguage;
	private boolean conceptRecommendation; 
	private boolean hasSpaceDialogSupport;
	private boolean hasAnnotatePeopleDeleteTopic;//if it is true, the person can delete the annotation which other persons made.
	private boolean canRated;// if it is true the rating of Web sides will be possible
	// set methods
	public void setUserID(String userID) { this.userID = userID; }
	public void setUserName(String userName) { this.userName = userName; }
	public void setUserKey(String key) { this.key = key;}
	public void setDefaultSpace(String defaultSpace) {this.defaultSpace = defaultSpace; }
	public void setDefaultUserLanguage(Language defaultLanguage) { this.defaultUserLanguage = defaultLanguage; }
	public void setDefaultSpaceLanguage(Language defaultSpaceLanguage) { this.defaultSpaceLanguage = defaultSpaceLanguage; } 
	public void setSpaceDialogSupport(boolean hasSpaceDialogSupport) {this.hasSpaceDialogSupport = hasSpaceDialogSupport;}
	public void setPrototypicalConceptsName(String protoName) {this.prototypicalConceptsName = protoName;}
	/**
	 * Sets the configuration for AnnotatePeople.
	 * If it is set to true, the person can delete annotations made by other persons.
	 * Then a red cross appears after the Topic,
	 * if the tagged person is the same as the logged on person.
	 * If it is set to false, it will not be possible.
	 * This is set for each space separately. 
	 * @param hasAnnotatePeopleDeleteTopic
	 */
	public void setAnnotatePeopleDeleteTopic(boolean hasAnnotatePeopleDeleteTopic) {this.hasAnnotatePeopleDeleteTopic = hasAnnotatePeopleDeleteTopic;}
	/**
	 * Sets the configuration for Annotate Web Page.
	 * If it is true, a Rating Component will be shown, and 
	 * the rating will be possible.
	 * If it is false the rating will not be shown.
	 * @param canRated
	 */
	public void setRatingWebsides(boolean canRated) {this.canRated = canRated;}
	// get methods
	public String getUserID() {
		return userID;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public String getDefaultSpace() {
		return defaultSpace;
	}
	
	public String getPrototypicalConceptsName(){
		return prototypicalConceptsName;
	}
	
	public Language getDefaultUserLanguage() {
		return defaultUserLanguage;
	}
	
	public boolean conceptRecommendation() {
		return conceptRecommendation;
	}
	
	/**
	 * Returns the default language set for this space
	 *  
	 * @return Language
	 */
	public Language getDefaultSpaceLanguage() {
		return defaultSpaceLanguage;
	}
	
	public String getUserKey() {
		return key;
	}
	
	public boolean hasSpaceDialogSupport(){
		return this.hasSpaceDialogSupport;
	}
	/**
	 * Returns the boolean value of the AnnotatePeopleDeleteTopic.
	 * It says, if the topic of a person annotated by others is deletable or not.
	 * 
	 * @return hasAnnotatePeopleDeleteTopic boolean
	 */
	public boolean getAnnotatePeopleDeleteTopic()
	{
	    return this.hasAnnotatePeopleDeleteTopic;
	}
	/**
	 * Returns the boolean value of the Annotate Web Page configuration.
	 * If it is true, the web side can be rated.
	 * @return boolean 
	 */
	public boolean getRatingWebsides()
	{
	    return this.canRated;
	}
}
