/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.document;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class RemoveTag extends CommandEvent implements IsSerializable{

	private String tagURI;
	private String docURI;
	private String conceptURI;

	public RemoveTag(String docURI, String tagURI, String conceptURI) {
		this.docURI = docURI;
		this.tagURI = tagURI;
		this.conceptURI = conceptURI;
	}
	
	protected RemoveTag() {}
	
	public String getTagID() {
		return tagURI;
	}
	
	public String getDocURI(){
		return docURI;
	}
	
	public String getConceptURI(){
		return conceptURI;
	}

	
	
}
