package de.fzi.ipe.soboleo.event.editor;

import de.fzi.ipe.soboleo.event.EventImpl;

/**
 * Class for all events that are only used in the editor
 */
public abstract class EditorOnlyEvent extends EventImpl{
	
	
}
