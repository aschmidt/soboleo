/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.document;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;

public class AddTag extends CommandEvent implements IsSerializable{

	private SemanticAnnotation sia;
	private String documentURI;
	private String tagURI; 

	
	public AddTag(String documentURI, SemanticAnnotation sia) {
		this.documentURI = documentURI;
		this.sia = sia;
	}

	
	protected AddTag() {}
	
	public void setTagURI(String tagURI) {
		this.tagURI = tagURI;
	}
	

	
	public String getTagURI() {
		return tagURI;
	}
	
	public String getDocumentURI() {
		return documentURI;
	}


	public SemanticAnnotation getSia() {
		return sia;
	}


	
	
	
}
