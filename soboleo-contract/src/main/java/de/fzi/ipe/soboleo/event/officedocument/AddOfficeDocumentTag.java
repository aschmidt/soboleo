/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.officedocument;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.event.document.AddTag;

public class AddOfficeDocumentTag extends AddTag{

	
	public AddOfficeDocumentTag(String documentURI, SemanticAnnotation sia) {
		super(documentURI,sia);
	}
	
	@SuppressWarnings("unused")
	private AddOfficeDocumentTag(){;}
}
