package de.fzi.ipe.soboleo.event.peopletagging;

import de.fzi.ipe.soboleo.event.execution.CommandEvent;

/**
 * @author FZI - Information Process Engineering Created on 20.08.2009 by zach
 */
public class RemovePerson extends CommandEvent {

	private String personTagURI;
	@SuppressWarnings("unused")
	private String taggedPersonURI;
	@SuppressWarnings("unused")
	private String conceptURI;

	/**
	 * Command event for removing the given person tag from the store.
	 * 
	 * @param personTagURI
	 */
	public RemovePerson(String personTagURI, String taggedPersonURI) {
		this.personTagURI = personTagURI;
		this.taggedPersonURI = taggedPersonURI;

	}

	@SuppressWarnings("unused")
	private RemovePerson() {
		;
	}

	public String getPersonTagURI() {
		return personTagURI;
	}

}