/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.event.gardening;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;


/**
 * Query to retrieve gardening recommendations. 
 */
public class GetGardeningRecommendations extends QueryEvent implements IsSerializable{

	private boolean refresh;
	
	/**
	 * @param refresh whether to return any cached recommendations (refresh = false)
	 * or to compute new recommendations. 
	 */
	public GetGardeningRecommendations(boolean refresh) {
		this.refresh = refresh;
	}
	
	@SuppressWarnings("unused") //needed for GWT
	private GetGardeningRecommendations() { ;}  

	/**
	 * whether to return any cached recommendations (refresh = false)
	 * or to compute new recommendations. 
	 */
	public boolean isRefresh() {
		return refresh;
	}
	
}
