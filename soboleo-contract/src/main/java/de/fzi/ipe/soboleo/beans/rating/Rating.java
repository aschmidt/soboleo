package de.fzi.ipe.soboleo.beans.rating;

import java.io.Serializable;
import java.util.Date;

public class Rating implements Serializable {

	private static final long serialVersionUID = 1L;

	private int ratingFrequency;
	private int score;
	// for rating of a user or list of all ratings
	private Date ratingDate;
	
	
	public Rating()
	{
		
	}
	
	public Rating(int ratingFrequency,int score)
	{
		this.ratingFrequency=ratingFrequency;
		this.score=score;
	}
	
	public Rating(int score,Date ratingDate)
	{
		this.score=score;
		this.ratingDate=ratingDate;
	}
	
	public int getRatingFrequency() {
		return ratingFrequency;
	}
	public void setRatingFrequency(int ratingFrequency) {
		this.ratingFrequency = ratingFrequency;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}

	public Date getRatingDate() {
		return ratingDate;
	}

	public void setRatingDate(Date ratingDate) {
		this.ratingDate = ratingDate;
	}
	
	
	
}
