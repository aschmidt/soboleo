/*
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.event.ontology;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;

public class RemoveConnectionCmd extends ComplexTaxonomyChangeCommand implements ReadableEditorEvent{
	
	private String fromConceptURI, toConceptURI;
	private SKOS connection;
	
	public RemoveConnectionCmd(String fromConceptURI, SKOS connection, String toConceptURI) {
		this.fromConceptURI = fromConceptURI;
		this.connection = connection;
		this.toConceptURI = toConceptURI;
	}

	@SuppressWarnings("unused") //only used for gwt serialization
	private RemoveConnectionCmd(){;}
	
	public String getFromConceptURI() {
		return fromConceptURI;
	}

	public String getToConceptURI() {
		return toConceptURI;
	}

	public SKOS getConnection() {
		return connection;
	}
	
	@Override
	public String getReadable(Language lan, ClientSideTaxonomy tax) {
		String spacesForName = "   ";// needed to shift the user name, if not the first two characters are missing
		String conceptNameFrom ="";
		ClientSideConcept conceptFrom = tax.get(getFromConceptURI());
		if (conceptFrom != null) conceptNameFrom = conceptFrom.getBestFitText(SKOS.PREF_LABEL, lan).getString();
		String conceptNameTo ="";
		ClientSideConcept conceptTo = tax.get(getToConceptURI());
		if (conceptTo != null) conceptNameTo= conceptTo.getBestFitText(SKOS.PREF_LABEL, lan).getString();

		if (lan == Language.de) {
			if (connection == SKOS.HAS_BROADER) return spacesForName + getSenderName() + " hat " + conceptNameTo + " als ein weiteres Konzept zu "+conceptNameFrom+" entfernt.";
			if (connection == SKOS.HAS_NARROWER) return spacesForName + getSenderName() + " hat " + conceptNameFrom + " als ein engeres Konzept zu "+conceptNameTo+" entfernt.";			
			if (connection == SKOS.RELATED) return spacesForName + getSenderName() + " hat " + conceptNameFrom + " als verwandtes Konzept zu "+conceptNameTo+" entfernt.";			
		}
		else if (lan == Language.es){
			if (connection == SKOS.HAS_BROADER) return spacesForName + getSenderName() + " 	quitaba " + conceptNameTo + " como tema más amplios de "+conceptNameFrom+".";
			if (connection == SKOS.HAS_NARROWER) return spacesForName + getSenderName() + " quitaba " + conceptNameFrom + " como tema más restringidos de "+conceptNameTo+".";			
			if (connection == SKOS.RELATED) return spacesForName + getSenderName() + " 	quitaba " + conceptNameFrom + " como tema relacionado de "+conceptNameTo+".";
		}
		else {
			if (connection == SKOS.HAS_BROADER) return spacesForName + getSenderName() + " removed " + conceptNameTo + " as a broader topic of "+conceptNameFrom+".";
			if (connection == SKOS.HAS_NARROWER) return spacesForName + getSenderName() + " removed " + conceptNameFrom + " as a broader topic of "+conceptNameTo+".";			
			if (connection == SKOS.RELATED) return spacesForName + getSenderName() + " removed " + conceptNameFrom + " as a related topic to "+conceptNameTo+".";			
		}
		return "";
	}
	

}
