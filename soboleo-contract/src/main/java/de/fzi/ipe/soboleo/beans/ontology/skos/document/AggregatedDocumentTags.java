package de.fzi.ipe.soboleo.beans.ontology.skos.document;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;


/**
 * A convenience class that enables easy access to all annotations
 * that use one concept. 
 */
public class AggregatedDocumentTags implements IsSerializable{
	
	private String conceptID;
	private int weight = 0;
	private Set<Tag> tags = new HashSet<Tag>();
	
	
	public AggregatedDocumentTags(String conceptID) {
		this.conceptID = conceptID;
	}

	@SuppressWarnings("unused")
	private AggregatedDocumentTags() { ; }
	
	public void addTag(Tag personTag) {
		weight++;
		tags.add(personTag);
	}
	
	public String getConceptID() {
		return conceptID;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public Set<Tag> getTags() {
		return tags;
	}
	
	public boolean addAll(AggregatedDocumentTags aggregated){
		if(aggregated.getConceptID().equals(conceptID)){
			if(tags.addAll(aggregated.getTags())){
				weight = weight + aggregated.getWeight();
				return true;
			}
		}
		return false;
	}
	
}
