/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.event.webdocument;


import de.fzi.ipe.soboleo.event.document.AddDocument;

public class AddWebDocument extends AddDocument{
	
	private String url;
	
	public AddWebDocument(String url, String title, String docOwner) {
		super(title,docOwner);
		this.url = url;
	}
	
	public String getURL() {
		return url;
	}
	
	@SuppressWarnings("unused")
	private AddWebDocument(){;}
}
