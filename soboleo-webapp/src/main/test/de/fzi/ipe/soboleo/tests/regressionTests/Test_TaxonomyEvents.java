/*
 * This class is responsible for testing the actual taxonomy change events and stuff. 
 * 
 * FZI - Information Process Engineering 
 * Created on 01.02.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;


import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString.Language;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddTextCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.ChangeTextCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.RemoveConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.RemoveTextCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.queries.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceImpl;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;

public class Test_TaxonomyEvents extends Assert{

	static final File tempDirectory = new File("TaxonomyEvents-testDirectory");

	private Space space; 

	
	@Test
	public void testConceptConnections() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		
		String uri1 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("1")), EventSenderCredentials.SERVER);
		String uri2 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("2")), EventSenderCredentials.SERVER);
		String uri3 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("3")), EventSenderCredentials.SERVER);
		String uri4 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("4")), EventSenderCredentials.SERVER);

		eb.executeCommandNow(new AddConnectionCmd(uri2,SKOS.HAS_BROADER,uri1), EventSenderCredentials.SERVER);
		try {
			eb.executeCommandNow(new AddConnectionCmd(uri1, SKOS.HAS_NARROWER,uri2), EventSenderCredentials.SERVER); 
			fail(); //call must fail because the instruction before already added an equivalent connection.
		} catch (EventPermissionDeniedException epde) {;}
		
		eb.executeCommandNow(new AddConnectionCmd(uri4, SKOS.HAS_BROADER, uri2), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri3, SKOS.HAS_BROADER, uri1), EventSenderCredentials.SERVER);
	
		eb.executeCommandNow(new AddConnectionCmd(uri4,SKOS.HAS_BROADER,uri3), EventSenderCredentials.SERVER);
		getConcept(eb, uri1);
		eb.executeCommandNow(new RemoveConnectionCmd(uri4,SKOS.HAS_BROADER,uri3), EventSenderCredentials.SERVER);
		try {
			eb.executeCommandNow(new RemoveConnectionCmd(uri4,SKOS.HAS_BROADER,uri3), EventSenderCredentials.SERVER);
			fail(); //removed already.
		} catch(EventPermissionDeniedException epde) {;}
		
		//add a few related connections
		eb.executeCommandNow(new AddConnectionCmd(uri2,SKOS.RELATED,uri3), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri4,SKOS.RELATED,uri1), EventSenderCredentials.SERVER);
		ClientSideConcept c1 = getConcept(eb,uri1);
		assertTrue(c1.getConnected(SKOS.RELATED).size()==1);
		
		//attempt a few cycles
		try {
			eb.executeCommandNow(new AddConnectionCmd(uri1, SKOS.HAS_NARROWER,uri1), EventSenderCredentials.SERVER); 
			fail(); //connection to self - must fail
		} catch (EventPermissionDeniedException epde) {;}
		try {
			eb.executeCommandNow(new AddConnectionCmd(uri2, SKOS.HAS_BROADER,uri4), EventSenderCredentials.SERVER); 
			fail(); //cycle - must fail
		} catch (EventPermissionDeniedException epde) {;}
		try {
			eb.executeCommandNow(new AddConnectionCmd(uri1, SKOS.HAS_BROADER,uri4), EventSenderCredentials.SERVER); 
			fail(); //cycle - must fail
		} catch (EventPermissionDeniedException epde) {;}
		eb.executeCommandNow(new RemoveConnectionCmd(uri4,SKOS.HAS_BROADER,uri2), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddConnectionCmd(uri1, SKOS.HAS_BROADER,uri4), EventSenderCredentials.SERVER); 
		
		//test delete
		eb.executeCommandNow(new RemoveConceptCmd(uri4),EventSenderCredentials.SERVER);
		eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("4")), EventSenderCredentials.SERVER);
		c1 = getConcept(eb, uri1);
		assertTrue(c1.getConnected(SKOS.HAS_BROADER).size() == 0);
		assertTrue(c1.getConnected(SKOS.RELATED).size() == 0);
		assertNull(getConcept(eb,uri4));

		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testConceptConnection(<50): "+duration);
	}
	
	
	@Test
	public void testCreateRemoveConcept() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		String uri = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("uri")), EventSenderCredentials.SERVER);
		assertNotNull(uri);
		
		ClientSideConcept clientSideConcept = getConcept(eb, uri);
		assertNotNull(clientSideConcept);
		
		//create concept for which a concept with the same prefLabel already exists
		try{
			eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("uri")), EventSenderCredentials.SERVER);
			fail(); //must fail, a concept with this prefLabel in the same language already exists
		} catch ( EventPermissionDeniedException epe) {
			;
		}
			
		eb.executeCommandNow(new RemoveConceptCmd(uri), EventSenderCredentials.SERVER);

		clientSideConcept = getConcept(eb,uri);
		assertNull(clientSideConcept);
		
		try {
			eb.executeCommandNow(new RemoveConceptCmd(uri), EventSenderCredentials.SERVER);
			fail(); // must fail, concept does not exist
		} catch (EventPermissionDeniedException epe) {
			;
		}
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testCreateConcept (<50): "+duration);
	}

	@Test
	public void testPrefLabels() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		String conceptURI = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);

		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);
		
		
		//testing preflabel 
		try {
			eb.executeCommandNow(new RemoveTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);
			fail(); //removing last prefLabel - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		//testing preflabel 
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel2")), EventSenderCredentials.SERVER);
			fail(); //double pref label in one language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}		
		//testing preflabel 
		try {
			eb.executeCommandNow(new RemoveTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("", Language.de)), EventSenderCredentials.SERVER);
			fail(); //adding empty string - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		//add pref label which already exists as alt label in same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
			fail(); //pref label collides with alt label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		//add pref label which already exists as hidden label in the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("HiddenLabel-de", Language.de)), EventSenderCredentials.SERVER);
			fail(); //pref label collides with hidden label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		//add pref label in German language
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel-de",Language.de)), EventSenderCredentials.SERVER);
		
		ClientSideConcept c = getConcept(eb, conceptURI);
		assertTrue(c.getTexts(SKOS.PREF_LABEL).size()==2);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.en).getString().equals("PrefLabel"));
		
		//testing add prefLabel against another existing concept
		String concept2URI = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("PrefLabel-C2",Language.fr)), EventSenderCredentials.SERVER);
		ClientSideConcept c2 = getConcept(eb, concept2URI);
		assertTrue(c2.getText(SKOS.PREF_LABEL, Language.fr).getString().equals("PrefLabel-C2"));
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel-C2",Language.fr)), EventSenderCredentials.SERVER);
			fail(); //pref label already exists for another concept - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		c = getConcept(eb,conceptURI);
		assertNull(c.getText(SKOS.PREF_LABEL, Language.fr));
		
		//testing changeText (for pref label)
		
		//change pref label to the same label
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel"),new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);
		//change pref label in the same language
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel"),new LocalizedString("PrefLabelNew")), EventSenderCredentials.SERVER);
		//change pref label which does not exist 
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel"),new LocalizedString("PrefLabelNew")), EventSenderCredentials.SERVER);		
			fail("Should fail, PrefLabel has already been changed");
		} catch (EventPermissionDeniedException epde) {
			;
		}
		c = getConcept(eb,conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.en).getString().equals("PrefLabelNew"));
		//change pref label with language change
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabelNew"),new LocalizedString("PrefLabelNew", Language.es)), EventSenderCredentials.SERVER);		
		c = getConcept(eb,conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.es).getString().equals("PrefLabelNew"));
		assertNull(c.getText(SKOS.PREF_LABEL, Language.en));
		//change pref label back 
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabelNew", Language.es),new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);
		c = getConcept(eb,conceptURI);
		assertNull(c.getText(SKOS.PREF_LABEL, Language.es));
		//change German pref label and language thus it collides with an existing English pref label
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel-de",Language.de),new LocalizedString("PrefLabel-en",Language.en)), EventSenderCredentials.SERVER);
			fail("Sneaky - changed the language of a german pref label such that it now collides with an english pref label that existed already");
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		c = getConcept(eb,conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.de).getString().equals("PrefLabel-de"));
		assertTrue(c.getText(SKOS.ALT_LABEL, Language.de).getString().equals("AltLabel-de"));
		//change pref label which is already an alt label in the same language 
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL,new LocalizedString("PrefLabel-de",Language.de), new LocalizedString("AltLabel-de", Language.de)), EventSenderCredentials.SERVER);
			fail(); //pref label collides with alt label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.de).getString().equals("PrefLabel-de"));
		assertTrue(c.getText(SKOS.HIDDEN_LABEL, Language.de).getString().equals("HiddenLabel-de"));
		//change pref label which is already an hidden label in the same language
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel-de",Language.de), new LocalizedString("HiddenLabel-de", Language.de)), EventSenderCredentials.SERVER);
			fail(); //pref label collides with hidden label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		//testing change prefLabel against another existing concept
		assertTrue(c2.getText(SKOS.PREF_LABEL, Language.fr).getString().equals("PrefLabel-C2"));
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel"), new LocalizedString("PrefLabel-C2",Language.fr)), EventSenderCredentials.SERVER);
			fail(); //pref label already exists for another concept - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		c = getConcept(eb,conceptURI);
		assertNull(c.getText(SKOS.PREF_LABEL, Language.fr));
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.en).getString().equals("PrefLabel"));
		
		//deleting test
		eb.executeCommandNow(new RemoveConceptCmd(conceptURI), EventSenderCredentials.SERVER);
		assertNull(getConcept(eb,conceptURI));

		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration TestConceptLabels (<50): "+duration);
	}

	@Test
	public void testAltLabels()throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		String conceptURI = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel-de",Language.de)), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);
				
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);
				
		//testing altlabel
		//add alt label which already exists for the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
			fail(); //must fail because of duplicate value
		} catch (EventPermissionDeniedException epde) {
			;
		}
		ClientSideConcept c = getConcept(eb, conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.en).getString().equals("PrefLabel"));
		//add alt label which is already a pref label in the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);
			fail(); //must fail because alt label collides with pref label in same language
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		c = getConcept(eb,conceptURI);
		assertTrue(c.getTexts(SKOS.HIDDEN_LABEL).contains(new LocalizedString("HiddenLabel", Language.en)));
		//add alt label which is already a hidden label in the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("HiddenLabel")), EventSenderCredentials.SERVER);
			fail(); //must fail because alt label collides with hidden label in same language
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		c = getConcept(eb, conceptURI);
		assertTrue(c.getTexts(SKOS.ALT_LABEL).size()==3);
		//remove existing alt label
		eb.executeCommandNow(new RemoveTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);
		//add new alt label
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabelNew")), EventSenderCredentials.SERVER);
		c = getConcept(eb, conceptURI);
		assertTrue(c.getText(SKOS.ALT_LABEL, Language.en).getString().equals("AltLabelNew"));
		
		//add alt label which already exists in another language 
		c = getConcept(eb, conceptURI);
		assertFalse(c.getTexts(SKOS.ALT_LABEL).contains(new LocalizedString("AltLabelNew", Language.de)));
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabelNew", Language.de)), EventSenderCredentials.SERVER);
		c = getConcept(eb, conceptURI);
		assertTrue(c.getTexts(SKOS.ALT_LABEL).contains(new LocalizedString("AltLabelNew", Language.de)));
		assertTrue(c.getTexts(SKOS.ALT_LABEL).size()==4);
		
		//testing changeText (for alt label)
				
		//change alt label to the same label
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabelNew"),new LocalizedString("AltLabelNew")), EventSenderCredentials.SERVER);
//		//change alt label
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabelNew"),new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);
		//change alt label which does not exist 
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabelNew"),new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);		
			fail("Should fail, AltLabel has already been changed");
		} catch (EventPermissionDeniedException epde) {
			;
		}
		assertTrue(c.getTexts(SKOS.ALT_LABEL).contains(new LocalizedString("AltLabel", Language.en)));
		//change alt label which already exists
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel2"),new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);		
			fail("Should fail, AltLabel already exists");
		} catch (EventPermissionDeniedException epde) {
			;
		}		
		
		c = getConcept(eb, conceptURI);
		assertTrue(c.getText(SKOS.ALT_LABEL, Language.en).getString().equals("AltLabel2"));
		//change alt label back 
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel2"),new LocalizedString("AltLabelNew")), EventSenderCredentials.SERVER);
		c = getConcept(eb,conceptURI);
		assertFalse(c.getText(SKOS.ALT_LABEL, Language.en).getString().equals("AltLabel2"));
		//change German alt label and language thus it collides with an existing English alt label
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel-de",Language.de),new LocalizedString("AltLabel",Language.en)), EventSenderCredentials.SERVER);
			fail("Sneaky - changed the language of a german alt label such that it now collides with an english alt label that existed already");
		} catch (EventPermissionDeniedException epde) {
			;
		}
						
		c = getConcept(eb,conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.de).getString().equals("PrefLabel-de"));
		assertTrue(c.getText(SKOS.ALT_LABEL, Language.de).getString().equals("AltLabel-de"));
		//change alt label which is already an pref label in the same language 
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL,new LocalizedString("AltLabel-de",Language.de), new LocalizedString("PrefLabel-de", Language.de)), EventSenderCredentials.SERVER);
			fail(); //alt label collides with pref label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		assertTrue(c.getText(SKOS.ALT_LABEL, Language.de).getString().equals("AltLabel-de"));
		assertTrue(c.getText(SKOS.HIDDEN_LABEL, Language.de).getString().equals("HiddenLabel-de"));
		//change alt label which is already an hidden label in the same language
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel-de",Language.de), new LocalizedString("HiddenLabel-de", Language.de)), EventSenderCredentials.SERVER);
			fail(); //alt label collides with hidden label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration TestAltLabels (<50): "+duration);
	}
	
	@Test
	public void testHiddenLabels() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		String conceptURI = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.PREF_LABEL, new LocalizedString("PrefLabel-de",Language.de)), EventSenderCredentials.SERVER);
		
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.ALT_LABEL, new LocalizedString("AltLabel2")), EventSenderCredentials.SERVER);
				
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel")), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel-de",Language.de)), EventSenderCredentials.SERVER);
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);
		
		//testing hidden label
		//add hidden label which already exists for the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel")), EventSenderCredentials.SERVER);
			fail(); //must fail because of duplicate value
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		ClientSideConcept c = getConcept(eb, conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.en).getString().equals("PrefLabel"));
		//add hidden label which is already a pref label in the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);
			fail(); //must fail because hidden label collides with pref label in same language
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		c = getConcept(eb,conceptURI);
		assertTrue(c.getTexts(SKOS.ALT_LABEL).contains(new LocalizedString("AltLabel", Language.en)));
		//add hidden label which is already a alt label in the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
			fail(); //must fail because hidden label collides with pref label in same language
		} catch (EventPermissionDeniedException epde) {
			;
		}
				
		c = getConcept(eb, conceptURI);
		assertTrue(c.getTexts(SKOS.HIDDEN_LABEL).size()==3);
		//remove existing hidden label
		eb.executeCommandNow(new RemoveTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);
		//add new hidden label
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabelNew")), EventSenderCredentials.SERVER);
		c = getConcept(eb, conceptURI);
		assertTrue(c.getText(SKOS.HIDDEN_LABEL, Language.en).getString().equals("HiddenLabelNew"));
		
		//add hidden label which already exists in another language (should work)
		c = getConcept(eb, conceptURI);
		assertFalse(c.getTexts(SKOS.HIDDEN_LABEL).contains(new LocalizedString("HiddenLabelNew", Language.de)));
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabelNew", Language.de)), EventSenderCredentials.SERVER);
		c = getConcept(eb, conceptURI);
		assertTrue(c.getTexts(SKOS.HIDDEN_LABEL).contains(new LocalizedString("HiddenLabelNew", Language.de)));
		assertTrue(c.getTexts(SKOS.HIDDEN_LABEL).size()==4);
		
		//testing changeText (for hidden label)
				
		//change hidden label to the same label
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabelNew"),new LocalizedString("HiddenLabelNew")), EventSenderCredentials.SERVER);
		//change hidden label
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabelNew"),new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);
		//change hidden label which does not exist 
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabelNew"),new LocalizedString("HiddenLabel2")), EventSenderCredentials.SERVER);		
			fail("Should fail, HiddenLabel has already been changed");
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		assertTrue(c.getTexts(SKOS.HIDDEN_LABEL).contains(new LocalizedString("HiddenLabel", Language.en)));
		//change alt label which already exists
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2"),new LocalizedString("HiddenLabel")), EventSenderCredentials.SERVER);		
			fail("Should fail, HiddenLabel already exists");
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		c = getConcept(eb, conceptURI);
		assertTrue(c.getTexts(SKOS.HIDDEN_LABEL).contains(new LocalizedString("HiddenLabel2")));
		//change hidden label back 
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel2"),new LocalizedString("HiddenLabelNew")), EventSenderCredentials.SERVER);
		c = getConcept(eb,conceptURI);
		assertFalse(c.getTexts(SKOS.HIDDEN_LABEL).contains(new LocalizedString("HiddenLabel2")));
		//change German hidden label and language thus it collides with an existing English hidden label
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL, new LocalizedString("HiddenLabel-de",Language.de),new LocalizedString("HiddenLabel",Language.en)), EventSenderCredentials.SERVER);
			fail("Sneaky - changed the language of a german hidden label such that it now collides with an english hidden label that existed already");
		} catch (EventPermissionDeniedException epde) {
			;
		}
						
		c = getConcept(eb,conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.de).getString().equals("PrefLabel-de"));
		assertTrue(c.getText(SKOS.HIDDEN_LABEL, Language.de).getString().equals("HiddenLabel-de"));
		//change hidden label which is already an pref label in the same language 
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL,new LocalizedString("HiddenLabel-de",Language.de), new LocalizedString("PrefLabel-de", Language.de)), EventSenderCredentials.SERVER);
			fail(); //hidden label collides with pref label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}
		
		c = getConcept(eb,conceptURI);
		assertTrue(c.getText(SKOS.ALT_LABEL, Language.de).getString().equals("AltLabel-de"));
		assertTrue(c.getText(SKOS.HIDDEN_LABEL, Language.de).getString().equals("HiddenLabel-de"));
		//change hidden label which is already an alt label in the same language 
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.HIDDEN_LABEL,new LocalizedString("HiddenLabel-de",Language.de), new LocalizedString("AltLabel-de", Language.de)), EventSenderCredentials.SERVER);
			fail(); //hidden label collides with alt label in same language - should raise an exception. 
		} catch (EventPermissionDeniedException epde) {
			;
		}	
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration TestHiddenLabels (<50): "+duration);
	}
	
	@Test
	public void testNotes()throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		
		EventBus eb = space.getEventBus();
		String conceptURI = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("PrefLabel")), EventSenderCredentials.SERVER);

		ClientSideConcept c = getConcept(eb, conceptURI);
		assertTrue(c.getText(SKOS.PREF_LABEL, Language.en).getString().equals("PrefLabel"));
		//testing note
		//add new note
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("note")), EventSenderCredentials.SERVER);
		//add a note in another language
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("Notiz",Language.de)), EventSenderCredentials.SERVER);
		//add note which already exists for the same language
		try {
			eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("AltLabel")), EventSenderCredentials.SERVER);
			fail(); //must fail, already text in language
		} catch (EventPermissionDeniedException epde) {
			;
		}
		c = getConcept(eb, conceptURI);
		assertTrue(c.getTexts(SKOS.NOTE).size()==2);
		//remove note
		eb.executeCommandNow(new RemoveTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("note")), EventSenderCredentials.SERVER);
		//add note
		eb.executeCommandNow(new AddTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("Note")), EventSenderCredentials.SERVER);
		c = getConcept(eb, conceptURI);
		assertTrue(c.getText(SKOS.NOTE, Language.en).getString().equals("Note"));
		assertTrue(c.getTexts(SKOS.NOTE).size()==2);
		
		//testing change text for note
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("Notiz",Language.de),new LocalizedString("Notiz2",Language.de) ), EventSenderCredentials.SERVER);
		//change note which does not exist
		try {
			eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("Notiz2",Language.en),new LocalizedString("Notiz",Language.en) ), EventSenderCredentials.SERVER);
			fail("Must fail, no such text in this language");
		} catch (EventPermissionDeniedException e) {
			;
		}
		c = getConcept(eb,conceptURI);
		assertTrue(c.getText(SKOS.NOTE, Language.de).getString().equals("Notiz2"));
		eb.executeCommandNow(new ChangeTextCmd(conceptURI, SKOS.NOTE, new LocalizedString("Notiz2",Language.de),new LocalizedString("Notiz",Language.de) ), EventSenderCredentials.SERVER);		
		c = getConcept(eb,conceptURI);
		assertFalse(c.getText(SKOS.NOTE, Language.de).getString().equals("Notiz2"));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration TestNotes (<50): "+duration);
	}
	
	
	private static ClientSideConcept getConcept(EventBus eb, String uri) throws EventPermissionDeniedException {
		ClientSideTaxonomy tax = (ClientSideTaxonomy) eb.executeQuery(new GetClientSideTaxonomy(false), EventSenderCredentials.SERVER);
		ClientSideConcept clientSideConcept = tax.get(uri);
		return clientSideConcept;
	}	
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) {
			throw new IOException("Temp Directory already exists!");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		;
	}
	
	//carefull with this method .. it can really clean a harddrive in no time ;)
	private static void deleteRecursive(File file) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) deleteRecursive(f);
		}
		file.delete();
	}
	
	@Before
	public void setUp() throws Exception {
		space = new SpaceImpl(tempDirectory,"testSpace",null);
	}

	@After
	public void tearDown() throws Exception {
		space.close();
		try {
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	

}
