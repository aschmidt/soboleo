	package de.fzi.ipe.soboleo.test.otherTests;


import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;

import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;

public class Test_SPARQLQueries extends Assert {
	
	static final File tempDirectory = new File("SPARQL-testDirectory");
	private RepositoryConnection con;
	ValueFactory vf;
	public static final String METACHARS = "\\.?*+{}()[]";
	
	@Test
		public void exportRDF() throws Exception {
			long time = System.currentTimeMillis();
			

			MemoryStore memStore = new MemoryStore(tempDirectory);
			memStore.setSyncDelay(1000L);
			
			Repository repository = new SailRepository(memStore);
			repository.initialize();
			con = repository.getConnection();
			
			vf = repository.getValueFactory();
						
			URI subject = vf.createURI("http://soboleo.com/ns/1.0#space-default-gen9");
			URI predicate = vf.createURI(SKOS.PREF_LABEL.toString());
			Value object = vf.createLiteral("te{st", "en");	
			URI context = null;
			con.add(subject, predicate, object, context);
			
			subject = vf.createURI("http://soboleo.com/ns/1.0#space-default-gen10");
			object = vf.createLiteral("Test", "fr");
			con.add(subject, predicate, object, context);
			Set<Value> result = checkPrefLabel(new LocalizedString("Te{st"));

			for(Value conceptURI : result) System.out.println(conceptURI);
			
			con.close();
			con.getRepository().shutDown();
			long duration = System.currentTimeMillis() - time;
			System.out.println("Duration exportRDF (<20000): "+duration);
			
				
		}
	
		public Set<Value> checkPrefLabel(LocalizedString toCheck) throws IOException{
			
			String toMatch = regexEncode(toCheck.getString());
			toMatch = "^" +toMatch +"$";
			System.out.println(toMatch);
			
			String bindingName = "conceptURI";
			String query = "SELECT ?" +bindingName +
	        " WHERE { " +
	            "?" + bindingName +" <" + SKOS.PREF_LABEL.toString() +"> ?name . " +
	            "FILTER (regex(str(?name), \"" + toMatch +"\", \"i\") &&  lang(?name) = \"" +toCheck.getLanguage().toString() +"\")" +
	        "}";
			Set<Value> result = executeSelectSPARQLQueryForParameter(query, bindingName);
			return result;
		}
		
		public Set<Value> executeSelectSPARQLQueryForParameter(String queryString, String paramName) throws IOException{
			try {
	    		TupleQuery query = con.prepareTupleQuery(org.openrdf.query.QueryLanguage.SPARQL, queryString);
	    		TupleQueryResult result = query.evaluate();
	    		Set<Value> resultList = new HashSet<Value>();
	    		if(!result.getBindingNames().contains(paramName)) throw new IllegalArgumentException(paramName +" is not a binding name");
	    		while (result.hasNext()) {
	    			BindingSet bindingSet = result.next();
	    			Value val = bindingSet.getValue(paramName);
	    			if(val != null) resultList.add(val);
	    		}
	    		result.close();
	    		return resultList;
	    	}
	    	catch (RepositoryException re) {
	    		throw new IOException(re);
	    	} catch (MalformedQueryException e) {
	    		throw new IOException(e);
			} catch (QueryEvaluationException e) {
				throw new IOException(e);
			}
		}
		
		 /**
		  * @param raw a regular expression (syntax see class comment) , never null
		  * @return an encoded regular expression
		  */
		 public static String regexEncode(String raw) {
		  if(raw == null)
		   throw new IllegalArgumentException("raw may not be null");
		  
		  String result = raw;
		  // escape all meta characters
		  for(int i = 0; i < METACHARS.length(); i++) {
		   char c = METACHARS.charAt(i);
		   result = result.replace("" + c, "" + "\\\\" + c);
		  }
		  return result;
		 }
		
			@BeforeClass
			public static void setUpBeforeClass() throws Exception {
				//check if temp directory already exist - fail if it does becomes we want to be 
				//able to simply delete the directory after we're done
				deleteRecursive(tempDirectory);
				if (tempDirectory.exists()) throw new IOException("Temp Directory already exists!");
			} 
		 
			//carefull with this method .. it can really clean a harddrive in no time ;)
			private static void deleteRecursive(File file) {
				if (file.isDirectory()) {
					for (File f: file.listFiles()) deleteRecursive(f);
				}
				file.delete();
			}
			
	
}
