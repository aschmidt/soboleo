	package de.fzi.ipe.soboleo.data;

	import java.io.File;
import java.util.ArrayList;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;

import de.fzi.ipe.soboleo.skosTaxonomy.base.SoboleoNS;

public class RepositoryModifier {
	
	private String taxonomyFilePath = "C://soboleo/spaces/pt_test/taxonomy";

		public void exportRDF() throws Exception {
			long time = System.currentTimeMillis();
			

			MemoryStore memStore = new MemoryStore(new File(taxonomyFilePath));
			memStore.setSyncDelay(1000L);
			
			Repository repository = new SailRepository(memStore);
			repository.initialize();
			RepositoryConnection con = repository.getConnection();
			
			ValueFactory vf = repository.getValueFactory();
			
			URI hasEmail = vf.createURI(SoboleoNS.PEOPLE_PERSON_EMAIL.toString());
			URI person = vf.createURI("http://soboleo.com/ns/1.0#space-default-gen92");
			URI hasName = vf.createURI(SoboleoNS.PEOPLE_PERSON_NAME.toString());

			RepositoryResult<Statement> statements = con.getStatements(person, null, null, true);
//			RepositoryResult<Statement> statements = con.getStatements(null, null, person, true);
//			RepositoryResult<Statement> statements = con.getStatements(null, hasEmail, null, true);

			while (statements.hasNext()){
				Statement next = statements.next();
				System.out.println(next.getSubject() + ", "+ next.getPredicate() + ", " +  next.getObject() +", "+next.getContext());
//				System.out.println(System.getProperty("line.separator"));
//				con.remove(next.getSubject(), next.getPredicate(), next.getObject(), next.getContext());
//				String email = ((Literal) next.getObject()).toString();
//				System.out.println(email);
//				email = email.replace("\"", "");
//				System.out.println(email);
//				con.add(next.getSubject(), next.getPredicate(), vf.createLiteral("Brenda Donnison"), next.getContext());
			}
			System.out.println("deleted");
			statements = con.getStatements(person, null, null, true);
			
			while (statements.hasNext()){
				Statement next = statements.next();
				System.out.println(next.getSubject() + ", "+ next.getPredicate() + ", " +  next.getObject() +", "+next.getContext());
				System.out.println(System.getProperty("line.separator"));
			}
			statements.close();
			con.close();
			con.getRepository().shutDown();
			long duration = System.currentTimeMillis() - time;
			System.out.println("Duration exportRDF (<20000): "+duration);
			
				
		}
		
		public static void main(String[] args) {
			RepositoryModifier rm = new RepositoryModifier();
			try {
				rm.exportRDF();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}
