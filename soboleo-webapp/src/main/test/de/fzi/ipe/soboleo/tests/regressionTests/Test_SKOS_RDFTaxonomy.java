/*
 * This is the class test the functionality of the SKOS_RDFTaxonomy and SKOS_RDFConcept in
 * conjunction with a sesame triple store. For the test a new directory is created that will
 * contain the information created during the test, this directory is deleted afterwards
 * FZI - Information Process Engineering 
 * Created on 24.01.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;

import static de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.URI;

import de.fzi.ipe.soboleo.data.tripleStore.TripleStore;
import de.fzi.ipe.soboleo.data.tripleStore.TripleStoreFactory;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.SesameTripleStoreFactory;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.eventBusAdaptor.SKOS_RDFConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.eventBusAdaptor.SKOS_RDFTaxonomy;

public class Test_SKOS_RDFTaxonomy {

	static final File tempDirectory = new File("SKOSRDFTaxonomy-testDirectory");

	private SKOS_RDFTaxonomy tax;
	private TripleStore tripleStore;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) throw new IOException("Temp Directory already exists!");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (tempDirectory.exists()) {
			for (File f:tempDirectory.listFiles()) {
				f.delete();
			}
			tempDirectory.delete();
		}
	}
	
	@Before
	public void setUp() throws Exception {
		TripleStoreFactory factory = new SesameTripleStoreFactory();
		tripleStore = factory.getTripleStore(tempDirectory,"space-test");
		tax = new SKOS_RDFTaxonomy(tripleStore);
	}

	@After
	public void tearDown() throws Exception {
		for (SKOS_RDFConcept c:tax.getAllConcepts()) tax.deleteConcept(c); //cleaning tax between tests.
		tripleStore.close();
	}

	/**
	 * Closes and reopens the triple store
	 */
	private void reopen() throws IOException {
		tripleStore.close();
		TripleStoreFactory factory = new SesameTripleStoreFactory();
		tripleStore = factory.getTripleStore(tempDirectory,"space-test");
		tax = new SKOS_RDFTaxonomy(tripleStore);
	}
	
	@Test 
	public void testSuperSub() throws IOException {
		long timeBegin = System.currentTimeMillis();
		
		//core thing to test is the prevention of cicles, other stuff is already tested in testRelated
		SKOS_RDFConcept c1 = tax.createConcept();
		SKOS_RDFConcept c2 = tax.createConcept();
		SKOS_RDFConcept c3 = tax.createConcept();
		SKOS_RDFConcept c4 = tax.createConcept();
		SKOS_RDFConcept c5 = tax.createConcept();

		//c1 c2
		// c3
		//c4,c5
		assertNull(c3.checkAddConnection(HAS_BROADER, c1, false));
		c3.addConnection(HAS_BROADER, c1);
		assertNull(c3.checkAddConnection(HAS_BROADER, c2, false));
		c3.addConnection(HAS_BROADER, c2);
	
		assertNull(c4.checkAddConnection(HAS_BROADER, c3, false));
		c4.addConnection(HAS_BROADER, c3);
		assertNull(c5.checkAddConnection(HAS_BROADER, c3, false));
		c5.addConnection(HAS_BROADER, c3);
		
		assertNull(c5.checkAddConnection(HAS_BROADER, c2, false));
		c5.addConnection(HAS_BROADER, c2);
		
		assertNotNull(c3.checkAddConnection(HAS_BROADER, c4, false));
		assertNotNull(c1.checkAddConnection(HAS_BROADER, c4, false));
		
		assertTrue(tax.getRootConcepts().contains(c1));
		assertTrue(tax.getRootConcepts().contains(c2));
		assertFalse(tax.getRootConcepts().contains(c4));
		
		assertNull(c4.checkRemoveConnection(HAS_BROADER, c3));
		c4.removeConnection(HAS_BROADER, c3);
		assertTrue(tax.getRootConcepts().contains(c4));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration SuperSub(<50): "+duration);	
	}
	
	@Test
	public void testDescription() throws IOException {
		long timeBegin = System.currentTimeMillis();

		SKOS_RDFConcept c1 = tax.createConcept();
		URI c1URI = c1.getURI();
		
		LocalizedString desc = new LocalizedString("desc");
		LocalizedString desc2 = new LocalizedString("new");
		LocalizedString descD = new LocalizedString("desc",LocalizedString.Language.de);
		
		assertTrue(c1.getTexts(NOTE).size() == 0);
		assertNull(c1.checkAddText(desc, NOTE, true));
		c1.addText(desc, NOTE);
		assertNull(c1.checkAddText(descD, NOTE, true));
		c1.addText(descD, NOTE);
		assertTrue(c1.getTexts(NOTE).size() == 2);
		assertNotNull(c1.checkAddText(desc2, NOTE, true));
		assertNull(c1.checkRemoveText(desc,NOTE));
		c1.removeText(desc,NOTE);
		assertNull(c1.checkAddText(desc2, NOTE, true));
		c1.addText(desc2, NOTE);
	
		reopen();
		SKOS_RDFConcept c2 = tax.getConcept(c1URI);
		LocalizedString ls = c2.getText(NOTE, LocalizedString.Language.en);
		assertTrue(ls.equals(desc2));
			
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration Description (<50): "+duration);
	}
	
	@Test 
	public void testSynonyms() throws IOException {
		long timeBegin = System.currentTimeMillis();
		SKOS_RDFConcept c1 = tax.createConcept();
		URI c1URI = c1.getURI();
		
		LocalizedString syn = new LocalizedString("The Mouse",LocalizedString.Language.en);
		LocalizedString synD = new LocalizedString("The Mouse",LocalizedString.Language.de);
		assertNull(c1.checkAddText(syn,ALT_LABEL,false));
		c1.addText(syn, ALT_LABEL);
		assertNotNull(c1.checkAddText(syn,ALT_LABEL,false));

		assertNull(c1.checkAddText(synD,ALT_LABEL,false));
		c1.addText(synD, ALT_LABEL);
		assertNotNull(c1.checkAddText(synD,ALT_LABEL,false));

		reopen();
		c1 = tax.getConcept(c1URI);
		
		assertTrue(c1.getTexts(ALT_LABEL).contains(syn));
		assertTrue(c1.getTexts(ALT_LABEL).contains(synD));
		assertNull(c1.checkRemoveText(syn, ALT_LABEL));
		c1.removeText(syn, ALT_LABEL);
		assertFalse(c1.getTexts(ALT_LABEL).contains(syn));
		assertNotNull(c1.checkRemoveText(syn, ALT_LABEL));
		assertTrue(c1.getTexts(ALT_LABEL).contains(synD));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration Synonyms (<50): "+duration);
	}
	
	@Test 
	public void testHiddenLabels() throws IOException {
		long timeBegin = System.currentTimeMillis();
		SKOS_RDFConcept c1 = tax.createConcept();
		URI c1URI = c1.getURI();
		
		LocalizedString hidden = new LocalizedString("The Elephant",LocalizedString.Language.en);
		LocalizedString hiddenD = new LocalizedString("The Elephant",LocalizedString.Language.de);
		assertNull(c1.checkAddText(hidden,HIDDEN_LABEL,false));
		c1.addText(hidden, HIDDEN_LABEL);
		assertNotNull(c1.checkAddText(hidden,HIDDEN_LABEL,false));

		assertNull(c1.checkAddText(hiddenD,HIDDEN_LABEL,false));
		c1.addText(hiddenD, HIDDEN_LABEL);
		assertNotNull(c1.checkAddText(hiddenD,HIDDEN_LABEL,false));

		reopen();
		c1 = tax.getConcept(c1URI);
		
		assertTrue(c1.getTexts(HIDDEN_LABEL).contains(hidden));
		assertTrue(c1.getTexts(HIDDEN_LABEL).contains(hiddenD));
		assertNull(c1.checkRemoveText(hidden, HIDDEN_LABEL));
		c1.removeText(hidden, HIDDEN_LABEL);
		assertFalse(c1.getTexts(HIDDEN_LABEL).contains(hidden));
		assertNotNull(c1.checkRemoveText(hidden, HIDDEN_LABEL));
		assertTrue(c1.getTexts(HIDDEN_LABEL).contains(hiddenD));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration Hiddens (<50): "+duration);
	}
	
	
	@Test
	public void testRelatedConcept() throws IOException {
		long timeBegin = System.currentTimeMillis();
		SKOS_RDFConcept c1 = tax.createConcept();
		SKOS_RDFConcept c2 = tax.createConcept();
		SKOS_RDFConcept c3 = tax.createConcept();
		tax.createConcept();
		
		String msg1 = c1.checkAddConnection(SKOS.RELATED,c1,true);
		assertTrue(msg1 != null); //No related relation to self
		
		String msg2= c1.checkAddConnection(SKOS.RELATED,c2,true);
		assertTrue(msg2 == null); //should be ok. 
		
		c1.addConnection(SKOS.RELATED, c2);
		String msg3 = c1.checkAddConnection(SKOS.RELATED,c2,true);
		assertTrue(msg3 != null);  //relation exists already.
		assertFalse(c2.getConnected(SKOS.RELATED).contains(c1)); //c2 should be related to c1
		assertTrue(c1.getConnected(SKOS.RELATED).contains(c2)); //c1 should be related to c2
		assertTrue(!c1.getConnected(SKOS.RELATED).contains(c3)); //c1 should not be related to c3
		
		String msg4 = c1.checkAddConnection(SKOS.RELATED,c3,true);
		assertTrue(msg4 == null);
		c1.addConnection(SKOS.RELATED,c3);
		String msg5 = c1.checkRemoveConnection(SKOS.RELATED,c2);
		assertTrue(msg5 == null);
		c1.removeConnection(SKOS.RELATED,c2);
		String msg6 = c1.checkRemoveConnection(SKOS.RELATED,c2);
		assertTrue(msg6 != null);
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration RelatedConcept (<50): "+duration);
	}
	
	@Test 
	public void testConceptCreationAndRemoval() throws IOException {
		long timeBegin = System.currentTimeMillis();
		SKOS_RDFConcept c1 = tax.createConcept();
		c1.addText(new LocalizedString("Dagobert"), PREF_LABEL);
		LocalizedString name = c1.getText(PREF_LABEL,LocalizedString.Language.en);
		String uri = c1.getURI().toString();
		tax.createConcept();
		reopen();
		SKOS_RDFConcept c3 = tax.getConcept(tripleStore.getValueFactory().createURI(uri));
		assertTrue(c3.getText(PREF_LABEL,LocalizedString.Language.en).equals(name));
		assertTrue(tax.getAllConcepts().size() == 2);
		tax.deleteConcept(c3);
		assertTrue(tax.getAllConcepts().size() == 1);
		reopen();
		assertTrue(tax.getAllConcepts().size() == 1);
		SKOS_RDFConcept c4 = tax.getConcept(tripleStore.getValueFactory().createURI(uri));
		assertTrue(c4 == null);
		for (SKOS_RDFConcept c:tax.getAllConcepts()) tax.deleteConcept(c); //cleaning tax between tests.
		assertTrue(tax.getAllConcepts().size()==0);
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration ConceptCreationAndRemoval (<200): "+duration);
	}

}
