/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;

import java.io.IOException;
import java.util.HashSet;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.dialog.commands.AddDialogTag;
import de.fzi.ipe.soboleo.dialog.commands.SetDialogContent;
import de.fzi.ipe.soboleo.dialog.commands.SetDialogTitle;
import de.fzi.ipe.soboleo.documentsCommons.clientSide.Tag;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceImpl;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;
import de.fzi.ipe.soboleo.webdocument.clientSide.WebDocument;
import de.fzi.ipe.soboleo.webdocument.commands.AddWebDocument;
import de.fzi.ipe.soboleo.webdocument.commands.AddWebDocumentTag;
import de.fzi.ipe.soboleo.webdocument.commands.RemoveWebDocument;
import de.fzi.ipe.soboleo.webdocument.commands.RemoveWebDocumentTag;
import de.fzi.ipe.soboleo.webdocument.commands.SetWebDocumentContent;
import de.fzi.ipe.soboleo.webdocument.commands.SetWebDocumentTitle;
import de.fzi.ipe.soboleo.webdocument.commands.SetWebDocumentURL;
import de.fzi.ipe.soboleo.webdocument.lucene.LuceneResult;
import de.fzi.ipe.soboleo.webdocument.lucene.LuceneResult.ResultType;
import de.fzi.ipe.soboleo.webdocument.lucene.queries.GetDocumentsForConcept;
import de.fzi.ipe.soboleo.webdocument.lucene.queries.GetSearchResult;
import de.fzi.ipe.soboleo.webdocument.queries.GetDocumentByURLQuery;
import de.fzi.ipe.soboleo.webdocument.queries.GetWebDocument;
import de.fzi.ipe.soboleo.webdocument.search.SearchResult;

public class Test_DocumentEvents extends Assert{

		static final File tempDirectory = new File("DocumentEvents-testDirectory");

		private Space space; 

	
		@Test
		public void TestDocumentEvents() throws IOException, EventPermissionDeniedException {
			long timeBegin = System.currentTimeMillis();
			
			EventSenderCredentials cred = EventSenderCredentials.SERVER;
			EventBus eventBus = space.getEventBus();

			WebDocument doc = (WebDocument) eventBus.executeCommandNow(new AddWebDocument("http://www.spiegel.de","Spiegel"), cred);
			assertNotNull(doc);
			eventBus.executeCommandNow(new AddWebDocumentTag(doc.getURI(),"http://aConcept.de"), cred);
			eventBus.executeCommandNow(new AddWebDocumentTag(doc.getURI(),"http://anotherConcept.de"), cred);			
			
			WebDocument doc1 = (WebDocument) eventBus.executeQuery(new GetDocumentByURLQuery("http://www.spiegel.de"), cred);
			assertTrue(doc1.getTitle().equals("Spiegel"));
			assertTrue(doc1.getAllTags().size()==2);
			
			Tag tag1 = doc1.getAllTags().iterator().next();
			String tag1ConceptURI = tag1.getConceptID();
			assertTrue(doc1.getTagConceptIDs().contains(tag1ConceptURI));
			
			eventBus.executeCommandNow(new RemoveWebDocumentTag(doc1.getURI(),tag1.getTagID(), tag1ConceptURI), cred);
			doc1 = (WebDocument) eventBus.executeQuery(new GetDocumentByURLQuery("http://www.spiegel.de"), cred);
			assertTrue(doc1.getAllTags().size()==1);
			assertFalse(doc1.getTagConceptIDs().contains(tag1ConceptURI));
			
			eventBus.executeCommandNow(new SetWebDocumentTitle(doc1.getURI(),"Spiegel Neu"), 	cred);
			eventBus.executeCommandNow(new SetWebDocumentURL(doc1.getURI(),"http://www.spgl.de"),cred);

			doc1 = (WebDocument) eventBus.executeQuery(new GetDocumentByURLQuery("http://www.spgl.de"), cred);
			assertTrue(doc1.getTitle().equals("Spiegel Neu"));
			doc1 = (WebDocument) eventBus.executeQuery(new GetDocumentByURLQuery("http://www.spiegel.de"), cred);
			assertTrue(doc1== null);
			
			doc1 = (WebDocument) eventBus.executeQuery(new GetDocumentByURLQuery("http://www.spgl.de"), cred);
			eventBus.executeCommandNow(new RemoveWebDocument(doc1.getURI()), cred);
			doc1 = (WebDocument) eventBus.executeQuery(new GetDocumentByURLQuery("http://www.spgl.de"), cred);
			assertTrue(doc1== null);
			
			eventBus.executeQuery(new GetSearchResult("Spiegel",null), cred);
			
			long duration = System.currentTimeMillis() - timeBegin;
			System.out.println("Duration TestDocumentEvents(<1000): "+duration);	
		}
		
		
		@Test
		public void TestDocumentSearch() throws IOException, EventPermissionDeniedException {
			long timeBegin = System.currentTimeMillis();
			
			EventSenderCredentials cred = EventSenderCredentials.SERVER;
			EventBus eventBus = space.getEventBus();

			// two dialogs - just textual content, no tags
			// http://ThisIsMyThest1.de/ - ditle1, ditleContent, 
			// http://ThisIsMyThest1.de/ - , ditleContent, 
			String dURI_1 = "http://ThisIsMyThest1.de/";
			String dURI_2 = "http://ThisIsMyThest2.de/";
			
			eventBus.executeCommandNow(new SetDialogTitle(dURI_1,"ditle1"), cred);
			eventBus.executeCommandNow(new SetDialogContent(dURI_1,"ditleContent"), cred);
			eventBus.executeCommandNow(new SetDialogContent(dURI_2,"dupple"), cred);
			
			SearchResult searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("ditle1",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("dupple",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("dupple",LuceneResult.ResultType.DIALOG), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("dupple",LuceneResult.ResultType.WEB_DOCUMENT), cred);
			assertTrue(searchResult.getLuceneResult().length() == 0);
			
			//three web documents - just textual content, no tags
			//doc = thisIsMyThest, ditle, ditleContent
			//doc2 = thisIsMyThest4, ThisIsMyThest4, dupple4
			//doc3 = thisisMyThest3, testtest
			
			WebDocument doc = (WebDocument) eventBus.executeCommandNow(new AddWebDocument("http://ThisIsMyThest.de/","test"), cred);
			assertNotNull(doc);			
			WebDocument doc2 = (WebDocument) eventBus.executeCommandNow(new AddWebDocument("http://ThisIsMyThest4.de/","ThisIsMyThest4"), cred);
			assertNotNull(doc2);			
			WebDocument doc3 = (WebDocument) eventBus.executeCommandNow(new AddWebDocument("http://ThisIsMyThest3.de/","testtest"), cred);			
			assertNotNull(doc3);
			
			eventBus.executeCommandNow(new SetWebDocumentTitle(doc.getURI(),"ditle3"), cred);
			doc = (WebDocument) eventBus.executeQuery(new GetWebDocument(doc.getURI()), cred);
			assertTrue(doc.getTitle().equals("ditle3"));
			
			eventBus.executeCommandNow(new SetWebDocumentContent(doc.getURI(),"ditle3Content"), cred);
			eventBus.executeCommandNow(new SetWebDocumentContent(doc2.getURI(),"dupple4"), cred);
			
			// // // // 
			//Test Textual Search
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("ditle3",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("dupple4",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("dupple4",LuceneResult.ResultType.DIALOG), cred);
			assertTrue(searchResult.getLuceneResult().length() == 0);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("dupple4",LuceneResult.ResultType.WEB_DOCUMENT), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1);

			//create a taxonomy. 
			//Concept1 with children Concept1.1 and Concept 1.2 
			//Concept2 with Child C2.1
			//Concept3
			String c1 = (String) eventBus.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept1")), cred);
			String c11 = (String) eventBus.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept1.1")), cred);
			String c12 = (String) eventBus.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept1.2")), cred);
			String c2 = (String) eventBus.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept2")), cred);
			String c21 = (String) eventBus.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept2.1")), cred);
			String c3 = (String) eventBus.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept3")), cred);
			eventBus.executeCommand(new AddConnectionCmd(c1,SKOS.HAS_NARROWER,c11), cred);
			eventBus.executeCommand(new AddConnectionCmd(c1,SKOS.HAS_NARROWER,c12), cred);
			eventBus.executeCommand(new AddConnectionCmd(c2,SKOS.HAS_NARROWER,c21), cred);
			
			// add a few tags
			// dialog 1 is annotated with c11 & c2
			// doc with c12 and c3
			// doc 2 with c2
			eventBus.executeCommandNow(new AddDialogTag(dURI_1, c11), cred);
			eventBus.executeCommandNow(new AddDialogTag(dURI_1, c2), cred);
			eventBus.executeCommandNow(new AddWebDocumentTag(doc.getURI(),c12), cred);
			eventBus.executeCommandNow(new AddWebDocumentTag(doc.getURI(),c3), cred);
			eventBus.executeCommandNow(new AddWebDocumentTag(doc2.getURI(),c2), cred);
			
			// Test Concept Search
			LuceneResult luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c3,null), cred); 
			assertTrue(luceneResult.length() == 1); // doc 
			
			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c2,null), cred); 
			assertTrue(luceneResult.length() == 2); // doc 2 & dialog 
			
			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c2,ResultType.WEB_DOCUMENT), cred); 
			assertTrue(luceneResult.length() == 1); // doc 2  

			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c2,ResultType.DIALOG), cred); 
			assertTrue(luceneResult.length() == 1); // dialog
			assertTrue(luceneResult.getType(0).equals(ResultType.DIALOG));

			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c1,null), cred); 
			assertTrue(luceneResult.length() == 2); // doc  & dialog 

			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c11,null), cred); 
			assertTrue(luceneResult.length() == 1); // dialog 
			
			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c11,ResultType.WEB_DOCUMENT), cred); 
			assertTrue(luceneResult.length() == 0); // doc 			
			
			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c1,ResultType.WEB_DOCUMENT), cred); 
			assertTrue(luceneResult.length() == 1); // doc 

			luceneResult = (LuceneResult) eventBus.executeQuery(new GetDocumentsForConcept(c1,ResultType.DIALOG), cred); 
			assertTrue(luceneResult.length() == 1); // dialog
			assertTrue(luceneResult.getType(0).equals(ResultType.DIALOG));

			// Test combined semantic search
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept2 ThisIsMyThest4",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 2); 
			assertTrue(searchResult.getLuceneResult().doc(0).getURI().equals(doc2.getURI()));
			
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept2 ThisIsMyThest4",ResultType.DIALOG), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1); 
			assertTrue(searchResult.getLuceneResult().doc(0).getURI().equals(dURI_1));

			HashSet<String> mustConceptUris = new HashSet<String>();
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept2 ThisIsMyThest4",null,mustConceptUris), cred);
			assertTrue(searchResult.getLuceneResult().length() == 2); 
			assertTrue(searchResult.getLuceneResult().doc(0).getURI().equals(doc2.getURI()));
			
			mustConceptUris.add(c11);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept2 ThisIsMyThest4",null,mustConceptUris), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1); 
			assertTrue(searchResult.getLuceneResult().doc(0).getURI().equals(dURI_1));

//			mustConceptUris.add(c3); this test will fail - there is currently a bug with multiple must conceptUris
//			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept2 ThisIsMyThest4",null,mustConceptUris), cred);
//			assertTrue(searchResult.getLuceneResult().length() == 0); 

			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept1 ",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 2); 
			
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("asfdsdlfjdsf Concept1 sfdsdf sdf ",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 2); 

			mustConceptUris.clear();
			mustConceptUris.add(c11);
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept1 ",null,mustConceptUris), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1); 

			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept1 ",ResultType.DIALOG,mustConceptUris), cred);
			assertTrue(searchResult.getLuceneResult().length() == 1); 
			
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept1 ",ResultType.WEB_DOCUMENT,mustConceptUris), cred);
			assertTrue(searchResult.getLuceneResult().length() == 0); 

			
			searchResult = (SearchResult) eventBus.executeQuery(new GetSearchResult("Concept2 ThisIsMyThest4",null), cred);
			assertTrue(searchResult.getLuceneResult().length() == 2); 
			assertTrue(searchResult.getLuceneResult().doc(0).getURI().equals(doc2.getURI()));
		
			
			long duration = System.currentTimeMillis() - timeBegin;
			System.out.println("Duration DocumentSearch(<1000): "+duration);	
		}
		
		@BeforeClass
		public static void setUpBeforeClass() throws Exception {
			//check if temp directory already exist - fail if it does becomes we want to be 
			//able to simply delete the directory after we're done
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
//				throw new IOException("Temp Directory already exists!");
			}
//			ServerInitializer.getInstance().setServer(tempDirectory.getAbsolutePath(), "spaces");
		}

		@AfterClass
		public static void tearDownAfterClass() throws Exception {
			;
		}
		
		//careful with this method .. it can really clean a harddrive in no time ;)
		private static void deleteRecursive(File file) {
			if (file.isDirectory()) {
				for (File f: file.listFiles()) deleteRecursive(f);
			}
			file.delete();
		}
		
		@Before
		public void setUp() throws Exception {
			try {
				space = new SpaceImpl(tempDirectory,"testSpace",null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@After
		public void tearDown() throws Exception {
			space.close();
			try {
				if (tempDirectory.exists()) {
//					deleteRecursive(tempDirectory);
				}
			} catch(Exception e) {
				e.printStackTrace();
				throw e;
			}
		}
		

	
}
