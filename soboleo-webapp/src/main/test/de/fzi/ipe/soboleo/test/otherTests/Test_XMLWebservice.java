package de.fzi.ipe.soboleo.test.otherTests;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceException;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceRequest;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceResult;
import de.fzi.ipe.soboleo.server.xmlWebservice.client.SOBOLEORemoteSpace;
import de.fzi.ipe.soboleo.server.xmlWebservice.client.SOBOLEOServerConnection;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.queries.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;
import de.fzi.ipe.soboleo.webdocument.lucene.queries.GetSearchResult;
import de.fzi.ipe.soboleo.webdocument.search.SearchResult;

/**
 * Tests the XML Webservice - note that this test requires a running tomcat. 
 */
public class Test_XMLWebservice extends Assert{

	private XStream xstream = new XStream(new DomDriver());
	private EventSenderCredentials credentials = new EventSenderCredentials("sdfsdf","","");
	private Random random = new Random();
	
//	static final String SERVER_URL = "http://localhost:8080/xmlService";
	static final String SERVER_URL = "http://tool.soboleo.com/xmlService";
	
	@Test
	public void testSoboleoRemoteSpace() throws XMLWebserviceException {
		long timeBegin = System.currentTimeMillis();

		SOBOLEORemoteSpace remoteSpace = new SOBOLEORemoteSpace(SERVER_URL,"default",1,EventSenderCredentials.ANONYM);
		assertNotNull(remoteSpace.getTaxonomy());
		
		String conceptURI = (String) remoteSpace.sendEvent(new CreateConceptCmd(new LocalizedString(""+random.nextInt())));
		assertNotNull(conceptURI);
		
		ClientSideConcept clientSideConcept = remoteSpace.getTaxonomy().get(conceptURI);
		assertNotNull(clientSideConcept);
		
		String searchString = "747";

		GetSearchResult query = 
		new GetSearchResult(searchString,null);

		SearchResult result = (SearchResult)
		remoteSpace.sendEvent(query); 
		
		remoteSpace.close();
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testSoboleoRemoteSpace(<500): "+duration);		
	}
	
	@Test
	public void testSoboleoServerConnect() throws IOException, XMLWebserviceException {
		long timeBegin = System.currentTimeMillis();

		EventSenderCredentials cred = EventSenderCredentials.ANONYM;
		SOBOLEOServerConnection connection = new SOBOLEOServerConnection(SERVER_URL,cred);
		
		GetClientSideTaxonomy getTaxonomy = new GetClientSideTaxonomy(false);
		connection.send("default", getTaxonomy, Long.MAX_VALUE);
		assertNotNull(connection.send("default", getTaxonomy, Long.MAX_VALUE));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testSoboleoServerConnect(<500): "+duration);
		
	}
	
	@Test
	public void testXMLWebserviceDirectly() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();

		for (int i=0;i<20;i++) {
			URL url = new URL("http://localhost:8080/xmlService");
			URLConnection urlCon = url.openConnection();
			urlCon.setDoInput(true);
			urlCon.setDoOutput(true);
	
			GetClientSideTaxonomy getTaxonomy = new GetClientSideTaxonomy(false);
			XMLWebserviceRequest request= new XMLWebserviceRequest(getTaxonomy,"default",credentials);
			xstream.toXML(request, new OutputStreamWriter(urlCon.getOutputStream()));
			urlCon.getOutputStream().flush();
			urlCon.getOutputStream().close();
			
			XMLWebserviceResult result = (XMLWebserviceResult) xstream.fromXML(new InputStreamReader(urlCon.getInputStream()));
			assertTrue(result != null);		
		}
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testXMLWebserviceDirectly(<1000): "+duration);
	}
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
	
	
	
	
}
