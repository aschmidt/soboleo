/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.model.Resource;

import de.fzi.ipe.soboleo.data.tripleStore.TripleStore;
import de.fzi.ipe.soboleo.data.tripleStore.TripleStoreFactory;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.SesameTripleStoreFactory;
import de.fzi.ipe.soboleo.webdocument.clientSide.WebDocument;
import de.fzi.ipe.soboleo.webdocument.eventBusAdaptor.RDFWebDocument;
import de.fzi.ipe.soboleo.webdocument.eventBusAdaptor.RDFWebdocumentDatabase;

public class Test_RDFWebDocument extends Assert{

	static final File tempDirectory = new File("RDFDocument-testDirectory");

	private RDFWebdocumentDatabase documents; 
	private TripleStore tripleStore;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) throw new IOException("Temp Directory already exists!");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (tempDirectory.exists()) {
			for (File f:tempDirectory.listFiles()) {
				f.delete();
			}
			tempDirectory.delete();
		}
	}
	
	@Before
	public void setUp() throws Exception {
		TripleStoreFactory factory = new SesameTripleStoreFactory();
		tripleStore = factory.getTripleStore(tempDirectory,"space-test");
		documents = new RDFWebdocumentDatabase(tripleStore);
	}

	@After
	public void tearDown() throws Exception {
		for (RDFWebDocument d:documents.getAllDocuments()) documents.deleteDocument(d.getURI().toString());
		tripleStore.close();
	}

	@Test 
	public void testAddDeleteCache() throws IOException {
		long timeBegin = System.currentTimeMillis();
		RDFWebDocument document1 = documents.getDocumentByURL("http://www.spiegel.de", true);
		assertNotNull(document1);
		RDFWebDocument document2 = documents.getDocumentByURL("http://www.spiegel.de",true);
		assertTrue(document1 == document2);	
		assertTrue(document2.getURL().equals("http://www.spiegel.de"));
		assertNull(document2.getTitle());
		document2.setTitle("Titel");
		assertTrue(document1.getTitle().equals("Titel"));
		documents.deleteDocument(document1.getURI().toString());
		assertTrue(documents.getAllDocuments().size()==0);
		assertNull(documents.getDocument(document1.getURI().toString()));
		assertNull(documents.getDocumentByURL("http://www.spiegel.de", false));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration AddDeleteCache(<50): "+duration);	
	}
	
	@Test
	public void testAddRemoveTags() throws IOException {
		long timeBegin = System.currentTimeMillis();
		int statementCountEmpty = getStatementCount();
		
		RDFWebDocument doc = documents.getDocumentByURL("www.test.de",true);
		doc.addTag("http://someUser", "http://id1");
		doc.addTag("http://someUser", "http://id2");
		doc.addTag("http://someOtherUser","http://id2");
		assertTrue(doc.getTags().size()==3);

		int withoutTag = getStatementCount();
		doc.addTag("http://toRemoveUser","http://id2");
		assertTrue(getStatementCount()>withoutTag);
		WebDocument cDoc = doc.getClientSideDocument();
		String tagID = cDoc.getUserTags("http://toRemoveUser").iterator().next().getTagID();
		doc.removeTag(tagID);
		assertTrue(getStatementCount()==withoutTag);
		cDoc = doc.getClientSideDocument();
		assertTrue(cDoc.getUserTags("http://toRemoveUser").size()==0);
		
		documents.deleteDocument(cDoc.getURI());
		assertTrue("Delete document seems to leave stuff",getStatementCount()==statementCountEmpty); 			
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testAddTags(<50): "+duration);	
	}
	
	private int getStatementCount() throws IOException{
		return tripleStore.getStatementList((Resource)null, null, null).size();
	}
	
}
