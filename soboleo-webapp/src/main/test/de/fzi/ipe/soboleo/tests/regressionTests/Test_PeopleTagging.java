/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.tests.regressionTests;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.peopleTagging.clientSide.ManualPersonTag;
import de.fzi.ipe.soboleo.peopleTagging.clientSide.PersonTag;
import de.fzi.ipe.soboleo.peopleTagging.clientSide.TaggedPerson;
import de.fzi.ipe.soboleo.peopleTagging.commands.AddPersonTag;
import de.fzi.ipe.soboleo.peopleTagging.commands.AddTaggedPerson;
import de.fzi.ipe.soboleo.peopleTagging.commands.RemovePersonTag;
import de.fzi.ipe.soboleo.peopleTagging.queries.GetAllTaggedPersons;
import de.fzi.ipe.soboleo.peopleTagging.queries.GetPersonTags;
import de.fzi.ipe.soboleo.peopleTagging.queries.GetTaggedPersonByURL;
import de.fzi.ipe.soboleo.peopleTagging.queries.GetTaggedPersonForEmail;
import de.fzi.ipe.soboleo.peopleTagging.queries.SearchPersons;
import de.fzi.ipe.soboleo.peopleTagging.queries.SearchPersonsForConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceImpl;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;

public class Test_PeopleTagging extends Assert {

	static final File tempDirectory = new File("PeopleTagging-testDirectory");
	static final EventSenderCredentials cred = EventSenderCredentials.SERVER;
	
	private Space space; 

	
	@SuppressWarnings("unchecked")
	@Test
	public void testPeopleTaggingEvents() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		EventBus eb = space.getEventBus();
		
		assertNull(eb.executeQuery(new GetTaggedPersonByURL("http://www.news.bbc.co.uk/"), cred));
		assertNull(eb.executeQuery(new GetTaggedPersonForEmail("zach@fzi.de"), cred));
		
		TaggedPerson tp = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("zach@fzi.de","Valentin"), cred);
		assertNotNull(tp);
		TaggedPerson tp2 = (TaggedPerson) eb.executeQuery(new GetTaggedPersonForEmail("zach@fzi.de"), cred);
		assertEquals(tp2.getName(),tp.getName());
		assertTrue(tp2.getTags().size()==0);
		assertTrue(tp2.getUserTags("http://user").size() == 0);
		assertTrue(tp.getAggregatedTags().size()==0);
		
		eb.executeCommandNow(new AddPersonTag(tp2.getURI(),"http://www.spiegel.de/","http://concept","http://user"), cred);
		eb.executeCommandNow(new AddPersonTag(tp2.getURI(),"http://www.spiegel.de/","http://concept2","http://user2"), cred);
		eb.executeCommandNow(new AddPersonTag(tp2.getURI(),"http://www.news.bbc.co.uk/","http://concept2","http://user2"), cred);

		TaggedPerson tp3 = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("zach@fzi.de","Valentin"), cred);
		eb.executeCommandNow(new AddPersonTag(tp3.getURI(),"http://www.spiegel.de/","http://concept","http://user"), cred);

		TaggedPerson tp4= (TaggedPerson) eb.executeQuery(new GetTaggedPersonByURL("http://www.news.bbc.co.uk/"), cred);
		assertTrue(tp4.getTags().size()==3);
		assertTrue(tp4.getUserTags("http://user2").size()==2);
		assertTrue(tp4.getAggregatedTags().size()==2);

		ManualPersonTag tag = tp4.getUserTags("http://user").iterator().next();
		eb.executeCommandNow(new RemovePersonTag(tag.getTagID()), cred);
		tp4= (TaggedPerson) eb.executeQuery(new GetTaggedPersonByURL("http://www.news.bbc.co.uk/"), cred);
		assertFalse(tp4.getTags().size()==3);
		assertTrue(tp4.getUserTags("http://user2").size()==2);
		assertTrue(tp4.getUserTags("http://user").size()==0);
		assertTrue(tp4.getAggregatedTags().size()==1);
		
		SearchPersons futileQuery = new SearchPersons("Das ist das Haus vom Nikolaus!");
		List<TaggedPerson> matchingPersons = (List<TaggedPerson>) eb.executeQuery(futileQuery, cred);
		assertTrue("There really should'nt be a result", matchingPersons.size()==0);
		
		ArrayList<TaggedPerson> allPersons = (ArrayList<TaggedPerson>) eb.executeQuery(new GetAllTaggedPersons(), cred);
		assertTrue(allPersons.size() == 2);
 		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testPeopleTaggingEvents(<100): "+duration);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testPeopleSearch() throws IOException, EventPermissionDeniedException {
		long timeBegin = System.currentTimeMillis();
		EventBus eb = space.getEventBus();
		
		//create a taxonomy. 
		//Concept1 with children Concept1.1 and Concept 1.2 
		//Concept2 with Child C2.1
		//Concept3
		String c1 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept1")), cred);
		String c11 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept1.1")), cred);
		String c12 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept1.2")), cred);
		String c2 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept2")), cred);
		String c21 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept2.1")), cred);
		String c3 = (String) eb.executeCommandNow(new CreateConceptCmd(new LocalizedString("Concept3")), cred);
		eb.executeCommand(new AddConnectionCmd(c1,SKOS.HAS_NARROWER,c11), cred);
		eb.executeCommand(new AddConnectionCmd(c1,SKOS.HAS_NARROWER,c12), cred);
		eb.executeCommand(new AddConnectionCmd(c2,SKOS.HAS_NARROWER,c21), cred);

		//Four users, search for concept1/c1 - expected result: user3, user2, user1 (user3 is best match)
		//user1 tagged with C1.1
		//user2 tagged with C1.1 and C1.2
		//user3 tagged *twice* with C1.1 and C1.2
		//user4 tagged with c3
		TaggedPerson user1 = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("email1","user1"),cred);
		TaggedPerson user2 = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("email2","user2"),cred);
		TaggedPerson user3 = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("email3","user3"),cred);
		TaggedPerson user4 = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("email4","user4"),cred);
		eb.executeCommandNow(new AddPersonTag(user1.getURI(),"url",c11,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user2.getURI(),"url",c11,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user2.getURI(),"url",c12,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user3.getURI(),"url",c11,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user3.getURI(),"url",c12,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user3.getURI(),"url",c11,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user3.getURI(),"url",c12,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user4.getURI(),"url",c3,"http://someUser.de"), cred);
		
		List<TaggedPerson> taggedPersons = (List<TaggedPerson>) eb.executeQuery(new SearchPersons("Concept1"), cred);
		assertTrue(taggedPersons.size()==3);
		assertTrue(taggedPersons.get(0).getName().equals("user3"));
		assertTrue(taggedPersons.get(1).getName().equals("user2"));
		assertTrue(taggedPersons.get(2).getName().equals("user1"));

		//Two more users, search for concept21/C2.1, expected user6, user5 (user6 is best match)
		//user5 tagged with C2
		//user6 tagged with C2.1
		TaggedPerson user5 = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("email5","user5"),cred);
		TaggedPerson user6 = (TaggedPerson) eb.executeCommandNow(new AddTaggedPerson("email6","user6"),cred);
		eb.executeCommandNow(new AddPersonTag(user5.getURI(),"url",c2,"http://someUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user6.getURI(),"url",c21,"http://someUser.de"), cred);

		taggedPersons = (List<TaggedPerson>) eb.executeQuery(new SearchPersons("Concept2.1"), cred);
		assertTrue(taggedPersons.size()==1);
		assertTrue(taggedPersons.get(0).getName().equals("user6"));
//		assertTrue(taggedPersons.get(1).getName().equals("user5")); - changed heuristic
		
		//Search for Concept2.1 and Concept1.1 - first should be user3 (because he's tagged twice with Concept1.1
		taggedPersons = (List<TaggedPerson>) eb.executeQuery(new SearchPersons("Concept2.1 Concept1.1"), cred);
		assertTrue(taggedPersons.get(0).getName().equals("user3"));
		
		//Search persons who are annotated with a specific concept C1 - none is tagged with C1 but with subconcepts
		taggedPersons = (List<TaggedPerson>) eb.executeQuery(new SearchPersonsForConcept(c1), cred);
		assertTrue(taggedPersons.get(0).getName().equals("user3"));
		
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration testPeopleSearch(<100): "+duration);
		
		eb.executeCommandNow(new AddPersonTag(user3.getURI(),"url",c3,"http://anotherUser.de"), cred);
		eb.executeCommandNow(new AddPersonTag(user3.getURI(),"url",c11,"http://anotherUser.de"), cred);
		Set<PersonTag> tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, null, null), cred);
		assertTrue(tags.size()==12);
		
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(user3.getURI(), null, null), cred);
		assertTrue(tags.size()==6);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags("http://noResourceURI.de", null, null), cred);
		assertTrue(tags.size()==0);
		
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, c11, null), cred);
		assertTrue(tags.size()==5);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, "http://noConceptURI", null), cred);
		assertTrue(tags.size()==0);
		
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, null, "http://anotherUser.de"), cred);
		assertTrue(tags.size()==2);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, null, "http://noMakerURI.de"), cred);
		assertTrue(tags.size()==0);
		
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(user3.getURI(), c11, null), cred);
		assertTrue(tags.size()==3);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(user3.getURI(), "http://noConceptURI", null), cred);
		assertTrue(tags.size()==0);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags("http://noResourceURI.de", c11, null), cred);
		assertTrue(tags.size()==0);
		
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(user3.getURI(), null, "http://someUser.de"), cred);
		assertTrue(tags.size()==4);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(user3.getURI(), null, "http://noMakerURI.de"), cred);
		assertTrue(tags.size()==0);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags("http://noResourceURI.de", null, "http://someUser.de"), cred);
		assertTrue(tags.size()==0);
		
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, c11, "http://someUser.de"), cred);
		//TODO dismisses same assignments
		assertTrue(tags.size()==3);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, c1, "http://someUser.de"), cred);
		assertTrue(tags.size()==0);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, "http://noConceptURI", "http://someUser.de"), cred);
		assertTrue(tags.size()==0);
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(null, c11, "http://noMakerURI.de"), cred);
		assertTrue(tags.size()==0);
		
		tags = (Set<PersonTag>) eb.executeQuery(new GetPersonTags(user3.getURI(), c11, "http://someUser.de"), cred);
		//TODO dismisses same assignments
		assertTrue(tags.size()==1);
		
		duration = System.currentTimeMillis() - duration;
		System.out.println("Duration testGetTags(<100): "+duration);
		
	}
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		if (tempDirectory.exists()) {
			throw new IOException("Temp Directory already exists!");
		}
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		;
	}
	
	//carefull with this method .. it can really clean a harddrive in no time ;)
	private static void deleteRecursive(File file) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) deleteRecursive(f);
		}
		file.delete();
	}
	
	@Before
	public void setUp() throws Exception {
		try {
			space = new SpaceImpl(tempDirectory,"testSpace",null);
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@After
	public void tearDown() throws Exception {
		space.close();
		try {
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
}
