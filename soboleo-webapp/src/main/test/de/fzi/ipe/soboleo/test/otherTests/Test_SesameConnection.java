package de.fzi.ipe.soboleo.test.otherTests;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.openrdf.OpenRDFException;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;


public class Test_SesameConnection {
		public Repository tripleStore;
		private RepositoryConnection con;
		
		
		public Test_SesameConnection()
		{
			try
			{
				this.tripleStore = new HTTPRepository("http://octopus35.perimeter.fzi.de:8080/openrdf-sesame", "test");
				this.tripleStore.initialize();
			}
			catch(RepositoryException re)
			{
				re.printStackTrace();
			}
		}
		
		public void createAndUploadRDFGraph()
		{
			
			try {
				con = this.tripleStore.getConnection();
				ValueFactory vf = this.tripleStore.getValueFactory();
				
				BufferedReader br = new BufferedReader(new FileReader("C:/webPortal/contexts.txt"));
				String line = br.readLine();
				while (line != null) {
					System.out.println(line);
					String[] parts = line.split(",");
					String uri = parts[0];
					String title = parts[1];
				
				URI subject = vf.createURI(parts[0]);
				URI predicate = vf.createURI(parts[1]);
				URI object = vf.createURI(parts[2]);
				URI number = vf.createURI(parts[3]);
				
				con.remove(subject,predicate,object);
				con.add(subject,predicate,object,number);
				line = br.readLine();
				}
			} catch(OpenRDFException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
				
		}
		
		public static void main(String[] args)
		{
			Test_SesameConnection trr = new Test_SesameConnection();
//			trr.createAndUploadRDFGraph();
			
		}

		
	}
