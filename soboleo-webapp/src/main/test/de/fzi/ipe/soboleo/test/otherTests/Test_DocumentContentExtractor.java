package de.fzi.ipe.soboleo.test.otherTests;


import static org.junit.Assert.*;

import java.net.URL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.webdocument.indexer.DocumentContentExtractor;

public class Test_DocumentContentExtractor {

	@Test
	public void contentExtractionTest() throws Exception {
		long time = System.currentTimeMillis();
		
		URL url = new URL("http://download.microsoft.com/download/f/3/4/f34dbbd3-c023-4e84-b37e-8564a2febc5e/How%20to%20write%20a%20cover%20letter_Tutorial%20Text.docx?10") ;
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.fzi.de/downloads/sim/seminar/vorlagen/Vorlage_Powerpoint.pptx") ;
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("https://svn.origo.ethz.ch/csel2/consortium1/Implementation/Components/A/docs/SRS_EvenList.docx") ;
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.physics.ox.ac.uk/research/graduate/C%20++%20course%20content.doc") ;
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.airs.com/ian/cxx-slides.pdf");
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.jobpunt.be/fube_ondersteuner_personeelsadministratie_BZ.doc");
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.ifc.org/ifcext/sme.nsf/AttachmentsByTitle/Entrepreneurship+Database/$FILE/Entrepreneurship+Data+101006.xls");
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.owd.alabama.gov/Budget%20%20Budget%20Amendment%20Jul28.xlsx") ;
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://bcclassof1988.wikispaces.com/space/showimage/classreunionaddresses.xlsx");
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.heintze.com/resume/resumeG.docx");
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		url = new URL("http://www.martination.com/resume/resume.docx");
		assertFalse(DocumentContentExtractor.getDocumentContent(url).isEmpty());
		
		long duration = System.currentTimeMillis() - time;
		System.out.println("Duration contentExtractionTest (<20000): "+duration);
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

}
