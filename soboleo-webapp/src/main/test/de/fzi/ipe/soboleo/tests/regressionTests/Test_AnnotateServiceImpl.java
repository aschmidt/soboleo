package de.fzi.ipe.soboleo.tests.regressionTests;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.annotate.service.AnnotateServiceImpl;
import de.fzi.ipe.soboleo.server.ServerInitializer;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString.Language;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.Spaces;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;
import de.fzi.ipe.soboleo.webdocument.clientSide.WebDocument;
import de.fzi.ipe.soboleo.webdocument.lucene.LuceneResult;
import de.fzi.ipe.soboleo.webdocument.lucene.SearchingLuceneWrapper;
import de.fzi.ipe.soboleo.webdocument.lucene.queries.GetAllDocuments;
import de.fzi.ipe.soboleo.webdocument.lucene.queries.GetDocumentsForConcept;
import de.fzi.ipe.soboleo.webdocument.queries.GetDocumentByURLQuery;

public class Test_AnnotateServiceImpl extends Assert {


	static final File tempDirectory = new File("AnnotateServiceImpl-testDirectory");
	private Spaces spaces;
	private Space currentSpace;
	
	@Test
	public void TestAnnotateServiceImpl () throws IOException, EventPermissionDeniedException{
		long timeBegin = System.currentTimeMillis();
		
		EventSenderCredentials cred = EventSenderCredentials.SERVER;
		EventBus eb = currentSpace.getEventBus();
		ClientSideTaxonomy  clientSideTaxonomy = currentSpace.getClientSideTaxonomy();
		SearchingLuceneWrapper lucene = currentSpace.getSearchingLuceneWrapper();
		
		AnnotateServiceImpl annoService = new AnnotateServiceImpl();
		
		assertNull(eb.executeQuery(new GetDocumentByURLQuery("http://www.google.de"), cred));
		//check lucene index
		LuceneResult result = lucene.getAllDocuments();
		assertTrue(result.length() == 0);
		
		WebDocument doc = annoService.createWebDocument("testspace", cred, "http://www.google.de", "http://www.google.de");
		assertNotNull(doc); 
		assertTrue(doc.getAllTags().isEmpty()); 
		
		doc = (WebDocument) eb.executeQuery(new GetDocumentByURLQuery("http://www.google.de"), cred);
		assertNotNull(doc);
		assertTrue(doc.getAllTags().isEmpty());
		assertFalse(doc.getTitle().equals("Google"));
		//check lucene index
		result = lucene.getAllDocuments();
		assertTrue(result.length() == 1);
		assertTrue(result.doc(0).getURI().equals(doc.getURI()));
		assertTrue(result.doc(0).getURL().equals("http://www.google.de"));
		assertTrue(result.doc(0).getTitle().equals("http://www.google.de"));
		assertTrue(result.doc(0).getConceptURIs().isEmpty());

		Map<String, String> conceptNames = new HashMap<String,String>();
		conceptNames.put("concept1", "");
		annoService.saveAnnotationData("testspace", cred, Language.en, doc, conceptNames, "Google");
		
		doc = (WebDocument) eb.executeQuery(new GetDocumentByURLQuery("http://www.google.de"), cred);		
		ClientSideConcept c1 = clientSideTaxonomy.getConceptForPrefLabel(new LocalizedString("concept1", Language.en));
		assertTrue(doc.getTitle().equals("Google"));
		assertTrue(doc.getAllTags().size() == 1);
		assertTrue(doc.getTagConceptIDs().contains(c1.getURI()));
		//check lucene index
		result = lucene.getAllDocuments();
		assertTrue(result.doc(0).getURI().equals(doc.getURI()));
		assertTrue(result.doc(0).getURL().equals("http://www.google.de"));
		assertTrue(result.doc(0).getTitle().equals("Google"));
		assertTrue(result.doc(0).getConceptURIs().size() == 1);
		assertTrue(result.doc(0).getConceptURIs().contains(c1.getURI()));
		conceptNames.put("concept1", c1.getURI());
		
		doc = annoService.createWebDocument("testspace", cred, "http://www.google.de", "http://www.google.de");
		assertTrue(!doc.getAllTags().isEmpty()); 
		conceptNames.put("concept2", "");
		annoService.saveAnnotationData("testspace", cred, Language.en, doc, conceptNames, "Google");
		doc = (WebDocument) eb.executeQuery(new GetDocumentByURLQuery("http://www.google.de"), cred);
		ClientSideConcept c2 = clientSideTaxonomy.getConceptForPrefLabel(new LocalizedString("concept2", Language.en));
		assertTrue(doc.getAllTags().size() == 2);
		assertTrue(doc.getTagConceptIDs().contains(c1.getURI()));
		assertTrue(doc.getTagConceptIDs().contains(c2.getURI()));
		//check lucene index
		result = lucene.getAllDocuments();
		assertTrue(result.doc(0).getURI().equals(doc.getURI()));
		assertTrue(result.doc(0).getURL().equals("http://www.google.de"));
		assertTrue(result.doc(0).getConceptURIs().size() == 2);
		assertTrue(result.doc(0).getConceptURIs().contains(c1.getURI()));
		assertTrue(result.doc(0).getConceptURIs().contains(c2.getURI()));
		conceptNames.put("concept2", c2.getURI());
		
		conceptNames.remove("concept1");
		annoService.saveAnnotationData("testspace", cred, Language.en, doc, conceptNames, "Google");
		doc = (WebDocument) eb.executeQuery(new GetDocumentByURLQuery("http://www.google.de"), cred);
		assertTrue(doc.getAllTags().size() == 1);
		assertFalse(doc.getTagConceptIDs().contains(c1.getURI()));
		assertTrue(doc.getTagConceptIDs().contains(c2.getURI()));
		//check lucene index
		result = lucene.getAllDocuments();
		assertTrue(result.doc(0).getURI().equals(doc.getURI()));
		assertTrue(result.doc(0).getURL().equals("http://www.google.de"));
		assertTrue(result.doc(0).getConceptURIs().size() == 1);
		assertFalse(result.doc(0).getConceptURIs().contains(c1.getURI()));
		assertTrue(result.doc(0).getConceptURIs().contains(c2.getURI()));
		
		doc = annoService.createWebDocument("testspace", cred, "www.google.de", "http://www.google.de");
		WebDocument doc2 = (WebDocument) eb.executeQuery(new GetDocumentByURLQuery("http://www.google.de"), cred);
		assertTrue(doc.getURI().equals(doc2.getURI()));
		
		annoService.removeFromIndex("testspace", cred, "http://www.google.de");
		doc = (WebDocument) eb.executeQuery(new GetDocumentByURLQuery("http://www.google.de"), cred);
		assertNull(doc);
		result = lucene.getAllDocuments();
		assertTrue(result.length() == 0);
		result = (LuceneResult) eb.executeQuery(new GetAllDocuments(LuceneResult.ResultType.WEB_DOCUMENT), cred);
		assertTrue(result.length() == 0);
				
		long duration = System.currentTimeMillis() - timeBegin;
		System.out.println("Duration TestAnnotateServiceImpl(<1000): "+duration);	
	}
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//check if temp directory already exist - fail if it does becomes we want to be 
		//able to simply delete the directory after we're done
		deleteRecursive(tempDirectory);
		if (tempDirectory.exists()) throw new IOException("Temp Directory already exists!");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		;
	}

	@Before
	public void setUp() throws Exception {
		ServerInitializer.getInstance().setServer(tempDirectory+"/server/", tempDirectory+"/spaces/");
		spaces = ServerInitializer.getInstance().getServer().getSpaces();
		spaces.createNewSpace("testspace");
		spaces.createNewSpace("default");
		currentSpace = spaces.getSpace("testspace");
	}

	//carefull with this method .. it can really clean a harddrive in no time ;)
	private static void deleteRecursive(File file) {
		if (file.isDirectory()) {
			for (File f: file.listFiles()) deleteRecursive(f);
		}
		file.delete();
	}
	
	@After
	public void tearDown() throws Exception {
		for(String spaceName : spaces.getSpaces()){
			spaces.getSpace(spaceName).close();
		}
		try {
			if (tempDirectory.exists()) {
				deleteRecursive(tempDirectory);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
