/*
 * FZI - Information Process Engineering 
 * Helper class for testing - a thread that generates random 
 * changes to the taxonomy (well, not really random .. but well).
 */
package de.fzi.ipe.soboleo.tests.regressionTests.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddConnectionCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.AddTextCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.CreateConceptCmd;
import de.fzi.ipe.soboleo.skosTaxonomy.commands.RemoveConceptCmd;
import de.fzi.ipe.soboleo.space.eventBus.EventBus;
import de.fzi.ipe.soboleo.space.events.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;

public class RandomChangeGenerator extends Thread{
	
	private Random r;
	private Set<String> myUris = new HashSet<String>(); 
	private EventBus eb;
	private EventSenderCredentials cred = EventSenderCredentials.SERVER;
	private int repetitions; //how many changes are made?
	
	public RandomChangeGenerator(long randSeed, EventBus eb, int repetitions) {
		r = new Random(randSeed);
		this.eb = eb;
		this.repetitions = repetitions;
	}
	
	public void run() {
		for (int i=0;i<20;i++) createConcept();
		while ((repetitions--) > 0) {
			try {
				switch (r.nextInt(4)) {
				case 0: createConcept(); break;
				case 1: createAttribute(); break;
				case 2: createRelation(); break;
				case 3: deleteConcept(); break;
				}
			}catch (EventPermissionDeniedException epde) {
				; //I'm not doing any checks on my changes so some are 
				// just bound to be bs and cause eventpermission denied exceptions
			}
		}
	}

	private void createAttribute() throws EventPermissionDeniedException {
		String c = getRandomURI();
		LocalizedString t = getRandomText();
		SKOS a = getRandomAttribute();
		eb.executeCommandNow(new AddTextCmd(c,a,t), cred);
	}
	
	private void createRelation() throws EventPermissionDeniedException {
		String c1 = getRandomURI();
		String c2 = getRandomURI();
		SKOS r = getRandomRelation();
		eb.executeCommandNow(new AddConnectionCmd(c1,r,c2), cred);
	}
	
	private void deleteConcept() throws EventPermissionDeniedException {
		String uri = getRandomURI();
		myUris.remove(uri);
		eb.executeCommandNow(new RemoveConceptCmd(uri), cred);
	}
	
	private void createConcept() {
		String uri;
		try {
			uri = (String) eb.executeCommandNow(new CreateConceptCmd(getRandomText()), cred);
			myUris.add(uri);
		} catch (EventPermissionDeniedException e) {
			e.printStackTrace(); //  really should not happen
		}
	}
	
	private LocalizedString getRandomText() {
		return new LocalizedString(""+r.nextInt());
	}
	
	private SKOS getRandomAttribute() {
		switch(r.nextInt(4)) {
			case 0: return SKOS.PREF_LABEL;
			case 1: return SKOS.ALT_LABEL;
			case 2: return SKOS.HIDDEN_LABEL;
			case 3: return SKOS.NOTE;
		}
		return null;
	}
	
	private SKOS getRandomRelation() {
		switch(r.nextInt(3)) {
			case 0: return SKOS.HAS_BROADER;
			case 1: return SKOS.HAS_NARROWER;
			case 2: return SKOS.RELATED;
		}
		return null;
	}
	
	private String getRandomURI() {
		int i = r.nextInt(myUris.size());
		for (String s:myUris) {
			if (i==0) return s;
			else i--;
		}
		return null;
	}
	
	
	

}
