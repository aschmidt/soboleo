package de.fzi.ipe.soboleo.test.otherTests;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceException;
import de.fzi.ipe.soboleo.server.xmlWebservice.client.SOBOLEORemoteSpace;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString;
import de.fzi.ipe.soboleo.skosTaxonomy.base.SKOS;
import de.fzi.ipe.soboleo.skosTaxonomy.base.LocalizedString.Language;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideConcept;
import de.fzi.ipe.soboleo.skosTaxonomy.clientSide.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.events.EventSenderCredentials;
import de.fzi.ipe.soboleo.webdocument.IndexDocument;
import de.fzi.ipe.soboleo.webdocument.clientSide.WebDocument;
import de.fzi.ipe.soboleo.webdocument.lucene.LuceneResult;
import de.fzi.ipe.soboleo.webdocument.lucene.queries.GetAllDocuments;
import de.fzi.ipe.soboleo.webdocument.queries.GetWebDocument;

/**
 * Tests the XML Webservice - note that this test requires a running tomcat. 
 */
public class XMLWebservice_Example extends Assert{

	private EventSenderCredentials credentials = new EventSenderCredentials("http://soboleo.com/ns/1.0#users-db-gen1","Anonymous", "N4W5I4N334M6X45C6K1H2B6147233H7E3H6H79");
	static final String SERVER_URL = "http://tool.soboleo.com/xmlService";
//	private EventSenderCredentials credentials = new EventSenderCredentials("http://soboleo.com/ns/1.0#users-db-gen1","Anonymous", "2A34D3V721LI7069L5F5F4X41552S274C2529 ");
//	static final String SERVER_URL = "http://localhost:8080/xmlService";
	
	@Test
	public void testSoboleoRemoteSpace() throws XMLWebserviceException {

		SOBOLEORemoteSpace remoteSpace = new SOBOLEORemoteSpace(SERVER_URL,"default",1,credentials);
		ClientSideTaxonomy tax = remoteSpace.getTaxonomy();
		
//		//get root concepts
//		Set<ClientSideConcept> roots = tax.getRootConcepts();
//		
//		//get prototypical concepts which contain the concepts to position
//		ClientSideConcept proto = tax.getConceptForLabel(new LocalizedString("latest topics", Language.en)); 
//		
//		//get URIs of concepts to position (or the narrower concepts of "prototypical concepts")
//		Set<String> conceptURIsToPosition = proto.getConnected(SKOS.HAS_NARROWER);
//		// or as ClientSideConcept
//		Set<ClientSideConcept> conceptsToPosition = proto.getConnectedAsConcepts(SKOS.HAS_NARROWER, tax);
//		
//		//get all preferred labels of a concept
//		Set<LocalizedString> prefLabels = proto.getTexts(SKOS.PREF_LABEL);
//		//get English preferred label of a concept
//		LocalizedString englishPrefLabel = proto.getText(SKOS.PREF_LABEL, Language.en);		
//		//get all alternative labels of a concept
//		Set<LocalizedString> altLabels = proto.getTexts(SKOS.ALT_LABEL);
//		//get all English alternative labels of a concept
//		Set<LocalizedString> englishAltLabels = proto.getTexts(SKOS.ALT_LABEL, Language.en);
//		
		//get all annotated document (for your case a little bit complicated)
		GetAllDocuments query =  new GetAllDocuments(LuceneResult.ResultType.WEB_DOCUMENT);

		LuceneResult result = (LuceneResult) remoteSpace.sendEvent(query); 
		Iterator i =  result.iterator();
		Set<WebDocument> webDocs = new HashSet<WebDocument>();
		while(i.hasNext()){
			IndexDocument current = (IndexDocument) i.next();
			WebDocument doc = (WebDocument) remoteSpace.sendEvent(new GetWebDocument(current.getURI()));
			webDocs.add(doc);
		}
		remoteSpace.close();
	}
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


}
