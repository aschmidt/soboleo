package de.fzi.ipe.soboleo.test.otherTests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class Test_WidgetConnectorManager {

	public static void main(String[] args) {
		String test = "www.bla";
		if(!(test.startsWith("http:") || test.startsWith("https:") ||  test.startsWith("file:") ||  test.startsWith("ftp:") ||  test.startsWith("sftp:"))) test = "http://" +test;
		System.out.println(test);
		try {
			URL url = new URL(test);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
		
//		try {
//			WidgetConnector manager = new WidgetConnector();
//			System.out.println(manager.sendMessage(readFileToString(new File("file3.txt"))));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (WidgetServerConnectorException e){
//			e.printStackTrace();
//		}
	}
	
	
	public static String readFileToString(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuilder builder = new StringBuilder();
		String temp = reader.readLine();
		while (temp != null) {
			builder.append(temp);
			temp = reader.readLine();
		}
		reader.close();
		return builder.toString();
	}

	
}
