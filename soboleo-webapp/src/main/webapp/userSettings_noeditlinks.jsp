<html>
<%@include file="header_head.prt" %>

<%@ page import=
  "de.fzi.ipe.soboleo.server.*,
  de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language,
  java.util.Set,
  de.fzi.ipe.soboleo.server.user.UserInput"  
%>
<link rel='stylesheet' href='${pageContext.request.contextPath}/style/soboleo-form.css'>
<% 
  Server server =  ServerInitializer.getInstance().getServer(); 
  UserDatabase userDatabase = server.getUserDatabase();
  UserInput userInput = new UserInput();
  String validation = null;
  
  String uri = request.getParameter("uri");
  if (uri != null) {
     userInput.load(userDatabase.getUser(uri),server);
  }
  if (request.getParameter("save")!=null) {
     userInput.load(request);
     validation = userInput.validate(userDatabase);
     if (validation == null) {
        userInput.save(userDatabase);
        RequestDispatcher rd = request.getRequestDispatcher("@@rel/");
        rd.forward(request,response);
     }
  }
%>

<title>SOBOLEO User Settings %></title>
<!-- <title><%=session.getAttribute("user.title") %></title> -->

<%@include file="header_body.prt" %>

<h1><%=session.getAttribute("user.settings") %> </h1>
<!--<h1>Settings</h1>-->

<% if (validation != null) { %>
  <span class="warning"><%= validation %></span>
<%}%>

<table><td>
<form action="userSettings.jsp" method="post">
<table class="form"> 
<tr>
<td class="label"><%=session.getAttribute("user.email") %></td>
<!--  <td class="label">Email</td>-->
  <td>
    <input name="email" type="text" size="50" value="<%=userInput.getEmail()%>">
  </td>
</tr>

<tr>
<td class="label"><%=session.getAttribute("user.userName") %></td>
<!--  <td class="label">Username</td>-->
  <td>
    <input name="name" type="text" size="50" value="<%=userInput.getName()%>">
  </td>
</tr>

<tr>
<td class="label"><%=session.getAttribute("user.link") %></td>
<!--  <td class="label">Link</td>-->
  <td>
    <input name="link" type="text" size="50"  value="<%=userInput.getLink()%>">
  </td>
</tr>

<tr><td class="label"/><td/></tr>

<tr>
<td class="label"><%=session.getAttribute("user.pwd") %></td>
<!--  <td class="label">Password</td>-->
  <td>
    <input name="password1" type="password" size="50" maxlength="30">
  </td>
</tr>

<tr>
<td class="label"><%=session.getAttribute("user.pwd2") %></td>
<!--  <td class="label">Password - Repeat</td>-->
  <td>
    <input name="password2" type="password" size="50" maxlength="30">
  </td>
</tr>




<tr><td class="label"/><td/></tr>


<tr>
  <td class="label">URI</td>
  <td>
    <% if (userInput.getUri() != null)  { %>
      <%= userInput.getUri() %>
      <input name="uri" type="hidden" value="<%= userInput.getUri() %>">
    <%} else { %>
      not yet defined
    <% } %>
  </td>
</tr>

<tr>
<td class="label"><%=session.getAttribute("user.key") %></td>
<!--  <td class="label">Key</td>-->
  <td>
    <% if (userInput.getKey() != null)  { %>
      <%= userInput.getKey() %>
    <%} else { %>
      not yet defined
    <%}%>
  </td>
</tr>

<tr><td class="label"/><td/></tr>

<tr>
<td class="label"><%=session.getAttribute("user.setDefSpace") %></td>
  <td>
    <% 
    	String spaceID = userInput.getDefaultSpace();
	    for (String currentSpaceID:userInput.getSpaceIdentifier()) { 
	    	if (currentSpaceID.equals(spaceID)) {
	    		out.println("<input type='radio' name='defaultSpace' value='"+currentSpaceID+"' checked> "+currentSpaceID+"</input> <br/> ");
	    	}
	    	else {
	    		out.println("<input type='radio' name='defaultSpace' value='"+currentSpaceID+"'> "+currentSpaceID+ "</input> <br/> ");
	    	}
	    }
    %>
  </td>
</tr>

<tr><td class="label"/><td/></tr>

<tr>
<td class="label"><%=session.getAttribute("user.setDefLang") %></td>
<!--  <td class="label">Set Default Language</td>-->
  <td>
    <% 
    	String currentDefaultLanguage = userInput.getDefaultLanguage();
	    for (Language currentLanguage:Language.values()) { 
	    	String currentLanguageString = currentLanguage.getAcronym();
	    	if (currentDefaultLanguage.equals(currentLanguageString )) {
	    		out.println("<input type='radio' name='defaultLanguage' value='"+currentLanguageString+"' checked> "+currentLanguage.getName()+"</input><br/>");
	    	}
	    	else {
	    		out.println("<input type='radio' name='defaultLanguage' value='"+currentLanguageString+"'> "+currentLanguage.getName()+"</input><br/>");
	    	}
	    }
    %>
  </td>
</tr>

<tr><td class="label"/><td/></tr>

<tr>
  <td class="label"/>
  <td><input type="submit" name="save" value=<%=session.getAttribute("user.submit") %> ></td>
<!--  <td><input type="submit" name="save" value=" Submit "></td>-->
</tr>
</table>
</form>
</td>
<td>

</td>
</table>

<%@include file="/footer.prt"%>
