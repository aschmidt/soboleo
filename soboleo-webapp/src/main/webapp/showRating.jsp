<jsp:root xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page" version="2.0"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:my="urn:jsptagdir:/WEB-INF/tags"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions">

	<jsp:directive.page language="java"
		contentType="text/html; charset=utf-8" pageEncoding="UTF-8"
		isELIgnored="false" 
		/>
	<jsp:output doctype-root-element="html"
		doctype-public="-//W3C//DTD XHTML 1.1//EN"
		doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" />

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel='stylesheet' href='${pageContext.request.contextPath}/style/browse.css' />
		<link rel='stylesheet' href='${pageContext.request.contextPath}/style/soboleo-base.css' />
	</head>
	
	<body>

	<div class="ratingArea">
		<div style="width:68px;float:left;font-size:10;">
			<c:forEach var="x" begin="0" end="${ratingvalue-1}">
				<img src="/img/rating/active2.jpg" style="border-width:0px;" />
			</c:forEach>
			<c:forEach var="x" begin="${ratingvalue}" end="5">
				<img src="/img/rating/inactive.jpg" style="border-width:0px;" />
			</c:forEach>
		</div>
		<div style="width:42px;float:left;font-size:10;">
			(${sumrating} votes)
		</div>
	</div>

	</body>
</html>

