package de.fzi.ipe.soboleo.annotateDocuments.client;


import com.google.gwt.core.client.EntryPoint;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;


public class AnnotatePanel implements EntryPoint {

    public void onModuleLoad() {

		String uri = Window.Location.getParameter("furi");
		
		if (uri != null)
		{    	
	    	AnnotateMain am=new AnnotateMain(null,uri);    	
	    	RootPanel.get("annotate").add(am);	
	    	
	    	focusWindow();
		}
		else
		{
	
			//show file upload if empty	
			FileUploadPanel fup=new FileUploadPanel();
			RootPanel.get("annotate").add(fup);	
	    	focusWindow();
		}
		
	}

    private static native void focusWindow() /*-{
    	return $wnd.focus();
	}-*/;

}
