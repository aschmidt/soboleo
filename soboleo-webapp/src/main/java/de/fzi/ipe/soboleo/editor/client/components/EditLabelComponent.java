package de.fzi.ipe.soboleo.editor.client.components;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.HasDoubleClickHandlers;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.VerticalPanel;

public class EditLabelComponent extends Composite implements DoubleClickHandler, KeyUpHandler, BlurHandler{
	
	private TextBoxBase inputText = null;
	private DoubleClickHTML labelHTML = new DoubleClickHTML();
	
	public EditLabelComponent(String label, boolean isTextArea){
		VerticalPanel panel = new VerticalPanel();
		labelHTML.setText(label);
		labelHTML.addDoubleClickHandler(this);
		if(isTextArea) inputText = new TextArea();
		else inputText = new TextBox();
		inputText.setText(label);
		inputText.addKeyUpHandler(this);
		inputText.addBlurHandler(this);
		panel.add(inputText);
		panel.add(labelHTML);
		inputText.setVisible(false);
		initWidget(panel);
		setStyleName("editLabelComponent"); 
	}

	public void startEditing(){
		labelHTML.setVisible(false);
		inputText.setVisible(true);
		inputText.setFocus(true);
	}
	

	public String getText(){
		return inputText.getText();
	}
	
	public void addKeyUpHandler(KeyUpHandler handler){
		inputText.addKeyUpHandler(handler);
	}
	
	@Override
	public void onDoubleClick(DoubleClickEvent event) {
		if(event.getSource().equals(labelHTML)){
			labelHTML.setVisible(false);
			inputText.setText(labelHTML.getText());
			inputText.setVisible(true);
			inputText.setFocus(true);
		}
	}

	@Override
	public void onKeyUp(KeyUpEvent event) {
			if(event.getNativeKeyCode() == KeyCodes.KEY_ESCAPE) {
				inputText.setVisible(false);
				labelHTML.setVisible(true);
			}
			else if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER){
//				labelHTML.setText(inputText.getText());
			}
	}
	
	@Override
	public void onBlur(BlurEvent event) {
		if(event.getSource().equals(inputText)){
			//TODO ask user if to cancel
			inputText.setVisible(false);
			labelHTML.setVisible(true);
		}
	}
}
