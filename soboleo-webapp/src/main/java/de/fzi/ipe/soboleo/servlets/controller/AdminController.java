package de.fzi.ipe.soboleo.servlets.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.server.Server;

@Controller
public class AdminController
{
	@Autowired Server server;
	
	@RequestMapping("/admin")
	public ModelAndView adminMain(ModelMap m)
	{
		m.put("server",server);
		return new ModelAndView("admin/index",m);
	}
}
