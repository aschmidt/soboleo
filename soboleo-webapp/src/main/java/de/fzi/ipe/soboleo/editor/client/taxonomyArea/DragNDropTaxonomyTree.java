/*
 * FZI - Information Process Engineering 
 * Created on 20.01.2007 by zach
 */
package de.fzi.ipe.soboleo.editor.client.taxonomyArea;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;


import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventPreview;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MouseListenerAdapter;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;

import de.fzi.ipe.soboleo.editor.client.EventDistributor;

public class DragNDropTaxonomyTree extends TaxonomyTree {

	public static final int MODIFIER_CTRL = 2;
	public static final int MODIFIER_SHIFT = 3;
	public static final int MODIFIER_NONE = -1;
	
	private Set<DragNDropListener> listeners = new HashSet<DragNDropListener>();
	

	private ConceptTreeItem selectedTreeItem = null;
	private Event event = null;
	private boolean clicked = false;
	private boolean dragged = false;
	private int modifier = MODIFIER_NONE;
	
	private HTML floater;
	private String baseHTML;
	private int startDragX;
	private int startDragY;
	private int dragThreshold = 20;
	
	
	public DragNDropTaxonomyTree(EventDistributor eventDistrib) {
		super(eventDistrib);
		initialize();
//		boundaryPanel = absolute;
	}
	
	public void addDragNDropListener(DragNDropListener listener) {
		listeners.add(listener);
	}

	private void notifyDropped(ConceptTreeItem dragged, ConceptTreeItem droppedOn) {
		if (dragged!= null && droppedOn != null) {
			Iterator<DragNDropListener> it = listeners.iterator();
			while (it.hasNext()) {
				DragNDropListener listener = (DragNDropListener) it.next();
				listener.drop(dragged, droppedOn,modifier);
			}
		}
	}
	
	private void modifyFloaterHTML() {
		if (modifier == MODIFIER_NONE) floater.setHTML(baseHTML);
		else if (modifier == MODIFIER_SHIFT) floater.setHTML("<b>-</b> "+baseHTML + "(remove broader concept)");
		
	}
	
	public void initialize() {
		floater = new HTML();
		floater.setStylePrimaryName("dnd-floatingItem");
		
		this.sinkEvents(Event.MOUSEEVENTS);
		floater.sinkEvents(Event.MOUSEEVENTS);
		floater.setVisible(false);
		floater.setWordWrap(false);

		DOM.addEventPreview(new EventPreview() {
			public boolean onEventPreview(Event e) {
				event = e;
				return true;
			}
		});

		this.addKeyboardListener(new KeyboardListener() {

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				if (modifiers == MODIFIER_SHIFT) {
					modifier = DragNDropTaxonomyTree.MODIFIER_SHIFT;
				}
				else modifier = DragNDropTaxonomyTree.MODIFIER_NONE;
				modifyFloaterHTML();
			}

			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				onKeyDown(sender,keyCode,modifiers);
			}

			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				modifier = -1;
				modifyFloaterHTML();
			}
			
		});
		
		this.addMouseDownHandler(new MouseDownHandler(){
			@Override
			public void onMouseDown(MouseDownEvent event) {
//				selectedTreeItem = (ConceptTreeItem) getElementClicked(event.getNativeEvent());
				selectedTreeItem = (ConceptTreeItem) getElementClicked(event);
				if (selectedTreeItem != null) {
					clicked = true;
//					startDragX = event.getClientX();
//					startDragY = event.getClientY();
					startDragX = event.getRelativeX(RootPanel.get("topbar").getElement());
					startDragY = event.getRelativeY(RootPanel.get("topbar").getElement());
					
					baseHTML = selectedTreeItem.getHTML();
					modifyFloaterHTML();
					DOM.setCapture(floater.getElement());
				}
			}
		});
		
		this.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent mouseUpEvent) {
				if (dragged) {
					floater.setVisible(false);

					if (selectedTreeItem != null) {
//						ConceptTreeItem item = (ConceptTreeItem) getElementClicked(mouseUpEvent.getNativeEvent());
						ConceptTreeItem item = (ConceptTreeItem) getElementClicked(mouseUpEvent);
						if (selectedTreeItem != item) notifyDropped(selectedTreeItem, item);
					}
				}
				clicked = false;
				dragged = false;
				selectedTreeItem = null;
			}});			
		
		floater.addMouseListener(new MouseListenerAdapter() {
			public void onMouseUp(Widget sender, int x, int y) {
				if (selectedTreeItem != null) {
					DOM.releaseCapture(floater.getElement());
					onBrowserEvent(event);
				}
				super.onMouseUp(sender, x, y);
			}

			public void onMouseMove(Widget sender, int x, int y) {
				int xpos = DOM.eventGetClientX(event);
				int ypos = DOM.eventGetClientY(event);
				
				if (clicked && ! dragged) {
					int difX = Math.abs(xpos - startDragX);
					int difY = Math.abs(ypos - startDragY);
	
					if (difX > dragThreshold || difY > dragThreshold) 
							dragged = true;
				}	
				if(dragged){
						RootPanel.get().setWidgetPosition(floater, x+sender.getAbsoluteLeft()-RootPanel.get("topbar").getElement().getAbsoluteLeft(), y+sender.getAbsoluteTop()-RootPanel.get("topbar").getElement().getAbsoluteTop());
						floater.setVisible(true);
				}				
				super.onMouseMove(sender, x, y);
			}
		});

		RootPanel.get().add(floater);
	}

	
//	private TreeItem getElementClicked(NativeEvent event) {	
//		TreeItem toReturn = null;
//		for (int i=0;i<getItemCount();i++) {
//			TreeItem current = getItem(i);
//			TreeItem temp = findItem(current,event);
//			if (temp != null) toReturn = temp;
//		}
//		return toReturn;
//	}
	private TreeItem getElementClicked(MouseUpEvent event) {	
		TreeItem toReturn = null;
		for (int i=0;i<getItemCount();i++) {
			TreeItem current = getItem(i);
			TreeItem temp = findItem(current,event);
			if (temp != null) toReturn = temp;
		}
		return toReturn;
	}
	
	private TreeItem getElementClicked(MouseDownEvent event) {	
		TreeItem toReturn = null;
		for (int i=0;i<getItemCount();i++) {
			TreeItem current = getItem(i);
			TreeItem temp = findItem(current,event);
			if (temp != null) toReturn = temp;
		}
		return toReturn;
	}
	
	private TreeItem findItem(TreeItem current, MouseUpEvent event) {
		int eventX = event.getRelativeX(RootPanel.get("topbar").getElement());
		int eventY = event.getRelativeY(RootPanel.get("topbar").getElement());
		int left = current.getAbsoluteLeft();
		int right = current.getAbsoluteLeft() + current.getOffsetWidth();
		int top = current.getAbsoluteTop();
		int bottom = current.getAbsoluteTop() + current.getOffsetHeight();
		TreeItem toReturn = null;
		if (eventX>=left && eventX<=right && eventY>=top && eventY<= bottom) {
			toReturn = current;
			for (int i=0;i<current.getChildCount();i++) {
				TreeItem currentChild = current.getChild(i);
				TreeItem tempFound = findItem(currentChild,event);
				if (tempFound != null) toReturn = tempFound;
			}
		}
		return toReturn;
	}
	
	private TreeItem findItem(TreeItem current, MouseDownEvent event) {
		int eventX = event.getRelativeX(RootPanel.get("topbar").getElement());
		int eventY = event.getRelativeY(RootPanel.get("topbar").getElement());
		int left = current.getAbsoluteLeft();
		int right = current.getAbsoluteLeft() + current.getOffsetWidth();
		int top = current.getAbsoluteTop();
		int bottom = current.getAbsoluteTop() + current.getOffsetHeight();
		TreeItem toReturn = null;
		if (eventX>=left && eventX<=right && eventY>=top && eventY<= bottom) {
			toReturn = current;
			for (int i=0;i<current.getChildCount();i++) {
				TreeItem currentChild = current.getChild(i);
				TreeItem tempFound = findItem(currentChild,event);
				if (tempFound != null) toReturn = tempFound;
			}
		}
		return toReturn;
	}
	
	
//	private TreeItem findItem(TreeItem current, NativeEvent event) {
//		int eventX = event.getClientX();
//		int eventY = event.getClientY();
//		int left = current.getAbsoluteLeft();
//		int right = current.getAbsoluteLeft() + current.getOffsetWidth();
//		int top = current.getAbsoluteTop();
//		int bottom = current.getAbsoluteTop() + current.getOffsetHeight();
//		TreeItem toReturn = null;
//		if (eventX>=left && eventX<=right && eventY>=top && eventY<= bottom) {
//			toReturn = current;
//			pos.setHTML(pos.getText() + ", " + eventX + ", " + eventY + "<br />");
//			RootPanel.get().add(pos);
//			for (int i=0;i<current.getChildCount();i++) {
//				TreeItem currentChild = current.getChild(i);
//				TreeItem tempFound = findItem(currentChild,event);
//				if (tempFound != null) toReturn = tempFound;
//			}
//		}
//		return toReturn;
//	}
	

	interface DragNDropListener {
		void drop(ConceptTreeItem dragged, ConceptTreeItem droppedOn, int modifier);
		
	}

}