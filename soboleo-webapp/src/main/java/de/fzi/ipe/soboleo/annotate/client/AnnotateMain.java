/*
 * 
 * TODO: maybe auto search for existing url and its concepts when url is filled in manually
 * 
 */

package de.fzi.ipe.soboleo.annotate.client;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHTML;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.annotate.client.components.AnnotatePopup;
import de.fzi.ipe.soboleo.annotate.client.components.HorizontalFlowPanel;
import de.fzi.ipe.soboleo.annotate.client.components.ShowRatingComponent;
import de.fzi.ipe.soboleo.annotate.client.components.TopicLink;
import de.fzi.ipe.soboleo.annotate.client.rpc.AnnotateService;
import de.fzi.ipe.soboleo.annotate.client.rpc.AnnotateServiceAsync;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.beans.rating.Rating;
import de.fzi.ipe.soboleo.event.logging.StartAnnotateWebDocument;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.client.ClientUtil;
import de.fzi.ipe.soboleo.server.client.ConceptMultiWordSuggestion;
import de.fzi.ipe.soboleo.server.client.ConceptNameSuggestOracle;
import de.fzi.ipe.soboleo.server.client.util.Location;
import de.fzi.ipe.soboleo.server.client.util.WindowUtils;
//import com.google.gwt.user.client.ui.SuggestOracle;

/**
 * @author FZI
 * 
 */

public class AnnotateMain extends VerticalPanel {

	private static AnnotateServiceAsync annoService;
	private FlexTable flexTable;
	private FlexCellFormatter cellFormatter;
	private VerticalPanel annotatePanel;
	private HorizontalPanel inputPanel;
	private MultiWordSuggestOracle multiOracle = new MultiWordSuggestOracle();
	private TextBox titleInput;
	private TextBox urlInput;
	private HTML urlLink;
	private Button removeFromIndexButton = new Button();
	private HashMap<String, String> annotationsList = new HashMap<String, String>(); // Strings
																						// of
																						// concept
																						// names
																						// for
																						// annotation
																						// within
																						// the
																						// currentTopicsflowPanel
																						// ;
	private FlowPanel extractedConceptsPanel = new FlowPanel();
	private FlowPanel extractedTopicsPanel = new FlowPanel();
	private FlowPanel classifiedConceptsPanel = new FlowPanel();
	private WebDocument webDoc;
	private String spacename;
	private LocalizedString.Language userLanguage;
	private LocalizedString.Language spaceLanguage;
	private EventSenderCredentials creds;
	// Needed for Internationalization
	private AnnotateConstants constants = GWT.create(AnnotateConstants.class);
	private ConceptNameSuggestOracle conceptOracle = new ConceptNameSuggestOracle(
			"(, ");
	private SuggestBox conceptSuggestBox;
	private ConceptMultiWordSuggestion conceptSuggestion;
	private HorizontalFlowPanel currentTopicsflowPanel = new HorizontalFlowPanel();
	private ShowRatingComponent showRatingComponent;// Rating component to show
													// and rate a web page
													// initialization in create
													// Annotate
	private double ratingOfThisWebPage;// the rating for this webpage
	private int totalNumberOfRatings;// the number of ratings for this web page
										// made.
	private boolean canBeRatedByUser = true;
	Button bUpload; // Button only for document upload. Only if it is not used
					// as popup or edit.
	private AbsolutePanel spacePanelForEmptyTopicsPanel = new AbsolutePanel();// for
																				// min
																				// height
																				// in
																				// IE

	AnnotationWebsiteController controller;
	
	
	
	public AnnotationWebsiteController getController() {
		return controller;
	}

	public AnnotateMain(AnnotationWebsiteController controller) {

		this.controller=controller;
		ClientUtil.initConfig(new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
				PopupPanel p = new PopupPanel();

				HTML headline = new HTML("<h3>" + titleInput + "</h3><br/>"
						+ caught.getMessage());
				p.add(headline);
				System.out.println("Failure in Annotate initConfig"
						+ caught.getMessage());
				p.setStyleName("ks-popups-Popup");
				p.setPopupPosition(250, 250);
				p.show();
			}

			public void onSuccess(Void result) {
				spacename = ClientUtil.getSpaceName();
				creds = ClientUtil.getEventSenderCredentials();
				userLanguage = ClientUtil.getUserLanguage();
				spaceLanguage = ClientUtil.getSpaceLanguage();
				// if it is set to false the user has rated this web page
				// already and the total rating is shown
				canBeRatedByUser = true; // TODO set this information
				getAnnotateService().sendEvent(new StartAnnotateWebDocument(),
						ClientUtil.getSpaceName(),
						ClientUtil.getEventSenderCredentials(),
						new AsyncCallback<Void>() {
							public void onFailure(Throwable caught) {
								focusWindow();
								handleException(caught);
							}

							public void onSuccess(Void arg0) {
								;
							}
						});
				createAnnotate();
			}
		});
	}

	
	String uri=null;
	String title=null;
	String conceptLabelToAdd=null;
	
	boolean init=false;
	
	/**
	 * This method creates the annotate form with it's buttons and labels.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void createAnnotate() {
		
		if (uri == null )
		{
			 uri = Window.Location.getParameter("furi");
			 title = Window.Location.getParameter("title");
			 conceptLabelToAdd = "undefined";
	
			if (Location.decodeURIComponent(Window.Location
					.getParameter("conceptLabel")) != null) {
				conceptLabelToAdd = Location.decodeURIComponent(Window.Location
						.getParameter("conceptLabel"));
				if (conceptLabelToAdd.equals("null")) {
					conceptLabelToAdd = "undefined";
				}
			}

			init=true;
		}

		this.flexTable = new FlexTable();
		this.cellFormatter = flexTable.getFlexCellFormatter();
		this.flexTable.setWidth("320px");
		this.flexTable.setCellPadding(3);

		this.cellFormatter.setHorizontalAlignment(0, 0,
				HasHorizontalAlignment.ALIGN_CENTER);
		flexTable.getFlexCellFormatter().setColSpan(5, 0, 2);
		this.cellFormatter.setColSpan(0, 0, 2);

		HTML html = new HTML("<h3>" + this.constants.annotate() + "</h3>");
		html.setStyleName("gwtLabelAnnotate");
		this.flexTable.setWidget(0, 0, html);

		createURLPanel("200px");
		createTitlePanel("200px");

		
		boolean isExistingWebpage=false;
		
		if (uri == null || uri.equalsIgnoreCase("undefined")) {
			isExistingWebpage=false;
		}
		else
		{
			// try to load webdoc
			getWebDocumentByURL(uri, title);
			
			if (webDoc != null)
			{
				isExistingWebpage=true;
			}
		}
		
		createAnnotationListPanel("300px");		
		createButtonPanel();
		

		if (!isExistingWebpage) {
			
			
			fillData("", "");
			if (conceptLabelToAdd != null
					&& !conceptLabelToAdd.equalsIgnoreCase("undefined")) {
				updateAnnotationsBox(conceptLabelToAdd, null);
			}
			
			//currentTopicsCaptionPanel.setVisible(false);
			//conceptSuggestBox.setVisible(false);
			
			// panels for annotations, creates
			createTopicInputPanel();
		} else
		{
			// fill data
			fillData(init);
			
			// panels for annotations, creates
			createTopicInputPanel();
			

			// String recommendationAvailable =
			// ClientUtil.getServerConfig().get(ServerConfig.KEY_CONCEPT_RECOMMENDATION_AVAILABLE);
			String recommendationAvailable = "false";
			if (recommendationAvailable.equals("true"))
				createRecommendationPanel();
		}
		
		add(flexTable);
		flexTable.getCellFormatter().setHorizontalAlignment(3, 0,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setHorizontalAlignment(2, 1,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setHorizontalAlignment(6, 1,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setHorizontalAlignment(6, 0,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setHorizontalAlignment(2, 0,
				HasHorizontalAlignment.ALIGN_LEFT);
		flexTable.getCellFormatter().setHorizontalAlignment(1, 0,
				HasHorizontalAlignment.ALIGN_LEFT);
		focusWindow();
	}

		

	
	private void getWebDocumentByURL(final String docURL, final String docTitle) {
		getAnnotateService().getWebDocumentByURL(ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(),
				ClientUtil.getUserLanguage(), docURL,
				new AsyncCallback<WebDocument>() {
					public void onSuccess(WebDocument doc) {
						if (doc != null) {
						
						
							currentTopicsCaptionPanel.setVisible(true);
							conceptSuggestBox.setVisible(true);

							// panels for annotations, creates
							createTopicInputPanel();
							

							// String recommendationAvailable =
							// ClientUtil.getServerConfig().get(ServerConfig.KEY_CONCEPT_RECOMMENDATION_AVAILABLE);
							String recommendationAvailable = "false";
							if (recommendationAvailable.equals("true"))
								createRecommendationPanel();
							
							setWebDocument(doc);
							fillData(init);
						} else
							fillData(docURL, docTitle);
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	private void setWebDocument(WebDocument webDoc) {
		this.webDoc = webDoc;
		this.removeFromIndexButton.setEnabled(true);
	}

	
	private void fillData(boolean init) {
		// get the rating for this webpage from server
		if (ClientUtil.getRatingWebsides()) {
			getAnnotateService().getAnnotationRating(ClientUtil.getSpaceName(),
					ClientUtil.getEventSenderCredentials(), webDoc,
					new AsyncCallback<Rating>() {
						@Override
						public void onSuccess(Rating result) {
							//Window.alert("requested rating: " +result.getScore());
							ratingOfThisWebPage = result.getScore();
							totalNumberOfRatings = result.getRatingFrequency();
							
							try
							{
								
								showRatingComponent.setInitialUserRating((int)ratingOfThisWebPage);
								
							}
							catch(Exception e)
							{
								Window.alert("error setting rating : " +e);
							}
							
						}
	
						@Override
						public void onFailure(Throwable caught) {
							ratingOfThisWebPage = -1;
							handleException(caught);
						}
					});
		}
		fillData(webDoc.getURL(), webDoc.getTitle());

		Set<Tag> tags = webDoc.getAllTags();
		Set<String> conceptURIs = new HashSet<String>();
		for (Tag tag : tags)
			conceptURIs.add(tag.getConceptID());

		

		if (init)
		{
			getAnnotateService().getConceptsForURIs(spacename, creds, conceptURIs,
					new AsyncCallback<Set<ClientSideConcept>>() {
						public void onSuccess(Set<ClientSideConcept> concepts) {
							for (ClientSideConcept c : concepts) {
								String currentLabel = c.getBestFitText(
										SKOS.PREF_LABEL, userLanguage,
										ClientUtil.getSpaceLanguage(), Language.en)
										.getString();
								annotationsList.put(currentLabel, c.getURI());
								// add new Topic to the current topics panel
								createTopicLinkPanel(currentLabel);
							}
						}
	
						public void onFailure(Throwable caught) {
							handleException(caught);
						}
					});
		}	
		
	}

	private void fillData(String docURL, String docTitle) {
		urlInput.setText(docURL);
		if (docURL.length() > 40)
			docURL = docURL.substring(0, 40) + "...";
		urlLink.setHTML(docURL);
		if (docURL.equals("")) {
			flexTable.remove(urlLink);
			flexTable.setWidget(1, 1, urlInput);
		}
		if (docTitle == null || docTitle.equalsIgnoreCase("undefined"))
			titleInput.setText(docURL);
		else
			titleInput.setText(docTitle);
	}

	private CaptionPanel currentTopicsCaptionPanel;
	
	private void createAnnotationListPanel(String width) {
		if (ClientUtil.hasSpaceDialogSupport()) {
			HTML startDialog = new HTML("<a href=\"javascript:undefined;\">"
					+ constants.startDialog() + "</a>");
			startDialog.setStylePrimaryName("startDialogLink");
			startDialog.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent arg0) {
					prepareStartDialog();
				}
			});
			flexTable.setWidget(6, 1, startDialog);
			flexTable.getFlexCellFormatter().setHorizontalAlignment(6, 1,
					HasHorizontalAlignment.ALIGN_RIGHT);
			flexTable.getCellFormatter().setVerticalAlignment(6, 1,
					HasVerticalAlignment.ALIGN_BOTTOM);
		}
		if (ClientUtil.getRatingWebsides()) {
			
			HTML htmlRating = new HTML(constants.rating());
			htmlRating.setStyleName("gwtLabelAnnotate");
			flexTable.setWidget(3, 0, htmlRating);
			showRatingComponent = new ShowRatingComponent(
					this.ratingOfThisWebPage, this.totalNumberOfRatings,
					this.canBeRatedByUser);
			// showRatingComponent = new ShowRatingComponent(1.33,28,false); for
			// test
			flexTable.setWidget(3, 1, showRatingComponent);
			// showRatingComponent.setSize("110px","22px");
		}
		
		
		// Caption Panel with Topics
		String captionForCurrentTopicsCaptionPannel = "<font color=\"#705132\">"
				+ constants.currentTopics() + "</font>";
		currentTopicsCaptionPanel = new CaptionPanel(
				captionForCurrentTopicsCaptionPannel, true);
		currentTopicsCaptionPanel.setStyleName("gwt-TopicsCaptionPanel");
		flexTable.setWidget(5, 0, currentTopicsCaptionPanel);
		spacePanelForEmptyTopicsPanel.setHeight("100px");
		currentTopicsflowPanel.add(spacePanelForEmptyTopicsPanel);
		currentTopicsflowPanel.setStyleName("TopicsPannel");
		currentTopicsCaptionPanel.setContentWidget(currentTopicsflowPanel);
		currentTopicsCaptionPanel.setWidth(width);

		conceptSuggestBox = new SuggestBox(conceptOracle);
		conceptSuggestBox.setStyleName("gwt-SuggestBox");
		conceptSuggestBox.setLimit(6);
		conceptSuggestBox.setWidth("200px");

		conceptSuggestBox.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				String current = conceptSuggestBox.getText();
				// if comma or semicolon is entered
				if (event.getNativeKeyCode() == 188
						|| event.getNativeKeyCode() == 110
						|| event.getNativeKeyCode() == 186) {
					current = current.replaceAll(",*;*", "").trim();
					if (!current.equals("")
							&& !annotationsList.containsKey(current)) {
						// add a new TopicLink to the current topic panel
						createTopicLinkPanel(current);
						if (conceptSuggestion != null)
							annotationsList.put(current, conceptSuggestion
									.getConcept().getURI());
						else
							annotationsList.put(current, "");
					}
					conceptSuggestBox.setText("");
					return;
				}
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					current = current.trim();
					if (!current.equals("")
							&& !annotationsList.containsKey(current)) {
						// add a new TopicLink to the current topic panel
						createTopicLinkPanel(current);
						if (conceptSuggestion != null)
							annotationsList.put(current, conceptSuggestion
									.getConcept().getURI());
						else
							annotationsList.put(current, "");
					}
					conceptSuggestBox.setText("");
				}
			}
		});

		conceptSuggestBox
				.addSelectionHandler(new SelectionHandler<Suggestion>() {
					@Override
					public void onSelection(SelectionEvent<Suggestion> event) {
						conceptSuggestion = (ConceptMultiWordSuggestion) event
								.getSelectedItem();
						String current = conceptSuggestBox.getText().trim();
						if (!current.equals("")
								&& !annotationsList.containsKey(current)) {
							// add a new TopicLink to the current topic panel
							createTopicLinkPanel(current);
							if (conceptSuggestion != null)
								annotationsList.put(current, conceptSuggestion
										.getConcept().getURI());
							else
								annotationsList.put(current, "");
						}
						conceptSuggestBox.setText("");
					}
				});
		HTML html = new HTML(constants.topics());
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(6, 0, html);
		html.setWidth("70");

		flexTable.setWidget(6, 1, conceptSuggestBox);
	}

	private void prepareStartDialog() {
		if (webDoc == null) {
			String url = urlInput.getText().trim();
			String t = titleInput.getText().trim();
			if (t.equals(""))
				t = url;
			final String title = t;
			if (url.equals("")) {
				
				/*
				AnnotatePopup popup = new AnnotatePopup(constants.noUrl(),
						constants.fillUrl());
				popup.show();
				*/
				
				Window.alert(constants.fillUrl());
			
				
			} else {
				// no existing webDoc or changed url
				createWebDocument(url, title, new AsyncCallback<Void>() {
					public void onSuccess(Void result) {
						startDialog();
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
			}
		} else {
			startDialog();
		}
	}

	private void startDialog() {
		getAnnotateService().startDialog(webDoc.getURL(),
				ClientUtil.getSpaceName(), ClientUtil.getUserLanguage(),
				ClientUtil.getEventSenderCredentials(),
				new AsyncCallback<Void>() {
					public void onSuccess(Void result) {
						;
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	private void createButtonPanel() {
		final Button saveButton = new Button();
		saveButton.setStyleName("buttonPannel");
		saveButton.setText(constants.save());
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
//				blurWindow();
				String current = conceptSuggestBox.getText().trim();
				if (!current.equals("")
						&& !annotationsList.containsKey(current)) {
					updateAnnotationsBox(current, new AsyncCallback<Void>() {
						public void onSuccess(Void result) {
							prepareSaveAnnotation();
						}

						public void onFailure(Throwable caught) {
							handleException(caught);
						}
					});
				} else
					prepareSaveAnnotation();
			}
		});
		saveButton.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				saveButton.setStyleName("buttonPannelOver");
			}
		});
		saveButton.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				saveButton.setStyleName("buttonPannel");
			}
		});

		final Button cancelButton = new Button();
		cancelButton.setStyleName("buttonPannel");
		cancelButton.setText(constants.cancel());
		cancelButton.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				if (controller == null)
				{
					
				}
				else
				{
					blurWindow();
					Window.Location.reload();
				}
				closeWindow();
			}
		});
		cancelButton.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				cancelButton.setStyleName("buttonPannelOver");
			}
		});
		cancelButton.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				cancelButton.setStyleName("buttonPannel");
			}
		});
		removeFromIndexButton.setStyleName("buttonPannel");
		removeFromIndexButton.setText(constants.delete());
		removeFromIndexButton.setEnabled(false);
		removeFromIndexButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				getAnnotateService().removeFromIndex(ClientUtil.getSpaceName(),
						ClientUtil.getEventSenderCredentials(),
						webDoc.getURL(), new AsyncCallback<Void>() {

							public void onFailure(Throwable caught) {
								focusWindow();
								handleException(caught);
							}

							public void onSuccess(Void arg0) {
								Window.alert("Document deleted");
								updateWindow();
//								Window.Location.reload();
//								closeWindow();
//								reloadOpenerWindow();
							}
						});
			}
		});
		removeFromIndexButton.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				removeFromIndexButton.setStyleName("buttonPannelOver");
			}
		});
		removeFromIndexButton.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				removeFromIndexButton.setStyleName("buttonPannel");
			}
		});
		
		if (uri == null)
		{
			removeFromIndexButton.setVisible(false);
		}
		else
		{
			removeFromIndexButton.setVisible(true);
		}
		
		final Button newButton = new Button();
		newButton.setStyleName("buttonPannel");
		newButton.setText(constants.createnew());
		newButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				uri=null;
				remove(flexTable);
				init=true;
				createAnnotate();
			}
		});
			
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(saveButton);
		buttonPanel.add(removeFromIndexButton);
		buttonPanel.add(cancelButton);
		
		if (controller != null)
		{
			buttonPanel.add(newButton);
		}

		cellFormatter.setColSpan(8, 0, 2);
		cellFormatter.setHorizontalAlignment(8, 0,
				HasHorizontalAlignment.ALIGN_CENTER);
		flexTable.setWidget(8, 0, buttonPanel);

	}

	private void createRecommendationPanel() {

		// getAnnotateService().getConceptRecommendation(uri, new
		// AsyncCallback<Map<String, Set<String>>>() {
		//
		//			
		// public void onSuccess (Map<String, Set<String>>
		// recommendationResult){
		//				
		// Set<String> recommendedConcepts =
		// recommendationResult.get("recommendedConcepts");
		// Set<String> freeTerms = recommendationResult.get("freeTerms");
		// Set<String> derivedTerms = recommendationResult.get("derivedTerms");
		//				
		// createRecommendedLabelsLinkList(recommendedConcepts,
		// extractedConceptsPanel);
		// createRecommendedLabelsLinkList(freeTerms, extractedTopicsPanel);
		// createRecommendedLabelsLinkList(derivedTerms,
		// classifiedConceptsPanel);
		// }
		// public void onFailure(Throwable caught) {
		// handleException(caught);
		// }
		// });
		//			
		// HTML recommendationsLabel = new HTML("Recommendations:");
		// flexTable.setWidget(7, 0, recommendationsLabel);
		//		
		// HTML classifiedConceptsLabel = new HTML("is about:");
		// classifiedConceptsLabel.setStyleName("recommendationLables");
		// flexTable.setWidget(8, 0, classifiedConceptsLabel);
		// classifiedConceptsPanel.setStyleName("flowPanel");
		// flexTable.setWidget(8, 1, classifiedConceptsPanel);
		//		
		// HTML extractedConceptsLabel = new HTML("extracted concepts:");
		// extractedConceptsLabel.setStyleName("recommendationLables");
		// flexTable.setWidget(9, 0, extractedConceptsLabel);
		// extractedConceptsPanel.setStyleName("flowPanel");
		// flexTable.setWidget(9, 1, extractedConceptsPanel);
		//		
		// HTML extractedTopicsLabel = new HTML("new topics:");
		// extractedTopicsLabel.setStyleName("recommendationLables");
		// flexTable.setWidget(10, 0, extractedTopicsLabel);
		// extractedTopicsPanel.setStyleName("flowPanel");
		// flexTable.setWidget(10, 1, extractedTopicsPanel);
	}

	private void createURLPanel(String width) {

		HTML html = new HTML("URL:");
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(1, 0, html);
		html.setWidth("70");
		urlInput = new TextBox();
		urlInput.setText(" ");
		urlInput.setWidth(width);
		urlLink = new HTML("<a href=\"javascript:undefined;\"> </a> ");
		urlLink.setStyleName("urlLink");
		urlLink.setWidth(width);
		flexTable.setWidget(1, 1, urlLink);
		urlLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				flexTable.remove(urlLink);
				flexTable.setWidget(1, 1, urlInput);
			}
		});
	}

	private void createTitlePanel(String width) {
		HTML html = new HTML(constants.title());
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(2, 0, html);
		html.setWidth("70");
		titleInput = new TextBox();
		titleInput.setText(" ");
		titleInput.setWidth(width);
		flexTable.setWidget(2, 1, titleInput);
	}

	private void createTopicInputPanel() {

		getAnnotateService().getAllLabels(ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(),
				ClientUtil.getUserLanguage(), new AsyncCallback<Set<String>>() {
					public void onSuccess(Set<String> labels) {
						for (String label : labels) {
							multiOracle.add(label);
						}
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});

		// Fill the ClientSideConcept objects
		annoService.getConcepts(spacename, creds,
				new AsyncCallback<Collection<ClientSideConcept>>() {

					@Override
					public void onFailure(Throwable caught) {
						handleException(caught);
					}

					@Override
					public void onSuccess(Collection<ClientSideConcept> result) {
						for (ClientSideConcept c : result) {
							conceptOracle
									.add(new ConceptMultiWordSuggestion(c));
							for (LocalizedString altLabel : c.getTexts(
									SKOS.ALT_LABEL, userLanguage)) {
								ClientSideConcept newConcept = c;
								conceptOracle
										.add(new ConceptMultiWordSuggestion(
												newConcept, altLabel
														.getString()));
							}
						}
					}
				});
	}

	private void updateAnnotationsBox(final String label,
			final AsyncCallback<Void> callback) {
		final String prefLabel = prepareLabel(label);
		getAnnotateService().getConceptForLabel(spacename, creds,
				new LocalizedString(prefLabel, userLanguage),
				new AsyncCallback<ClientSideConcept>() {
					public void onSuccess(ClientSideConcept concept) {
						// add a new TopicLink to the current topic panel
						createTopicLinkPanel(label);
						if (concept != null)
							annotationsList.put(label, concept.getURI());
						else
							annotationsList.put(label, "");
						if (callback != null)
							callback.onSuccess(null);
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	/**
	 * Shows a PopUp window with the occurred exception.
	 * 
	 * @param t
	 */
	public static void handleException(Throwable t) {
		
		/*
		AnnotatePopup popup = new AnnotatePopup(t);
		popup.show();
		*/
		
		if(t.getMessage().contains("201")) WindowUtils.redirect(ClientUtil.getAbsURL() +"login?redirectURL=" + URL.encode(WindowUtils.getLocation().getHref()));
//		else Window.alert(t.getLocalizedMessage());
		else WindowUtils.redirect(ClientUtil.getAbsURL() +"login?redirectURL=" + URL.encode(WindowUtils.getLocation().getHref()));
	
	}

	/**
	 * Returns the AnnotateServiceAsync Object to get Configurations and to
	 * store data on the server.
	 * 
	 * @return AnnotateServiceAsync
	 */
	public static AnnotateServiceAsync getAnnotateService() {
		if (annoService == null) {
			annoService = (AnnotateServiceAsync) GWT
					.create(AnnotateService.class);
			ServiceDefTarget endpoint = (ServiceDefTarget) annoService;
			endpoint.setServiceEntryPoint(ClientUtil.getServerURL()
					+ "annotate/service");
		}
		return annoService;
	}

	private void prepareSaveAnnotation() {
		String url = urlInput.getText().trim();
		String t = titleInput.getText().trim();
		if (t.equals(""))
			t = url;
		final String title = t;
		if (url.equals("")) {
			AnnotatePopup popup = new AnnotatePopup(constants.noUrl(),
					constants.fillUrl());
			popup.show();
		} else {
			if (webDoc == null || !url.equals(webDoc.getURL())) {
				// no existing webDoc or changed url
				
				// check if the created url already exists
				
				checkDoesWebWebDocumentByURLExist(url,title);
				
			} else
				saveAnnotation(title);
		}
	}

	private void checkDoesWebWebDocumentByURLExist(final String docURL, final String title) {

	
		getAnnotateService().getWebDocumentByURL(ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(),
				ClientUtil.getUserLanguage(), docURL,
				new AsyncCallback<WebDocument>() {
					public void onSuccess(WebDocument doc) {
						if (doc != null) {
			
							setWebDocument(doc);							
							// load this doc
							
							// rebuild this page with parameter url
							uri=webDoc.getURL();
							remove(flexTable);
							
							init=false;
							conceptOracle = new ConceptNameSuggestOracle(
							"(, ");
							createAnnotate();
							fillData(true);
							Window.alert("Loaded alredy existing entry for this URL");
						}
						else
						{
							// it is a new url
							createWebDocument(docURL, title, new AsyncCallback<Void>() {
								public void onSuccess(Void result) {
									saveAnnotation(title);
								}

								public void onFailure(Throwable caught) {
									handleException(caught);
								}
							});

						}
					}
					public void onFailure(Throwable caught) {
						handleException(caught);
					}			
		});
		
	}
	
	
	private boolean saveAnnotation(String title) {
		

		// save the rating first
		if (ClientUtil.getRatingWebsides()) {
			if (this.showRatingComponent.isRated()) {
				getAnnotateService().saveAnnotationRating(
						ClientUtil.getSpaceName(),
						ClientUtil.getEventSenderCredentials(), webDoc,
						this.showRatingComponent.getRating(),
						new AsyncCallback<Void>() {
							public void onSuccess(Void result) {

							}

							public void onFailure(Throwable caught) {
								handleException(caught);
							}
						});
			}
		}
		// save the topics and other modifications
		getAnnotateService().saveAnnotationData(ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(),
				ClientUtil.getUserLanguage(), webDoc, annotationsList, title,
				new AsyncCallback<Void>() {
					public void onSuccess(Void result) {
						
						/*
						new AnnotatePopup(AnnotateMain.this).showSuccessDialog(
								constants.success(), "");
						 */
						
						Window.alert(constants.success());
						
						updateWindow();
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
		
		return false;

	}

	
	private void updateWindow()
	{
		String windowLocation = "";
		try {
			windowLocation = Window.Location.getHref();
		} catch (Exception ex) {
			windowLocation = "";
		}
		// if on soboleo page, reload annotatemain with empty values
		if (windowLocation.contains("annotation.jsp"))
		{
			resetWindow();
		}
		else
		{
			AnnotateMain.closeWindow();
			
			// update the caller window
			try
			{
				AnnotateMain.reloadOpenerWindow();
			}
			catch(Exception e)
			{
				
			}
		}			
	}
	
	private void createWebDocument(String url, String title,
			final AsyncCallback<Void> callback) {
		getAnnotateService().createWebDocument(ClientUtil.getSpaceName(),
				ClientUtil.getEventSenderCredentials(), url, title,
				new AsyncCallback<WebDocument>() {
					public void onSuccess(WebDocument webDoc) {
						setWebDocument(webDoc);
						fillData(false);
						if (callback != null)
							callback.onSuccess(null);
					}

					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
	}

	
	/** reloads the window */
	public void resetWindow()
	{
		uri=null;
		title=null;
		conceptLabelToAdd=null;
		annotationsList.clear();
		conceptOracle = new ConceptNameSuggestOracle(
		"(, ");
		webDoc=null;
		
		currentTopicsflowPanel.clear();
		
		remove(flexTable);
		createAnnotate();
	}
	
	private void createRecommendedLabelsLinkList(
			Set<String> recommendedLabelsList, FlowPanel panel) {
		for (String concept : recommendedLabelsList) {
			final InlineHTML conceptLink = new InlineHTML(
					"<a href=\"javascript:undefined;\">" + concept + "</a> ");
			conceptLink.setStyleName("conceptLink");
			panel.add(conceptLink);
			conceptLink.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					String conceptName = conceptLink.getText().trim();
					if (!conceptName.equals("")
							&& !annotationsList.containsKey(conceptName)) {
						updateAnnotationsBox(conceptName, null);
					}
				}
			});
		}
	}

	private String prepareLabel(String label) {
		if (label.indexOf("(") != -1 && label.indexOf(")") != -1) {
			int index1 = label.indexOf("(");
			int index2 = label.lastIndexOf(")");
			return label.substring(index1 + 1, index2);
		} else
			return label;
	}

	private static native void focusWindow() /*-{
												return $wnd.focus();
												}-*/;

	private static native void blurWindow() /*-{
											return $wnd.blur();
											}-*/;

	public static native void closeWindow() /*-{
												return $wnd.close();
												}-*/;

	/**
	 * This is a native JSNI function. This allows to execute java script code
	 * in java code. It execute the window.opener.location.reload() command and
	 * reloads the window, which has opened the annotate window. Used to update
	 * the edited data.
	 */
	private static native void reloadOpenerWindow() /*-{
													return $wnd.opener.location.reload();
													}-*/;

	/**
	 * Adds a new TopicLink panel to the current topics flow panel
	 * 
	 * @param currentLabel
	 */
	private void createTopicLinkPanel(String currentLabel) {
		currentTopicsflowPanel.remove(spacePanelForEmptyTopicsPanel);
		TopicLink topicLink = new TopicLink(currentLabel, annotationsList, true);


		
		currentTopicsflowPanel.add(topicLink);
	}
}
