package de.fzi.ipe.soboleo.editor.client;

import de.fzi.ipe.soboleo.event.Event;

public interface EditorEventListener {
	
	public void notify(Event event);

}
