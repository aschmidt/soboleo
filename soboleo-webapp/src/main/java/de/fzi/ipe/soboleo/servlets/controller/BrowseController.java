package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CONCEPT_ID;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CRED_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.LANGUAGES_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerUtils.addSubconcepts;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerUtils.getShowRating;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerUtils.getTaggedPersonForUser;

import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.beans.dialog.Dialog;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.event.logging.BrowseConcept;
import de.fzi.ipe.soboleo.event.logging.BrowseRoots;
import de.fzi.ipe.soboleo.event.logging.SubscribeConcept;
import de.fzi.ipe.soboleo.event.logging.SubscribeRoots;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetAllDocuments;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsForConcept;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetConceptDialogsForConcept;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsForConcept;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchSimilarPersons;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.User;

@Controller
public class BrowseController {
	
	private final static Logger logger = Logger.getLogger(BrowseController.class);
		
	@RequestMapping("/taxonomy")
	protected ModelAndView doBrowse(@RequestParam(value=CONCEPT_ID, required=false) String conceptIDString,
			@RequestParam(value="format", required=false) String format,
			@Value(CRED_SPEL) EventSenderCredentials cred,
			@Value(USER_SPACE_SPEL) Space space,
			@Value(USER_USER_SPEL) User user,
			@Value(LANGUAGES_SPEL) Language[] langs,
			HttpServletRequest rq)
			throws EventPermissionDeniedException, ServletException,
			IOException 
		{
		
		TaggedPerson taggedPerson = getTaggedPersonForUser(user, cred, space);
		
		boolean showRating = getShowRating(space);

		ModelMap m = new ModelMap();
		m.put("showRating", showRating);
		
		if (conceptIDString != null) {
			return doBrowseConcept(m,cred, space, taggedPerson, conceptIDString, format,langs);
		} else {
			return doBrowseRoot(m,cred, space, taggedPerson, format,langs);
		}
	}
	
	@SuppressWarnings("unchecked")
	private ModelAndView doBrowseConcept(ModelMap m,
			EventSenderCredentials cred, Space space,
			TaggedPerson taggedPerson, String conceptIDString,
			String format, Language[] languages)
			throws EventPermissionDeniedException,
			IOException {

		ClientSideConcept concept = space.getClientSideTaxonomy().get(
				TripleStore.SOBOLEO_NS + conceptIDString);
		m.put("concept", concept);
		
		String conceptLabel = concept.getBestFitText(SKOS.PREF_LABEL, languages).getString();
		LocalizedString desc =  concept.getBestFitText(SKOS.NOTE, languages);
		if(desc != null)
			m.put("conceptdesc",desc.getString()); 
		
		m.put("conceptLabel", conceptLabel);
		m.put("concepturi", concept.getURI().substring(concept.getURI().indexOf("#")+1));

		List<String> altLabels = new LinkedList<String>();
		
		for (LocalizedString s:concept.getTexts(SKOS.ALT_LABEL, languages[0])) 
		{ //TODO was ist, wenn kein altLabel in der Sprache da ist?
			altLabels.add(s.getString()); }
		m.put("altLabels",altLabels);
		
		// get the subconcepts (its a prototype for small ontologies only!!!)
		Set<String> narrowersset = new HashSet<String>();

		narrowersset = addSubconcepts(narrowersset, concept, space);

		GetDocumentsForConcept job = new GetDocumentsForConcept(concept
				.getURI());

		job.setSubconceptURIs(narrowersset);

		LuceneResult result = (LuceneResult) space.getEventBus().executeQuery(
				job, cred);
		m.put("result", result.iterator());

		List<TaggedPerson> persons = (List<TaggedPerson>) space.getEventBus()
				.executeQuery(
						new SearchPersonsForConcept(concept.getURI(), taggedPerson.getURI()), cred);
		m.put("peopleresult", persons);

		if (Boolean.valueOf(space.getProperty(SpaceProperties.DIALOG_SUPPORT))
				.booleanValue()) {
			List<Dialog> dialogs = (List<Dialog>) space.getEventBus()
					.executeQuery(
							new GetConceptDialogsForConcept(concept.getURI()),
							cred);
			m.put("conceptdialogs", dialogs);
		}
		
		List<ClientSideConcept> broaders = ClientSideTaxonomy.sortConcepts(concept.getConnectedAsConcepts(SKOS.HAS_BROADER, space.getClientSideTaxonomy()), languages);
		m.put("broaders", broaders);
		List<ClientSideConcept> narrowers = ClientSideTaxonomy.sortConcepts(concept.getConnectedAsConcepts(SKOS.HAS_NARROWER, space.getClientSideTaxonomy()), languages);
		m.put("narrowers", narrowers);
		List<ClientSideConcept> relateds = ClientSideTaxonomy.sortConcepts(concept.getConnectedAsConcepts(SKOS.RELATED, space.getClientSideTaxonomy()), languages);
		m.put("relateds", relateds);
		
		if (format != null)
		{
			space.getEventBus().sendEvent(
					new SubscribeConcept(concept.getURI()), cred);
			//TODO
			// rs.setCharacterEncoding("UTF-8");
			return new ModelAndView("conceptAtom",m);
		} else {
			space.getEventBus().sendEvent(new BrowseConcept(concept.getURI()),
					cred);
			return new ModelAndView("concept",m);
		}
	}
	
	@SuppressWarnings("unchecked")
	private ModelAndView doBrowseRoot(ModelMap m, EventSenderCredentials cred, Space space,
			TaggedPerson taggedPerson, String format,
			Language[] languages) throws EventPermissionDeniedException,
			IOException {

		LuceneResult result = (LuceneResult) space.getEventBus().executeQuery(
				new GetAllDocuments(), cred);

		logger.info("doBrowseRoot - results : " + (result != null ? result.length() : "null") );

		m.put("result", result);
	
		if (taggedPerson != null) 
		{
			List<TaggedPerson> similarPersons = (List<TaggedPerson>) space
					.getEventBus().executeQuery(
							new SearchSimilarPersons(taggedPerson.getURI()), cred);
			m.put("peopleresult", similarPersons);
		}
		else
			m.put("peopleresult", new LinkedList<TaggedPerson>());
		
		ClientSideTaxonomy tax = space.getClientSideTaxonomy(); 
		if (tax == null)
			logger.fatal("ClientSideTaxonomy is null");
		
		List<ClientSideConcept> cls = tax.getSortedRootConcepts(
				space.getProperty(SpaceProperties.PROTOTYPICAL_CONCEPT_NAME), languages);
		
		if (cls == null)
			logger.error("SortedRootConcepts is null");
		
		m.put("rootconcepts", cls);

		if (format != null) {
			space.getEventBus().sendEvent(new SubscribeRoots(), cred);
			// TODO
			// rs.setCharacterEncoding("UTF-8");
			return new ModelAndView("conceptAtom",m);
		} else {

			space.getEventBus().sendEvent(new BrowseRoots(), cred);
			return new ModelAndView("roots",m);
		}
	}
}
