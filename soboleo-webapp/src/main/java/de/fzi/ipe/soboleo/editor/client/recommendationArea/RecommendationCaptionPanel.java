package de.fzi.ipe.soboleo.editor.client.recommendationArea;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.FlexTable;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.editor.client.recommendationArea.RecommendationDisplayComponent.ConceptAnchor;
import de.fzi.ipe.soboleo.event.editor.ChangeConceptSelectionEvent;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public abstract class RecommendationCaptionPanel<A extends GardeningRecommendation> extends CaptionPanel {
	private EventDistributor eventDistrib;
	private FlexTable recTable;
	// Internationalization
	protected EditorConstants constants = GWT.create(EditorConstants.class); 
	
	public RecommendationCaptionPanel(EventDistributor eventDistrib, String caption){
		this.eventDistrib = eventDistrib;
		setCaptionText(caption);
		recTable = new FlexTable();
		this.setContentWidget(recTable);
	}	

	public abstract void addRecommendation(GardeningRecommendation toAdd); 
	public abstract void removeRecommendation();
	
	
	protected RecommendationDisplayComponent createDisplay(String preText, String fillingText, String sufText, ClientSideConcept primary, ClientSideConcept... concepts){
		
		
		int rowCount = recTable.getRowCount();
		RecommendationDisplayComponent rec = new RecommendationDisplayComponent(preText, fillingText, sufText, primary, concepts);
		rec.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if(event.getSource() instanceof ConceptAnchor){
					sendSetConceptSelectionEvent(((ConceptAnchor)event.getSource()).getConcept());
				}
			}
		});
		recTable.setWidget(rowCount, 0, rec);
//		PushButton removeButton = new PushButton(ClientUtil.getImageProvider().cross().createImage(), new ClickHandler(){
//			@Override
//			public void onClick(ClickEvent event) {
//				parent.sendRemoveRelationCmd(concept);
//			}
//	    });
//		this.setWidget(rowCount, 1, removeButton);
		return rec;
	}
	
	protected String getTextForType(SKOS type){
		String text = "";
		if(type.equals(SKOS.PREF_LABEL)) text = constants.recsPrefLabel();
		else if(type.equals(SKOS.ALT_LABEL)) text = constants.recsAltLabel();
		else if(type.equals(SKOS.HIDDEN_LABEL)) text = constants.recsHiddenLabel();
		return text;
	}
	
	protected void sendSetConceptSelectionEvent(ClientSideConcept toSelect) {
		if(toSelect != null) eventDistrib.notify(new ChangeConceptSelectionEvent(toSelect));
	}
	

}
