package de.fzi.ipe.soboleo.server.client;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;

public class ConceptNameSuggestOracle extends MultiWordSuggestOracle {
		
	private List<ConceptMultiWordSuggestion> conceptSuggestions = null;
	private static HTML helper = new HTML();


	
	public ConceptNameSuggestOracle() {
		super(" ");
	}

	public ConceptNameSuggestOracle(String whiteSpace) {
		super(whiteSpace);
	}

	@Override
	public void requestSuggestions(final Request request, final Callback callback) {
		request.setLimit(50);
		Response resp = new Response(matchingConcepts(request.getQuery(),request.getLimit()));
		callback.onSuggestionsReady(request, resp);
	}

	/**
     * 
     * @param query The current text being entered into the suggest box
     * @param limit The maximum number of results to return 
     * @return A collection of concept suggestions that match.
     */

	private Collection<ConceptMultiWordSuggestion> matchingConcepts(String query, int limit) {
		List<ConceptMultiWordSuggestion> matchingConcepts = new ArrayList<ConceptMultiWordSuggestion>(limit);
		
		String prefixToMatch = query.toLowerCase();
		int i = 0;
		int listSize = conceptSuggestions.size();
		
		// skip forward the concepts that don't match at the beginning of the array
		while(i<listSize && !conceptSuggestions.get(i).getDisplayString().toLowerCase().contains(prefixToMatch)){
			i++;
		}
		
		// We are at the start of the block of matching names. Add matching names till we
		// run out of names, stop finding matches, or have enough matches.
		int count = 0;
		while(i < listSize && count < limit){
			ConceptMultiWordSuggestion current = conceptSuggestions.get(i);
			if(	current.getReplacementString().toLowerCase().startsWith(prefixToMatch) ||
				current.getReplacementString().toLowerCase().contains(" " + prefixToMatch)){

				String formattedDisplay =  getFormattedDisplay(prefixToMatch, current.getDisplayString());
				ConceptMultiWordSuggestion suggestion = new ConceptMultiWordSuggestion(current.getConcept(), current.getDisplayString(), formattedDisplay);
				matchingConcepts.add(suggestion);
			}
			i++;
			count++;
		}
		return matchingConcepts;
	}

	/**
     * @param o 
     * @return
     * @see java.util.List#add(java.lang.Object)
     */
	public boolean add(ConceptMultiWordSuggestion sug){
		if (conceptSuggestions == null){
			conceptSuggestions = new ArrayList<ConceptMultiWordSuggestion>();
		}
		return conceptSuggestions.add(sug);
	}
	
	/**
     * @param o
     * @return
     * @see java.util.List#remove(java.lang.Object)
     */
	public boolean remove (Object o){
		if(conceptSuggestions != null){
			return conceptSuggestions.remove(o);
		}
		return false;
	}
	
	private String getFormattedDisplay(String toMatch, String original){
			      int index = 0;
			      int cursor = 0;
			      int limiter = original.indexOf('(');
			      String lower = original.toLowerCase();

			      // Create strong search string.
			      StringBuffer accum = new StringBuffer();

			      while (true) {
			        index = lower.indexOf(toMatch, index);
			        if (index == -1 || (limiter > 0 && index > limiter)) {
			          break;
			        }
			        int endIndex = index + toMatch.length();
			        if (index == 0 || (lower.charAt(index - 1) == ' ')) {
			          String part1 = toHTMLText(original.substring(cursor, index));
			          String part2 = toHTMLText(original.substring(index, endIndex));
			          cursor = endIndex;
			          accum.append(part1).append("<strong>").append(part2).append("</strong>");
			        }
			        index = endIndex;
			      }

			      // Finish creating the formatted string.
			      String end = toHTMLText(original.substring(cursor));
			      accum.append(end);

			    return accum.toString();

	}
	
	  private String toHTMLText(String text) {
		    helper.setText(text);
		    String escaped = helper.getHTML();
		    return escaped;
		  }

}
