package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CRED_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.TAGGED_PERSON_ID;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_TAXONOMY;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerUtils.getShowRating;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.event.logging.BrowsePeople;
import de.fzi.ipe.soboleo.event.logging.BrowseProfile;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetAllTaggedPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPerson;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchSimilarPersons;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;

@Controller
public class PeopleController
{

	private static final String VIEW_BROWSE = "browsePeople";
	private static final String VIEW_PROFILE = "profile";
	private final static Logger logger = Logger
			.getLogger(PeopleController.class);

	@Autowired
	protected Server server;

	@RequestMapping("/people")
	protected ModelAndView doShowPeople(
			@RequestParam(value = TAGGED_PERSON_ID, required = false) String personToShowID,
			@Value(CRED_SPEL) EventSenderCredentials cred,
			@Value(USER_SPACE_SPEL) Space space,
			@Value(USER_USER_SPEL) User user)
			throws EventPermissionDeniedException, IOException
	{

		if (personToShowID != null)
		{
			TaggedPerson self = ControllerUtils.getTaggedPersonForUser(user,
					cred, space);

			return doShowProfile(cred, space, personToShowID, self);
		} else
		{
			return doShowPeopleList(cred, space);
		}
	}

	@RequestMapping("/ownprofile")
	protected ModelAndView doShowOwnProfile(
			@Value(CRED_SPEL) EventSenderCredentials cred,
			@Value(USER_SPACE_SPEL) Space space,
			@Value(USER_USER_SPEL) User user)
			throws EventPermissionDeniedException, IOException
	{

		logger.info(" sender uri : " + cred.getSenderURI());

		return doShowProfile(cred, space, cred.getSenderURI(),
				ControllerUtils.getTaggedPersonForUser(user, cred, space));

	}

	private ModelAndView doShowProfile(EventSenderCredentials cred,
			Space space, String personToShowID, TaggedPerson self)
			throws EventPermissionDeniedException, IOException
	{

		ModelMap m = new ModelMap();

		String urlPerson = personToShowID;
		if (!urlPerson.startsWith("http://"))
		{
			urlPerson = TripleStore.SOBOLEO_NS + personToShowID;
		}

		logger.info("url person: " + urlPerson);

		TaggedPerson personToShow = (TaggedPerson) space.getEventBus()
				.executeQuery(new GetTaggedPerson(urlPerson), cred);

		User currentuser = null;

		if (personToShow.getEmail() == null)
		{
			currentuser = server.getUserDatabase().getUserForKey(cred.getKey());
			logger.info("user key for " + personToShow.getURI() + " : "
					+ currentuser.getKey());
			personToShow = self;
		} else
		{
			logger.info("person to show: " + personToShow.getEmail());
			currentuser = server.getUserDatabase().getUserForEmail(
					personToShow.getEmail());
			// logger.info("user key for " +personToShow.getURI() + " : "
			// +currentuser.getKey());
		}

		if (currentuser != null)
			m.put("tagcloudkey", currentuser.getKey());

		if (personToShow != null)
		{
			space.getEventBus().sendEvent(
					new BrowseProfile(personToShow.getURI()), cred);
			m.put("personToShow", personToShow);
		}
		// logger.info("loading documents of user : " +currentuser.getKey());

		GetDocumentsSearchResult job = new GetDocumentsSearchResult("");
		if (currentuser != null)
		{
			// job.setDocOwner(currentuser.getKey());
			// if community annotation
			job.setAnnotationOwner(currentuser.getKey());
		}
		SearchResult result = (SearchResult) space.getEventBus().executeQuery(
				job, cred);

		logger.info("documents found: " + result.getLuceneResult().length());

		boolean showRating = getShowRating(space);
		logger.info("show rating in search result:" + showRating);
		m.put("showRating", showRating);

		m.put("result", result.getLuceneResult());
		m.put(USER_TAXONOMY, space.getClientSideTaxonomy());

		try
		{
			@SuppressWarnings("unchecked")
			List<TaggedPerson> similarPersons = (List<TaggedPerson>) space
					.getEventBus().executeQuery(
							new SearchSimilarPersons(personToShow.getURI()),
							cred);
			m.put("peopleresult", similarPersons);
		} catch (Exception e)
		{
			logger.error(e);
		}

		return new ModelAndView(VIEW_PROFILE, m);
	}

	@SuppressWarnings("unchecked")
	private ModelAndView doShowPeopleList(EventSenderCredentials cred,
			Space space) throws EventPermissionDeniedException
	{
		List<TaggedPerson> persons = (List<TaggedPerson>) space.getEventBus()
				.executeQuery(new GetAllTaggedPersons(), cred);
		space.getEventBus().sendEvent(new BrowsePeople(), cred);
		logger.debug("Showing " + VIEW_BROWSE);
		return new ModelAndView(VIEW_BROWSE, "peopleresult", persons);
	}
}
