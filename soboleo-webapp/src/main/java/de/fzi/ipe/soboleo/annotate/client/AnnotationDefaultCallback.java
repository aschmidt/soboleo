package de.fzi.ipe.soboleo.annotate.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class AnnotationDefaultCallback implements AsyncCallback<Object>{

	public static final AnnotationDefaultCallback INSTANCE = new AnnotationDefaultCallback();
	
	private AnnotationDefaultCallback() {;}
	
	public void onSuccess(Object result) {
		;
	}

	public void onFailure(Throwable caught) {
		AnnotateMain.handleException(caught);
	}
	

}
