package de.fzi.ipe.soboleo.editor.client.messageArea;

import java.util.List;

import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.editor.client.EditorEventListener;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.HistoryLoadedEvent;
import de.fzi.ipe.soboleo.event.editor.LoadTaxonomyEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyLoadedEvent;
import de.fzi.ipe.soboleo.event.message.ChatCommand;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class SystemMessagePanel extends VerticalPanel implements EditorEventListener{

		private ChatContent chatContent;
		private ClientSideTaxonomy tax;
			
		public SystemMessagePanel(){
			setStyleName("chatPanel");
			chatContent = new ChatContent();
			add(chatContent);
		}

		public void notify(Event event) {
			if (event instanceof ReadableEditorEvent && !(event instanceof ChatCommand)) {
				ReadableEditorEvent readableEvent = (ReadableEditorEvent) event;
				chatContent.addMessage(readableEvent);
			}
			else if (event instanceof TaxonomyLoadedEvent) {
				TaxonomyLoadedEvent loadedEvent = (TaxonomyLoadedEvent) event;
				this.tax = loadedEvent.getTaxonomy();
			}
			else if (event instanceof LoadTaxonomyEvent) {
				chatContent.clear();
			}
			else if (event instanceof HistoryLoadedEvent){
				 HistoryLoadedEvent historyEvent = (HistoryLoadedEvent) event;
				 List<? extends Event> eventList = historyEvent.getHistory();
				 for(Event e : eventList) chatContent.addMessage((ReadableEditorEvent) e);
			}
		}
		
		private class ChatContent extends ScrollPanel {
			
			final int MAX_NUMBER_MESSAGES = 40;
			
			VerticalPanel vPanel = new VerticalPanel();
			int currentNumberOfMessages = 0;
			
			public ChatContent() {
				setWidget(vPanel);
				setWidth("250px");
				setHeight("630px");
			}
			
			void addMessage(ReadableEditorEvent event) {
				currentNumberOfMessages ++;
				Label currentMessage = new Label(event.getReadable(ClientUtil.getUserLanguage(), tax),true);
//				currentMessage.setWidth("230px");
				currentMessage.setWordWrap(true);
				currentMessage.setStyleName("chatMessage");
				if (event.getSenderURI().equals(ClientUtil.getEventSenderCredentials().getSenderURI())) { 
					currentMessage.setStyleName("chatMessageOwn");
				}
				else {
					currentMessage.setStyleName("chatMessage");
				}
				
				
				vPanel.add(currentMessage);
				if (currentNumberOfMessages > MAX_NUMBER_MESSAGES) vPanel.remove(0);
				ensureVisible(currentMessage);
			}
			
			public void clear() {
				vPanel.clear();
				currentNumberOfMessages = 0;
			}
			
		}
		
		
	}
