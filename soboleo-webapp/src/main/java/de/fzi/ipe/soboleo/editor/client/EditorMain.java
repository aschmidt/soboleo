package de.fzi.ipe.soboleo.editor.client;

import java.util.List;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.dnd.client.drop.AbsolutePositionDropController;
import com.allen_sauer.gwt.dnd.client.drop.DropController;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.editor.client.conceptDetails.ConceptDetailsPanel;
import de.fzi.ipe.soboleo.editor.client.menuBar.EditorMenuBar;
import de.fzi.ipe.soboleo.editor.client.messageArea.MessageTabPanel;
import de.fzi.ipe.soboleo.editor.client.rpc.EditorServiceAsync;
import de.fzi.ipe.soboleo.editor.client.taxonomyArea.TaxonomyPanel;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.HistoryLoadedEvent;
import de.fzi.ipe.soboleo.event.editor.LoadTaxonomyEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyLoadedEvent;
import de.fzi.ipe.soboleo.event.logging.StartEditor;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class EditorMain implements EntryPoint { //, HistoryListener {

	public static final String EDITOR_PANEL_WIDTH = "350px";
	public static final String EDITOR_INPUT_FIELD_WIDTH = "232px";

	
	DockPanel rootPanel;
	TaxonomyPanel taxonomyPanel;
	ConceptDetailsPanel conceptDetailsPanel;

	MessageTabPanel messageTabPanel;
	AbsolutePanel absolute;
	EditorMenuBar menuBar;

	EventDistributor eventDistrib = EventDistributor.getInstance();
	private TaxonomyUpdater updater;

	public void onModuleLoad() {
//		Window.alert("before login");
		ClientUtil.initConfig(new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
//				Window.alert("no login");
				EditorUtil.handleException(caught);
			}

			public void onSuccess(Void result) {
//				Window.alert("login");
				EditorUtil.sendEvent(new StartEditor());
				createEditor();
				taxonomyPanel.setFocusSearchBox();
			}});
	}
	
	private void createEditor() {
//		Window.alert("create editor");
		rootPanel = new DockPanel();

		menuBar = new EditorMenuBar(eventDistrib);
		rootPanel.add(menuBar, DockPanel.NORTH);

		createEditorPanel();

		RootPanel.get("editor").add(rootPanel);
		eventDistrib.notify(new LoadTaxonomyEvent());
		loadTaxonomy();
		loadRecentHistory();
	}

	private void createEditorPanel() {

		
		taxonomyPanel = new TaxonomyPanel(eventDistrib);
		rootPanel.add(taxonomyPanel, DockPanel.WEST);

		conceptDetailsPanel = new ConceptDetailsPanel(eventDistrib);
		rootPanel.add(conceptDetailsPanel, DockPanel.CENTER);
		
		messageTabPanel = new MessageTabPanel(eventDistrib);
		DOM.setElementAttribute(messageTabPanel.getElement(), "id", "messageTabPanel");
//		chatPanel.setSpacing(8);
		rootPanel.add(messageTabPanel, DockPanel.EAST);

		// Add gwt-dnd support - experimental
		// addDragAndDrop();
	}
	
	private void addDragAndDrop() {			
		AbsolutePanel container = new AbsolutePanel();
		container.setPixelSize(500, 600);
		container.getElement().getStyle().setProperty("position", "relative");
		container.getElement().getStyle().setProperty("border", "1px solid black");
		
		AbsolutePanel leftPanel = new AbsolutePanel();
		leftPanel.setSize("200", "400");
		leftPanel.getElement().getStyle().setProperty("border", "1px solid red");

		AbsolutePanel rightPanel = new AbsolutePanel();
		rightPanel.setSize("200", "400");
		rightPanel.getElement().getStyle().setProperty("border", "1px solid red");

		Label toDragAndDrop = new Label("Drag me");
		Label toDragAndDrop2 = new Label("Drag me2");
		
		leftPanel.add(toDragAndDrop);
		leftPanel.add(toDragAndDrop2);
		
		PickupDragController dragController = new PickupDragController(RootPanel.get(), true);
		dragController.setBehaviorMultipleSelection(false);
		
		container.add(leftPanel, 0, 0);
		container.add(rightPanel, 200, 0);
		
		DropController rightDropController = new AbsolutePositionDropController(rightPanel);
		DropController leftDropController = new AbsolutePositionDropController(leftPanel);
		DropController containerDropController = new AbsolutePositionDropController(container);
		DropController rootDropController = new AbsolutePositionDropController(RootPanel.get());
		
		dragController.registerDropController(leftDropController);
		dragController.registerDropController(rightDropController);
		dragController.registerDropController(containerDropController);
		dragController.registerDropController(rootDropController);
		
		dragController.makeDraggable(toDragAndDrop);
		dragController.makeDraggable(toDragAndDrop2);
		
		RootPanel.get().add(container);
	}

	private void loadTaxonomy() {
		EditorServiceAsync service = EditorUtil.getEditorService();
		AsyncCallback<ClientSideTaxonomy> callback = new AsyncCallback<ClientSideTaxonomy>() {
			public void onSuccess(ClientSideTaxonomy tax) {
				if (updater != null) eventDistrib.removeListener(updater);
				updater = new TaxonomyUpdater(tax,eventDistrib);
				eventDistrib.addListener(updater);
				eventDistrib.start(tax.getVersion());
				eventDistrib.notify(new TaxonomyLoadedEvent(tax));
			}
			public void onFailure(Throwable caught) {
				EditorUtil.handleException(caught);
			}}; 
		service.getClientSideTaxonomy(ClientUtil.getSpaceName(),ClientUtil.getEventSenderCredentials(),callback);	
	}
	
	private void loadRecentHistory(){
		EditorServiceAsync service = EditorUtil.getEditorService();
		AsyncCallback<List<? extends Event>> callback = new AsyncCallback<List<? extends Event>>() {
			public void onSuccess(List<? extends Event> eventList) {
				eventDistrib.notify(new HistoryLoadedEvent(eventList));
			}
			public void onFailure(Throwable caught) {
				EditorUtil.handleException(caught);
			}}; 
		service.getRecentHistory(ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), callback); 
		
	}

}
