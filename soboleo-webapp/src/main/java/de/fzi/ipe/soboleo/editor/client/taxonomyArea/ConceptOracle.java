package de.fzi.ipe.soboleo.editor.client.taxonomyArea;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gwt.user.client.ui.SuggestOracle;


public class ConceptOracle extends SuggestOracle {

    	Response response=null;
	    private Map<String,ConceptTreeItem> suggestions = new HashMap<String,ConceptTreeItem>() ;
	    
	    
	    public void add(String suggestion, ConceptTreeItem data)
	    {
	        suggestions.put(suggestion, data) ;
	    }

	    public static class ConceptSuggestion implements SuggestOracle.Suggestion
	    {
	        private String text ;
			private ConceptTreeItem data ;

	        public ConceptSuggestion(String text, ConceptTreeItem data)
	        {

	            this.text = text ;
	            this.data = data ;
	        }

			public String getDisplayString() {
				return text;
			}
			public boolean isDisplayStringHTML(){
				return true;
			}
			public String getReplacementString() {

				return text;
			}

	        public ConceptTreeItem getData()
	        {
	             return data ;
	        }
	    }

	public void requestSuggestions(Request request,Callback callback){
        Collection<ConceptSuggestion> result = new ArrayList<ConceptSuggestion>() ;
        Iterator<Entry<String,ConceptTreeItem>> it = suggestions.entrySet().iterator();
        
        while ( it.hasNext()){
            Map.Entry<String,ConceptTreeItem> suggestion = it.next();
            if(suggestion.getKey().toString().startsWith(request.getQuery())) {
                result.add(new ConceptSuggestion(suggestion.getKey().toString(), (ConceptTreeItem)suggestion.getValue())) ;
            }
        }
        response = new Response(result);
        callback.onSuggestionsReady(request, response);
	}
}
