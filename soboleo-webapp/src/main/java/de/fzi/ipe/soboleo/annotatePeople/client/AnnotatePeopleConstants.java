package de.fzi.ipe.soboleo.annotatePeople.client;

import com.google.gwt.i18n.client.Constants;

public interface AnnotatePeopleConstants extends Constants {
	@DefaultStringValue("Tag People")
	String annotatePeople();
	@DefaultStringValue("Name:")
	String name();
	@DefaultStringValue("Email:")
	String email();
	@DefaultStringValue("New Topic:")
	String newTopic();
	@DefaultStringValue("People's Topics:")
	String peoplesTopics();
	@DefaultStringValue("Suggested Topics:")
	String suggestedTopics();
	@DefaultStringValue("My Topics:")
	String myTopics();
	@DefaultStringValue("Enter Person's Name")
	String enterPersonsName();
	@DefaultStringValue("Enter Person's Email")
	String enterPersonsEmail();
	@DefaultStringValue("Please enter the person's email address.")
	String pleaseEnterPersonsEmail();
	
	// Buttons
	@DefaultStringValue("Remove Topic")
	String removeTopic();
	@DefaultStringValue("Save")
	String save();
	@DefaultStringValue("Delete")
	String delete();
	@DefaultStringValue("Cancel")
	String cancel();
	@DefaultStringValue("No topics associated")
	String noTopicsAssociated();
	@DefaultStringValue("No suggestions available")
	String noSuggestedTopics();
	@DefaultStringValue("Send")
	String send();
	@DefaultStringValue("Your data has been successfully saved.")
	String success();
	
	
	// Exception Handling
	@DefaultStringValue("Please enter the person's name.")
	String noPersonForEmail();
	@DefaultStringValue("Permission Denied")
	String permissionDenied();
	@DefaultStringValue("You do not have the necessary rights. Please <a href='/login'>login</a>!")
	String pleaseLogin();
	@DefaultStringValue("An Exception occurred")
	String exceptionOccured();
	@DefaultStringValue("Please close and reload the Annotate popup <br />")
	String closeAndReload();
	@DefaultStringValue("You are not allowed to change this space! - Please login or ask for permission for this space")
	String error_200();
	@DefaultStringValue("You are not allowed to view this space! Please login or ask for permission for this space")
	String error_201();
	/*String pBDelete_upText();
	String pBDeleteUpFace_html();
	String pBDeleteDownDisabledFace_html();*/
	
	// confirmation
	@DefaultStringValue("Really delete this person and all her annotations?")
	String reallyDeletePerson();
	
}
