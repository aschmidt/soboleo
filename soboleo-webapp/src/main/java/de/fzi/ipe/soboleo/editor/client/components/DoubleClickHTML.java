package de.fzi.ipe.soboleo.editor.client.components;

import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.HasDoubleClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.HTML;

public class DoubleClickHTML extends HTML implements HasDoubleClickHandlers {
	public DoubleClickHTML(){
		super();
		this.sinkEvents(Event.ONDBLCLICK);
	}
	
	public DoubleClickHTML(String label){
		super(label);
		this.sinkEvents(Event.ONDBLCLICK);
	}

	@Override
	public HandlerRegistration addDoubleClickHandler(DoubleClickHandler handler) {
		return addDomHandler(handler, DoubleClickEvent.getType());
	}
}
