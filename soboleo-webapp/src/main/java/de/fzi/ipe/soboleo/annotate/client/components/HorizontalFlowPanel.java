package de.fzi.ipe.soboleo.annotate.client.components;

/**
 * This is used for the block alignment for the Topic panels
 * It extends the abstract class AnnotateFlowPannel that is a FlowPanel
 * 
 * @author Stephan Kluge
 *
 */
public class HorizontalFlowPanel extends AnnotateFlowPanel {
	@Override
	protected String getFlowStyle() {
		return "inline";
	}
}
