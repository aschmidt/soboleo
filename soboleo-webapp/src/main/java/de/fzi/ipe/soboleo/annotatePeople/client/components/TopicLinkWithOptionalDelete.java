package de.fzi.ipe.soboleo.annotatePeople.client.components;

import java.util.Set;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.user.client.ui.PushButton;


import de.fzi.ipe.soboleo.annotatePeople.client.AnnotatePeopleConstants;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

/**
 * This constructor is used in AnnotatePeopleMain to create a specific component.
 * It is a label combined with a push button. The push button is used to delete the topic displayed 
 * in the corresponding label. This behavior is only available if it is set in the configuration and
 * only a person can delete his own topics from others.  
 * @author Stephan Kluge
 *
 */
public class TopicLinkWithOptionalDelete extends Composite {

    private AnnotatePeopleConstants constants = GWT.create(AnnotatePeopleConstants.class);
    private ClientSideConcept concept;
    /**
     * Constructor for the label with optional delete button
     * @param topicCaption The text for the label (topic)
     * @param title the text shown if the mouse is over the label
     * @param annotateHandler ClickHandler what should happen if you click on the lable
     * @param taggedPerson the person who should be tagged
     * @param conceptList A list of concepts, who stores the topics which are in the parent panel
     * @param concept the concept for this panel
     * @param deletable if the delete button should be there or not
     */
    public TopicLinkWithOptionalDelete(final String topicCaption,String title,ClickHandler annotateHandler,final TaggedPerson taggedPerson,final Set<String> conceptList,final ClientSideConcept concept, boolean deletable)//,AnnotatePeopleMain parent) 
    {
	this.concept = concept;
    	final HorizontalPanel horizontalPanel = new HorizontalPanel();
    	initWidget(horizontalPanel);
    	
    	final PushButton bDeleteTopic = new PushButton(ClientUtil.getImageProvider().crossMouseOut2Small().createImage(),ClientUtil.getImageProvider().crossMouseOverSmall().createImage());
    	final Label label = new Label(topicCaption); 
    	AbsolutePanel spacePanel = new AbsolutePanel();//needed that the components are not aligned without spaces between them
    	final AbsolutePanel spacePanelBetweenDelete = new AbsolutePanel(); // needed between DeleteButton and label 
    	AbsolutePanel spacePanelFront = new AbsolutePanel(); // add space to left
    	spacePanelBetweenDelete.setWidth("2px");
    	spacePanelFront.setWidth("4px");
    	final Label spacelabel = new Label("   ");
    	spacelabel.addMouseOverHandler(new MouseOverHandler() 
    	{
	    
	    @Override
	    public void onMouseOver(MouseOverEvent event) 
	    {
		label.setStyleName("gwt-topicLabelOver");
		spacelabel.setStyleName("gwt-topicLabelOver");
		spacePanelBetweenDelete.setStyleName("gwt-topicLabelOver");
		bDeleteTopic.setStyleName("gwt-topicLabelOver");
	    }
	});
    	spacelabel.addMouseOutHandler(new MouseOutHandler() 
    	{
	    
	    @Override
	    public void onMouseOut(MouseOutEvent event) 
	    {
		label.setStyleName("gwt-topicLabel");
		spacelabel.setStyleName("gwt-topicLabel");
		spacePanelBetweenDelete.setStyleName("gwt-topicLabel");
		bDeleteTopic.setStyleName("gwt-topicLabel");
		
	    }
	});
    	spacelabel.setStyleName("gwt-topicLabel");
    	spacePanelBetweenDelete.add(spacelabel);
    	label.setTitle(title);
    	label.setStyleName("gwt-topicLabel");
    	label.addMouseOverHandler(new MouseOverHandler() {
	    
    	    @Override
    	    public void onMouseOver(MouseOverEvent event) 
    	    {
    		label.setStyleName("gwt-topicLabelOver");
    		spacelabel.setStyleName("gwt-topicLabelOver");
    		spacePanelBetweenDelete.setStyleName("gwt-topicLabelOver");
    		bDeleteTopic.setStyleName("gwt-topicLabelOver");
    	    }
    	});
    	label.addMouseOutHandler(new MouseOutHandler() 
    	{
	    
    	    @Override
    	    public void onMouseOut(MouseOutEvent event) 
    	    {
    		label.setStyleName("gwt-topicLabel");
    		spacelabel.setStyleName("gwt-topicLabel");
    		spacePanelBetweenDelete.setStyleName("gwt-topicLabel");
    		bDeleteTopic.setStyleName("gwt-topicLabel");
    	    }
	});
    	label.addClickHandler(annotateHandler);
    	horizontalPanel.setCellVerticalAlignment(label, HasVerticalAlignment.ALIGN_MIDDLE);
	horizontalPanel.setCellHorizontalAlignment(label, HasHorizontalAlignment.ALIGN_LEFT);
	horizontalPanel.add(spacePanelFront); //SpacePanel to get a space at the front
	horizontalPanel.add(label);
    	
    	if(deletable)
    	{	
    	    bDeleteTopic.setStyleName("gwt-topicLabel");
    	    bDeleteTopic.getUpHoveringFace().setImage(ClientUtil.getImageProvider().crossMouseOverSmall().createImage());
    	    bDeleteTopic.addMouseOverHandler(new MouseOverHandler() 
    	    {

    		@Override
    		public void onMouseOver(MouseOverEvent event) 
    		{
    		    label.setStyleName("gwt-topicLabelOver");
    		    spacelabel.setStyleName("gwt-topicLabelOver");
    		    spacePanelBetweenDelete.setStyleName("gwt-topicLabelOver");
    		    bDeleteTopic.setStyleName("gwt-topicLabelOver");
    		}
    	    });
    	    bDeleteTopic.addMouseOutHandler(new MouseOutHandler() 
    	    {

    		@Override
    		public void onMouseOut(MouseOutEvent event) 
    		{
    		    label.setStyleName("gwt-topicLabel");
    		    spacelabel.setStyleName("gwt-topicLabel");
    		    spacePanelBetweenDelete.setStyleName("gwt-topicLabel");
    		    bDeleteTopic.setStyleName("gwt-topicLabel");
    		}
    	    });
    	    bDeleteTopic.addClickHandler(new ClickHandler() 
    	    {
	        
	        @Override
	        public void onClick(ClickEvent event) 
	        {
	    	//delete this from parent
	            horizontalPanel.getParent().removeFromParent();
	            conceptList.add(TopicLinkWithOptionalDelete.this.concept.getURI());
	            
	    	
	        }
	    });
    	    horizontalPanel.setCellVerticalAlignment(bDeleteTopic, HasVerticalAlignment.ALIGN_BOTTOM);
    	    horizontalPanel.setCellHorizontalAlignment(bDeleteTopic, HasHorizontalAlignment.ALIGN_RIGHT);
    	    bDeleteTopic.setStyleName("gwt-topicLabel");
    	    bDeleteTopic.setTitle(constants.removeTopic());
    	    spacePanelBetweenDelete.setWidth("2px");
	    horizontalPanel.add(spacePanelBetweenDelete);
    	    horizontalPanel.add(bDeleteTopic);
    	}
    	spacePanel.setWidth("8px"); // sets the space between two TopicLinkWithOptionalDelte to 10 Pixels
    	horizontalPanel.add(spacePanel);
    }    
}
