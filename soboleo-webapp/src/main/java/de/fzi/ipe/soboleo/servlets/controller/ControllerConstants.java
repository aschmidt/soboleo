package de.fzi.ipe.soboleo.servlets.controller;

public interface ControllerConstants {

	public static final String USER_LANGUAGE = "lang";
	public static final String USER_LANGUAGE_SPEL = "#{request.getAttribute('" + USER_LANGUAGE + "')}";
	public static final String USER_SPACE = "space";
	public static final String USER_SPACE_SPEL = "#{request.getAttribute('" + USER_SPACE + "')}";
	public static final String USER_SPACE_URI ="spaceuri";
	public static final String USER_USER = "user";
	public static final String USER_USER_SPEL = "#{request.getAttribute('" + USER_USER + "')}";
	public static final String USER_TAXONOMY = "tax";
	public static final String CRED = "cred";
	public static final String CRED_SPEL = "#{request.getAttribute('" + CRED + "')}";
	public static final String CONCEPT_ID = "concept";
	public static final String TAGGED_PERSON_ID = "taggedPerson";
	public static final String DIALOG_ID = "dialog";
	public static final String USER_KEY = "key";
	public static final String SEARCH_PARAMETER = "searchEntry";
	public static final String SEARCH_PARAMETER_MUST = "searchMust";
	public static final String SEARCH_PARAMETER_PEOPLE = "searchPeople";
	public static final String SEARCH_PARAMETER_HIDDEN = "searchSite";

	public static final String RESOURCE_URI = "ruri";
	
	public static final String COOKIE_USER = "SoboleoCookie";

	public static final String SERVER_URL = "absurl";

	public static final String LANGUAGES = "languages";
	public static final String LANGUAGES_SPEL = "#{request.getAttribute('" + LANGUAGES + "')}";
}
