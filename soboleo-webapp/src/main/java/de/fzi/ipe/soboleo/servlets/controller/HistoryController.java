package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CRED;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CRED_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_LANGUAGE_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER_SPEL;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.message.ReadableEvent;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.queries.logging.GetRecentEvents;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;

@Controller
public class HistoryController {
	
	@SuppressWarnings("unchecked")
	@RequestMapping("/history")
	protected ModelAndView doShowHistory(
			@RequestParam(value="page",required=false,defaultValue="0") Integer pageNumber,
			@Value(CRED_SPEL) EventSenderCredentials cred,
			@Value(USER_SPACE_SPEL) Space space,
			@Value(USER_USER_SPEL) User user,
			@Value(USER_LANGUAGE_SPEL) Language userLanguage,
			HttpServletRequest rq)
			throws EventPermissionDeniedException, 
			IOException {

		EventBus eb = space.getEventBus();
		eb.checkReadPermission(cred);		

		ClientSideTaxonomy tax = space.getClientSideTaxonomy();
		
		int offset = pageNumber  * 20;
				
		@SuppressWarnings("unchecked")
		List<? extends Event> eventList = (List<? extends Event>) eb.executeQuery(new GetRecentEvents(offset, ReadableEvent.class), new EventSenderCredentials(user.getURI(), user.getName(), user.getKey()));
		List<String> result = new LinkedList<String>();
		
		if(eventList.size() != 0)
		{
			for(Event e: eventList)
			{
				result.add(((ReadableEvent)e).getReadable(userLanguage,tax));
				//out.write(e.getId() + ": " + e.getSenderName() + " had \"" + className + "\" type of event on " + new Date(e.getCreationTime()) + ".<br/><br/>");
				
				//out.write("Event ID: " + e.getId() + "<br/>");
				//out.write("Event class: " + e.getClass() + "<br/>");
				//out.write("Event type: " + e.getEventType() + "<br/>");
				//out.write("Event creation time: " + new Date(e.getCreationTime()) + "<br/>");
				//out.write("Event sender: " + e.getSenderName() + "<br/>");
				//out.write("eventSenderURI: " + e.getSenderURI() + "<br/>");
				//out.write("<br/>");
			}
		}

		ModelMap m = new ModelMap();
		m.put("events",result);
		m.put("hasMore",(((List<Event>) eb.executeQuery(new GetRecentEvents(offset + 20, ReadableEvent.class), new EventSenderCredentials(user.getURI(), user.getName(), user.getKey()))).size() != 0));
		m.put("isFirst", pageNumber == 0);
		m.put("previousPage", pageNumber - 1);
		m.put("nextPage", pageNumber + 1);
		return new ModelAndView("history","events",result);
	}
}
