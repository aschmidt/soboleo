package de.fzi.ipe.soboleo.servlets;

import static de.fzi.ipe.soboleo.servlets.SessionConstants.COOKIE;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.servlets.controller.ControllerConstants;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.User;

@Component("requestFilter")
public class RequestFilter implements Filter
{
	private final static Logger logger = Logger.getLogger(RequestFilter.class);

	@Autowired
	Server server;

	@Override
	public void destroy()
	{

	}

	@Override
	public void doFilter(ServletRequest rq, ServletResponse rs,
			FilterChain chain) throws ServletException, IOException
	{
		User user = getUser(rq);

		// logger.info("have  user : " +user.getEmail());

		EventSenderCredentials cred = new EventSenderCredentials(user.getURI(),
				user.getName(), user.getKey());

		logger.info("cred loaded: " + cred.getKey());

		Language language = getLanguage(rq, user);
		Space currentSpace = null;
		try
		{
			currentSpace = getSpace(rq, user);
		} catch (EventPermissionDeniedException e)
		{
			logger.error("Permission denied getting space", e);
		}
		
		logger.debug("Space is null? " + (currentSpace == null));

		
		setRequestAttributes(rq, currentSpace, language, user, cred);

		boolean dialogSupport = Boolean.valueOf(
				currentSpace.getProperty(SpaceProperties.DIALOG_SUPPORT))
				.booleanValue();
		rq.setAttribute("dialogSupport", dialogSupport);

		String userLang = user.getDefaultLanguage().getAcronym();
		rq.setAttribute("userLang", userLang);

		// Check the "locale" parameter in URL and set the right locale to
		// enable the internationalisation.
		Locale myLocale = null;

		if (userLang == null)
		{
			myLocale = Locale.US; // Default Locale
			userLang = "en";
		} else
		{
			if (userLang.equals("de"))
			{
				myLocale = Locale.GERMANY;
			} else if (userLang.equals("es"))
			{
				myLocale = new Locale("es", "ES");
			} else
			{
				myLocale = Locale.US;
			}
		}

		if (user.getName().equalsIgnoreCase("Anonymous")
				&& !myLocale.equals(rq.getLocale()))
		{
			myLocale = rq.getLocale();
			userLang = rq.getLocale().getLanguage();
		}

		((HttpServletRequest) rq).getSession(true).setAttribute("myLocale",
				myLocale);
		ResourceBundle bundle = ResourceBundle.getBundle(
				"de.fzi.ipe.soboleo.internationalisation.Soboleo", myLocale);

		for (Enumeration e = bundle.getKeys(); e.hasMoreElements();)
		{
			String key = (String) e.nextElement();
			String s = bundle.getString(key);
			((HttpServletRequest) rq).getSession(true).setAttribute(key, s);
		}

		rq.setAttribute(ControllerConstants.SERVER_URL,
				server.getServerAbsoluteURL());
		chain.doFilter(rq, rs);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException
	{
	}

	public User getUser(ServletRequest r) throws IOException
	{
		HttpServletRequest rq = (HttpServletRequest) r;
		String key = rq.getParameter(ControllerConstants.USER_KEY);
		if (key != null)
		{
			return server.getUserDatabase().getUserForKey(key);
		} else
		{
			Cookie[] cookies = rq.getCookies();
			if (cookies != null)
			{
				for (Cookie cookie : cookies)
				{
					if (cookie.getName().equals(COOKIE))
					{
						key = cookie.getValue();
						// logger.info("key to use: " +key);

						User toReturn = server.getUserDatabase().getUserForKey(
								key);

						if (toReturn != null)
						{
							logger.info("return : found user: "
									+ toReturn.getEmail());

							return toReturn;
						}
					}
				}
			}
			return server.getUserDatabase().getDefaultUser();
		}
	}

	private Language getLanguage(ServletRequest rq, User user)
			throws IOException
	{
		String lang = rq.getParameter(ControllerConstants.USER_LANGUAGE);
		if (lang == null)
			lang = user.getDefaultLanguage().toString();
		return Language.getLanguage(lang);
	}

	public Space getSpace(ServletRequest rq, User user) throws IOException,
			EventPermissionDeniedException
	{
		String spacename = rq.getParameter(ControllerConstants.USER_SPACE);
		if (spacename == null)
			spacename = user.getDefaultSpaceIdentifier();
		return server.getSpaces().getSpace(spacename);
	}

	private void setRequestAttributes(ServletRequest rq, Space space,
			Language language, User user, EventSenderCredentials cred)
	{
		logger.info("Space set to request is null: " + (space == null) );
		rq.setAttribute(ControllerConstants.USER_SPACE, space);
		rq.setAttribute(ControllerConstants.USER_SPACE_URI, space.getURI());
		rq.setAttribute(ControllerConstants.USER_LANGUAGE, language);
		rq.setAttribute(ControllerConstants.USER_TAXONOMY,
				space.getClientSideTaxonomy());
		rq.setAttribute(ControllerConstants.USER_USER, user);
		rq.setAttribute(ControllerConstants.CRED, cred);
		try
		{
			Language spaceLanguage = Language.getLanguage(space
					.getProperty(SpaceProperties.DEFAULT_LANGUAGE));
			Language[] languages = { language, spaceLanguage, Language.en };
			rq.setAttribute("languages", languages);
		} catch (IOException e)
		{
			logger.error("Error assembling languages", e);
		}

	}

}
