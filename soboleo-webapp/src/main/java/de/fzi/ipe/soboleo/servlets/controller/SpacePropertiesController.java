package de.fzi.ipe.soboleo.servlets.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.space.Spaces;

@Controller
@RequestMapping("/admin/spaces/spaceProperties")
public class SpacePropertiesController {

	@Autowired
	protected Spaces spaces;

	protected ModelAndView setPermission(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("setPermission") String s,
			@RequestParam("permissions") String permission) throws IOException, EventPermissionDeniedException {
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.SECURITY, permission);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setLogging(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("sysLogging") String sysLevel,
			@RequestParam("setLogging") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.SYSOUT_LOGGING, sysLevel);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setEventLogging(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("eventLogging") String eventLevel,
			@RequestParam("setEventLogging") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.EVENT_HISTORY_LOGGING, eventLevel);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setDefaultLanguage(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("defaultLanguage") String language,
			@RequestParam("setLanguage") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.DEFAULT_LANGUAGE, language);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setPeopleSearch(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("peopleSearch") String peopleSearch,
			@RequestParam("setPeopleSearch") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.PEOPLE_SEARCH, peopleSearch);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setCommunityAnnotation(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("communityAnnotation") String communityAnnotation,
			@RequestParam("setCommunityAnnotation") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.COMMUNITY_ANNOTATION,
				communityAnnotation);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setDialogSupport(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("dialogSupport") String ds,
			@RequestParam("setDialogSupport") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.DIALOG_SUPPORT, ds);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setAnnotatePeopleDeleteTopic(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("annotatePeopleDeleteTopic") String ds,
			@RequestParam("setAnnotatePeopleDeleteTopic") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.ANNOTATEPEOPLE_DELETABLE_TOPIC, ds);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}

	protected ModelAndView setAnnotateWebPageRating(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("annotateWebPageRating") String ds,
			@RequestParam("setAnnotateWebPageRating") String s) throws IOException, EventPermissionDeniedException

	{
		Space space = spaces.getSpace(spaceName);
		space.setProperty(SpaceProperties.ANNOTATE_WEB_PAGE_RATING, ds);
		spaces.closeSpace(spaceName);
		return new ModelAndView("spaceEdit", "spaceName", spaceName);
	}
}
