package de.fzi.ipe.soboleo.editor.client.conceptDetails;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;

public class ConceptRelationsPanel extends VerticalPanel{
	
	// Internationalization
	private EditorConstants constants = GWT.create(EditorConstants.class); 
	
	public ConceptRelationsPanel(EventDistributor eventDistrib){
		RelationPanel broaderConceptsPanel = new RelationPanel(eventDistrib, SKOS.HAS_BROADER, constants.broaderConcepts());
//		RelationPanel broaderConceptsPanel = new RelationPanel(eventDistrib, SKOS.HAS_BROADER, "Broader Concepts");
		broaderConceptsPanel.setStyleName("conceptDetailsBorderedPanel");
		this.add(broaderConceptsPanel);
		
		RelationPanel narrowerConceptsPanel = new RelationPanel(eventDistrib, SKOS.HAS_NARROWER, constants.narrowerConcepts());
//		RelationPanel narrowerConceptsPanel = new RelationPanel(eventDistrib, SKOS.HAS_NARROWER, "Narrower Concepts");
		narrowerConceptsPanel.setStyleName("conceptDetailsBorderedPanel");
		this.add(narrowerConceptsPanel);

		RelationPanel relatedConceptsPanel = new RelationPanel(eventDistrib, SKOS.RELATED, constants.relatedConcepts());
//		RelationPanel relatedConceptsPanel = new RelationPanel(eventDistrib, SKOS.RELATED, "Related Concepts");
		relatedConceptsPanel.setStyleName("conceptDetailsBorderedPanel");
		this.add(relatedConceptsPanel);

	}
}