package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The element added to this by addProgressEventHandler can receive a ProgressEvent.
 * The added element must implement the ProgressEventHandler.
 * This should be used to update the Progressbar in Upload dialog. 
 * If it is possible to do this.
 * 
 * @author kluge
 *
 */
public class ProgressSender implements HasHandlers,IsSerializable
{
    private HandlerManager handlerManager;

    /**
     * Constructor to generate an Handler. After creating this, you have to add your Object which should be notified if
     * by a ProcessEvent.
     */
    public ProgressSender() 
    {
	handlerManager = new HandlerManager(this);
    }
    @Override
    public void fireEvent(GwtEvent<?> event) 
    {
	handlerManager.fireEvent(event);	
    }

    /**
     * add you object which implements the ProgressEventHander Interface.
     * 
     * @param handler
     * @return HandlerRegistration
     */
    public HandlerRegistration addProgressEventHandler(ProgressEventHandler handler) 
    {
        return handlerManager.addHandler(ProgressEvent.TYPE, handler);
    }
    /**
     * Triggers the Event and you can set the progress.
     * The value must between 0 and 100.
     * @param progress
     */
    public void newProgress(int progress) 
    {
        ProgressEvent event = new ProgressEvent(progress);
        fireEvent(event);
    }



}
