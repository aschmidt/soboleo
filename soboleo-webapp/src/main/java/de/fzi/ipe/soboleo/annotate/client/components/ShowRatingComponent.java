package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PushButton;

import de.fzi.ipe.soboleo.server.client.ClientUtil;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
/**
 * This class is a rating component to rate web pages.
 * It consists of 5 stars.
 * 
 * @author Stephan Kluge
 *
 */
public class ShowRatingComponent extends Composite{

    private double rating = -1;
    private int ratingsMade = 0;
    private int userRating = -1;
    private boolean israted = false;
    private boolean canBeRated;
    private String STAR_SIZE = "22px";// adapt this if the star is not 22px X 22px
    private String STAR_SIZE_5 = "110px";// adapt this if the star is not 22px X 22px
    private final PushButton pbRatingStar1 = new PushButton(ClientUtil.getImageProvider().starNotRated().createImage(),ClientUtil.getImageProvider().starRated().createImage());
    private final PushButton pbRatingStar2 = new PushButton(ClientUtil.getImageProvider().starNotRated().createImage(),ClientUtil.getImageProvider().starRated().createImage());
    private final PushButton pbRatingStar3 = new PushButton(ClientUtil.getImageProvider().starNotRated().createImage(),ClientUtil.getImageProvider().starRated().createImage());
    private final PushButton pbRatingStar4 = new PushButton(ClientUtil.getImageProvider().starNotRated().createImage(),ClientUtil.getImageProvider().starRated().createImage());
    private final PushButton pbRatingStar5 = new PushButton(ClientUtil.getImageProvider().starNotRated().createImage(),ClientUtil.getImageProvider().starRated().createImage());
    /**
     * Creates a Rating Component with 5 Stars.
     * @param totalRating the total rating for this component
     * @param numberOfRatings 
     * @param canBeRated if it is still rated by the person
     */
    public ShowRatingComponent(double totalRating,int numberOfRatings,boolean canBeRated) {
	this.ratingsMade = numberOfRatings;
	this.rating = totalRating;
	this.canBeRated = canBeRated;

	this.userRating=(int)totalRating;
	
	HorizontalPanel horizontalPanel = new HorizontalPanel();
	horizontalPanel.setSize(STAR_SIZE_5, STAR_SIZE);// 110 is STAR_SIZE * 5
	initWidget(horizontalPanel);
	

	if(this.canBeRated)
	{
	    //if this can be rated, a hovering face is needed
	    //only if the person haven't rated yet, the click and Mouse over and out Handler are needed
	    //Rating Star 1
	    initpbRatingStar1();
	    //Rating Star 2
	    initpbRatingStar2();
	    //Rating Star 3
	    initpbRatingStar3();
	    //Rating Star 4
	    initpbRatingStar4();
	    //Rating Star 5
	    initpbRatingStar5();
	}
	else
	{
	    //init show total rating
	    if(this.rating==0)
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    if((this.rating<=0.25)&&(this.rating>0))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedQuarter().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=0.5)&&(this.rating>0.25))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedHalf().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=0.75)&&(this.rating>0.5))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedThreeQuarters().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=1)&&(this.rating>0.75))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=1.25)&&(this.rating>1))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedQuarter().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=1.5)&&(this.rating>1.25))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedHalf().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=1.75)&&(this.rating>1.5))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedThreeQuarters().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=2)&&(this.rating>1.75))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=2.25)&&(this.rating>2))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedQuarter().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=2.5)&&(this.rating>2.25))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedHalf().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=2.75)&&(this.rating>2.5))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedThreeQuarters().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=3)&&(this.rating>2.75))
	    {
	    	
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=3.25)&&(this.rating>3))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedQuarter().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=3.5)&&(this.rating>3.25))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedHalf().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=3.75)&&(this.rating>3.5))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedThreeQuarters().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=4)&&(this.rating>3.75))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	    else if((this.rating<=4.25)&&(this.rating>4))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedQuarter().createImage());
	    }
	    else if((this.rating<=4.5)&&(this.rating>4.25))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedHalf().createImage());
	    }
	    else if((this.rating<=4.75)&&(this.rating>4.5))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedThreeQuarters().createImage());
	    }
	    else if((this.rating<=5)&&(this.rating>4.75))
	    {
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starShowRatedFull().createImage());
	    }
	    //add Hover title with total rating
	    pbRatingStar1.setTitle("Total rating: " + this.rating + " by " + this.ratingsMade + " users rated!");
	    pbRatingStar2.setTitle("Total rating: " + this.rating + " by " + this.ratingsMade + " users rated!");
	    pbRatingStar3.setTitle("Total rating: " + this.rating + " by " + this.ratingsMade + " users rated!");
	    pbRatingStar4.setTitle("Total rating: " + this.rating + " by " + this.ratingsMade + " users rated!");
	    pbRatingStar5.setTitle("Total rating: " + this.rating + " by " + this.ratingsMade + " users rated!");
	    //set new style name with default mouse
	    pbRatingStar1.setStyleName("RatingButtonDisabled");
	    pbRatingStar2.setStyleName("RatingButtonDisabled");
	    pbRatingStar3.setStyleName("RatingButtonDisabled");
	    pbRatingStar4.setStyleName("RatingButtonDisabled");
	    pbRatingStar5.setStyleName("RatingButtonDisabled");
	    
	}
	//add all 5 Star buttons to panel
	addAllStarsToPanel(horizontalPanel);
    }
    
    private void addAllStarsToPanel(HorizontalPanel horizontalPanel) 
    {
	//add Star1 to Panel
	horizontalPanel.add(pbRatingStar1);
	pbRatingStar1.setSize(STAR_SIZE, STAR_SIZE);
	//add Star2 to Panel
	horizontalPanel.add(pbRatingStar2);
	pbRatingStar2.setSize(STAR_SIZE,STAR_SIZE);
	//add Star3 to Panel
	horizontalPanel.add(pbRatingStar3);
	pbRatingStar3.setSize(STAR_SIZE, STAR_SIZE);
	//add Star4 to Panel
	horizontalPanel.add(pbRatingStar4);
	pbRatingStar4.setSize(STAR_SIZE, STAR_SIZE);
	//add Star5 to Panel
	horizontalPanel.add(pbRatingStar5);
	pbRatingStar5.setSize(STAR_SIZE, STAR_SIZE);
	
    }

    
    public void updateRating(double totalRating)
    {
   
    	setUserRating((int)totalRating);
    }

    //Adds Click Mouse over,Mouse out Handler for star 1..5
    private void initpbRatingStar1() 
    {
	pbRatingStar1.addClickHandler(new ClickHandler() {
	    public void onClick(ClickEvent event) 
	    {
		setUserRating(1);
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	});
	pbRatingStar1.addMouseOutHandler(new MouseOutHandler() {
	    public void onMouseOut(MouseOutEvent event) 
	    {
	    }
	});
	pbRatingStar1.addMouseOverHandler(new MouseOverHandler() {
	    public void onMouseOver(MouseOverEvent event) 
	    {

	    }
	});
	

	if (rating >= 1)
	{	
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
	}
	else
	{
		pbRatingStar1.getUpHoveringFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	}
	
	pbRatingStar1.setStyleName("RatingButton");
    }
    private void initpbRatingStar2() 
    {
	pbRatingStar2.addClickHandler(new ClickHandler() {
	    public void onClick(ClickEvent event) 
	    {
		setUserRating(2);
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	});
	pbRatingStar2.addMouseOutHandler(new MouseOutHandler() {
	    public void onMouseOut(MouseOutEvent event) 
	    {
		mouseOut();
	    }
	});
	pbRatingStar2.addMouseOverHandler(new MouseOverHandler() {
	    public void onMouseOver(MouseOverEvent event) 
	    {
		mouseOver(2);
	    }
	});
	
	if (rating >= 2)
	{	
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
	}
	else
	{
		pbRatingStar2.getUpHoveringFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	}
	

	
	
	pbRatingStar2.setStyleName("RatingButton");
    }
    private void initpbRatingStar3() 
    {
	pbRatingStar3.addClickHandler(new ClickHandler() {
	    public void onClick(ClickEvent event) 
	    {
		setUserRating(3);
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	});
	pbRatingStar3.addMouseOutHandler(new MouseOutHandler() {
	    public void onMouseOut(MouseOutEvent event) 
	    {
		mouseOut();
	    }
	});
	pbRatingStar3.addMouseOverHandler(new MouseOverHandler() {
	    public void onMouseOver(MouseOverEvent event) 
	    {
		mouseOver(3);
	    }
	});
	
	if (rating >= 3)
	{	
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
	}
	else
	{
		pbRatingStar3.getUpHoveringFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	}
	
	
	
	
	pbRatingStar3.setStyleName("RatingButton");
    }
    private void initpbRatingStar4() 
    {
	pbRatingStar4.addClickHandler(new ClickHandler() {
	    public void onClick(ClickEvent event) 
	    {
		setUserRating(4);
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
	    }
	});
	pbRatingStar4.addMouseOutHandler(new MouseOutHandler() {
	    public void onMouseOut(MouseOutEvent event) 
	    {
		mouseOut();
	    }
	});
	pbRatingStar4.addMouseOverHandler(new MouseOverHandler() {
	    public void onMouseOver(MouseOverEvent event) 
	    {
		mouseOver(4);
	    }
	});

	if (rating >= 4)
	{	
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
	}
	else
	{
		pbRatingStar4.getUpHoveringFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	}
	
	
	pbRatingStar4.setStyleName("RatingButton");
    }
    private void initpbRatingStar5() 
    {
	pbRatingStar5.addClickHandler(new ClickHandler() {
	    public void onClick(ClickEvent event) 
	    {
		setUserRating(5);
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
	    }
	});
	pbRatingStar5.addMouseOutHandler(new MouseOutHandler() {
	    public void onMouseOut(MouseOutEvent event) 
	    {
		mouseOut();
	    }
	});
	pbRatingStar5.addMouseOverHandler(new MouseOverHandler() {
	    public void onMouseOver(MouseOverEvent event) 
	    {
		mouseOver(5);
	    }
	});
	

	if (rating >= 5)
	{	
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
	}
	else
	{
		pbRatingStar5.getUpHoveringFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	}
	
	pbRatingStar5.setStyleName("RatingButton");
    }
    /**
     * Sets the rating, rated by many people.
     * @param rating
     */
    public void setRating(double rating)
    {
	this.rating = rating;
    }
    /**
     * Returns the current rating of this component
     * @return double 1..5
     */
    public double getRating()
    {
	double newRating = ((this.rating * this.ratingsMade) + this.getUserRating())/(this.ratingsMade + 1); 
	return newRating;
    }
    private void setUserRating(int userRating)
    {
	this.israted = true;
	this.userRating = userRating;
	
    }
    
    public void setInitialUserRating(int userRating)
    {
    	this.userRating = userRating;
    	this.israted = true;
    	if (userRating >= 1)
    	{
    		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
    	}
    	
    	if (userRating >= 2)
    	{
    		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
    	}
    	if (userRating >= 3)
        {
    		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
        }
    	if (userRating >= 4)
        {
    		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
        }
    	if (userRating >= 5)
        { 	
    		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
        }
	
    }
    
    /**
     * This returns the current selected rating of the user.
     * It is negative -1 if the user didn't rate.
     * It is 1-5 if the User rate.
     * @return Integer -1, 0..5
     */
    private int getUserRating()
    {
	return this.userRating;
    }
    private void mouseOver(int overStar)
    {
	switch(overStar)
	{
	case 5:
	    pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	case 4:
	    pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	case 3:
	    pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	case 2:
	    pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starNotRatedHover().createImage());
	    break;	    
	}
    }
    private void mouseOut()
    {
	switch(getUserRating())
	    {
	    case 1:
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		break;
	    case 2:
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		break;
	    case 3:
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		break;
	    case 4:
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		break;
	    case 5:
		pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starRated().createImage());
		break;
		default:
		    pbRatingStar1.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		    pbRatingStar2.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		    pbRatingStar3.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		    pbRatingStar4.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());
		    pbRatingStar5.getUpFace().setImage(ClientUtil.getImageProvider().starNotRated().createImage());

	    }
    }
    /**
     * if the user rated, is returns true
     * @return boolean true,false
     */
    public boolean isRated()
    {
	return this.israted;
    }
}
