package de.fzi.ipe.soboleo.editor.client.conceptDetails;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;

public class ConceptLabelsDescriptionPanel extends VerticalPanel{
	
	// Internationalisation
	private EditorConstants constants = GWT.create(EditorConstants.class);
	
	public ConceptLabelsDescriptionPanel(EventDistributor eventDistrib){
		LabelPanel conceptPanel = new LabelPanel(eventDistrib, SKOS.PREF_LABEL, constants.preferredLabels());
//		LabelPanel conceptPanel = new LabelPanel(eventDistrib, SKOS.PREF_LABEL, "Preferred Labels");
		conceptPanel.setStyleName("conceptDetailsBorderedPanel");
		this.add(conceptPanel);

		LabelPanel synonymsPanel = new LabelPanel(eventDistrib, SKOS.ALT_LABEL, constants.alternativeLabels());
//		LabelPanel synonymsPanel = new LabelPanel(eventDistrib, SKOS.ALT_LABEL, "Alternative Labels");
		synonymsPanel.setStyleName("conceptDetailsBorderedPanel");
		this.add(synonymsPanel);
		
		LabelPanel hiddenLabelsPanel = new LabelPanel(eventDistrib, SKOS.HIDDEN_LABEL, constants.hiddenLabels());
//		LabelPanel hiddenLabelsPanel = new LabelPanel(eventDistrib, SKOS.HIDDEN_LABEL, "Hidden Labels");
		hiddenLabelsPanel.setStyleName("conceptDetailsBorderedPanel");
		this.add(hiddenLabelsPanel);
		
		LabelPanel conceptDescriptionPanel = new LabelPanel(eventDistrib, SKOS.NOTE, constants.descriptions());
//		LabelPanel conceptDescriptionPanel = new LabelPanel(eventDistrib, SKOS.NOTE, "Descriptions");
		conceptDescriptionPanel.setStyleName("conceptDetailsBorderedPanel");
		this.add(conceptDescriptionPanel);

	}

}
