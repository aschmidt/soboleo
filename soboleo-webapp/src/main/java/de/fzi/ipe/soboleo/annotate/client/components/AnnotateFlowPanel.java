package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
/**
 * Extends the FlowPanel. It is used to extend this in HorizontalFlowPanel.
 * This align the panels that they are aligned as a block.
 * 
 * @author Stephan Kluge
 *
 */
public abstract class AnnotateFlowPanel extends FlowPanel {
	protected abstract String getFlowStyle();
	/**
	 * @param w
	 */
	@Override
	public void add(Widget w) {
		w.getElement().getStyle().setProperty("display", getFlowStyle());
		super.add(w);
	}
}


