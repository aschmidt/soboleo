package de.fzi.ipe.soboleo.servlets.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

	@RequestMapping("/")
	protected String index()
	{
		return "index";
	}
	
	@RequestMapping("/annotate")
	protected String annotate()
	{
		return "annotate/annotateDocument";
	}

	@RequestMapping("/annotateDocument")
	protected String annotateDocument()
	{
		return "annotate/annotateDocument";
	}

	@RequestMapping("/annotatePeople")
	protected String annotatePeople()
	{
		return "annotate/annotatePeople";
	}
}
