/*
 * FZI - Information Process Engineering 
 * Created on 03.07.2009 by zach
 */
package de.fzi.ipe.soboleo.editor.client.components;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EditorUtil;
import de.fzi.ipe.soboleo.event.dialog.StartConceptDialog;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

/**
 * Popup window that asks the user for the name of the new concept (or to
 * cancel) and then sends the appropriate command
 */
public class StartDialogPopup extends PopupPanel {

	private TextBox input;
	String selectedURI;
	String title;
	private EditorConstants constants = GWT.create(EditorConstants.class);

	public StartDialogPopup(String selectedURI, String title) {
		super(true);
		this.selectedURI = selectedURI;
		this.title = title;
		setContent();
		show();
	}

	private void setContent() {
		setStyleName("ks-popups-Popup");

		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(5);
		add(vPanel);

		setTitle(constants.startMaturingDialog());
//		setTitle("Start Maturing Dialog");
		Label label = new Label(constants.enterATitle());
//		Label label = new Label("Please enter a title for the dialog");
		vPanel.add(label);

		input = new TextBox();
		input.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getCharCode() == KeyCodes.KEY_ENTER) processStartDialog();
			}			
		});
				
		HorizontalPanel inputPanel = new HorizontalPanel();
		inputPanel.add(input);
		vPanel.add(inputPanel);

		createButtons(vPanel);
	}

	private void createButtons(VerticalPanel vPanel) {
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		hPanel.setSpacing(5);
		
		final Button okButton = new Button(constants.startDialog());
//		final Button okButton = new Button("Start Dialog");
		okButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent ce) {
				processStartDialog();
			}
		});
		hPanel.add(okButton);

		final Button cancelButton = new Button(constants.cancel());
//		final Button cancelButton = new Button("Cancel");
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent ce) {
				StartDialogPopup.this.hide();
			}
		});
		hPanel.add(cancelButton);
		
		vPanel.add(hPanel);
	}

	public void show() {
		setPopupPosition(250, 250);
		super.show();
	}

	private void processStartDialog() {
		if(!input.getText().equals("")) title = input.getText();		
		StartConceptDialog cmd = new StartConceptDialog(selectedURI, title, ClientUtil.getUserLanguage());
		EditorUtil.sendCommand(cmd);
		StartDialogPopup.this.hide();
	}

}
