package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.RESOURCE_URI;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER_SPEL;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.rating.Rating;
import de.fzi.ipe.soboleo.rating.client.RatingService;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;

public class RatingController {
	
	@Autowired protected RatingService ratingService; 
	
	private final static Logger logger = Logger.getLogger(AnalyticsController.class);

	protected ModelAndView doShowRating(
			@RequestParam(RESOURCE_URI) String resourceUri,
			@Value(USER_SPACE_SPEL) Space space,
			@Value(USER_USER_SPEL) User user
			) 
	{
		ModelMap m = new ModelMap();
		
		logger.info("show rating for resource " +resourceUri);
				
		try {
			if(resourceUri.startsWith("doc")) resourceUri = SoboleoNS.DOC_NS + resourceUri;
			Rating rating = ratingService.getOverallRating(user.getKey(), "mailto:" +user.getEmail(), space.getURI(),
					resourceUri);
			
//			logger.info("rating:" +rating.getScore() + " - " +rating.getRatingFrequency());
			
			m.put("ratingvalue", rating.getScore());
			m.put("sumratings", rating.getRatingFrequency());
		} catch (Exception e) {
			logger.error("error calling rating webservice : " +e);
		}

		return new ModelAndView("showRating",m);		
	}

}
