package de.fzi.ipe.soboleo.editor.client.taxonomyArea;

import com.google.gwt.user.client.ui.TreeItem;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.server.client.ClientUtil;



public class EditorSuggestBehavior {
	
	private boolean itemfound=false;
	
	public EditorSuggestBehavior(){	
	}
	
	public void performSelection(TaxonomyTree taxtree,String suggestURI){
		for(int i = 0; i < taxtree.getItemCount(); i++){
			ConceptTreeItem cti = (ConceptTreeItem)taxtree.getItem(i);
//			String treeitemName = cti.getConcept().getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage()).getString();
			if(cti.getConcept().getURI().equals(suggestURI)){
				taxtree.setSelectedItem(taxtree.getItem(i),true);
				taxtree.ensureSelectedItemVisible();
				return;
			}
			else if(cti.getChildCount()>0){
				treeItemHasChild(taxtree, cti,suggestURI);
				if(itemfound) return;
			}
		}
		
	}
	
	private void treeItemHasChild(TaxonomyTree taxtree,TreeItem cti, String suggestURI){
		for(int i = 0; i < cti.getChildCount(); i++){
			ConceptTreeItem child = (ConceptTreeItem)cti.getChild(i);
//			String childName = child.getConcept().getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage()).getString();
			if(child.getConcept().getURI().equals(suggestURI)){
				cti.setState(true);
				child.setSelected(true);
				taxtree.setSelectedItem(child);
				taxtree.ensureSelectedItemVisible();
				itemfound = true;
				return;
			}
			else if(child.getChildCount()>0) {
				treeItemHasChild(taxtree, child,suggestURI);
				if(itemfound) return;
			}
		}		
	}
	public void setItemfound(boolean bool){
		itemfound = bool;
	}
}
