package de.fzi.ipe.soboleo.editor.client.conceptDetails;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.PushButton;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.editor.client.components.EditLabelComponent;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class LabelLanguageComponent extends FlexTable{
	
	private Language lang;
	private LabelPanel parent;
	private ArrayList<LocalizedString> tableRows = new ArrayList<LocalizedString>();
	
	public LabelLanguageComponent(Language lang, LabelPanel parent){
		this.lang = lang;
		this.parent = parent;
		setStyleName("labelLanguageComponent");

	    FlexCellFormatter cellFormatter = getFlexCellFormatter();
	    cellFormatter.setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_RIGHT);
	    final HTML langHTML = new HTML(lang.getAcronym());
	    langHTML.setStyleName("language");
	    setWidget(0, 0, langHTML);
	    cellFormatter.setColSpan(0, 0, 4);
	}
	
	  /**
	   * Add a new row for the given label.
	   */
	  public void addLabel(final LocalizedString label) {
		int numRows = insertRowData(label);
	    boolean isTextArea = false;
	    if(parent.getSkosType().equals(SKOS.NOTE)) isTextArea = true;
		final EditLabelComponent labelHTML = new EditLabelComponent(label.getString(), isTextArea);
		labelHTML.setStyleName("labelHTML");
		labelHTML.addKeyUpHandler(new EnterKeyUpHandler(labelHTML, label, isTextArea));
		
		PushButton editButton = new PushButton(ClientUtil.getImageProvider().pen().createImage(), new ClickHandler(){
			@Override
			public void onClick(ClickEvent arg0) {
				labelHTML.startEditing();
			}
	    });
		PushButton removeButton = new PushButton(ClientUtil.getImageProvider().crossMouseOut2().createImage(),ClientUtil.getImageProvider().crossMouseOver().createImage(), new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				parent.sendRemoveTextCmd(label);
			}
	    });
				
		if(numRows <= getRowCount()) insertRow(numRows);
		setWidget(numRows, 0, labelHTML);			   
	    setWidget(numRows, 1, editButton);	    
	    setWidget(numRows, 2, removeButton);
	  }

	  /**
	   * Remove a the row for the given label.
	   * 
	   */
	 public void removeLabel(LocalizedString toRemove) {
		 int i = tableRows.indexOf(toRemove);
		 if(tableRows.remove(toRemove)) removeRow(i+1);
	  }
	  
	  private int insertRowData(LocalizedString newLabel){
		  tableRows.add(newLabel);
		  Collections.sort(tableRows, new Comparator<LocalizedString>() {
				public int compare(LocalizedString one, LocalizedString two) {
					return (one.getString().compareTo(two.getString()));					
				}});
		  // + 1 for the header row
		  return tableRows.indexOf(newLabel)+1; 
	  }
	  
		private final class EnterKeyUpHandler implements KeyUpHandler {
			private final EditLabelComponent labelHTML;
			private final LocalizedString label;
			private final boolean isTextArea;

			private EnterKeyUpHandler(EditLabelComponent labelHTML,
					LocalizedString label, boolean isTextArea) {
				this.labelHTML = labelHTML;
				this.label = label;
				this.isTextArea = isTextArea;
			}

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(isTextArea){
					if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER && event.isControlKeyDown()){
						parent.sendChangeTextCmd(label, new LocalizedString(labelHTML.getText(), lang));
					}
				}
				else {
					if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER){
						parent.sendChangeTextCmd(label, new LocalizedString(labelHTML.getText(), lang));
					}
				}
			}
		}
}
