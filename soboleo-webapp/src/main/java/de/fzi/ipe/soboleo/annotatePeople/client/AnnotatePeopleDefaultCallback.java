package de.fzi.ipe.soboleo.annotatePeople.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class AnnotatePeopleDefaultCallback implements AsyncCallback<Object>{

	public static final AnnotatePeopleDefaultCallback INSTANCE = new AnnotatePeopleDefaultCallback();
	
	private AnnotatePeopleDefaultCallback() {;}
	
	public void onSuccess(Object result) {
		;
	}

	public void onFailure(Throwable caught) {
		AnnotatePeopleMain.handleException(caught);
	}
	

}
