/*
 * Ontoprise code for project ksi_underground
 * Created on 25.07.2006 by zach
 */
package de.fzi.ipe.soboleo.annotate.client.components;

public interface CompletionItems {
        /**
         * Returns an array of all completion items matching
         * @param match The user-entered text all completion items have to match
         * @return      Array of strings
         */
        public String[] getCompletionItems(String match); 


        public boolean contains(String s);
        
}