package de.fzi.ipe.soboleo.annotation.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DecoratedTabPanel;
import com.google.gwt.user.client.ui.Label;

import com.google.gwt.user.client.ui.RootPanel;

import de.fzi.ipe.soboleo.annotate.client.AnnotateMain;
import de.fzi.ipe.soboleo.annotate.client.AnnotationWebsiteController;
import de.fzi.ipe.soboleo.annotateDocuments.client.AnnotateDocumentController;
import de.fzi.ipe.soboleo.annotateDocuments.client.FileUploadPanel;
//import de.fzi.ipe.soboleo.annotateDocuments.client.FileUploadPanel;
//import de.fzi.ipe.soboleo.annotatePeople.client.AnnotatePeopleMain;
import de.fzi.ipe.soboleo.annotatePeople.client.AnnotatePeopleController;
import de.fzi.ipe.soboleo.annotatePeople.client.AnnotatePeopleMain;


public class AnnotationPanel implements EntryPoint {

	
	

    public void onModuleLoad() {
     	
    	
    
    	
    	String duri = Window.Location.getParameter("duri");
    	String page = Window.Location.getParameter("page");
    	
    	if (page != null)
    	{
    		//Window.alert("page: " +page);
    	}
    	
    	
		DecoratedTabPanel selectResourcePanel = new DecoratedTabPanel();
		selectResourcePanel.setStyleName("conceptDetailsTabPanel");
		    	
    	AnnotateMain am=new AnnotateMain(new AnnotationWebsiteController());
    	
    	selectResourcePanel.add(am, "Webpages");
    	
    	if (duri == null)
    	{
    		// set upload panel for a new doc
	    	FileUploadPanel fu=new FileUploadPanel();    	
	    	selectResourcePanel.add(fu, "Office documents");
    	}
    	else
    	{
    		// set panel to edit existing document
    		de.fzi.ipe.soboleo.annotateDocuments.client.AnnotateMain amd=new de.fzi.ipe.soboleo.annotateDocuments.client.AnnotateMain(new AnnotateDocumentController(),duri);
    		selectResourcePanel.add(amd, "Office documents");
    	}

    	AnnotatePeopleMain ap=new AnnotatePeopleMain(new AnnotatePeopleController());
    
    	selectResourcePanel.add(ap, "People");
    	
    	
    	if (page == null)
    	{
    		selectResourcePanel.selectTab(2);
    	}
    	else
    	{
	    	if (page.equals("od"))
	    	{
	    		selectResourcePanel.selectTab(1);
	    	}
	    	
	    	if (page.equals("wd"))
	    	{
	    		selectResourcePanel.selectTab(0);
	    	}
    	}
    	
    	selectResourcePanel.setHeight("400px");
    	
    	RootPanel.get("annotation").add(selectResourcePanel);	
    
    	
    	
    }


}
