package de.fzi.ipe.soboleo.editor.client.taxonomyArea;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.editor.client.EditorEventListener;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.ChangeConceptSelectionEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyChangedEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyLoadedEvent;
import de.fzi.ipe.soboleo.event.editor.TreeLoadedEvent;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.ComplexTaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.TaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveTaxonomyChangeCommand;
import de.fzi.ipe.soboleo.server.client.ClientUtil;


public class TaxonomyTree extends Tree implements EditorEventListener{

	private EventDistributor eventDistrib;
	
	private ClientSideTaxonomy tax;
	
	private static class PendingItem extends TreeItem {
		public PendingItem() {
			super("Loading Taxonomy...");
		}
	}

	public TaxonomyTree(EventDistributor eventDistrib) {
		this.eventDistrib = eventDistrib;
		eventDistrib.addListener(this);
		addItem(new PendingItem());
	}
	
	private void setTaxonomy(ClientSideTaxonomy newTax) {
		this.tax = newTax;
		this.removeItems();
		Iterator<ClientSideConcept> it = tax.getSortedRootConcepts(ClientUtil.getPrototypicalConceptsName(), ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage()).iterator();
		
		while (it.hasNext()) {
			ClientSideConcept c = it.next();
			ConceptTreeItem treeitem = new ConceptTreeItem(c,tax);
			addItem(treeitem);
		}
		eventDistrib.notify(new TreeLoadedEvent());
	}

	public void notify(Event event) {
		if (event instanceof TaxonomyLoadedEvent) {
			setTaxonomy(((TaxonomyLoadedEvent) event).getTaxonomy());
		}
		else if (event instanceof TaxonomyChangedEvent) {
			ComplexTaxonomyChangeCommand cmd = ((TaxonomyChangedEvent)event).getCommand();
			if (cmd instanceof RemoveConceptCmd) {
				checkRoots();
			}
			else if (cmd instanceof CreateConceptCmd) {
				checkRoots();
			}
			else if (cmd instanceof AddConnectionCmd) {
				AddConnectionCmd addConnectionCmd = (AddConnectionCmd) cmd;
				if ((addConnectionCmd.getConnection() == SKOS.HAS_BROADER) || (addConnectionCmd.getConnection() == SKOS.HAS_NARROWER)) checkRoots();
			}
			else if (cmd instanceof RemoveConnectionCmd) {
				RemoveConnectionCmd removeConnectionCmd = (RemoveConnectionCmd) cmd;
				if ((removeConnectionCmd.getConnection() == SKOS.HAS_BROADER) || (removeConnectionCmd.getConnection() == SKOS.HAS_NARROWER)) checkRoots();
			}
			processImpliedCommands(cmd);
		}
		else if(event instanceof ChangeConceptSelectionEvent){
			ChangeConceptSelectionEvent setSelectionEvent = (ChangeConceptSelectionEvent) event;
			updateSelection(setSelectionEvent.getConcept());
		}
	}
	
	private void updateSelection(ClientSideConcept toSelect) {
		Iterator<TreeItem> it = this.treeItemIterator();
		while(it.hasNext()){
			ConceptTreeItem cti = (ConceptTreeItem) it.next();
			if(cti.getConcept().hasEqualValues((toSelect))){
				setSelectedItem(cti,true);
				ensureSelectedItemVisible();
			}
		}		
	}

	private void processImpliedCommands(ComplexTaxonomyChangeCommand complexCommand) {
		for (TaxonomyChangeCommand cmd:complexCommand.getImpliedCommands()) {
			if (cmd instanceof ComplexTaxonomyChangeCommand) { 
				processImpliedCommands((ComplexTaxonomyChangeCommand) cmd);
			}
			else {
				PrimitiveTaxonomyChangeCommand primitiveCommand = (PrimitiveTaxonomyChangeCommand) cmd;
				ClientSideConcept affected = tax.get(primitiveCommand.getFromURI());
				for (int i=0;i<getItemCount();i++) {
					ConceptTreeItem current = (ConceptTreeItem) getItem(i);
					current.update(affected,tax,this);
				}
			}
		}
	}
	
	
	/**
	 * Checks whether the current root concepts are still the correct ones (and adds / removes if necessary)
	 */
	private void checkRoots() {
		Collection<ClientSideConcept> roots = tax.getSortedRootConcepts(ClientUtil.getPrototypicalConceptsName(), ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage());

		//check whether currentRoots contains something that isn't in roots - remove, if necessary.
		boolean changed;
		do {
			changed = false;
			for (int i=0; i < getItemCount(); i++) {
				if (!roots.contains(((ConceptTreeItem)getItem(i)).getConcept())) {
					if (getItem(i) == getSelectedItem()) {
						setSelectedItem(null,true);
					}
					removeItem(getItem(i));
					changed=true;
					break;
				}
			}
		} while (changed);
		
		//check whether new roots need to be added 
		Set<ClientSideConcept> currentRoots = new HashSet<ClientSideConcept>();
		for (int i = 0; i < getItemCount(); i++) {
			currentRoots.add(((ConceptTreeItem)getItem(i)).getConcept());
		}
		for (ClientSideConcept current: roots) {
			if (!currentRoots.contains(current)) {
				ConceptTreeItem treeItem = new ConceptTreeItem(current,tax);
				addItem(treeItem);
			}
		}
	}
	

	public ClientSideTaxonomy getTaxonomy() {
		return tax;
	}


	public void onBrowserEvent(com.google.gwt.user.client.Event event) {
		// Preven Collision with browser --> CTRL+N = new browser window, CTRL+D = new bookmark within Firefox
		if (DOM.eventGetCtrlKey(event)) {
			if (DOM.eventGetKeyCode(event) == 'D' || DOM.eventGetKeyCode(event)== 'N') {
				DOM.eventPreventDefault(event);
			}
		}
		super.onBrowserEvent(event);
	}
//	public DnDTreeController getTreeController(){
//		return treeController;
//	}

}
