package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.SEARCH_PARAMETER;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;

@Controller
public class AutocompleteController {
	private final static Logger logger = Logger
			.getLogger(AutocompleteController.class);

	@Autowired protected Server server;

	protected String style = "overflow:auto;height:60px;background:#ffeec1";
	
	@RequestMapping("/autocomplete")
	public void doAutocomplete(@RequestParam(SEARCH_PARAMETER) String query,
			@Value(USER_SPACE_SPEL) Space space,
			HttpServletResponse rs) 
	{
		logger.info("autocomplete search for : " + query);
		ClientSideTaxonomy tax = space.getClientSideTaxonomy();

		try {
			PrintWriter out = rs.getWriter();
			out.write("<ul style=\"" + style + "\">");

			for (ClientSideConcept concept : tax.getConcepts()) {

				for (LocalizedString ls : concept.getTexts(SKOS.PREF_LABEL,
						SKOS.ALT_LABEL, SKOS.HIDDEN_LABEL)) {
					if (ls.getString().startsWith(query)) {

						out.write("<li style=\"text-align:left\">"
								+ ls.getString() + "</li>");

						// logger.info("add word : " + ls.getString());
					}
				}
			}
			out.write("</ul>");
		} catch (IOException e) {
		}
	}

	@RequestMapping("/autocomplete-people")
	public void doAutocompletePeople(@RequestParam(SEARCH_PARAMETER) String query,
			@Value(USER_SPACE_SPEL) Space space,
			HttpServletResponse rs) 
	{
		logger.info("autocomplete search for : " + query);
		
		ClientSideTaxonomy tax = space.getClientSideTaxonomy();

		try {
			PrintWriter out = rs.getWriter();
			out.write("<ul style=\"" + style + "\">");

			for (ClientSideConcept concept : tax.getConcepts()) {

				for (LocalizedString ls : concept.getTexts(SKOS.PREF_LABEL,
						SKOS.ALT_LABEL, SKOS.HIDDEN_LABEL)) {
					if (ls.getString().startsWith(query)) {

						out.write("<li style=\"text-align:left\">"
								+ ls.getString() + "</li>");

						// logger.info("add word : " + ls.getString());
					}
				}
			}
			out.write("</ul>");
		} catch (IOException e) {
		}
	}

}
