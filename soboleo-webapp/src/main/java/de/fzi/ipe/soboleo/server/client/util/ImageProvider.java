package de.fzi.ipe.soboleo.server.client.util;

import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.ImageBundle;

public interface ImageProvider extends ImageBundle {

	/**
	 * Returns the resource name of the pen image.
	 * @return AbstractImagePrototype
	 * @Resource ("pen.png")
	 */
	 public AbstractImagePrototype pen();
	 
	 /**
	  * Returns the resource name of the red cross image.
	  * Size 21px X 21px. Color red.
	  * @return AbstractImagePrototype
	  * @Resource ("crossMouseOut.png")
	  */
	 public AbstractImagePrototype cross();
	 
	 /**
	  * Returns the resource name of the brown cross image,
	  *  if the mouse is not over the cross.
	  * Size 21px X 21px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("crossMouseOut.png")
	  */
	 public AbstractImagePrototype crossMouseOut();
	 /**
	  *  Returns the resource name of the brown cross image,
	  *  if the mouse is over the cross.
	  *  Size 21px X 21px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("crossMouseOver.png")
	  */
	 public AbstractImagePrototype crossMouseOver();
	 /**
	  *  Returns the resource name of the brown cross image,
	  *  if the mouse is over the cross.
	  *  Size 21px X 21px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("crossMouseOutSmall.png")
	  */
	 public AbstractImagePrototype crossMouseOutSmall();
	 /**
	  *  Returns the resource name of the brown cross image,
	  *  if the mouse is over the cross.
	  *  Size 21px X 21px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("crossMouseOverSmall.png")
	  */
	 public AbstractImagePrototype crossMouseOverSmall();
	 
	 /**
	  *  Returns the resource name of the brown cross image,
	  *  if the mouse is over the cross.
	  *  Size 21px X 21px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("crossMouseOut2.png")
	  */
	 public AbstractImagePrototype crossMouseOut2();
	
	 /**
	  *  Returns the resource name of the brown cross image,
	  *  if the mouse is over the cross.
	  *  Size 21px X 21px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("crossMouseOut2Small.png")
	  */
	 public AbstractImagePrototype crossMouseOut2Small();
	 /**
	  *  Returns the resource name of the brown Star image only 
	  *  as a brown border without fill color.
	  *  It is shown, when the star was not clicked by the user and the ratings of the other users
	  *  did not reach it.
	  *  Rating is 4, so the 5. star will be this kind of star.
	  *  Size 40px X 40px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("starNotRated.png")
	  */
	 public AbstractImagePrototype starNotRated();
	 /**
	  *  Returns the resource name of the brown Star image only 
	  *  as a brown border with bright brown fill color.
	  *  It is shown, when the user hovers over the star.
	  *  
	  *  Size 40px X 40px. Color dark brown, bright brown.
	  * @return AbstractImagePrototype
	  * @Resource ("starNotRatedHover.png")
	  */
	 public AbstractImagePrototype starNotRatedHover();
	 /**
	  *  Returns the resource name of the brown Star image dark brown star.
	  *  It is shown, when the user clicked this star.
	  *  
	  *  Size 40px X 40px. Color dark brown.
	  * @return AbstractImagePrototype
	  * @Resource ("starRated.png")
	  */
	 public AbstractImagePrototype starRated();
	 /**
	  *  Returns the resource name of the brown Star image dark brown 
	  *  star with light brown fill color.
	  *  It is shown in the overview, if the user have rated this web page.
	  *  It shows the full star, which means, that this is the full note.
	  *  Example: If the first two shows this it is rated as 2.
	  *  
	  *  Size 40px X 40px. Color dark brown, light brown inside.
	  * @return AbstractImagePrototype
	  * @Resource ("starShowRatedFull.png")
	  */
	 public AbstractImagePrototype starShowRatedFull();
	 /**
	  *  Returns the resource name of the brown Star image dark brown 
	  *  star with light brown fill color. But only filled till the half of the star.
	  *  Filled on the left side.
	  *  It is shown in the overview, if the user have rated this web page.
	  *  It shows the half star, which means, that this is the half note.
	  *  Example: If the second star shows this it is rated as 1,5.
	  *  
	  *  Size 40px X 40px. Color dark brown, light brown inside.
	  * @return AbstractImagePrototype
	  * @Resource ("starShowRatedHalf.png")
	  */
	 public AbstractImagePrototype starShowRatedHalf();
	 /**
	  *  Returns the resource name of the brown Star image dark brown 
	  *  star with light brown fill color. But only filled till the first quarter of the star.
	  *  Filled on the left side.
	  *  It is shown in the overview, if the user have rated this web page.
	  *  It shows the first quarter of the star, which means, that this is a quarter note.
	  *  Example: If the second star shows this it is rated as 1,25.
	  *  
	  *  Size 40px X 40px. Color dark brown, light brown inside.
	  * @return AbstractImagePrototype
	  * @Resource ("starShowRatedQuarter.png")
	  */
	 public AbstractImagePrototype starShowRatedQuarter();
	 /**
	  *  Returns the resource name of the brown Star image dark brown 
	  *  star with light brown fill color. But only filled till the three quarters of the star.
	  *  Filled on the left side.
	  *  It is shown in the overview, if the user have rated this web page.
	  *  It shows the three quarters of the star, which means, that this is a three quarter note.
	  *  Example: If the second star shows this it is rated as 1,75.
	  *  
	  *  Size 40px X 40px. Color dark brown, light brown inside.
	  * @return AbstractImagePrototype
	  * @Resource ("starShowRatedThreeQuarters.png")
	  */
	 public AbstractImagePrototype starShowRatedThreeQuarters();
}
