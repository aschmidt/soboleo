package de.fzi.ipe.soboleo.editor.client.messageArea;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EditorEventListener;
import de.fzi.ipe.soboleo.editor.client.EditorUtil;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.LoadTaxonomyEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyLoadedEvent;
import de.fzi.ipe.soboleo.event.message.ChatCommand;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class ChatMessagePanel extends VerticalPanel implements EditorEventListener{

	private ChatContent chatContent;
	private TextBox chatInput;
	private Button send;
	private ClientSideTaxonomy tax;
	private EditorConstants constants = GWT.create(EditorConstants.class);
		
	public ChatMessagePanel(){
		setStyleName("chatPanel");
		chatContent = new ChatContent();
		add(chatContent);
		
		createEditPanel();
	}

	private void createEditPanel() {

		chatInput = new TextBox();
		chatInput.setWidth("250px");
		chatInput.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getCharCode() == KeyCodes.KEY_ENTER) sendChat();				
			}
			
		});
		add(chatInput);
		setCellHorizontalAlignment(chatInput, VerticalPanel.ALIGN_CENTER);

		send = new Button();
		send.setStyleName("buttonPanel");
		send.setText(constants.send());
//		send.setText("send");
		send.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				sendChat();
			}
		});
		add(send);
		setCellHorizontalAlignment(send,VerticalPanel.ALIGN_CENTER);
	}
	
	private void sendChat() {
		ChatCommand event = new ChatCommand(chatInput.getText(), ClientUtil.getUserName());
		EditorUtil.sendCommand(event);
		chatInput.setText("");
	}

	public void notify(Event event) {
		if (event instanceof ChatCommand) {
			ChatCommand chatEvent = (ChatCommand) event;
			chatContent.addMessage(chatEvent);
		}
		else if (event instanceof TaxonomyLoadedEvent) {
			TaxonomyLoadedEvent loadedEvent = (TaxonomyLoadedEvent) event;
			this.tax = loadedEvent.getTaxonomy();
		}
		else if (event instanceof LoadTaxonomyEvent) {
			chatContent.clear();
			
		}
	}
	
	private class ChatContent extends ScrollPanel {
		
		final int MAX_NUMBER_MESSAGES = 40;
		
		VerticalPanel vPanel = new VerticalPanel();
		int currentNumberOfMessages = 0;
		
		public ChatContent() {
			setWidget(vPanel);
			setWidth("250px");
			setHeight("560px");
		}
		void addMessage(ReadableEditorEvent event) {
			currentNumberOfMessages ++;
			Label currentMessage = new Label(event.getReadable(ClientUtil.getUserLanguage(), tax),true);
			currentMessage.setWordWrap(true);
			currentMessage.setStyleName("chatMessage");
			if (event.getSenderURI().equals(ClientUtil.getEventSenderCredentials().getSenderURI())) { 
				currentMessage.setStyleName("chatMessageOwn");
			}
			else {
				currentMessage.setStyleName("chatMessage");
			}
			
			
			vPanel.add(currentMessage);
			if (currentNumberOfMessages > MAX_NUMBER_MESSAGES) vPanel.remove(0);
			ensureVisible(currentMessage);
		}
		
		public void clear() {
			vPanel.clear();
			currentNumberOfMessages = 0;
		}
		
	}
	
	
}
