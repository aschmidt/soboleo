package de.fzi.ipe.soboleo.editor.client.components;

import com.google.gwt.user.client.ui.ListBox;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;

public class LanguageDropDownBox extends ListBox {

	public LanguageDropDownBox(){
		for(Language lang: Language.values()){
			this.addItem(lang.getAcronym());
		}
	}
	
	public LanguageDropDownBox(Language toSelect){
		for(Language lang: Language.values()){
			this.addItem(lang.getAcronym());
			if(lang.equals(toSelect)) this.setSelectedIndex(this.getItemCount()-1); 
		}
	}
	
}
