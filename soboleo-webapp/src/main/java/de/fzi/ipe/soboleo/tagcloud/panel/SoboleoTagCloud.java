package de.fzi.ipe.soboleo.tagcloud.panel;

import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.client.ClientUtil;
import de.fzi.ipe.soboleo.tagcloud.rpc.TagCloudService;
import de.fzi.ipe.soboleo.tagcloud.rpc.TagCloudServiceAsync;

public class SoboleoTagCloud implements EntryPoint {

	
	private String spacename;
	private LocalizedString.Language userLanguage;
	private LocalizedString.Language spaceLanguage;
	private EventSenderCredentials creds;
	
	public static final String SOBOLEO_NS = "http://soboleo.com/ns/1.0#";
	
	public final static String ANALYTICS_SEARCH_REQUEST = "sr";
	public final static String ANALYTICS_ANNOTATIONS = "tags";
	public final static String PROFILE_AGGREGATED_PERSON_TAGS = "apt";
	public final static String PROFILE_ACTIVITY_CONCEPTS = "ac";
	// command sr = searchrequest | tags = tags for people | ua = user annotations as cloud
	String command;

	// userkey: the userkey to show the annotations if command = ua
	String userkey;
	
	String taggedPersonURI;
	int days;
	
	public void onModuleLoad() {
		
		try{
			command = Window.Location.getParameter("command");
		} catch(Exception e){
			command="sr";
		}
		
		try{			
			days=Integer.parseInt(Window.Location.getParameter("days"));
		}catch(Exception e){
			days=30;
		}
	
		
		
		try{
			taggedPersonURI = SoboleoTagCloud.SOBOLEO_NS + Window.Location.getParameter("taggedPerson");
			System.out.println("taggedPersonURI: " +taggedPersonURI);
		}catch(Exception e){
			
			
		}
		
		try
		{			
			userkey=Window.Location.getParameter("userkey");
		}
		catch(Exception e)
		{
			userkey=null;
		}
		
		
		
		System.out.println("command: " +command);

		// required parameters: space, type
		ClientUtil.initConfig(new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
				Window.alert("error: " +caught);
			}

			public void onSuccess(Void result) {
				spacename = ClientUtil.getSpaceName();
				creds = ClientUtil.getEventSenderCredentials();
				userLanguage = ClientUtil.getUserLanguage();
				spaceLanguage = ClientUtil.getSpaceLanguage();
				
				if (command.equals(SoboleoTagCloud.ANALYTICS_SEARCH_REQUEST)){
					loadTagCloudAnalyticsSearchRequests();
				}
				if (command.equals(SoboleoTagCloud.ANALYTICS_ANNOTATIONS)){
					System.out.println("loadTagCloudAnalyticsAnnotations");
					loadTagCloudAnalyticsAnnotations();
				}

				if (command.equals("ua"))
				{
					loadTagCloudUserAnnotations();
				}

				if(command.equals(SoboleoTagCloud.PROFILE_AGGREGATED_PERSON_TAGS)){
					System.out.println("loadTagCloudAggregatedPesonTags");
					loadTagCloudAggregatedPersonTags();
				}

			}
		});		
	}
	
	
	public void generateTagCloud(List<ConceptStatistic> statistic)
	{
		TagCloud tc=new TagCloud();
		
		// initialize the tags
		for (ConceptStatistic ct:statistic)
		{
			LocalizedString prefLabel = ct.getConcept().getBestFitText(SKOS.PREF_LABEL,userLanguage, spaceLanguage, Language.en);

			
			WordTag wt=new WordTag(prefLabel.getString(), "taxonomy?space=" + spacename +"&concept="+ct.getConceptId().substring(ct.getConceptId().indexOf("#")+1));
			tc.addNewWord(wt, ct.getCount());
		}
		
		
		RootPanel.get("tagcloud").add(tc);

	}

	

	
	public void loadTagCloudUserAnnotations()
	{
		final Label status=new Label("load tag cloud userannotations for user: " + userkey);
		RootPanel.get("tagcloud").add(status);
		
		getTagCloudService().getUserAnnotations(userkey, spacename, userLanguage, creds
				, new AsyncCallback<List<ConceptStatistic>>() {
			public void onSuccess(List<ConceptStatistic> statistic) {
				status.setText("done, loadead concepts : " +statistic.size() + " userkey: " +userkey);
				
				RootPanel.get("tagcloud").remove(status);
				generateTagCloud(statistic);
			}

			public void onFailure(Throwable caught) {
				status.setText("error occured : " +caught.getLocalizedMessage());
				
			}
		});
	}
	

	
	public void loadTagCloudAnalyticsSearchRequests()
	{
		final Label status=new Label("load tag cloud searchrequest for space : " + spacename);
		RootPanel.get("tagcloud").add(status);
		
		getTagCloudService().getSearchRequestsOverall(spacename, days, userLanguage
				, new AsyncCallback<List<ConceptStatistic>>() {
			public void onSuccess(List<ConceptStatistic> statistic) {
				//status.setText("done, loadead concepts : " +statistic.size());
				
				RootPanel.get("tagcloud").remove(status);
				generateTagCloud(statistic);
			}

			public void onFailure(Throwable caught) {
			
			}
		});
	}
	
	
	public void loadTagCloudAnalyticsAnnotations()
	{
		final Label status=new Label("load tag cloud tags for space : " + spacename);
		RootPanel.get("tagcloud").add(status);
		
		getTagCloudService().getUsedConceptsInPersonAnnotations(spacename, days, userLanguage, creds
				, new AsyncCallback<List<ConceptStatistic>>() {
			public void onSuccess(List<ConceptStatistic> statistic) {
				//status.setText("done, loadead concepts : " +statistic.size());
				
				RootPanel.get("tagcloud").remove(status);
				generateTagCloud(statistic);
			}

			public void onFailure(Throwable caught) {
			
			}
		});
	}
	
	public void loadTagCloudAggregatedPersonTags(){
		final Label status=new Label("load tag cloud tags for tagged person : " + taggedPersonURI);
		RootPanel.get("tagcloud").add(status);
		
		getTagCloudService().getAggregatedPersonTags(taggedPersonURI, spacename, userLanguage, creds
				, new AsyncCallback<List<ConceptStatistic>>() {
			public void onSuccess(List<ConceptStatistic> statistic) {
				//status.setText("done, loadead concepts : " +statistic.size());
				
				RootPanel.get("tagcloud").remove(status);
				generateTagCloud(statistic);
			}

			public void onFailure(Throwable caught) {
			
			}
		});
	}
	
	
	static TagCloudServiceAsync tagcloudService;
	
	/**
	 * Returns the TagCloudServiceAsync Object 
	 * 
	 * @return TagCloudServiceAsync
	 */
	public static TagCloudServiceAsync getTagCloudService() {
		if (tagcloudService == null) {
			tagcloudService = (TagCloudServiceAsync) GWT
					.create(TagCloudService.class);
			ServiceDefTarget endpoint = (ServiceDefTarget) tagcloudService;
			endpoint.setServiceEntryPoint(ClientUtil.getServerURL()
					+ "tagcloud/service");
		}
		return tagcloudService;
	}
	
	
}
