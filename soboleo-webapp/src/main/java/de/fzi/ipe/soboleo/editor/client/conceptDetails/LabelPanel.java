package de.fzi.ipe.soboleo.editor.client.conceptDetails;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EditorEventListener;
import de.fzi.ipe.soboleo.editor.client.EditorMain;
import de.fzi.ipe.soboleo.editor.client.EditorUtil;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.editor.client.components.LanguageDropDownBox;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.ConceptSelectionChangedEvent;
import de.fzi.ipe.soboleo.event.editor.LoadTaxonomyEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyChangedEvent;
import de.fzi.ipe.soboleo.event.ontology.AddTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ChangeTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ComplexTaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.RemoveTextCmd;
import de.fzi.ipe.soboleo.event.ontology.TaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveAddText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveChangeText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveConcept;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveTaxonomyChangeCommand;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class LabelPanel extends CaptionPanel implements EditorEventListener{

	private VerticalPanel contentPanel;
	private TextBox inputText;
	private LanguageDropDownBox langDropDownBox;
	private Button add;
	private VerticalPanel languagesPanel;
	
	private ClientSideConcept currentItem = null;
	private SKOS skosType;
	private HashMap<Language, LabelLanguageComponent> langTables = new HashMap<Language, LabelLanguageComponent>();
	
	// Internationalisation
	EditorConstants constants = GWT.create(EditorConstants.class);
	
	public LabelPanel(EventDistributor eventDistrib, SKOS skosType, String caption){
		eventDistrib.addListener(this);
		setCaptionText(caption);
		this.skosType = skosType;
		
		contentPanel = new VerticalPanel();
		languagesPanel = new VerticalPanel();
		contentPanel.add(languagesPanel);
		contentPanel.add(createInputPanel());
		setContentWidget(contentPanel);
	}

	private HorizontalPanel createInputPanel() {
		HorizontalPanel inputPanel = new HorizontalPanel();
		inputPanel.setStylePrimaryName("inputPanel");
		inputPanel.setSpacing(2);
		
		inputText = new TextBox();
		inputText.setWidth(EditorMain.EDITOR_INPUT_FIELD_WIDTH);
		inputText.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendAddTextCmd();
				}
			}});
		inputPanel.add(inputText);

		langDropDownBox = new LanguageDropDownBox(ClientUtil.getUserLanguage());
		inputPanel.add(langDropDownBox);
			
		add = new Button();
		add.setText(constants.add());
//		add.setText("add");
		add.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				sendAddTextCmd();
			}
		});
		inputPanel.add(add);
		
		return inputPanel;
	}

	protected void sendRemoveTextCmd(LocalizedString toRemove) {
		if (currentItem != null) {
			RemoveTextCmd command = new RemoveTextCmd(currentItem.getURI(),skosType,toRemove);
			EditorUtil.sendCommand(command);
		}
	}
	
	protected void sendChangeTextCmd(LocalizedString oldText, LocalizedString newText){
		if(currentItem != null){
			ChangeTextCmd command = new ChangeTextCmd(currentItem.getURI(), skosType, oldText, newText);
			EditorUtil.sendCommand(command);
		}
	}
	
	private void sendAddTextCmd() {
		if (currentItem != null) {
			LocalizedString currentText = new LocalizedString(inputText.getText(), langDropDownBox.getItemText(langDropDownBox.getSelectedIndex()));
				AddTextCmd command = new AddTextCmd(currentItem.getURI(),skosType, currentText);
				EditorUtil.sendCommand(command);
				inputText.setText("");
		}
	}
	

	public void notify(Event event) {
		if (event instanceof LoadTaxonomyEvent) {
			resetComponents();
			currentItem = null;
		}
		if (event instanceof ConceptSelectionChangedEvent) {
			ConceptSelectionChangedEvent csce = (ConceptSelectionChangedEvent) event;
			resetComponents();
			currentItem = csce.getConcept();
			if (currentItem != null) {
				for (LocalizedString ls: currentItem.getTexts(skosType)) {
					processAddText(ls);
				}
			}
		}
		else if (currentItem != null && event instanceof TaxonomyChangedEvent) {
			processTaxonomyChangeCommand(((TaxonomyChangedEvent) event).getCommand());
		}
	}


	
	private void processTaxonomyChangeCommand(TaxonomyChangeCommand command) {
		if (command instanceof ComplexTaxonomyChangeCommand) {
			ComplexTaxonomyChangeCommand complexCommand = (ComplexTaxonomyChangeCommand) command;
			for (TaxonomyChangeCommand current: complexCommand.getImpliedCommands()) {
				processTaxonomyChangeCommand(current);
			}
		}
		else if (command instanceof PrimitiveTaxonomyChangeCommand) {
			PrimitiveTaxonomyChangeCommand primitiveCommand = (PrimitiveTaxonomyChangeCommand) command;
			if (primitiveCommand.getFromURI().equals(currentItem.getURI())) {
				if (primitiveCommand instanceof PrimitiveRemoveConcept) {
					resetComponents();
					currentItem = null;
				}
				else if (primitiveCommand instanceof PrimitiveAddText) {
					PrimitiveAddText addTextCmd = (PrimitiveAddText) primitiveCommand;
					if (addTextCmd.getConnection().equals(skosType)) {
						processAddText(addTextCmd.getText());
					}
				}
				else if (primitiveCommand instanceof PrimitiveChangeText) {
					PrimitiveChangeText changeTextCmd = (PrimitiveChangeText) primitiveCommand;
					if (changeTextCmd.getConnection().equals(skosType)) {
						processRemoveText(changeTextCmd.getOldText());
						processAddText(changeTextCmd.getNewText());
					}
				}
				else if (primitiveCommand instanceof PrimitiveRemoveText) {
					PrimitiveRemoveText removeText = (PrimitiveRemoveText) primitiveCommand;
					if (removeText.getConnection().equals(skosType)) {
						processRemoveText(removeText.getText());
					}
				}
			}
		}
		
	}

	private void processRemoveText(LocalizedString toRemove) {
		LabelLanguageComponent table = langTables.get(toRemove.getLanguage());
		if(table != null){
			table.removeLabel(toRemove);
			//check if table is empty now
			if(table.getRowCount()==1) {
				langTables.remove(toRemove.getLanguage());
				languagesPanel.remove(table);
			}
		}
	}
	
	private void processAddText(LocalizedString ls) {
		if(!langTables.containsKey(ls.getLanguage())){
			LabelLanguageComponent langTable = new LabelLanguageComponent(ls.getLanguage(), this);
			languagesPanel.insert(langTable,0);
			langTables.put(ls.getLanguage(), langTable);
		}
		langTables.get(ls.getLanguage()).addLabel(ls);
	}
	
	private void resetComponents(){
		languagesPanel.clear();
		langTables.clear();
	}
	
	protected SKOS getSkosType(){
		return skosType;
	}
	
}
