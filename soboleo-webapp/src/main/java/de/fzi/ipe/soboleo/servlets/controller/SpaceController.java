package de.fzi.ipe.soboleo.servlets.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.space.Spaces;
import de.fzi.ipe.soboleo.user.User;
import de.fzi.ipe.soboleo.user.UserDatabase;

@Controller
@RequestMapping("/admin/spaces")
public class SpaceController {
 
	@Autowired
	protected Server server;

	@RequestMapping("spacesList")
	protected ModelAndView spaceList(ModelMap m)
	{
		Spaces spaces = server.getSpaces();
		m.put("spaces", spaces.getSpaces());
		return new ModelAndView("admin/spaces/spacesList",m);
	}
	
	@RequestMapping("spaceUser")
	protected ModelAndView spaceUser(
			@RequestParam("spaceName") String spaceName,
			@RequestParam("userEmail") String userEmail,
			@RequestParam(value = "addUser", required = false) String addUser,
			@RequestParam(value = "removeUser", required = false) String removeUser)
			throws IOException, EventPermissionDeniedException {

		Spaces spaces = server.getSpaces();
		Space space = spaces.getSpace(spaceName);
		String validation = null;
		UserDatabase udb = server.getUserDatabase();
		User tempUser = udb.getUserForEmail(userEmail);

		if (addUser != null) {
			if (space != null && tempUser != null) {
				tempUser.addSpaceIdentifier(space.getURI());
				spaces.closeSpace(spaceName);
				return new ModelAndView("admin/spaces/spaceEdit", "spaceName",
						spaceName);
			} else if (space == null)
				validation = "space.noSpace";
			else if (tempUser == null)
				validation = "space.noUser";
		}

		if (removeUser != null) {
			if (space != null && tempUser != null) {
				tempUser.removeSpaceIdentifier(space.getURI());
				spaces.closeSpace(spaceName);
				return new ModelAndView("redirect:"
						+ server.getServerRelativeURL());
			} else if (space == null)
				validation = "space.noSpace";
			else if (tempUser == null)
				validation = "space.noUser";
		}

		return new ModelAndView("admin/spaces/spaceUser", "validation",
				validation);
	}

	@RequestMapping("newSpace")
	protected ModelAndView newSpace(
			@RequestParam(value = "spaceName", required = false) String spaceName,
			HttpServletRequest rq) throws IOException,
			EventPermissionDeniedException {
		Spaces spaces = server.getSpaces();
		String validation = null;
		if (spaceName != null) {
			validation = spaces.isPossibleName(spaceName);
			if (validation == null) {
				spaces.createNewSpace(spaceName);
				Space tempSpace = spaces.getSpace(spaceName);
				User tempUser = (User) rq
						.getAttribute(ControllerConstants.USER_USER);
				tempUser.addSpaceIdentifier(tempSpace.getURI());
				tempSpace.setProperty(SpaceProperties.SECURITY, "write");
				spaces.closeSpace(spaceName);
				return new ModelAndView("userSettings", "uri",
						tempUser.getURI());
			}
		} else
			spaceName = "";

		return new ModelAndView("admin/spaces/newSpace", "validation", validation);
	}

	@RequestMapping("completeRDFDump")
	protected void completeRDFDump(@RequestParam("spaceName") String spaceName,
			HttpServletResponse rs) throws IOException,
			EventPermissionDeniedException {
		Spaces spaces = server.getSpaces();
		Space space = spaces.getSpace(spaceName);
		if (space != null) {
			rs.setContentType("application/rdf+xml");
			rs.getWriter().print(space.getCompleteRDFDump());
		}
	}

	@RequestMapping("taxonomyRDFDump")
	protected void taxonomyRDFDump(@RequestParam("spaceName") String spaceName,
			HttpServletResponse rs) throws IOException,
			EventPermissionDeniedException {
		Spaces spaces = server.getSpaces();
		Space space = spaces.getSpace(spaceName);
		if (space != null) {
			rs.setContentType("application/rdf+xml");
			rs.getWriter().print(space.getTaxonomyRDFDump());
		}
	}

	@RequestMapping("spaceEdit")
	protected ModelAndView editSpace(@RequestParam("spaceName") String spaceName)
			throws IOException, EventPermissionDeniedException {
		ModelMap m = new ModelMap();
		Space space = server.getSpaces().getSpace(spaceName);
		UserDatabase udb = server.getUserDatabase();
		m.put("space", space);
		m.put("spaceuri", space.getURI());
		m.put("spaceUsers", udb.getUserForSpace(spaceName));
		m.put("permission", space.getProperty(SpaceProperties.SECURITY));
		m.put("sysoutLogging",
				space.getProperty(SpaceProperties.SYSOUT_LOGGING));
		m.put("eventLogging", space.getProperty(SpaceProperties.SYSOUT_LOGGING));
		m.put("language", space.getProperty(SpaceProperties.DEFAULT_LANGUAGE));
		m.put("peopleSearch", space.getProperty(SpaceProperties.PEOPLE_SEARCH));
		m.put("communityAnnotation",
				space.getProperty(SpaceProperties.COMMUNITY_ANNOTATION));
		m.put("dialogSupport",
				space.getProperty(SpaceProperties.DIALOG_SUPPORT));
		m.put("annotatePeopleDeleteTopic", space
				.getProperty(SpaceProperties.ANNOTATEPEOPLE_DELETABLE_TOPIC));
		m.put("annotateWebPageRating",
				space.getProperty(SpaceProperties.ANNOTATE_WEB_PAGE_RATING));
		return new ModelAndView("admin/spaces/spaceEdit", m);

	}

	@RequestMapping("spaceDump")
	protected ModelAndView spaceDump(
			@RequestParam("spaceName") String spaceName) throws IOException, EventPermissionDeniedException {
		Spaces spaces = server.getSpaces();
		Space space = spaces.getSpace(spaceName);
		return new ModelAndView("admin/dump", "dump", space
				.getTripleStoreDump().replace("\n", "<br/>"));
	}
}
