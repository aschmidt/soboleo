package de.fzi.ipe.soboleo.editor.client;

import com.google.gwt.i18n.client.Constants;

public interface EditorConstants extends Constants {
	// Labels	
	@DefaultStringValue("Edit Topic")
	String editConcept();
	@DefaultStringValue("Labels & Description")
	String labelsAndDescription();
	@DefaultStringValue("Relations")
	String relations();
	@DefaultStringValue("Please select a topic to discuss about.")
	String pleaseSelectConceptToDiscuss();
	@DefaultStringValue("Please select a topic to show dialogues for.")
	String pleaseSelectConceptToShowDialogues();
	@DefaultStringValue("Search:")
	String search();
	@DefaultStringValue("Chat")
	String chat();
	@DefaultStringValue("Log")
	String log();
	@DefaultStringValue("Edit")
	String edit();
	@DefaultStringValue("Discuss")
	String discuss();
	@DefaultStringValue("Start Maturing Dialog")
	String startMaturingDialog();
	@DefaultStringValue("Show Performed Dialogs")
	String showPerformedDialogs();
	@DefaultStringValue("Improve It!")
	String improveIt();
	@DefaultStringValue("Show All Recommendations")
	String showAllRecs();
	@DefaultStringValue("Show Recommendations for Topic")
	String showConceptRecs();
	@DefaultStringValue("loading ....")
	String loading();
	@DefaultStringValue("Open >>")
	String open();
	@DefaultStringValue("Paste Topic(s)")
	String pasteConcepts();
	@DefaultStringValue("Cut Topic(s)")
	String cutConcepts();
	@DefaultStringValue("Copy Topic(s)")
	String copyConcepts();
	@DefaultStringValue("Add Topic")
	String addConcept();
	@DefaultStringValue("Delete Topic")
	String deleteConcept();
	@DefaultStringValue("Create new Topic")
	String createNewConcept();
	@DefaultStringValue("Please enter the name for the new topic")
	String enterTheName();
	@DefaultStringValue("Please enter a title for the dialog")
	String enterATitle();
	@DefaultStringValue("Start Dialog")
	String startDialog();
	@DefaultStringValue("Preferred Labels")
	String preferredLabels();
	@DefaultStringValue("Alternative Labels")
	String alternativeLabels();
	@DefaultStringValue("Hidden Labels")
	String hiddenLabels();
	@DefaultStringValue("Description")
	String descriptions();
	@DefaultStringValue("Broader Topics")
	String broaderConcepts();
	@DefaultStringValue("Narrower Topics")
	String narrowerConcepts();
	@DefaultStringValue("Related Topics")
	String relatedConcepts();
	
	// Recommendations
	@DefaultStringValue("Recommendations")
	String recommendations();
	@DefaultStringValue("Missing Descriptions")
	String missingDescriptions();	
	@DefaultStringValue("Identical Labels")
	String identicalLabels();
	@DefaultStringValue("Add Relations")
	String superConcepts();
	@DefaultStringValue("Preferred label of topic ")
	String recsPrefLabel();
	@DefaultStringValue("Alternative label of topic ")
	String recsAltLabel();
	@DefaultStringValue("Hidden label of topic ")
	String recsHiddenLabel();
	@DefaultStringValue("Topic ")
	String recsConcept();
	@DefaultStringValue("is identical with ")
	String recsIsIdentical();
	@DefaultStringValue("may be a narrower of ")
	String recsIsNarrower();
	@DefaultStringValue("may be related to ")
	String recsIsRelatd();
	@DefaultStringValue("lacks a description")
	String recsLacksDescription();
	@DefaultStringValue("No recommendations available")
	String noRecommendations();
	@DefaultStringValue("Please select a topic to get recommendations for.")
	String recsSelectConcept();
	


	
	// Buttons
	@DefaultStringValue("Create")
	String create();
	@DefaultStringValue("Delete")
	String delete();
	@DefaultStringValue("Send")
	String send();
	@DefaultStringValue("Create Topic")
	String createConcept();
	@DefaultStringValue("Cancel")
	String cancel();
	@DefaultStringValue("Add")
	String add();
	@DefaultStringValue("close")
	String close();
	
	// Exceptions
	@DefaultStringValue("An Exception occurred")
	String exceptionOccured();
	@DefaultStringValue("Please close and reload the editor <br />")
	String closeAndReload();
	@DefaultStringValue("No Such Topic")
	String noSuchConcept();
	@DefaultStringValue("Do you like to create it?")
	String createItFirst();
	
	@DefaultStringValue("No such relation to remove")
	String error_100();
	@DefaultStringValue("The relation already exists")
	String error_101();
	@DefaultStringValue("Cannot relate a topic to itself")
	String error_102();
	@DefaultStringValue("Adding such a relation would produce a circle")
	String error_103();
	@DefaultStringValue("The given label is blank")
	String error_104();
	@DefaultStringValue("The given description is blank")
	String error_105();
	@DefaultStringValue("The label already exists")
	String error_106();
	@DefaultStringValue("The description already exists")
	String error_107();
	@DefaultStringValue("Only one label already per language permitted")
	String error_108();
	@DefaultStringValue("Only one description per language permitted")
	String error_109();
	@DefaultStringValue("The label already exists for this language")
	String error_110();
	@DefaultStringValue("No such label to remove")
	String error_111();
	@DefaultStringValue("No such description to remove")
	String error_112();
	@DefaultStringValue("A topic with this preferred label already exists")
	String error_113();
	@DefaultStringValue("Please do not delete the last preferred label for this topic")
	String error_114();
	@DefaultStringValue("No such topic to delete")
	String error_115();
	@DefaultStringValue("Taxonomy Event Bus Adapter encountered unknown taxonomy change event! - Will not allow that!")
	String error_116();
	@DefaultStringValue("You are not allowed to change this space! - Please login or ask for permission for this space")
	String error_200();
	@DefaultStringValue("You are not allowed to view this space! Please login or ask for permission for this space")
	String error_201();
}

