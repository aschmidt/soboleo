package de.fzi.ipe.soboleo.editor.client.recommendationArea;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class RecommendationDisplayComponent  extends Composite {
	private HTML pre = new HTML();
	private HTML filling = new HTML();
	private HTML suf = new HTML();
	private ConceptAnchor primaryAnchor = new ConceptAnchor();
	private Set<ConceptAnchor> othersAnchors = new HashSet<ConceptAnchor>();
	
	public RecommendationDisplayComponent(String preText, String fillingText, String sufText, ClientSideConcept primary, ClientSideConcept... concepts){
		HorizontalPanel panel = new HorizontalPanel();
		pre.setText(preText);
		filling.setText(fillingText);
		suf.setText(sufText);
		primaryAnchor.setConcept(primary);
		primaryAnchor.setText(primary.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage()).getString());
		primaryAnchor.setStyleName("gwt-TreeItem");
		
		for(ClientSideConcept csc : concepts){
			ConceptAnchor ca = new ConceptAnchor(csc);
			ca.setText(csc.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage()).getString());
			othersAnchors.add(ca);
		}
		
		panel.add(pre);
		panel.add(new HTML("&nbsp")); // add space before the concept name
		panel.add(primaryAnchor);
		panel.add(new HTML("&nbsp")); // add space after the concept name
		panel.add(filling);
		panel.add(new HTML("&nbsp")); // add space before the second concept name
		for(ConceptAnchor ac : othersAnchors){
			ac.setStyleName("gwt-TreeItem");
			panel.add(ac);
		}
		panel.add(new HTML("&nbsp"));
		panel.add(suf);
		initWidget(panel);
	}
	

	public void addClickHandler(ClickHandler handler){
		primaryAnchor.addClickHandler(handler);
		for(ConceptAnchor ca : othersAnchors) ca.addClickHandler(handler);
	}
	
	protected class ConceptAnchor extends Anchor{
		ClientSideConcept c;
		protected ConceptAnchor(){;}
		protected ConceptAnchor(ClientSideConcept c){
			this.c = c;
		}			
		protected void setConcept(ClientSideConcept c){
			this.c = c;
		}
		protected ClientSideConcept getConcept(){
			return c;
		}
	}
	
}
