package de.fzi.ipe.soboleo.server.client;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

import de.fzi.ipe.soboleo.server.client.rpc.LoginService;
import de.fzi.ipe.soboleo.server.client.rpc.LoginServiceAsync;


public class LoginDialog extends DialogBox implements AsyncCallback<String> {
	
	private TextBox username;
	private PasswordTextBox password;
	private Label feedback;
	
	private String name;
	private String pwdMD5;
	
	private static LoginServiceAsync loginService = null;
	
	public LoginDialog() {
		setText("Login");
		VerticalPanel vPanel = new VerticalPanel();
		
		feedback = new Label(" ");
		feedback.setWidth("240px");
		vPanel.add(feedback);
		
		makeUsername(vPanel);
		makePassword(vPanel);
		
		HorizontalPanel hPanel = new HorizontalPanel();
		vPanel.setSpacing(2);
		makeOkButton(hPanel);
		makeCancelButton(hPanel);
		vPanel.add(hPanel);
		setWidget(vPanel);
	}

	public static LoginServiceAsync getLoginService() {
		if (loginService == null) {
			loginService = (LoginServiceAsync) GWT.create(LoginService.class);
			ServiceDefTarget endpoint = (ServiceDefTarget) loginService;
			endpoint.setServiceEntryPoint(ClientUtil.getServerURL()+ "user/service");
		}
		return loginService;
	}
	
	private void callLogin() {
		feedback.setText("verifying login ...");
		name = username.getText();
		pwdMD5 = password.getText();
		getLoginService().login(name, pwdMD5, LoginDialog.this);
	}
	public void callCancel(){
		History.back();
	}
	
	private void makeOkButton(HorizontalPanel hPanel) {
		Button okButton = new Button();
		okButton.setText("login");
		okButton.addClickListener(new ClickListener() {

			public void onClick(Widget sender) {
				callLogin();
			}
		});
		hPanel.add(okButton);
		hPanel.setCellHorizontalAlignment(okButton, HasHorizontalAlignment.ALIGN_CENTER);
	}
	private void makeCancelButton(HorizontalPanel hPanel){
		//just for ajusting the both buttons  
		Label ajust = new Label();
		ajust.setText("");
		ajust.setWidth("50px");
		hPanel.add(ajust);

		Button cancel= new Button();
		cancel.setText("cancel");
		cancel.addClickListener(new ClickListener(){
			public void onClick(Widget arg0) {
				callCancel();
			}
			
		});
		hPanel.add(cancel);
		hPanel.setCellHorizontalAlignment(cancel, HasHorizontalAlignment.ALIGN_RIGHT);
	}
	private void makePassword(VerticalPanel vPanel) {
		HorizontalPanel hPanel = new HorizontalPanel();
		Label passwordLabel = new Label("Password:");
		passwordLabel.setWidth("120px");
		hPanel.add(passwordLabel);
		
		password = new PasswordTextBox();
		password.addKeyboardListener(new KeyboardListener(){

			public void onKeyDown(Widget sender, char keyCode, int modifiers) {
				;
			}
			public void onKeyPress(Widget sender, char keyCode, int modifiers) {
				if (keyCode == KEY_ENTER) {
					callLogin();
				}		
			}
			public void onKeyUp(Widget sender, char keyCode, int modifiers) {
				;
			}
	});
		password.setWidth("120px");
		hPanel.add(password);
		
		vPanel.add(hPanel);
	}

	
	private void makeUsername(VerticalPanel vPanel) {
		HorizontalPanel hPanel = new HorizontalPanel();
		Label usernameLabel = new Label("Username:");
		usernameLabel.setWidth("120px");
		hPanel.add(usernameLabel);
		
		username = new TextBox();
		username.setWidth("120px");
		username.addKeyboardListener(new KeyboardListener(){

				public void onKeyDown(Widget sender, char keyCode, int modifiers) {
					;
				}
				public void onKeyPress(Widget sender, char keyCode, int modifiers) {
					if (keyCode == KEY_ENTER) {
						callLogin();
					}		
				}
				public void onKeyUp(Widget sender, char keyCode, int modifiers) {
					;
				}
		});
		hPanel.add(username);
		
		vPanel.add(hPanel);
	}
	
	public void show() {
		setPopupPosition(100, 100);		
		super.show();
		username.setFocus(true);		
	}

	public void onFailure(Throwable caught) {
		feedback.setText("Could not connect to server ..."+caught.getMessage());
	}

	public void onSuccess(String result) {
		if (result != null) {
			Cookies.setCookie("SoboleoCookie", result, new Date(System.currentTimeMillis()+ 6000000000l), (String) ClientUtil.getHostName(), ClientUtil.getRelURL(), false);
			this.hide();
		}
		else {
			feedback.setText("Login failed");
		}		
	}

	protected String getName() {
		return name;
	}

	protected String getPwdMD5() {
		return pwdMD5;
	}			
}
