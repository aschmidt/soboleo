/*
 * FZI - Information Process Engineering 
 * Created on 03.07.2009 by zach
 */
package de.fzi.ipe.soboleo.editor.client.components;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EditorUtil;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

/**
 * Popup window that asks the user for the name of the new concept (or to
 * cancel) and then sends the appropriate command
 */
public class CreateConceptPopup extends PopupPanel {

	private TextBox input;
	private LanguageDropDownBox langBox;
	private String fromURI;
	private SKOS connection;
	private EditorConstants constants = GWT.create(EditorConstants.class);

	public CreateConceptPopup() {
		super(true);
		setContent("");
		show();
	}
	
	public CreateConceptPopup(String initialName, String fromURI, SKOS connection){
		this.fromURI = fromURI;
		this.connection = connection;
		setContent(initialName);
		show();
	}

	private void setContent(String initialName) {
		setStyleName("ks-popups-Popup");

		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(5);
		add(vPanel);
		setTitle(constants.createNewConcept());
		Label label = new Label(constants.enterTheName());
		if(!initialName.equals("")){
			setTitle(constants.noSuchConcept());
			label.setText(constants.noSuchConcept()+constants.createItFirst());
		}
		vPanel.add(label);

		input = new TextBox();
		input.setText(initialName);
		input.addKeyPressHandler(new KeyPressHandler(){
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getCharCode() == KeyCodes.KEY_ENTER) processCreateConcept();
			}			
		});
		langBox = new LanguageDropDownBox(ClientUtil.getUserLanguage());
				
		HorizontalPanel inputPanel = new HorizontalPanel();
		inputPanel.setSpacing(3);
		inputPanel.add(input);
		inputPanel.add(langBox);
		
		vPanel.add(inputPanel);

		createButtons(vPanel);
	}
	

	private void createButtons(VerticalPanel vPanel) {
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		hPanel.setSpacing(5);
		
		final Button okButton = new Button(constants.createConcept());
//		final Button okButton = new Button("Create Concept");
		okButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent ce) {
				processCreateConcept();
			}
		});
		hPanel.add(okButton);

		final Button cancelButton = new Button(constants.cancel());
//		final Button cancelButton = new Button("Cancel");
		cancelButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent ce) {
				CreateConceptPopup.this.hide();
			}
		});
		hPanel.add(cancelButton);
		
		vPanel.add(hPanel);
	}

	public void show() {
		setPopupPosition(250, 250);
		super.show();
	}

	private void processCreateConcept() {
		CreateConceptCmd command = new CreateConceptCmd(new LocalizedString(input.getText(), langBox.getItemText(langBox.getSelectedIndex())));
		EditorUtil.getEditorService().sendCommand(command, ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), new AsyncCallback<String>(){

			public void onSuccess(String newConceptURI) {
		  		if(fromURI != null && connection != null) EditorUtil.sendCommand(new AddConnectionCmd(fromURI, connection, newConceptURI));
			}
			public void onFailure(Throwable caught) {
				EditorUtil.handleException(caught);
			}
		});

		CreateConceptPopup.this.hide();
	}
	
	
}
