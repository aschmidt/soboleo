package de.fzi.ipe.soboleo.servlets.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;

@Controller
public class ExceptionController {

	@ExceptionHandler(EventPermissionDeniedException.class)
	protected ModelAndView handleCredentialsException(EventPermissionDeniedException e,
			HttpServletRequest rq) {
		/*
		 * String requestURL = rq.getRequestURL().toString();
		 * if(rq.getQueryString()!= null) requestURL = requestURL +
		 * "?"+rq.getQueryString();
		 * rs.sendRedirect("/loginerror.jsp?redirectURL="
		 * +URLEncoder.encode(requestURL, "UTF-8"));
		 */

		String requestURL = rq.getRequestURL().toString();
		if (rq.getQueryString() != null)
			requestURL = requestURL + "?" + rq.getQueryString();

		ModelMap m = new ModelMap();
		m.put("redirectURL", requestURL);
		m.put("errormessage", e.toString());
		return new ModelAndView("loginerror",m); 
	}

}
