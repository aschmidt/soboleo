package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.event.shared.EventHandler;

/**
 * Interface for the Progress Event and the Receiver.
 * Each Receiver must implement this Handler to become a ProcessEventHandler and can be added to the 
 * ProgressSender.
 * 
 * @author kluge
 *
 */
public interface ProgressEventHandler extends EventHandler 
{
    /**
     * This is the Method, which handles the received event.
     * @param event
     */
    public void onProgressEventReceived(ProgressEvent event);
}
