package de.fzi.ipe.soboleo.editor.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

import de.fzi.ipe.soboleo.editor.client.components.EditorPopup;
import de.fzi.ipe.soboleo.editor.client.rpc.EditorService;
import de.fzi.ipe.soboleo.editor.client.rpc.EditorServiceAsync;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class EditorUtil {

	private static EditorServiceAsync editorService;
	private static ExceptionAsyncCallback exceptionCallback = new ExceptionAsyncCallback();
	private static ExceptionStringAsyncCallback exceptionStringCallback = new ExceptionStringAsyncCallback();
	
	private static Event lastEvent;
	
	public static void sendEvent(Event event) {
		if (lastEvent != null && event.equals(lastEvent)) return; //don't send same event twice.
		lastEvent = event;
		EditorServiceAsync editorService = getEditorService();
		editorService.sendEvent(event, ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), exceptionCallback);
		if (ClientUtil.getCurrentVersion()>-2) EventDistributor.getInstance().schedule(EventDistributor.DELAY_ON_CHANGE_MS);
	}

	public static void sendCommand(CommandEvent event) {
		if (lastEvent != null && event.equals(lastEvent)) return; //don't send same event twice.
		lastEvent = event;
		EditorServiceAsync editorService = getEditorService();
		editorService.sendCommand(event, ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), exceptionStringCallback);
		if (ClientUtil.getCurrentVersion()>-2) EventDistributor.getInstance().schedule(EventDistributor.DELAY_ON_CHANGE_MS);
	}


	public static EditorServiceAsync getEditorService() {
		if (editorService == null) {
			editorService = (EditorServiceAsync) GWT.create(EditorService.class);
			ServiceDefTarget endpoint = (ServiceDefTarget) editorService;
			endpoint.setServiceEntryPoint(ClientUtil.getServerURL()+"editor/service");
		}
		return editorService;
	}


	public static void handleException(Throwable caught) {
	    EditorPopup p = new EditorPopup(caught);
	    p.setPopupPosition(250, 250);
	    p.show();		
	}
}
