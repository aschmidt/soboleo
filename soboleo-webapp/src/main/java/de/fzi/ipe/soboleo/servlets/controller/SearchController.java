package de.fzi.ipe.soboleo.servlets.controller;


import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CRED_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.SEARCH_PARAMETER;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.SEARCH_PARAMETER_HIDDEN;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.SEARCH_PARAMETER_MUST;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerUtils.addSubconcepts;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsByNameAndTopic;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.User;

@Controller
public class SearchController
{
	private final static Logger logger = Logger
			.getLogger(SearchController.class);

	@RequestMapping("/search")
	protected ModelAndView doSearch(
			@RequestParam(SEARCH_PARAMETER) String query,
			@RequestParam(value = SEARCH_PARAMETER_MUST, required = false) String musts,
			@RequestParam(value = SEARCH_PARAMETER_HIDDEN, required = false, defaultValue = "" ) String hidden,
			@Value(CRED_SPEL) EventSenderCredentials cred,
			@Value(USER_SPACE_SPEL) Space space,
			@Value(USER_USER_SPEL) User user)
			throws EventPermissionDeniedException, ServletException,
			IOException
	{

		// System.out.println("Query Parameter: " + query);
		// System.out.println("Musts Parameter: " + musts);
		// System.out.println("Hidden Parameter: " + hidden);

		if (!"browsePeople".equalsIgnoreCase(hidden))
		{
			return searchForConcepts(query, musts, hidden, cred, space, user);
		} else
		{
			// handle browsePeople and searchBrowsePeople search for names
			// not for concepts
			return searchForNames(query, user, cred, space);
		}
	}

	private ModelAndView searchForNames(String query, User user,
			EventSenderCredentials cred, Space space)
			throws EventPermissionDeniedException, IOException
	{
		TaggedPerson taggedPerson = ControllerUtils.getTaggedPersonForUser(
				user, cred, space);

		// System.out.println("Handle Browse People");
		@SuppressWarnings("unchecked")
		List<TaggedPerson> persons = (List<TaggedPerson>) space.getEventBus()
				.executeQuery(
						new SearchPersonsByNameAndTopic(query,
								taggedPerson.getURI()), cred);

		// rq.setAttribute("peopleresult", persons);
		// rq.setAttribute(Dispatcher.SEARCH_PARAMETER, query);
		// System.out.println("Query: " + query);
		// System.out.println("SearchEntry: " +
		// rq.getAttribute(Dispatcher.SEARCH_PARAMETER));
		// RequestDispatcher dispatcher = rq
		// .getRequestDispatcher("browsePeople.jsp");
		// dispatcher.forward(rq, rs);
		HashMap<String, Object> m = new HashMap<String, Object>();
		m.put("peopleresult", persons);
		m.put(SEARCH_PARAMETER, query);
		return new ModelAndView("browsepeople", m);

	}

	private ModelAndView searchForConcepts(String query, String musts,
			String hidden, EventSenderCredentials cred, Space space, User user)
			throws EventPermissionDeniedException, IOException
	{
		TaggedPerson taggedPerson = ControllerUtils.getTaggedPersonForUser(
				user, cred, space);

		Set<String> mustConceptURIs = new HashSet<String>();
		Set<String> extractedConceptURIs = new HashSet<String>();
		Set<String> extractedSubConceptURIs = new HashSet<String>();

		String[] mustIDs = null;
		if (musts != null)
		{
			mustIDs = musts.split(",");
			for (String mustID : mustIDs)
				mustConceptURIs.add(TripleStore.SOBOLEO_NS + mustID);
			for (String mustID : mustIDs)
				mustConceptURIs.add(TripleStore.SOBOLEO_NS + mustID);
		}

		logger.info("query is : " + query);

		if (mustIDs == null)
		{
			// logger.info("try to map to concepts");

			ClientSideTaxonomy tax = space.getClientSideTaxonomy();

			String querywords[] = query.split(" ");

			for (ClientSideConcept concept : tax.getConcepts())
			{

				for (LocalizedString ls : concept.getTexts())
				{

					// does concept start with string?
					if (ls.getString().startsWith(query))
					{
						logger.info("add matching concept : "
								+ ls.getLanguage().toString() + " - "
								+ ls.getString());
						logger.info("concept id : " + concept.getURI());
						// add to mustconcepts
						extractedConceptURIs.add(concept.getURI());
						// get subconcepts, add to subconcepts
						extractedSubConceptURIs = addSubconcepts(
								extractedSubConceptURIs, concept, space);
					} else
					{
						try
						{
							// are one of the querywords containing the query
							for (int x = 0; x < querywords.length; x++)
							{
								if (ls.getString().startsWith(querywords[x]))
								{
									logger.info("add matching concept for queryword "
											+ querywords[x]
											+ " : "
											+ ls.getLanguage().toString()
											+ " - " + ls.getString());
									logger.info("concept id : "
											+ concept.getURI());
									// add to mustconcepts
									extractedConceptURIs.add(concept.getURI());
									// get subconcepts, add to subconcepts
									extractedSubConceptURIs = addSubconcepts(
											extractedSubConceptURIs, concept,
											space);
								}
							}
						} catch (Exception e)
						{
							logger.error(e);
						}
					}
				}
			}

		}

		GetDocumentsSearchResult job = new GetDocumentsSearchResult(query,
				mustConceptURIs);

		job.setExtractedConceptURIs(extractedConceptURIs);
		job.setExtractedSubconceptURIs(extractedSubConceptURIs);

		SearchResult result = (SearchResult) space.getEventBus().executeQuery(
				job, cred);

		// boolean showRating = getShowRating(space);
		// logger.info("show rating in search result:" + showRating);
		// rq.setAttribute("showRating", showRating);

		@SuppressWarnings("unchecked")
		List<TaggedPerson> persons = (List<TaggedPerson>) space.getEventBus()
				.executeQuery(
						new SearchPersonsByNameAndTopic(query,
								taggedPerson.getURI()), cred);

		// rq.setAttribute("peopleresult", persons);
		// rq.setAttribute(Dispatcher.SEARCH_PARAMETER, query);
		// RequestDispatcher dispatcher = rq
		// .getRequestDispatcher("searchResult.jsp");
		// dispatcher.forward(rq, rs);

		ModelMap m = new ModelMap();
		m.put("peopleresult", persons);
		m.put(SEARCH_PARAMETER, query);
		// m.put("showRating", showRating);
		m.put("result", result);
		return new ModelAndView("searchResult", m);

	}

	private boolean getShowRating(Space space)
	{
		boolean showRating = false;
		try
		{
			showRating = Boolean.valueOf(space
					.getProperty(SpaceProperties.ANNOTATE_WEB_PAGE_RATING));
		} catch (Exception e)
		{
			logger.error("error loading property SpaceProperties.ANNOTATE_WEB_PAGE_RATING "
					+ e);
		}
		return showRating;
	}

}
