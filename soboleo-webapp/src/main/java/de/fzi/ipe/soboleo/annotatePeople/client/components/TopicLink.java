package de.fzi.ipe.soboleo.annotatePeople.client.components;

import java.util.HashMap;

import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.user.client.ui.PushButton;


import de.fzi.ipe.soboleo.annotatePeople.client.AnnotatePeopleConstants;
import de.fzi.ipe.soboleo.server.client.ClientUtil;


/**
 * This class is used in AnnotatePeopleMain to create a specific component.
 * It is a label combined with a push button. The push button is used to delete the topic displayed 
 * in the corresponding label.  
 * @author kluge
 *
 */
public class TopicLink extends Composite {

    private AnnotatePeopleConstants constants = GWT.create(AnnotatePeopleConstants.class);
    /**
     *
     * @param topicCaption
     * @param myAnnotationsList 
     * @param deletable
     */
    public TopicLink(final String topicCaption,final HashMap<String,String> myAnnotationsList, boolean deletable)//,AnnotatePeopleMain parent) 
    {
    	final HorizontalPanel horizontalPanel = new HorizontalPanel();
    	//horizontalPanel.setBorderWidth(0);
    	initWidget(horizontalPanel);
    	
    	final PushButton bDeleteTopic = new PushButton(ClientUtil.getImageProvider().crossMouseOut2Small().createImage(),ClientUtil.getImageProvider().crossMouseOverSmall().createImage());
    	final Label label = new Label(topicCaption);
    	AbsolutePanel spacePanel = new AbsolutePanel();//needed that the components are not aligned without spaces between them
    	final AbsolutePanel spacePanelBetweenDelete = new AbsolutePanel(); // needed between DeleteButton and label 
    	AbsolutePanel spacePanelFront = new AbsolutePanel(); // add space to left
    	spacePanelBetweenDelete.setWidth("2px");
    	spacePanelFront.setWidth("4px");
    	final Label spacelabel = new Label("   ");
    	spacelabel.addMouseOverHandler(new MouseOverHandler() 
    	{
	    
	    @Override
	    public void onMouseOver(MouseOverEvent event) 
	    {
		label.setStyleName("gwt-topicLinkLabelOver");
		spacelabel.setStyleName("gwt-topicLinkLabelOver");
		spacePanelBetweenDelete.setStyleName("gwt-topicLinkLabelOver");
		bDeleteTopic.setStyleName("gwt-topicLinkLabelOver");
	    }
	});
    	spacelabel.addMouseOutHandler(new MouseOutHandler() 
    	{
	    
	    @Override
	    public void onMouseOut(MouseOutEvent event) 
	    {
		label.setStyleName("gwt-topicLinkLabel");
		spacelabel.setStyleName("gwt-topicLinkLabel");
		spacePanelBetweenDelete.setStyleName("gwt-topicLinkLabel");
		bDeleteTopic.setStyleName("gwt-topicLinkLabel");
		
	    }
	});
    	spacelabel.setStyleName("gwt-topicLinkLabel");
    	spacePanelBetweenDelete.add(spacelabel);
    	label.setStyleName("gwt-topicLinkLabel");
    	label.addMouseOverHandler(new MouseOverHandler() {
	    
	    @Override
	    public void onMouseOver(MouseOverEvent event) {
		label.setStyleName("gwt-topicLinkLabelOver");
		spacelabel.setStyleName("gwt-topicLinkLabelOver");
		spacePanelBetweenDelete.setStyleName("gwt-topicLinkLabelOver");
		bDeleteTopic.setStyleName("gwt-topicLinkLabelOver");
	    }
	});
    	label.addMouseOutHandler(new MouseOutHandler() {
	    
	    @Override
	    public void onMouseOut(MouseOutEvent event) {
		label.setStyleName("gwt-topicLinkLabel");
		spacelabel.setStyleName("gwt-topicLinkLabel");
		spacePanelBetweenDelete.setStyleName("gwt-topicLinkLabel");
		bDeleteTopic.setStyleName("gwt-topicLinkLabel");
		
	    }
	});
    	horizontalPanel.setCellVerticalAlignment(label, HasVerticalAlignment.ALIGN_MIDDLE);
	horizontalPanel.setCellHorizontalAlignment(label, HasHorizontalAlignment.ALIGN_LEFT);
	horizontalPanel.add(spacePanelFront); //Space to get a space at the front
	horizontalPanel.add(label);
    	
    	if(deletable)
    	{	
    	    bDeleteTopic.setStyleName("gwt-topicLabel");
    	    bDeleteTopic.getUpHoveringFace().setImage(ClientUtil.getImageProvider().crossMouseOverSmall().createImage());
    	    bDeleteTopic.addMouseOverHandler(new MouseOverHandler() {

    		@Override
    		public void onMouseOver(MouseOverEvent event) 
    		{
    		    label.setStyleName("gwt-topicLinkLabelOver");
    		    spacelabel.setStyleName("gwt-topicLinkLabelOver");
    		    spacePanelBetweenDelete.setStyleName("gwt-topicLinkLabelOver");
    		    bDeleteTopic.setStyleName("gwt-topicLinkLabelOver");
    		}
    	    });
    	    bDeleteTopic.addMouseOutHandler(new MouseOutHandler() {

    		@Override
    		public void onMouseOut(MouseOutEvent event) 
    		{
    		    label.setStyleName("gwt-topicLinkLabel");
    		    spacelabel.setStyleName("gwt-topicLinkLabel");
    		    spacePanelBetweenDelete.setStyleName("gwt-topicLinkLabel");
    		    bDeleteTopic.setStyleName("gwt-topicLinkLabel");
    		}
    	    });
    	    bDeleteTopic.addClickHandler(new ClickHandler() {
	        
	        @Override
	        public void onClick(ClickEvent event) 
	        {
	            //delete this from parent
	            horizontalPanel.getParent().removeFromParent();
	            myAnnotationsList.remove(topicCaption);        
	        }
	    });
    	    horizontalPanel.setCellVerticalAlignment(bDeleteTopic, HasVerticalAlignment.ALIGN_BOTTOM);
    	    horizontalPanel.setCellHorizontalAlignment(bDeleteTopic, HasHorizontalAlignment.ALIGN_CENTER);
    	    bDeleteTopic.setStyleName("gwt-topicLabel");
    	    bDeleteTopic.setTitle(constants.removeTopic());
    	    spacePanelBetweenDelete.setWidth("2px");
	    horizontalPanel.add(spacePanelBetweenDelete);
    	    horizontalPanel.add(bDeleteTopic);  
    	}
    	spacePanel.setWidth("8px");// sets the space between two TopicLink to 10 Pixels
    	horizontalPanel.add(spacePanel);
    }
}
