package de.fzi.ipe.soboleo.editor.client.messageArea;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.DecoratedTabPanel;

import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;

public class MessageTabPanel extends DecoratedTabPanel {
	private EventDistributor eventDistrib;
	private EditorConstants constants = GWT.create(EditorConstants.class);
	
	public MessageTabPanel(EventDistributor eventDistrib){
		this.eventDistrib = eventDistrib;
		ChatMessagePanel chatPanel = new ChatMessagePanel();
		DOM.setElementAttribute(chatPanel.getElement(), "id", "chatTab");
		eventDistrib.addListener(chatPanel);
		this.add(chatPanel, constants.chat());
		
		SystemMessagePanel systemPanel = new SystemMessagePanel();
		eventDistrib.addListener(systemPanel);
		this.add(systemPanel, constants.log());
		
		this.selectTab(0);
	}

}
