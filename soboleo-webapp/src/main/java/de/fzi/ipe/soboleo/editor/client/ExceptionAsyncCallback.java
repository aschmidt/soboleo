package de.fzi.ipe.soboleo.editor.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class ExceptionAsyncCallback implements AsyncCallback<Void>{

	public void onSuccess(Void result) {
	}

	public void onFailure(Throwable caught) {
		EditorUtil.handleException(caught);
	}

}

 class ExceptionStringAsyncCallback implements AsyncCallback<String>{
	public void onSuccess(String result) {
	}

	public void onFailure(Throwable caught) {
		EditorUtil.handleException(caught);
	}

}