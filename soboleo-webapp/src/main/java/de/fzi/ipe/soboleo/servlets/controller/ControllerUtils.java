package de.fzi.ipe.soboleo.servlets.controller;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.event.peopletagging.AddTaggedPerson;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPersonForEmail;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.User;

public class ControllerUtils {
	
	private static Logger logger = Logger.getLogger(ControllerUtils.class);
	
	/**
	 * check if email belongs to an existing user; if so create a tagged person
	 * for her,
	 */
	static TaggedPerson getTaggedPersonForUser(User user,
			EventSenderCredentials cred, Space space)
			throws EventPermissionDeniedException, IOException {
		TaggedPerson taggedPerson = (TaggedPerson) space.getEventBus()
				.executeQuery(new GetTaggedPersonForEmail(user.getEmail()),
						cred);
		if (taggedPerson == null) {
			taggedPerson = (TaggedPerson) space
					.getEventBus()
					.executeCommandNow(
							new AddTaggedPerson(user.getEmail(), user.getName()),
							cred);
		}
		return taggedPerson;
	}
	
	static boolean getShowRating(Space space) {
		boolean showRating = false;
		try {
			showRating = Boolean.valueOf(space
					.getProperty(SpaceProperties.ANNOTATE_WEB_PAGE_RATING));
		} catch (Exception e) {
			logger
					.error("error loading property SpaceProperties.ANNOTATE_WEB_PAGE_RATING "
							+ e);
		}
		return showRating;
	}

	public static  Set<String> addSubconcepts(Set<String> narrowersset,
			ClientSideConcept concept, Space space) {
		space.getClientSideTaxonomy();
		List<ClientSideConcept> narrowers = ClientSideTaxonomy
				.sortConcepts(
						concept.getConnectedAsConcepts(SKOS.HAS_NARROWER, space
								.getClientSideTaxonomy()));

		try {
			logger.info("narrower found: " + narrowers.size());

			for (ClientSideConcept sub : narrowers) {
				logger.info("add uri to set: " + sub.getURI());
				narrowersset.add(sub.getURI());

				narrowersset = addSubconcepts(narrowersset, sub, space);
			}

		} catch (Exception e) {
			logger.info("no narrowers concepts");
		}

		return narrowersset;

	}

}
