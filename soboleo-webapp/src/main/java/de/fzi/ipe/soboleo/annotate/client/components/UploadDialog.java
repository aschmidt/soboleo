package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;


import de.fzi.ipe.soboleo.annotate.client.AnnotateConstants;
import de.fzi.ipe.soboleo.annotate.client.rpc.AnnotateServiceAsync;

import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.FileUpload;

/**
 * Upload dialog for annotate web pages, that it is possible to upload a
 * document instead of a web side URL.
 * 
 * @author kluge
 *
 */
public class UploadDialog extends DialogBox implements ProgressEventHandler
{
    private String uploadedDocument = "";
    private boolean documentUploaded = false;
    private AnnotateConstants constants = GWT.create(AnnotateConstants.class);
    // Html panel for Error output;
    private final HTML debugOutput = new HTML();
    private final String spaceName;
    private ProgressBar progressBar;
    /**
     * @param annoService 
     * @param spaceName 
     */
    public UploadDialog(final AnnotateServiceAsync annoService, String spaceName) 
    {
	super(false,true);
	final ProgressSender progressSender = new ProgressSender();
	progressSender.addProgressEventHandler(UploadDialog.this);
	this.spaceName = spaceName;
	FlexTable flexTable = new FlexTable();
	flexTable.setBorderWidth(0);
	flexTable.setCellSpacing(0);
	flexTable.setCellPadding(3);
	setWidget(flexTable);
	flexTable.setSize("209px", "155px");
	flexTable.getFlexCellFormatter().setColSpan(0, 0, 2);
	//Caption Row 0
	HTML uploadDialogCation = new HTML("<h3>" + constants.upload()+"</h3>");
	uploadDialogCation.setStyleName("gwtLabelAnnotate");
	flexTable.setWidget(0, 0, uploadDialogCation);
	flexTable.getCellFormatter().setHorizontalAlignment(0, 0, HasHorizontalAlignment.ALIGN_CENTER);
	
	//File Name Row1
	HTML fileNameToUpload = new HTML(constants.document());
	fileNameToUpload.setStyleName("gwtLabelAnnotate");
	flexTable.setWidget(1, 0, fileNameToUpload);
	fileNameToUpload.setWidth("70");
	flexTable.getCellFormatter().setHorizontalAlignment(1, 0, HasHorizontalAlignment.ALIGN_LEFT);
	
	final FileUpload fileUpload = new FileUpload();
	flexTable.setWidget(1, 1, fileUpload);
	
	//Progessbar State Row 2
	HTML statusLabel = new HTML(constants.state());
	statusLabel.setStyleName("gwtLabelAnnotate");
	flexTable.setWidget(2, 0, statusLabel);
	statusLabel.setWidth("70");
	flexTable.getCellFormatter().setHorizontalAlignment(2, 0, HasHorizontalAlignment.ALIGN_LEFT);
	
	//ProgressBar progressBar = new ProgressBar(200);
	//flexTable.setWidget(2, 1, progressBar);
	//progressBar.setWidth("200");

	/*progressBar = new ProgressBar(20,ProgressBar.SHOW_TIME_REMAINING
		+ProgressBar.SHOW_TEXT);*/
	progressBar = new ProgressBar(20,ProgressBar.SHOW_TEXT);
	flexTable.setWidget(2, 1, progressBar);
	
	

	//Text field for message display Row 3
	
	//Row 4 empty
	debugOutput.setStyleName("gwtLabelAnnotate");
	//debugOutput.setHTML(currentSpacePath);
	flexTable.setWidget(4,0,debugOutput);
	flexTable.getFlexCellFormatter().setColSpan(4, 0, 2);
	
	//Row 5 ButtonPanel for the Upload and Cancel Buttons
	HorizontalPanel buttonPanel = new HorizontalPanel();
	
	
	//Upload
	final Button uploadDoc = new Button(constants.upload());
	uploadDoc.setStyleName("buttonPannel");
	uploadDoc.addMouseOverHandler(new MouseOverHandler() 
	{
	    public void onMouseOver(MouseOverEvent event) 
	    {
		uploadDoc.setStyleName("buttonPannelOver");
	    }
	});
	uploadDoc.addMouseOutHandler(new MouseOutHandler() 
	{
	    public void onMouseOut(MouseOutEvent event) 
	    {
		uploadDoc.setStyleName("buttonPannel");
	    }
	});
	uploadDoc.addClickHandler(new ClickHandler() 
	{
	    @Override
	    public void onClick(ClickEvent event) 
	    {
		if(fileUpload.getFilename().contentEquals(""))
		{
		    debugOutput.setHTML("No file selected!! Please select a file for upload!");
		}
		else
		{
		    progressBar.setText("Uploading in progress");
		    try
		    {	
			final Timer prozess = new Timer() 
			{
			    int i = 0;
			    public void run() 
			    {
				i++;
				progressBar.setProgress(i);
				if(i==100) i=0;
			    }
			};
			annoService.uploadDocToSpace(UploadDialog.this.spaceName,fileUpload.getFilename(), new AsyncCallback<String>() 
				{
			    @Override
			    public void onSuccess(final String result) 
			    {
				prozess.cancel();
				progressBar.setProgress(100);
				progressBar.setText("Document uploaded");
				Timer t = new Timer() 
				{
				    int i = 0;
				    public void run() 
				    {					
					if(i==1)
					{
					    handleUploadCompleted(result);
					    cancel();
					}
					i =1;
				    }
				};
				t.scheduleRepeating(1000);

			    }

			    @Override
			    public void onFailure(Throwable caught) 
			    {
				progressBar.setText("Upload failed!");

			    }
				}
			
			);
			prozess.scheduleRepeating(20);
			
		    }
		    catch(Exception e)
		    {
			e.printStackTrace();
			progressBar.setText("Upload failed!");
		    }
		}
		
		
	    }
	});

	//Cancel
	final Button cancelUpload = new Button(constants.cancel());
	cancelUpload.setStyleName("buttonPannel");
	cancelUpload.addMouseOverHandler(new MouseOverHandler() 
	{
	    public void onMouseOver(MouseOverEvent event) 
	    {
		cancelUpload.setStyleName("buttonPannelOver");
	    }
	});
	cancelUpload.addMouseOutHandler(new MouseOutHandler() 
	{
	    public void onMouseOut(MouseOutEvent event) 
	    {
		cancelUpload.setStyleName("buttonPannel");
	    }
	});
	cancelUpload.addClickHandler(new ClickHandler() 
	{

	    @Override
	    public void onClick(ClickEvent event) 
	    {
		UploadDialog.this.hide();

	    }
	});
		
	buttonPanel.add(uploadDoc);
	buttonPanel.add(cancelUpload);
	flexTable.getFlexCellFormatter().setColSpan(5, 0, 2);
	flexTable.setWidget(5, 0, buttonPanel);
	
	
    }
    
    /**
     * Returns the path on the server for the uploaded document.
     * 
     * @return String 
     */
    public String getUploadedDocumentPath()
    {
	return this.uploadedDocument;
    }
    
    private void handleUploadCompleted(String uploadedDocumentPath)
    {
	//close the dialog
	this.progressBar.setProgress(100);
	this.uploadedDocument = uploadedDocumentPath;
	documentUploaded = true;
	debugOutput.setHTML("");
	UploadDialog.this.hide();
    }
  
    /**
     * Returns true, if the document is uploaded false if not.
     * @return boolean
     */
    public boolean isDocumentUploaded()
    {
	return this.documentUploaded;
    }

    @Override
    public void onProgressEventReceived(ProgressEvent event) 
    {
	int progress = event.getProgress();
	if((progress>=0)&&(progress<=100))
	{
	    progressBar.setProgress(progress);
	}
	else if(progress>100)
	{
	    progress = 100;
	    progressBar.setProgress(progress);
	}
    }
 
   /* class ProgessListener implements ProgessChangedListener
    {

	@Override
	public void onProgessChanged(int progress) 
	{
	    if((progress>=0)&&(progress<=100))
		{
		    progressBar.setProgress(progress);
		}
		else if(progress>100)
		{
		    progress = 100;
		    progressBar.setProgress(progress);
		}
	    
	}
	
    }*/
}
