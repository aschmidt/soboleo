package de.fzi.ipe.soboleo.annotateDocuments.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.annotate.client.AnnotateConstants;
import de.fzi.ipe.soboleo.annotate.client.components.ProgressBar;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class FileUploadPanel extends VerticalPanel {

	public FileUploadPanel() {
		setForm();
	}

	FormPanel form;

	private AnnotateConstants constants = GWT.create(AnnotateConstants.class);

	FlexTable flexTable;
	
	VerticalPanel commandPanel;
	Label tb;
	
	private void setForm() {
		// Create a FormPanel and point it at a service.
		form = new FormPanel();
		form.setAction(ClientUtil.getAbsURL() + "uploader");

		// Because we're going to add a FileUpload widget, we'll need to set the
		// form to use the POST method, and multipart MIME encoding.
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);

		flexTable = new FlexTable();
		FlexCellFormatter cellFormatter = flexTable.getFlexCellFormatter();
		flexTable.setWidth("420px");
		flexTable.setCellPadding(3);

		HTML html = new HTML("<h3>" + this.constants.annotateOfficeDocument()
				+ "</h3>");
		html.setStyleName("gwtLabelAnnotate");
		flexTable.setWidget(0, 1, html);

		cellFormatter.setWidth(0, 0, "100px");
		cellFormatter.setWidth(1, 0, "100px");
		cellFormatter.setWidth(2, 0, "100px");

		cellFormatter.setHorizontalAlignment(0, 0,
				HasHorizontalAlignment.ALIGN_CENTER);
		flexTable.getFlexCellFormatter().setColSpan(5, 0, 2);

		// Create a panel to hold all of the form widgets.
		form.setWidget(flexTable);

		commandPanel=new VerticalPanel();
		
		// Create a Label to display the status
		tb = new Label("Please select a file to upload");
		commandPanel.add(tb);
		
		flexTable.setWidget(1, 1, commandPanel);

		// Create a ListBox, giving it a name and some values to be associated
		// with
		// its options.

		//Label fileType = new Label("File type");

		HTML htmlFileType = new HTML("File type:");
		htmlFileType.setStyleName("gwtLabelAnnotate");
		htmlFileType.setWidth("70");
		
		
		final ListBox lb = new ListBox();
		lb.setName("filetype");
		lb.addItem("select", "Select");

		lb.addItem("pdf (Adobe PDF Documents)", "pdf");
		lb.addItem("doc, docx (Microsoft Word Documents)", "doc");
		lb.addItem("xls, xlsx (Microsoft Excel Documents)", "xls");
		lb.addItem("ppt, pptx (Microsoft Powerpoint Documents)", "ppt");

		flexTable.setWidget(2, 0, htmlFileType);
		flexTable.setWidget(2, 1, lb);

		// Create a FileUpload widget.

		//final Label fileDocu = new Label("File");
		
		HTML htmlFile = new HTML("File:");
		htmlFile.setStyleName("gwtLabelAnnotate");
		htmlFile.setWidth("70");
		
		final FileUpload upload = new FileUpload();
		upload.setName("fileToUpload");

		flexTable.setWidget(3, 0, htmlFile);
		flexTable.setWidget(3, 1, upload);

		// Add a 'submit' button.
		Button button = new Button("Submit", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				form.submit();				
			}
		});

		flexTable.setWidget(4, 1, button);

		// Add an event handler to the form.
		form.addSubmitHandler(new FormPanel.SubmitHandler() {
			
			@Override
			public void onSubmit(SubmitEvent event) {
				boolean fileExtensionCorrect=false;
				
				if (lb.getSelectedIndex() == 0) {
					tb.setText("Please set file type");
					event.cancel();
				} else if (upload.getFilename().equals("")) {
					tb.setText("Please select the document to upload");
					event.cancel();
				} 
				
				// selected file type:
				String ft=lb.getValue(lb.getSelectedIndex());
				if (upload.getFilename().indexOf("." +ft) > -1){
					fileExtensionCorrect=true;
				}else{
					tb.setText("Please check selected filetype! Input file " + upload.getFilename() + " has incorrect extension for selected filetype " +ft);						
					event.cancel();
				}
			
				if (fileExtensionCorrect){
					//tb.setText("Starting upload ...");
					setProgressBar();
				}else{
					event.cancel();
				}
			}
		});

		form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
			
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				// When the form submission is successfully completed, this
				// event is
				// fired. Assuming the service returned a response of type
				// text/html,
				// we can get the result text here (see the FormPanel
				// documentation for
				// further explanation).
				String result = event.getResults();
				try {
					System.out.println("result : " +result);
					
					String fields[]=result.split(">");
					
					for (int x=0;x<fields.length;x++)
					{
						System.out.println("f : " +fields[x]);
					}
					
					result=fields[1];
					result = result.replaceAll("</pre", "");
					String results[] = result.split(":");
					
					String status = results[1];
					String uri = results[2];
				
					System.out.println("status : "+status);
					
					
					
					if (status.startsWith("success")) {
						System.out.println("go to uri : " +uri);
						showButtonEditNewUpload(uri);
					} else {
						tb.setText("error: " + status + " result : " + result);
					}
				} catch (Exception e) {
					Window.alert("error : " + e.toString() + " for  " + result);
				}

			}
		});

		add(form);
	}

	private void setProgressBar() {
		final ProgressBar progressBar = new ProgressBar(16,
				ProgressBar.SHOW_TEXT);
		progressBar.setText("Uploading file ...");
		
		commandPanel.remove(tb);
		commandPanel.add(progressBar);
		
		
		Timer t = new Timer() {
			public void run() {
				int progress = progressBar.getProgress() + 1;
				if (progress > 100)
					cancel();
				progressBar.setProgress(progress);
			}
		};
		t.scheduleRepeating(250);
	}

	VerticalPanel ep = new VerticalPanel();

	private void showButtonEditNewUpload(String uri) {
		this.remove(form);

		AnnotateMain am = new AnnotateMain(null, uri);

		this.add(am);

		/*
		 * Label l=new Label("File upload successful"); ep.add(l);
		 * 
		 * 
		 * Label luri=new Label("uri is " +uri); ep.add(luri);
		 * 
		 * HorizontalPanel buttons=new HorizontalPanel(); ep.add(buttons);
		 * 
		 * // Add a 'edit' button. Button buttonedit = new Button("Edit", new
		 * ClickListener() { public void onClick(Widget sender) { // todo : show
		 * edit Window.alert("todo : link to edit panel"); } });
		 * buttons.add(buttonedit);
		 * 
		 * // Add a 'edit' button. Button buttonnew = new
		 * Button("Upload next document", new ClickListener() { public void
		 * onClick(Widget sender) { remove(ep); setForm(); } });
		 * buttons.add(buttonnew);
		 */

	}
}