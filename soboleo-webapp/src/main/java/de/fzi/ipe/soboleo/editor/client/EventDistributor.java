/*
 * Created on 13.08.2006 by zach
 */
package de.fzi.ipe.soboleo.editor.client;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.server.client.ClientUtil;


/**
 * Responsible to distributing events from the server and components to all components of the editor. Note that 
 * this class will work only in a single thread context (JavaScript is only ever executed in one thread) - otherwise
 * it would not be thread safe. 
 */
public class EventDistributor extends Timer implements AsyncCallback<Event[]>{

	private static EventDistributor instance = new EventDistributor();
	
	public static int DELAY_REPEATING_TIME_MS = 3000;
	public static int DELAY_ON_CHANGE_MS = 250;
	
	private List<EditorEventListener> listeners = new ArrayList<EditorEventListener>();
	private boolean isRepeating = false; 
	
	private boolean isCurrentlyDistributingEvents = false;
	private Queue<Event> eventsToDistribute = new LinkedList<Event>();
	
		
	private EventDistributor() { ;}
	
	public static EventDistributor getInstance() {
		return instance;
	}
	
	public void addListener(EditorEventListener listener) {
		if (!listeners.contains(listener)) listeners.add(listener);
	}
	
	public void notify(Event event) {
		eventsToDistribute.add(event);
		if (!isCurrentlyDistributingEvents) {
			notifyPrivate();
		}
	}
	
	private void notifyPrivate() {
		try {
			isCurrentlyDistributingEvents = true;
			while (eventsToDistribute.peek() != null) {
				Event toDistribute = eventsToDistribute.poll();
				for (int i=0;i<listeners.size();i++) {
					EditorEventListener currentListener = (EditorEventListener) listeners.get(i);
					currentListener.notify(toDistribute);
				}
			}
		} finally {
			isCurrentlyDistributingEvents = false;
		}
	}
	
	public void start(long version) {
		ClientUtil.setCurrentVersion(version); 
		scheduleRepeating(DELAY_REPEATING_TIME_MS); 
	}
	
//	@Override
	public void scheduleRepeating(int periodMillis) {
		super.scheduleRepeating(periodMillis);
		isRepeating = true;
	}
	
//	@Override
	public void schedule(int delayMillis) {
		super.schedule(delayMillis);
		isRepeating = false;
	}
	
	public void run() {
		EditorUtil.getEditorService().getEvents(ClientUtil.getCurrentVersion(),ClientUtil.getSpaceName(),ClientUtil.getEventSenderCredentials(), this);
		if (!isRepeating) scheduleRepeating(DELAY_REPEATING_TIME_MS);
	}

	public void onFailure(Throwable caught) {
		EditorUtil.handleException(caught);
	}

	public void onSuccess(Event[] events) {
		if (events != null && events.length > 0) {		
			long previousVersion = ClientUtil.getCurrentVersion();
			ClientUtil.setCurrentVersion(events[events.length-1].getId());
			for (int i=0;i<events.length;i++) {
				if (events[i].getId()> previousVersion) notify(events[i]);
			}
		}
	}

	public void removeListener(EditorEventListener listener) {
		listeners.remove(listener);
		
	}
}
