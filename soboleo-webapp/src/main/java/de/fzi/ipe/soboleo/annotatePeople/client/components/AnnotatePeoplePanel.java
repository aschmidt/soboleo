/*
 * FZI - Information Process Engineering 
 * Created on 03.07.2009 by zach
 */
package de.fzi.ipe.soboleo.annotatePeople.client.components;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.annotatePeople.client.AnnotatePeopleConstants;
import de.fzi.ipe.soboleo.annotatePeople.client.AnnotatePeopleMain;
import de.fzi.ipe.soboleo.annotatePeople.client.rpc.AnnotatePeopleService;
import de.fzi.ipe.soboleo.annotatePeople.client.rpc.AnnotatePeopleServiceAsync;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

/**
 * Popup window that asks the user for the name of the new concept (or to
 * cancel) and then sends the appropriate command
 */
public class AnnotatePeoplePanel extends VerticalPanel {

	private TextBox input;
	private SuggestBox suggestion;
	private AnnotatePeopleMain caller;
	private String emailAddress;
	
	/**
	 * This is the regular expression for the email input field.
	 * Only email addresses which are in that format are accepted.
	 */
	public final static String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	private static AnnotatePeopleServiceAsync annotatePeopleService;
	
	// Internationalization
	private AnnotatePeopleConstants constants = GWT.create(AnnotatePeopleConstants.class);

	public AnnotatePeoplePanel(AnnotatePeopleMain caller) {
		
		this.caller = caller;
	}
	
	public AnnotatePeoplePanel(Throwable caught) {
		
	    if (caught instanceof EventPermissionDeniedException) {
//	    	setContent(constants.permissionDenied(), constants.pleaseLogin());
	    	 String reason = ((EventPermissionDeniedException) caught).getReason();
	    	  reason = reason.replaceAll("\\[error_|\\].*","");
	    	  try{
				switch(Integer.parseInt(reason)){
				case 200: setContent(constants.error_200(),""); break;
				case 201: setContent(constants.error_201(),""); break;
				default: setContent(((EventPermissionDeniedException) caught).getReason(), ""); break;
				}
	    	  } catch(NumberFormatException e){
	    		  setContent(((EventPermissionDeniedException) caught).getReason(), "");
	    	  }
		} else {
			setContent(constants.exceptionOccured(), constants.closeAndReload()+ caught.getMessage()+",<br/>,<br/>"+caught.toString());
		}
	    
		}

	public AnnotatePeoplePanel(String title, String message) 
	{
			
			setContent(title,message);
			
		}
		
	private void setContent(String title, String message) 
	{
			VerticalPanel vPanel = new VerticalPanel();
			vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			HTML headline = new HTML("<h3>"+title+"</h3><br/>"+message);
			vPanel.add(headline);
			final Button okButton = new Button("OK");
			okButton.setStyleName("buttonPannel");
			okButton.addClickHandler(new ClickHandler() 
			{
				@Override
				public void onClick(ClickEvent ce) 
				{
					//Window.Location.reload();
					//AnnotatePeoplePanel.this.hide();
					
				}
			});
			okButton.addMouseOverHandler(new MouseOverHandler() 
			{
				public void onMouseOver(MouseOverEvent event) 
				{
				    okButton.setStyleName("buttonPannelOver");
				}
			});
			okButton.addMouseOutHandler(new MouseOutHandler() 
			{
				public void onMouseOut(MouseOutEvent event) 
				{
				    okButton.setStyleName("buttonPannel");
				}
			});
			vPanel.add(okButton);
			setStyleName("ks-popups-Popup");
			add(vPanel);
	}
	/**
	 * Displays the success message, that the data was saved successfully. 
	 * @param title
	 * @param message
	 */
	public void showSuccessDialog(String title, String message){
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		HTML headline = new HTML("<h3>"+title+"</h3><br/>"+message);
		vPanel.add(headline);
		final Button okButton = new Button("OK");
		okButton.setStyleName("buttonPannel");
		okButton.addClickHandler(new ClickHandler() 
		{
			@Override
			public void onClick(ClickEvent ce) 
			{
				//Window.Location.reload();
				//AnnotatePeoplePanel.this.hide();
			}
		});
		okButton.addMouseOverHandler(new MouseOverHandler() 
		{
			public void onMouseOver(MouseOverEvent event) 
			{
			    okButton.setStyleName("buttonPannelOver");
			}
		});
		okButton.addMouseOutHandler(new MouseOutHandler() 
		{
			public void onMouseOut(MouseOutEvent event) 
			{
			    okButton.setStyleName("buttonPannel");
			}
		});
		vPanel.add(okButton);
		setStyleName("ks-popups-Popup");
		add(vPanel);
		
	}
		
	public VerticalPanel showEnterEmailDialog() 
	{
		VerticalPanel vp=new VerticalPanel();
		

		vp.setStyleName("ks-popups-Popup");

		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(5);
		vp.add(vPanel);

		setTitle(constants.enterPersonsEmail());
		Label label = new Label(constants.pleaseEnterPersonsEmail());
		vPanel.add(label);
		
//		-- Old style input box--
		input = new TextBox();
		input.addKeyUpHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER) processEnterEmail();
			}
		});
		vPanel.add(input);

//		-- New implementation: SuggestBox: autocomplete --
		
		
		suggestion = new SuggestBox(createOracleForEmails(),input);
		suggestion.setLimit(7);
		
		suggestion.setWidth("260px");
		
		vPanel.add(suggestion);

		createSendCancelButtons(vPanel, "email");
		return vp;
	}

	public VerticalPanel showEnterNameDialog(String emailAddress) {
		
		VerticalPanel vc=new VerticalPanel();
		
		this.emailAddress = emailAddress;
		setStyleName("ks-popups-Popup");

		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setSpacing(5);
		vc.add(vPanel);

		setTitle(constants.enterPersonsName());
		Label label = new Label(constants.noPersonForEmail());
//		Label label = new Label("There is no person for this email address.\nPlease enter the person's name.");
		vPanel.add(label);

//		-- Old style input box--
		
		input = new TextBox();
		input.addKeyUpHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if(event.getNativeKeyCode() == KeyCodes.KEY_ENTER) processEnterName();
			}			
		});
		vPanel.add(input);
		
//		-- New implementation: SuggestBox: autocomplete --
		
//		suggestion = new SuggestBox(createOracleForNames(),input);
//		vPanel.add(suggestion);
		createSendCancelButtons(vPanel, "name");

		return vc;
	}
	
	private SuggestOracle createOracleForEmails() {
		final MultiWordSuggestOracle oracle = new MultiWordSuggestOracle(" : ,@");
		annotatePeopleService = getAnnotatePeopleService();
		annotatePeopleService.getTaggedPersons(ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), new AsyncCallback<List<TaggedPerson>>() {
			
			@Override
			public void onSuccess(List<TaggedPerson> people) {
				// TODO Auto-generated method stub
				for(TaggedPerson tp:people){
					oracle.add(tp.getName() + " : " + tp.getEmail());
				}
				
			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
//				Window.alert("E-Mails failed!!");
			}
		});
		
		return oracle;
	}
	
	private SuggestOracle createOracleForNames() {
		final MultiWordSuggestOracle oracle = new MultiWordSuggestOracle(" : ,@");
		annotatePeopleService = getAnnotatePeopleService();
		annotatePeopleService.getTaggedPersons(ClientUtil.getSpaceName(), ClientUtil.getEventSenderCredentials(), new AsyncCallback<List<TaggedPerson>>() {
			
			@Override
			public void onSuccess(List<TaggedPerson> people) {
				// TODO Auto-generated method stub
				for(TaggedPerson tp:people){
					oracle.add(tp.getName() + " : " + tp.getEmail());
				}
			}
			
			@Override
			public void onFailure(Throwable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		return oracle;
	}

	private void createSendCancelButtons(VerticalPanel vPanel, final String type) {
		HorizontalPanel hPanel = new HorizontalPanel();
		hPanel.setSpacing(3);
		
		final Button okButton = new Button(constants.send());
		okButton.setStyleName("buttonPannel");
		okButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent ce) {
				if(!input.getText().trim().equals("")){
					if(type.equals("email")) processEnterEmail();
					else if (type.equals("name")) processEnterName();
				}
				else
				{
					if(type.equals("email"))
					{
						Window.alert("Please enter email adress");
					}
					else
					{
						Window.alert("Please enter name");						
					}
				}
			}
		});
		okButton.addMouseOverHandler(new MouseOverHandler() 
		{
			public void onMouseOver(MouseOverEvent event) 
			{
			    okButton.setStyleName("buttonPannelOver");
			}
		});
		okButton.addMouseOutHandler(new MouseOutHandler() 
		{
			public void onMouseOut(MouseOutEvent event) 
			{
			    okButton.setStyleName("buttonPannel");
			}
		});
		hPanel.add(okButton);

		final Button cancelButton = new Button(constants.cancel());
		cancelButton.setStyleName("buttonPannel");
		cancelButton.addClickHandler(new ClickHandler() 
		{
			@Override
			public void onClick(ClickEvent ce) 
			{
				//Window.Location.reload();
				//AnnotatePeoplePanel.this.hide();
			}
		});
		cancelButton.addMouseOverHandler(new MouseOverHandler() 
		{
			public void onMouseOver(MouseOverEvent event) 
			{
			    cancelButton.setStyleName("buttonPannelOver");
			}
		});
		cancelButton.addMouseOutHandler(new MouseOutHandler() 
		{
			public void onMouseOut(MouseOutEvent event) 
			{
			    cancelButton.setStyleName("buttonPannel");
			}
		});
		hPanel.add(cancelButton);
		
		vPanel.add(hPanel);
	}



	/**
	 * Gets the AnnotatePeopleService
	 * @return AnnotatePeopleServiceAsync
	 */
	public static AnnotatePeopleServiceAsync getAnnotatePeopleService() 
	{
		if (annotatePeopleService == null) 
		{
			annotatePeopleService = (AnnotatePeopleServiceAsync) GWT.create(AnnotatePeopleService.class);
			ServiceDefTarget endpoint = (ServiceDefTarget) annotatePeopleService;
			endpoint.setServiceEntryPoint(ClientUtil.getServerURL()+"annotatePeople/service");
		}
		return annotatePeopleService;
	}
	
	private void processEnterEmail() 
	{
		if(input.getText().contains(" : "))
		{
			String email = input.getText().split(" : ")[1];
			caller.loadAvailablePersonByEmailAndProcess(email);
			
		}
		else
		{
			String email = input.getText().trim();
			
			if(email.matches(EMAIL_REGEX)) 
			{
				caller.getTaggedPersonByEmail(email);
				//AnnotatePeoplePanel.this.hide();
				caller.askForNameOfPerson(email);
			}
			else 
			{
				Window.alert("Email not valid! Please check your input.");
			}
		}
			
	}
	
	private void processEnterName() {
		if(input.getText().contains(":"))
		{
			String name = input.getText().split(" : ")[0];
			caller.createTaggedPerson(name, emailAddress);
		} 
		else
		{
			//caller.createTaggedPerson(input.getText(), emailAddress);
			caller.addNewPersonByEnteredNameAndMail(emailAddress,input.getText());
		}
		//AnnotatePeoplePanel.this.hide();
	}
}
