/*
 * FZI - Information Process Engineering 
 * Created on 25.06.2009 by zach
 */
package de.fzi.ipe.soboleo.editor.client;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.TaxonomyChangedEvent;
import de.fzi.ipe.soboleo.event.ontology.ComplexTaxonomyChangeCommand;


/**
 * Responsible for updating the taxonomy based on events send by the eventDistributor
 */
public class TaxonomyUpdater implements EditorEventListener {

	private ClientSideTaxonomy tax;
	private EventDistributor eventDistributor;
	
	
	/**
	 * Constructor
	 * @param tax
	 * @param eventDistributor
	 */
	public TaxonomyUpdater(ClientSideTaxonomy tax, EventDistributor eventDistributor) {
		this.tax = tax;
		this.eventDistributor = eventDistributor;
	}
	
	@Override
	public void notify(Event event) {
		if (event instanceof ComplexTaxonomyChangeCommand) {
			ComplexTaxonomyChangeCommand command = (ComplexTaxonomyChangeCommand) event;
			this.tax.apply(command);
			this.eventDistributor.notify(new TaxonomyChangedEvent(command));
		}
	}

}
