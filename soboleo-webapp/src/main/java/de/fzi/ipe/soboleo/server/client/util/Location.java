   /*
    * Copyright 2006 Robert Hanson <iamroberthanson AT gmail.com>
    * 
    * Licensed under the Apache License, Version 2.0 (the "License");
    * you may not use this file except in compliance with the License.
    * You may obtain a copy of the License at
    * 
    *    http://www.apache.org/licenses/LICENSE-2.0
    * 
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
   */
  
  package de.fzi.ipe.soboleo.server.client.util;
  
  import java.util.HashMap;
import java.util.Map;
  
  public class Location
  {
      private String hash;
      private String host;
      private String hostName;
      private String href;
      private String path;
      private String port;
      private String protocol;
      private String queryString;
      private HashMap<String,String> paramMap;
  
  
     protected void setQueryString (String queryString)
     {
         this.queryString = queryString;
         paramMap = new HashMap<String,String>();
         
         if (queryString != null && queryString.length() > 1) {
             String qs = queryString.substring(1);
             String[] kv = qs.split("furi=|&title=");
             if(kv.length == 3){
            	 paramMap.put("furi", decodeURIComponent(kv[1]));
            	 paramMap.put("title", decodeURIComponent(kv[2]));
             }
             else {
            	 paramMap.put("furi", "");
            	 paramMap.put("title", "");
             }
         }
     }
     
     public static native String decodeURIComponent (String val) /*-{
     return decodeURIComponent(val);
 	}-*/;
     
     public native String unescape (String val) /*-{
         return unescape(val);
     }-*/;
 
     public String getParameter (String name)
     {
         return (String) paramMap.get(name);
     }
 
     public Map<String,String> getParameterMap ()
     {
         return paramMap;
     }

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getQueryString() {
		return queryString;
	}
  }