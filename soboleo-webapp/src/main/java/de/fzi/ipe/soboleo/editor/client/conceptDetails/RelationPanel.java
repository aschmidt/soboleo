package de.fzi.ipe.soboleo.editor.client.conceptDetails;

import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CaptionPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EditorEventListener;
import de.fzi.ipe.soboleo.editor.client.EditorUtil;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.editor.client.components.CreateConceptPopup;
import de.fzi.ipe.soboleo.editor.client.components.EditorPopup;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.editor.ChangeConceptSelectionEvent;
import de.fzi.ipe.soboleo.event.editor.ConceptSelectionChangedEvent;
import de.fzi.ipe.soboleo.event.editor.LoadTaxonomyEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyChangedEvent;
import de.fzi.ipe.soboleo.event.editor.TaxonomyLoadedEvent;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.AddTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ChangeTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ComplexTaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveTextCmd;
import de.fzi.ipe.soboleo.event.ontology.TaxonomyChangeCommand;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveAddConnection;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveAddText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveChangeText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveConcept;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveConnection;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveRemoveText;
import de.fzi.ipe.soboleo.event.ontology.primitive.PrimitiveTaxonomyChangeCommand;

public class RelationPanel extends CaptionPanel implements EditorEventListener{

	private VerticalPanel contentPanel;
	private SuggestBox suggest;
	private TextBox inputField;
	private MultiWordSuggestOracle oracle=new MultiWordSuggestOracle();
	private boolean oracleUpdateNeccessary = false;
	private Button add;
	private VerticalPanel relationPanel;
	private RelationComponent relTable = null;
	private EventDistributor eventDistrib;
	
	private ClientSideTaxonomy tax;
	private ClientSideConcept currentItem = null;
	private SKOS skosType;
	
	// Internationalisation
	private EditorConstants constants = GWT.create(EditorConstants.class);
	
	
	public RelationPanel(EventDistributor eventDistrib, SKOS skosType, String caption){
		this.eventDistrib = eventDistrib;
		eventDistrib.addListener(this);
		setCaptionText(caption);
		this.skosType = skosType;
		
		contentPanel = new VerticalPanel();
		relationPanel = new VerticalPanel();
		contentPanel.add(relationPanel);
		contentPanel.add(createInputPanel());
		setContentWidget(contentPanel);
	}

	private HorizontalPanel createInputPanel() {
		HorizontalPanel inputPanel = new HorizontalPanel();
		inputPanel.setStylePrimaryName("inputPanel");
		inputPanel.setSpacing(2);
		
		inputField  = new TextBox(); 
		inputField.setWidth("275px");
		inputField.addKeyUpHandler(new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) sendAddRelationCmd();				
		}});
		
		suggest = new SuggestBox(oracle, inputField);
		suggest.setStyleName("gwt-SuggestBox");
		suggest.setLimit(6);
		suggest.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if(oracleUpdateNeccessary){
					updateOracle();
					oracleUpdateNeccessary = false;
				}
			}});
		inputPanel.add(suggest);
		
		add = new Button();
		add.setText(constants.add());
//		add.setText("add");
		add.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent arg0) {
				sendAddRelationCmd();
			}
		});
		inputPanel.add(add);
		
		return inputPanel;
	}
	
	public void sendAddRelationCmd(){
		if (currentItem != null) {
			Set<ClientSideConcept> concepts = tax.getConceptsForPrefLabel(suggest.getText()); 
			if (concepts.isEmpty()) {
				new CreateConceptPopup(suggest.getText(), currentItem.getURI(), skosType);
				suggest.setText("");
			}
			else {
				suggest.setText("");
				AddConnectionCmd command = new AddConnectionCmd(currentItem.getURI(),skosType,concepts.iterator().next().getURI());
				EditorUtil.sendCommand(command);
			}
		}
	}	
	
	
	protected void sendRemoveRelationCmd(ClientSideConcept toRemove) {
		if (currentItem != null) {
			RemoveConnectionCmd command = new RemoveConnectionCmd(currentItem.getURI(),skosType,toRemove.getURI());
			EditorUtil.sendCommand(command);
		}
	}
	

	public void notify(Event event) {
		if (event instanceof LoadTaxonomyEvent) {
			oracleUpdateNeccessary = true;
			resetComponents();
			currentItem = null;
		}
		else if (event instanceof TaxonomyLoadedEvent) {
			TaxonomyLoadedEvent loadedEvent = (TaxonomyLoadedEvent) event;
			this.tax = loadedEvent.getTaxonomy();
		}
		else if (event instanceof ConceptSelectionChangedEvent) {
			ConceptSelectionChangedEvent csce = (ConceptSelectionChangedEvent) event;
			resetComponents();
			currentItem = csce.getConcept();
			if (currentItem != null) {
				for (ClientSideConcept currentConnected: currentItem.getConnectedAsConcepts(skosType,tax)){
					processAddRelation(currentConnected);
				}
			}
		}
		else if (currentItem != null && event instanceof TaxonomyChangedEvent) {
			processTaxonomyChangeCommand(((TaxonomyChangedEvent) event).getCommand());
		}		
	}


	
	private void processTaxonomyChangeCommand(TaxonomyChangeCommand command) {
		if (command instanceof ComplexTaxonomyChangeCommand) {
			ComplexTaxonomyChangeCommand complexCommand = (ComplexTaxonomyChangeCommand) command;
			if (complexCommand instanceof CreateConceptCmd) oracleUpdateNeccessary = true;
			else if (complexCommand instanceof ChangeTextCmd)oracleUpdateNeccessary = true;
			else if (complexCommand instanceof RemoveConceptCmd)oracleUpdateNeccessary = true;
			else if (complexCommand instanceof AddTextCmd)oracleUpdateNeccessary = true;
			else if (complexCommand instanceof RemoveTextCmd)oracleUpdateNeccessary = true;	

			for (TaxonomyChangeCommand current: complexCommand.getImpliedCommands()) {
				processTaxonomyChangeCommand(current);
			}
		}
		else if (command instanceof PrimitiveTaxonomyChangeCommand) {
			PrimitiveTaxonomyChangeCommand primitiveCommand = (PrimitiveTaxonomyChangeCommand) command;
			if (primitiveCommand.getFromURI().equals(currentItem.getURI())) {
				if (primitiveCommand instanceof PrimitiveRemoveConcept) {
					resetComponents();
					currentItem = null;
				}
				else if (primitiveCommand instanceof PrimitiveAddConnection) {
					PrimitiveAddConnection addRelCmd = (PrimitiveAddConnection) primitiveCommand;
					if (addRelCmd.getConnection().equals(skosType)) {
						processAddRelation(tax.get(addRelCmd.getToURI()));
					}
				}
				else if (primitiveCommand instanceof PrimitiveRemoveConnection) {
					PrimitiveRemoveConnection removeRel = (PrimitiveRemoveConnection) primitiveCommand;
					if (removeRel.getConnection().equals(skosType)) {
						processRemoveRelation(tax.get(removeRel.getToURI()));
					}
				}
			}
			else{ 
				if (primitiveCommand instanceof PrimitiveChangeText) {
					PrimitiveChangeText changeTextCmd = (PrimitiveChangeText) primitiveCommand;
					if (changeTextCmd.getConnection()==SKOS.PREF_LABEL){
						if(relTable != null) 
							relTable.updateRelation(tax.get(changeTextCmd.getFromURI()));
					}
				}
				else if (primitiveCommand instanceof PrimitiveAddText) {
					PrimitiveAddText addTextCmd = (PrimitiveAddText) primitiveCommand;
					if (addTextCmd.getConnection()==SKOS.PREF_LABEL){
						if(relTable != null) 
							relTable.updateRelation(tax.get(addTextCmd.getFromURI()));
					}
				}
				else if (primitiveCommand instanceof PrimitiveRemoveText) {
					PrimitiveRemoveText addTextCmd = (PrimitiveRemoveText) primitiveCommand;
					if (addTextCmd.getConnection()==SKOS.PREF_LABEL){
						if(relTable != null) 
							relTable.updateRelation(tax.get(addTextCmd.getFromURI()));
					}
				}
			}
		}
		
	}

	private void processRemoveRelation(ClientSideConcept toRemove) {
		if(relTable != null) {
			relTable.removeRelation(toRemove);
			//check if table is empty now
			if(relTable.getRowCount()==0) {
				relationPanel.remove(relTable);
			}
		}
	}
	
	private void processAddRelation(ClientSideConcept toAdd) {
		if(relTable == null){
			relTable = new RelationComponent(this);
			relationPanel.insert(relTable,0);
		}
		relTable.addRelation(toAdd);
	}
	
	private void resetComponents(){
		relTable = null;
		relationPanel.clear();
	}
	
	protected SKOS getSkosType(){
		return skosType;
	}
	
	 private void updateOracle() {
		  oracle.clear();
		  for (ClientSideConcept c: tax.getConcepts()) {
			  for (LocalizedString ls: c.getTexts(SKOS.PREF_LABEL)) {
				  oracle.add(ls.getString());
			  }
		  }
	}

	public void sendSetConceptSelectionEvent(ClientSideConcept toSelect) {
		if(toSelect != null) eventDistrib.notify(new ChangeConceptSelectionEvent(toSelect));
	}
}
