package de.fzi.ipe.soboleo.annotatePeople.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;


public class PeoplePanel implements EntryPoint {

    public void onModuleLoad() {

    	
    	AnnotatePeopleMain am=new AnnotatePeopleMain(null);    	
    	RootPanel.get("annotatePeople").add(am);	
    	
    	focusWindow();
    }

    private static native void focusWindow() /*-{
    	return $wnd.focus();
	}-*/;

}
