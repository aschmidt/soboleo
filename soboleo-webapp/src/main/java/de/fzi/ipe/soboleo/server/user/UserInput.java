package de.fzi.ipe.soboleo.server.user;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import de.fzi.ipe.soboleo.server.*;
import de.fzi.ipe.soboleo.space.*;
import de.fzi.ipe.soboleo.user.User;
import de.fzi.ipe.soboleo.user.UserDatabase;

import javax.servlet.ServletRequest;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;

/**
 *  Just a class used as temporary data holder during entering / changing of the user data. 
 * 
 * @author FZI - Information Process Engineering 
 * Created on 05.03.2009 by zach
 *
 */
public class UserInput {

	private String email="";
	private String name="";
	private String link="";
	private String uri=null;
	private String password1="";
	private String password2="";
	private String key=null; 
	private String defaultSpace=null;
	private String defaultLanguage="en";
	private Set<String> spaces;
	
	/**
	 * Constructor. It only creates the object without initializing.  
	 */
	public UserInput () 
	{
		;
	}
/*	
 * 
 * This is outlined, because it is replaced by the function beyond.
 * it checks if the spaces stored in the user still exists any longer. If not, in case someone has removed
 * it on the storage, it will be removed from the user. 
	/**
	 * Stores the given user data in this class. Stored values are:
	 * email, name, link, uri, key, spaces, defaultSpace, defaultLanguage
	 * @param user
	 * @throws IOException
	 *
	public void load(User user) throws IOException{
		this.email = user.getEmail();
		this.name = user.getName();
		this.link = user.getLink();
		this.uri = user.getURI();
		this.key = user.getKey();
		this.spaces = user.getSpaceIdentifier();
		this.defaultSpace = user.getDefaultSpaceIdentifier();
		this.defaultLanguage = user.getDefaultLanguage().getAcronym();
	}*/
	
	/**
	 * Stores the given user data in this class. Stored values are:
	 * email, name, link, uri, key, spaces, defaultSpace, defaultLanguage.
	 * It checks if the spaces stored in the user object still exists any longer. If not, in case someone has removed
	 * it on the storage, it will be removed from the user. 
	 * 
	 * @param user
	 * @param server 
	 * @throws IOException
	 */
	public void load(User user,Server server) throws IOException{
		this.email = user.getEmail();
		this.name = user.getName();
		this.link = user.getLink();
		this.uri = user.getURI();
		this.key = user.getKey();
		Spaces existingSpaces = server.getSpaces();
		for(String space: user.getSpaceIdentifier() )
		{
		    boolean spaceExists = false;
		    for(String existingSpace : existingSpaces.getSpaces())
		    {
			if(space.contentEquals(existingSpace))
			{
			    spaceExists = true;
			    break;
			}
		    }
		    if(!spaceExists)
		    {
			user.removeSpaceIdentifier(space);
		    }
		}
		this.spaces = user.getSpaceIdentifier();
		this.defaultSpace = user.getDefaultSpaceIdentifier();
		this.defaultLanguage = user.getDefaultLanguage().getAcronym();
	}
	
	/**
	 * Stores the given user data from a ServletRequest.
	 * Stored parameters are: email, name, link, uri, defaultSpace, defaultLanguage, password1 password2
	 * @param request
	 */
	public void load(ServletRequest request) {
		this.email = (String) request.getParameter("email");
		this.name = (String) request.getParameter("name");
		this.link = (String) request.getParameter("link");
		this.uri = (String) request.getParameter("uri");
		this.defaultSpace = (String) request.getParameter("defaultSpace");
		this.defaultLanguage = (String) request.getParameter("defaultLanguage");
		
		this.password1= (String) request.getParameter("password1");
		this.password2= (String) request.getParameter("password2");
	}
	
	/**
	 * Stores the data stored into the userInput object into a user object.
	 * If the UserInput object does not have a URI, then the User doesn't exists and is created. 
	 * If the URI exists, the data will be stored into this User.
	 * @param userDatabase
	 * @throws IOException
	 */
	public void save(UserDatabase userDatabase) throws IOException {
		User user;
		if (this.uri == null) 
		{
		    user = userDatabase.createNewUser();
		}
		else 
		{
		    user = userDatabase.getUser(this.uri);
		}

		if (email != null) user.setEmail(email);
		if (name != null) user.setName(name);
		if (link != null) user.setLink(link);
		if (defaultSpace != null) user.setDefaultSpaceIdentifier(defaultSpace);
		if (defaultLanguage != null) user.setDefaultLanguage(LocalizedString.Language.getLanguage(defaultLanguage));
		if (password1 != null && password1.trim().length()>0) user.setPassword(password1);
	}
	
	/**	
	 * Returns any problems that exist with the given data (or null if there is none)
	 * @param userDatabase 
	 * @return String
	 * @throws IOException 
	 */
	public String validate(UserDatabase userDatabase) throws IOException {
		User oldUser = null;
		if (uri != null) oldUser = userDatabase.getUser(uri);
		
		if (password1!=null && password2 != null) {
			if (!password1.equals(password2)) return "Passwords do not match";
		}
		if (email == null || email.length() == 0) return "Email must not be null";
		if (email != null) {
			User userForEmail = userDatabase.getUserForEmail(email);
			if (userForEmail != null) {
				if (oldUser != null) {
					if (!userForEmail.equals(oldUser)) return "Email is already in use";
				}
				else return "Email is already in use";
			}
		}
		return null;
	}

	/**
	 * Returns the actual stored email address in the UserInput.
	 * @return String
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Returns the actual stored name of the user in the UserInput.
	 * @return String
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the actual stored link of the user in the UserInput.
	 * @return String
	 */
	public String getLink() {
		return link;
	}

	/**
	 * Returns the actual stored URI of the user in the UserInput.
	 * @return String
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Returns the actual stored key of the user in the UserInput.
	 * @return String
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * Returns the actual stored default language of the user in the UserInput.
	 * @return String
	 */
	public String getDefaultLanguage() {
		return defaultLanguage;
	}
	/**
	 * Returns the actual stored default space of the user in the UserInput.
	 * @return String
	 */
	public String getDefaultSpace() {
		return defaultSpace;
	}
	
	/**
	 * Returns the actual stored Set of SpaceIdentifiers of the user in the UserInput.
	 * If there is no one present it will return Collections.EMPTY_SET.
	 * @return Set<String>
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getSpaceIdentifier() {
		return (spaces != null) ? this.spaces : Collections.EMPTY_SET;
	}
	
	
}
