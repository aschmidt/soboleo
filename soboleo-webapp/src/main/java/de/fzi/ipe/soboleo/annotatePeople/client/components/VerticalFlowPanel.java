package de.fzi.ipe.soboleo.annotatePeople.client.components;

/**
 * Vertical alignment of panels. Not used at the moment.
 * 
 * @author Stephan Kluge
 *
 */
public class VerticalFlowPanel extends AnnotateFlowPanel {
	@Override
	protected String getFlowStyle() {
		return "block";
	}
}
