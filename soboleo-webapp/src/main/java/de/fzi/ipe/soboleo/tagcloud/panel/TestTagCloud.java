package de.fzi.ipe.soboleo.tagcloud.panel;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

public class TestTagCloud implements EntryPoint {

	
	public void onModuleLoad() {

		
		TagCloud tc=new TagCloud();
		
		// initialize the tags
		for (int x=0;x<20;x++)
		{
			WordTag wt=new WordTag("tryme " +x, "http://www.mylink.de/"+x);
			tc.addNewWord(wt, x);
		}
		
		
		
		RootPanel.get("tagcloud").add(tc);
	}
	
}
