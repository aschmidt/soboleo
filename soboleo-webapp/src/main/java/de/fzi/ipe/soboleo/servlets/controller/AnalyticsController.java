package de.fzi.ipe.soboleo.servlets.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.space.Space;

@Controller
public class AnalyticsController {

	private final static Logger logger = Logger.getLogger(AnalyticsController.class);

	@RequestMapping("/analytics")
	protected String doExpertiseAnalytics(HttpServletRequest rq)
			throws EventPermissionDeniedException, ServletException,
			IOException {
		EventSenderCredentials cred = (EventSenderCredentials) rq.getAttribute(ControllerConstants.CRED);
		Space space = (Space) rq.getAttribute(ControllerConstants.USER_SPACE);
		logger.info("check Permission");
		space.getEventBus().checkReadPermission(cred);
		return "tagcloud";
	}

}
