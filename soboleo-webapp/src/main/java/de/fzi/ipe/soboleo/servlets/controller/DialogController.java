package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CRED_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.DIALOG_ID;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.beans.dialog.Dialog;
import de.fzi.ipe.soboleo.event.logging.BrowseDialog;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetDialog;
import de.fzi.ipe.soboleo.space.Space;

@Controller
public class DialogController {

	protected ModelAndView doShowDialog(
			@RequestParam(DIALOG_ID) String dialogID,
			@Value(CRED_SPEL) EventSenderCredentials cred,
			@Value(USER_SPACE_SPEL) Space space,
			HttpServletRequest rq)
			throws EventPermissionDeniedException {
		
		Dialog dialog = (Dialog) space.getEventBus().executeQuery(
				new GetDialog(TripleStore.SOBOLEO_NS + dialogID), cred);
		space.getEventBus().sendEvent(new BrowseDialog(dialog.getURI()), cred);
		return new ModelAndView("dialog","dialogToShow", dialog);
	}

}
