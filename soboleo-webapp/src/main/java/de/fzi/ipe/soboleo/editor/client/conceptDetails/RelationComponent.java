package de.fzi.ipe.soboleo.editor.client.conceptDetails;

import java.util.ArrayList;
import java.util.Collections;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PushButton;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConceptComparator;
import de.fzi.ipe.soboleo.server.client.ClientUtil;

public class RelationComponent extends FlexTable{
	
	private RelationPanel parent;
	private ArrayList<ClientSideConcept> tableRows = new ArrayList<ClientSideConcept>();
	
	public RelationComponent(RelationPanel parent){
		this.parent = parent;
		setStylePrimaryName("relationComponent");
	}
	
	  /**
	   * Add a new row for the given label.
	   */
	  public void addRelation(final ClientSideConcept concept) {
		int numRows = insertRowData(concept);
		LocalizedString label = concept.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage());
		HTML relationHTML = new HTML("<a href=\"javascript:undefined;\">" + label.getString() + "</a>");
		relationHTML.setStylePrimaryName("relationHTML");
		if(label.getLanguage().equals(ClientUtil.getUserLanguage())) relationHTML.removeStyleDependentName("italic");
		else relationHTML.addStyleDependentName("italic");
		relationHTML.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent arg0) {
				// TODO Auto-generated method stub
				parent.sendSetConceptSelectionEvent(concept);
			}
		});

		PushButton removeButton = new PushButton(ClientUtil.getImageProvider().crossMouseOut2().createImage(),ClientUtil.getImageProvider().crossMouseOver().createImage(), new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				parent.sendRemoveRelationCmd(concept);
			}
	    });
				
		if(numRows <= getRowCount()) insertRow(numRows);
		setWidget(numRows, 0, relationHTML);			   
	    setWidget(numRows, 2, removeButton);
	  }

	  /**
	   * Remove a the row for the given label.
	   * 
	   */
	 public void removeRelation(ClientSideConcept toRemove) {
		 int i = tableRows.indexOf(toRemove);
		 if(tableRows.remove(toRemove))	{
			 removeRow(i);
		 }
	  }
	 

	public void updateRelation(ClientSideConcept toUpdate) {
		int i = tableRows.indexOf(toUpdate);
		if(i != -1){
			LocalizedString label = toUpdate.getBestFitText(SKOS.PREF_LABEL, ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage());
			HTML updateHTML = ((HTML)this.getWidget(i, 0));
			updateHTML.setHTML("<a href=\"javascript:undefined;\">" + label.getString() + "</a>");
			if(label.getLanguage().equals(ClientUtil.getUserLanguage())) updateHTML.removeStyleDependentName("italic");
			else updateHTML.addStyleDependentName("italic");
		}
	}
	  
	  private int insertRowData(ClientSideConcept newConcept){
		  tableRows.add(newConcept);
		  Collections.sort(tableRows, new ClientSideConceptComparator(ClientUtil.getUserLanguage(), ClientUtil.getSpaceLanguage()));
		  return tableRows.indexOf(newConcept); 
	  }


}
