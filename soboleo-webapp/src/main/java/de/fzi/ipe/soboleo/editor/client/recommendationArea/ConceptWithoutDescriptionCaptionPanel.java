package de.fzi.ipe.soboleo.editor.client.recommendationArea;

import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptWithoutDescription;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public class ConceptWithoutDescriptionCaptionPanel extends RecommendationCaptionPanel<ConceptWithoutDescription>{

	public ConceptWithoutDescriptionCaptionPanel(EventDistributor eventDistrib, String caption) {
		super(eventDistrib, caption);
	}

	@Override
	public void addRecommendation(GardeningRecommendation rec){
		String preText = constants.recsConcept();
		String fillingText = " ";
		String sufText = " ";
		if(rec instanceof ConceptWithoutDescription){
			fillingText = constants.recsLacksDescription();
			createDisplay(preText,fillingText,sufText,rec.getConcept());
		}
		
	}

	@Override
	public void removeRecommendation() {
		// TODO Auto-generated method stub
	}	
}
