package de.fzi.ipe.soboleo.annotate.client.components;

import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.Widget;

public class SuggestBoxEventListener implements KeyboardListener, ClickListener{
	
	SuggestBox sgBox;
	ListBox lb;
	
	public SuggestBoxEventListener(){
		
	}
	
	public SuggestBoxEventListener(SuggestBox sg, ListBox lb){
		this.sgBox = sg;
		this.lb = lb;
	}
	public void onKeyUp(Widget sender, char arg1, int modifiers) {
		String current = sgBox.getText();
		// if comma or semicolon is entered
		if(arg1 == 188){
			if (!current.replaceAll(",*;*","").trim().equals("")) {
				lb.addItem(current.replaceAll(",*;*","").trim());
				sgBox.setText("");
			}
			return;
		}
		if (arg1 == KEY_ENTER) {
			if (!current.trim().equals("")) {
				lb.addItem(current);
				sgBox.setText("");
			}
		}
	}	
	
//	public abstract void onKeyUp(Widget arg0, char arg1, int arg2, SuggestBox sb, ListBox lb);


	public void onKeyDown(Widget sender, char keyCode, int modifiers) {
		// TODO Auto-generated method stub
		
	}


	public void onKeyPress(Widget sender, char keyCode, int modifiers) {
		// TODO Auto-generated method stub		
	}

	public void onClick(Widget sender) {
		// TODO Auto-generated method stub
		
	}
	public void extendedOnKeyUp(Widget arg0, char arg1, int arg2, SuggestBox sb, ListBox lb){
	}
}
