package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.CRED_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_SPACE_SPEL;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER_SPEL;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.fzi.ipe.soboleo.beans.officedocument.DownloadInformation;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;
import de.fzi.ipe.soboleo.webdocument.officedocument.DocumentLoader;
import de.fzi.ipe.soboleo.webdocument.officedocument.DocumentSaver;

@Controller
public class DocumentController {
	private final Logger logger = Logger.getLogger(this.getClass());

	@RequestMapping("/dl")
	public void doGetDocument(
			@RequestParam(value = "duri", required = false) String duri,
			@Value(USER_SPACE_SPEL) Space space,
			@Value(CRED_SPEL) EventSenderCredentials cred,
			@Value(USER_USER_SPEL) User user,
			final HttpServletRequest rq, final HttpServletResponse response)
			throws IOException {

		logger.info("duri: " + duri);

		try {

			DocumentLoader dl = new DocumentLoader();
			// get the document stream
			DownloadInformation di = dl.getDownloadInformation(user, cred,
					space, duri);

			final OutputStream outstream = response.getOutputStream();

			response.addHeader("Content-Type", "application/octet-stream");
			response.addHeader("Content-Disposition", "attachment; filename="
					+ di.getFileName());
			response.addHeader("Pragma", "public");
			response.addHeader("Cache-Control", "max-age=0");

			if (di.getIs() != null) {
				byte send[] = new byte[di.getIs().available()];
				di.getIs().read(send);

				outstream.write(send);
				outstream.flush();
				outstream.close();
				di.getIs().close();

			}
		} catch (final Exception e) {
			this.logger.error(e);
			try {
				PrintWriter out = response.getWriter();
				out = response.getWriter();
				out.println("error: " + e);
			} catch (final Exception f) {

			}
		}
	}

	@RequestMapping("/uploader")
	public void doUpload(@CookieValue("SoboleoCookie") String user,
			final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		String fileType = null;
		String fileName = null;
		InputStream is = null;

		PrintWriter out = response.getWriter();

		if (isMultipart) {

			ServletFileUpload upload = new ServletFileUpload();

			try {
				@SuppressWarnings("unchecked")
				List<FileItem> items = upload.parseRequest(request);
				FileItem fileItem = null;
				for (FileItem item : items) {
					if (item.isFormField()) {

						if (item.getFieldName().equals("filetype")) {
							fileType = item.getString();
						}

					}
					if (!item.isFormField()) {
						
						fileItem = item; 
						is=fileItem.getInputStream();
						logger.info("file content type:"  +fileItem.getContentType());
						
						logger.info("file name  : " +fileItem.getName());
						fileName = item.getName();
						
						if (fileName != null) {
							fileName = FilenameUtils.getName(fileName);
							
							// important for UNIX - replace ' ' with "_"
							fileName=fileName.replaceAll(" ", "_");
							
							logger.info("file name short : " +fileName);

							fileItem = item;
						}
						logger.info("file is available :" + is.available());
					}
				}

				if (is != null) {
					/**
					 * out.write("user : " +user + "\n");
					 * out.write("filetype : " +fileType + "\n");
					 * out.write("filename : " +fileName + "\n");
					 * out.write("input stream : " +is.available());
					 **/
					// save it
					DocumentSaver ds = new DocumentSaver();

					String docuri = ds.getUploadResult(user, fileName,
							fileType, is);
					out.write("status:success-docuri:" + docuri);
				}
			} catch (Exception e) {
				out.write("status:error-error:" + e.toString());
			}
		}
	}

}
