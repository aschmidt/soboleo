package de.fzi.ipe.soboleo.servlets.controller;

import static de.fzi.ipe.soboleo.servlets.SessionConstants.COOKIE;
import static de.fzi.ipe.soboleo.servlets.controller.ControllerConstants.USER_USER_SPEL;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.server.user.UserInput;
import de.fzi.ipe.soboleo.user.User;
import de.fzi.ipe.soboleo.user.UserDatabase;

@Controller
public class LoginController
{

	private static Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	protected Server server;

	@RequestMapping("/login")
	public ModelAndView login(
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "redirectURL", required = false) String redirectURL,
			HttpServletResponse response)
	{
		if ((email == null) || (password == null))
			return new ModelAndView("login");

		UserDatabase userDatabase = server.getUserDatabase();

		if ((redirectURL == null) || (redirectURL.trim().isEmpty()))
			redirectURL = server.getServerAbsoluteURL();

		try
		{
			User user = userDatabase.getUserForEmail(email);
			if (user != null)
			{
				if (user.isPassword(password))
				{
					Cookie cookie = new Cookie(COOKIE, user.getKey());
					cookie.setPath("/");
					cookie.setMaxAge(Integer.MAX_VALUE);
					response.addCookie(cookie);
					return new ModelAndView("redirect:" + redirectURL);
				}
			}
		} catch (IOException e)
		{
			logger.error("Error checking login credentials", e);
		}

		ModelMap m = new ModelMap();
		m.put("redirectURL", redirectURL);
		m.put("failed", Boolean.TRUE);
		return new ModelAndView("login", m);

	}
 
	@RequestMapping("/logout")
	public String logout(HttpServletRequest req, HttpServletResponse response)
	{
		Cookie cookie = new Cookie(COOKIE, "");
		cookie.setMaxAge(0);
		response.addCookie(cookie);
		return "redirect:/";
	}

	@RequestMapping("/userSettings")
	public ModelAndView userSettings(
			@RequestParam(value = "uri", required = false) String uri,
			@RequestParam(value = "save", required = false) String save,
			@Value(USER_USER_SPEL) User user, HttpServletRequest request,
			ModelMap m)
	{
		UserDatabase userDatabase = server.getUserDatabase();
		UserInput userInput = new UserInput();

		if (uri != null)
		{
			try
			{
				userInput.load(userDatabase.getUser(uri), server);
				m.put("userInput", userInput);
			} catch (IOException e)
			{
				logger.error("Error loading user data",e);
			}
		}

		if (save != null)
		{
			String validation = null;

			userInput.load(request);
			try
			{
				validation = userInput.validate(userDatabase);
				if (validation == null)
				{
					userInput.save(userDatabase);
					return new ModelAndView("redirect:/");
				}
			} catch (IOException e)
			{
				logger.error("Error validating & saving user input",e);
			}
		}

		m.put("user",user);
		return new ModelAndView("userSettings", m);
	}
}