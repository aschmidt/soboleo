package de.fzi.ipe.soboleo.editor.client.recommendationArea;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptRecommendation;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public class SuperRelatedConceptCaptionPanel extends RecommendationCaptionPanel<ConceptRecommendation>{

	public SuperRelatedConceptCaptionPanel(EventDistributor eventDistrib, String caption) {
		super(eventDistrib, caption);
	}

	@Override
	public void addRecommendation(GardeningRecommendation rec) {
		String preText = constants.recsConcept();
		String fillingText = "";
		ConceptRecommendation conceptRec = ((ConceptRecommendation) rec);
		if(conceptRec.getType().equals(SKOS.RELATED)) fillingText = constants.recsIsRelatd();
		else fillingText = constants.recsIsNarrower();
		String sufText = "  (" +conceptRec.getConfidence() +")";
		
		createDisplay(preText, fillingText + " ", sufText, conceptRec.getConcept(), conceptRec.getRecommendation());

	}	
	
	@Override
	public void removeRecommendation() {
		// TODO Auto-generated method stub
	}	
}
