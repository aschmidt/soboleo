package de.fzi.ipe.soboleo.annotate.client;

import com.google.gwt.i18n.client.Constants;

/**
 * Class with constants for the annotate web document page/popup
 * @author FZI
 *
 */
public interface AnnotateConstants extends Constants{
	/**
	 * Headline for the AnnotateWebDoc page
	 * @return String "Tag Web Document"
	 */
	@DefaultStringValue("Tag Web Page")
	String annotate();
	
	
	/**
	 * Headline for the AnnotateWebDoc page
	 * @return String "Tag Web Document"
	 */
	@DefaultStringValue("Tag Office Document")
	String annotateOfficeDocument();
	
	/**
	 * Text for the label of the current topics panel/box
	 * @return String "Current Topics:"
	 */
	@DefaultStringValue("Current Topics:")
	String currentTopics();
	/**
	 * Title for the web page
	 * @return String "Title:"
	 */
	@DefaultStringValue("Title:")
	String title();
	/**
	 * Title for the web page
	 * @return String "Rating:"
	 */
	@DefaultStringValue("Rating:")
	String rating();
	/**
	 * The string for the input panel for new topics
	 * @return String "New Topic:"
	 */
	@DefaultStringValue("New Topic:")
	String topics();
	/**
	 * String for the remove button to remove a topic
	 * @return String "Remove Topic"
	 */
	@DefaultStringValue("Remove Topic")
	String removeTopic();
	/**
	 * String for the Save button to save the current state of the annotate window
	 * @return String "Save"
	 */
	@DefaultStringValue("Save")
	String save();
	/**
	 * String for the delete button to delete all Tags 
	 * @return String "Delete"
	 */
	@DefaultStringValue("Delete")
	String delete();
	/**
	 * String for the cancel button, discard all modifications
	 * @return String "Cancel"
	 */
	@DefaultStringValue("Cancel")
	String cancel();
	
	
	/**
	 * String for the new button, discard all modifications
	 * @return String "New"
	 */
	@DefaultStringValue("New")
	String createnew();
	
	/**
	 * String for the dialog to start a discussion about the web doc and the tags
	 * @return String "start critical discussion"
	 */
	@DefaultStringValue("start critical discussion")
	String startDialog();
	/**
	 * String shown if you click the save button and it could save successfully
	 * @return String "Your data has been successfully saved."
	 */
	@DefaultStringValue("Your data has been successfully saved.")
	String success();
	
	/**
	 * String for No URL Message
	 * @return String "No URL"
	 */
	@DefaultStringValue("No URL")
	String noUrl();
	/**
	 * String to ask for a URL
	 * @return String "Please specify URL"
	 */
	@DefaultStringValue("Please specify URL")
	String fillUrl();
	/**
	 * String for Label Upload document
	 * @return String "Document: "
	 */
	@DefaultStringValue("Document: ")
	String document();
	/**
	 * String for Label Upload document
	 * @return String "Upload Document"
	 */
	@DefaultStringValue("Upload Document ")
	String upload();
	/**
	 * String for Label Upload document
	 * @return String "Upload Document"
	 */
	@DefaultStringValue("Document uploaded")
	String uploaded();
	/**
	 * String for Label Upload document
	 * @return String "State:"
	 */
	@DefaultStringValue("State:")
	String state();
	/**
	 * Exception String "An Exception occurred"
	 * @return String "An Exception occurred"
	 */
	// Exceptions
	@DefaultStringValue("An Exception occurred")
	String exceptionOccured();
	/**
	 *  Exception String "Please close and reload the popup <br />"
	 * @return String "Please close and reload the popup <br />"
	 */
	@DefaultStringValue("Please close and reload the popup <br />")
	String closeAndReload();
	
	/**
	 *  Exception String "You are not allowed to change this space! - Please login or ask for permission for this space"
	 * @return String "You are not allowed to change this space! - Please login or ask for permission for this space"
	 */
	@DefaultStringValue("You are not allowed to change this space! - Please login or ask for permission for this space")
	String error_200();
	/**
	 *  Exception String "You are not allowed to view this space! Please login or ask for permission for this space"
	 * @return String "You are not allowed to view this space! Please login or ask for permission for this space"
	 */
	@DefaultStringValue("You are not allowed to view this space! Please login or ask for permission for this space")
	String error_201();
	
	/**
	 *  Exception String "URL not valid. Please check the URL."
	 * @return String "URL not valid. Please check the URL."
	 */
	@DefaultStringValue("URL not valid. Please check the URL.")
	String error_300();
}
