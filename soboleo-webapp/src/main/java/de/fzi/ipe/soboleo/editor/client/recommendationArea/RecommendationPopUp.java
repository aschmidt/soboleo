package de.fzi.ipe.soboleo.editor.client.recommendationArea;

import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;

import de.fzi.ipe.soboleo.editor.client.EditorConstants;
import de.fzi.ipe.soboleo.editor.client.EventDistributor;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptLabelIdenticalProblem;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptRecommendation;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptWithoutDescription;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public class RecommendationPopUp extends DialogBox{
	private static final int TRESHOLD = 15;
	EventDistributor eventDistrib;
	// Internationalization
	private EditorConstants constants = GWT.create(EditorConstants.class); 
	HashMap<Class<? extends GardeningRecommendation>, RecommendationCaptionPanel<? extends GardeningRecommendation>> capPanelList = new HashMap<Class<? extends GardeningRecommendation>, RecommendationCaptionPanel<? extends GardeningRecommendation>>(); 
	
	public RecommendationPopUp(List<GardeningRecommendation> recs, EventDistributor eventDistrib){
		 super(false,false);
		 this.eventDistrib = eventDistrib;
		 setContent(recs);
	}
	
	private void setContent(List<? extends GardeningRecommendation> recs){
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
//		vPanel.setSpacing(5);
		add(vPanel);
		HTML title = new HTML("<h3>" +constants.recommendations() +"</h3>");
		vPanel.add(title);
		
		if(recs.size() >= TRESHOLD){
			for(int i = 0; i<TRESHOLD; i++){
				GardeningRecommendation rec = recs.get(i);			
				if(!capPanelList.containsKey(rec.getClass())){
					RecommendationCaptionPanel<? extends GardeningRecommendation> newPanel = createRecommendationCaptionPanelForClass(rec.getClass());
					newPanel.setStyleName("conceptDetailsBorderedPanel");
					capPanelList.put(rec.getClass(), newPanel);
					vPanel.add(newPanel);
				}
				capPanelList.get(rec.getClass()).addRecommendation(rec);
			}	
		} else{
			for(GardeningRecommendation rec : recs){
				if(!capPanelList.containsKey(rec.getClass())){
					RecommendationCaptionPanel<? extends GardeningRecommendation> newPanel = createRecommendationCaptionPanelForClass(rec.getClass());
					newPanel.setStyleName("conceptDetailsBorderedPanel");
					capPanelList.put(rec.getClass(), newPanel);
					vPanel.add(newPanel);
				}
				capPanelList.get(rec.getClass()).addRecommendation(rec);
			}
		}
		
		final Button close = new Button(constants.close());
		close.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent arg0) {
				RecommendationPopUp.this.hide();
			}
		});
		vPanel.add(close);
	}
	
	private RecommendationCaptionPanel<? extends GardeningRecommendation> createRecommendationCaptionPanelForClass(Class<? extends GardeningRecommendation> recClazz){
		RecommendationCaptionPanel<? extends GardeningRecommendation> recPanel = null;
		if(recClazz.equals(ConceptWithoutDescription.class)){
			recPanel = new ConceptWithoutDescriptionCaptionPanel(eventDistrib, constants.missingDescriptions());
		}
		else if(recClazz.equals(ConceptLabelIdenticalProblem.class)){
			recPanel = new IdenticalLabelCaptionPanel(eventDistrib, constants.identicalLabels());
		}
		else if(recClazz.equals(ConceptRecommendation.class)){
			recPanel = new SuperRelatedConceptCaptionPanel(eventDistrib, constants.superConcepts());
		}
		return recPanel;
	}

	public void show() {
		setPopupPosition(250, 250);
		super.show();
	}
}
