package de.fzi.ipe.soboleo.test;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.Spaces;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class TestClientSideTaxonomy
{
	@Autowired Spaces spaces;

	@Before
	public void setUp() throws Exception
	{
	}

	@Test
	public void test() throws IOException, EventPermissionDeniedException
	{
		for (String t : spaces.getSpaces())
		{
			Space s = spaces.getSpace(t);
			assertNotNull(s.getClientSideTaxonomy());
		}		
	}
}
