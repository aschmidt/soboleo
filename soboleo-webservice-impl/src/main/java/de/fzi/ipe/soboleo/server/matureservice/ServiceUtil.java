package de.fzi.ipe.soboleo.server.matureservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.AggregatedConceptTags;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.ManualPersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.PersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.server.SoboleoServiceUtils;

public class ServiceUtil {

	private MATUREService s;
	
	public ServiceUtil(MATUREService s)
	{ this.s = s; }
	
	public TagAssignment convertTagAssignment(de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag docTag, ClientSideTaxonomy tax){
		TagAssignment ta = new TagAssignment();
		try {
			ta.setMakerURI("mailto:" +SoboleoServiceUtils.getEmailForUserURI(s.getServer(),docTag.getUserID()));
		} catch (IOException e) {
			ta.setMakerURI("mailto:");
		}
		ta.setProxyURL(docTag.getPageURL());
		ta.setResourceURI(docTag.getPageURL());
		ta.setTaggingDate(SoboleoServiceUtils.convertCalendar(docTag.getDate()));
		ClientSideConcept c = tax.get(docTag.getConceptID());
		if(c!= null) ta.setTag(convertTag(c));
		return ta;
	}
	
	
	public Tag convertTag(ClientSideConcept current){
		return convertTag(current, null);
	}
	
	
	public  Tag convertTag(ClientSideConcept current, List<String> locales){
		Tag tag = new Tag();
		tag.setTagURI(current.getURI());
		
		List<LocalizedString> prefLabel = new ArrayList<LocalizedString>();
		List<LocalizedString> altLabel = new ArrayList<LocalizedString>();
		List<LocalizedString> hiddenLabel = new ArrayList<LocalizedString>();
		List<LocalizedString> description = new ArrayList<LocalizedString>();

		for (de.fzi.ipe.soboleo.beans.ontology.LocalizedString pref : current
				.getTexts(SKOS.PREF_LABEL)) {
			if (locales == null
					|| locales.contains(pref.getLanguage().getAcronym()))
				prefLabel.add(convertLocalizedString(pref));
		}
		tag.prefLabel = prefLabel;
		
		for (de.fzi.ipe.soboleo.beans.ontology.LocalizedString alt : current
				.getTexts(SKOS.ALT_LABEL)) {
			if (locales == null
					|| locales.contains(alt.getLanguage().getAcronym())) {
				altLabel.add(convertLocalizedString(alt));
			}
		}
		tag.altLabel = altLabel;
		
		for (de.fzi.ipe.soboleo.beans.ontology.LocalizedString hidden : current
				.getTexts(SKOS.HIDDEN_LABEL)) {
			if (locales == null
					|| locales.contains(hidden.getLanguage().getAcronym()))
				hiddenLabel.add(convertLocalizedString(hidden));
		}
		tag.hiddenLabel = hiddenLabel;
		
		for (de.fzi.ipe.soboleo.beans.ontology.LocalizedString desc : current
				.getTexts(SKOS.NOTE)) {
			if (locales == null
					|| locales.contains(desc.getLanguage().getAcronym()))
				description.add(convertLocalizedString(desc));
		}
		tag.description = description;

		List<String> broader=new ArrayList<String>(current.getConnected(SKOS.HAS_BROADER));
		tag.broader=broader;
		
		List<String> narrower=new ArrayList<String>(current.getConnected(SKOS.HAS_NARROWER));
		tag.narrower=narrower;
		
		List<String> related=new ArrayList<String>(current.getConnected(SKOS.RELATED));
		tag.related=related;
		
		return tag;
	}
	
	
	public LocalizedString convertLocalizedString(de.fzi.ipe.soboleo.beans.ontology.LocalizedString toConvert) {
		LocalizedString converted = new LocalizedString();
		converted.setLang(toConvert.getLanguage().getAcronym());
		converted.setValue(toConvert.getString());

		return converted;
	} 

	
	public TagAssignment convertTagAssignment(PersonTag personTag, ClientSideTaxonomy tax){
		TagAssignment ta = new TagAssignment();
		try {
			ta.setMakerURI("mailto:" +SoboleoServiceUtils.getEmailForUserURI(s.getServer(),personTag.getUserID()));
		} catch (IOException e) {
			ta.setMakerURI("mailto:");
		}
		if (personTag instanceof ManualPersonTag) ta.setProxyURL(((ManualPersonTag) personTag).getPageURL());
		ta.setResourceURI(personTag.getTaggedPersonURI());
		ta.setTaggingDate(SoboleoServiceUtils.convertCalendar(personTag.getDate()));
		ClientSideConcept c = tax.get(personTag.getConceptID());
		if(c!= null) ta.setTag(convertTag(c));
		return ta;
	}
	
	public LogEvent convertLogEvent(){
		LogEvent logEvent = new LogEvent();
		logEvent.setMakerURI("mailto:braun@fzi.de");
		logEvent.setObjectURI("http://www.fzi.de");
		logEvent.setContent("http://soboleo.com/ns/1.0#space-default-gen2");
		logEvent.setActionType("addTagAssignment");
		return logEvent;
	}

	public SearchPersonEntry convertSearchPersonEntry(TaggedPerson toConvert, ClientSideTaxonomy tax){
		SearchPersonEntry spe = new SearchPersonEntry();
		spe.setPersonURI("mailto:" +toConvert.getEmail());
		spe.setName(toConvert.getName());

		spe.setScore(0.0f);
		List<TagFreqEvidence> flist = new ArrayList<TagFreqEvidence>();
		TagFreqEvidence tagEv = new TagFreqEvidence();
		tagEv.setEvidence(EvidenceType.PERSON_TAGGED_WITH);
		List<TagFrequencyEntry> tlist = new ArrayList<TagFrequencyEntry>();
		for(AggregatedConceptTags aggregated : toConvert.getAggregatedTags()){
			ClientSideConcept concept = tax.get(aggregated.getConceptID());
			if(concept != null) tlist.add(convertTagFrequencyEntry(aggregated.getWeight(), concept));
		}
		if(!tlist.isEmpty()){
			tagEv.tagFreqsList=tlist;
			flist.add(tagEv);
		}
		spe.tagFreqsEvidenceList = flist;
		return spe;
	}

	public TagFrequencyEntry convertTagFrequencyEntry(int weight, ClientSideConcept aggregated) {
		TagFrequencyEntry tfe = new TagFrequencyEntry();
		tfe.setFrequency(weight);
		tfe.setTag(convertTag(aggregated));
		return tfe;
	}

	public SearchDigitalResourceEntry convertSearchDigitalResourceEntry() {
		SearchDigitalResourceEntry sdre = new SearchDigitalResourceEntry();
		sdre.setDigitalResourceURI("http://testdata.de");
		sdre.setTitle("Test Data");
		sdre.setScore(new Float(0.5));
		
		OverallRatingEntry overallRating = new OverallRatingEntry();
		overallRating.setRatingFreq(1);
		overallRating.setRatingScore(2);
		sdre.setOverallRating(overallRating);
		TagFrequencyEntry tagFreqEntry = new TagFrequencyEntry();
		tagFreqEntry.setFrequency(1);
		Tag t = new Tag();
		t.setTagURI("http://soboleo.com/ns/1.0#space-test-test");
		LocalizedString prefLabel = new LocalizedString();
		prefLabel.setLang("en");
		prefLabel.setValue("test");
		
		List<LocalizedString>  prefLabelList=new ArrayList<LocalizedString>();
		prefLabelList.add(prefLabel);
		t.prefLabel=prefLabelList;
		
		
		List<TagFrequencyEntry> tagFreqsList=new ArrayList<TagFrequencyEntry>();
		tagFreqsList.add(tagFreqEntry);
		sdre.tagFreqList=tagFreqsList;

	
		return sdre;
	}
	
}
