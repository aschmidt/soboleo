package de.fzi.ipe.soboleo.server.soapwebservice;

import java.io.IOException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.server.SoboleoServiceUtils;
import de.fzi.ipe.soboleo.user.User;
import de.fzi.ipe.soboleo.user.UserDatabase;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.6 in JDK 6 Generated
 * source version: 2.1
 * 
 */
@WebService(name = "AuthentificationService", targetNamespace = "http://soboleo.com")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso( { ObjectFactory.class })
public class AuthentificationServiceImpl implements
		AuthentificationService {

	@Autowired protected Server server;
	protected Server getServer() { return server; }
	/**
	 * 
	 * @param parameters
	 * @return returns
	 *         de.fzi.ipe.soboleo.server.soapwebservice.authentification.
	 *         AuthenticateResponse
	 * @throws InvalidLoginException
	 */
	@WebMethod(action = "http://soboleo.com/authenticate")
	@WebResult(name = "authenticateResponse", targetNamespace = "http://soboleo.com", partName = "parameters")
	public AuthenticateResponse authenticate(
			@WebParam(name = "authenticateRequest", targetNamespace = "http://soboleo.com", partName = "parameters") AuthenticateRequest parameters)
			throws InvalidLoginException {

		InvalidLoginMsg msg = new InvalidLoginMsg();

		UserDatabase udb = getServer().getUserDatabase();
		try {
			User user = udb.getUserForEmail(parameters.userEmail);
			if (user != null) {
				if (user.isPassword(parameters.password)) {
					AuthenticateResponse response = new AuthenticateResponse();
					response.setKey(user.getKey());
					return response;
				}

				msg.setInvalidLoginMsg("Invalid Password");

			} else {
				msg.setInvalidLoginMsg("Invalid Email");
			}
			InvalidLoginException ile = new InvalidLoginException("error", msg);

			throw ile;
		} catch (IOException e) {
			msg.setInvalidLoginMsg(e.getMessage());
			InvalidLoginException ile = new InvalidLoginException("error", msg);

			throw ile;
		}

	}

	/**
	 * 
	 * @param parameters
	 * @return returns
	 *         de.fzi.ipe.soboleo.server.soapwebservice.authentification.
	 *         GetGroupResponse
	 * @throws InvalidKeyException
	 */
	@WebMethod(action = "http://soboleo.com/getGroup")
	@WebResult(name = "getGroupResponse", targetNamespace = "http://soboleo.com", partName = "parameters")
	public GetGroupResponse getGroup(
			@WebParam(name = "getGroupRequest", targetNamespace = "http://soboleo.com", partName = "parameters") GetGroupRequest parameters)
			throws InvalidKeyException {
		getServer();
		InvalidKeyMsg msg = new InvalidKeyMsg();

		try {
			GetGroupResponse response = new GetGroupResponse();
			response.setGroupURI(SoboleoServiceUtils.getDefaultSpace(server,parameters.key).getURI());
			return response;
		} catch (EventPermissionDeniedException e) {
			msg.setInvalidKeyMsg(e.getMessage());
		} catch (java.security.InvalidKeyException e) {
			msg.setInvalidKeyMsg("Invalid Key");
		}

		InvalidKeyException ex = new InvalidKeyException("error", msg);
		throw ex;
	}

	/**
	 * 
	 * @param parameters
	 * @return returns
	 *         de.fzi.ipe.soboleo.server.soapwebservice.authentification.
	 *         VerifyKeyResponse
	 * @throws InvalidKeyException
	 */
	@WebMethod(action = "http://soboleo.com/verifyKey")
	@WebResult(name = "verifyKeyResponse", targetNamespace = "http://soboleo.com", partName = "parameters")
	public VerifyKeyResponse verifyKey(
			@WebParam(name = "verifyKeyRequest", targetNamespace = "http://soboleo.com", partName = "parameters") VerifyKeyRequest parameters)
			throws InvalidKeyException {
		InvalidKeyMsg msg = new InvalidKeyMsg();
		UserDatabase udb = getServer().getUserDatabase();
		try {
			VerifyKeyResponse response = new VerifyKeyResponse();

			User user = udb.getUserForEmail(parameters.userMail);
			if (user != null) {
				if (user.getKey().equals(parameters.key)) {
					response.ok=true;
					return response;
				} else
				{
					response.ok=false;
					return response;

				}
			} else
				msg.setInvalidKeyMsg("Invalid Email");
		} catch (IOException e) {
			msg.setInvalidKeyMsg(e.getMessage());
		}
		InvalidKeyException ike = new InvalidKeyException("error", msg);

		throw ike;
	}

}
