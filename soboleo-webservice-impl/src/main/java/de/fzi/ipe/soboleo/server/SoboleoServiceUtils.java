package de.fzi.ipe.soboleo.server;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;

public class SoboleoServiceUtils {

	public final static String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	public static Space getDefaultSpace(Server server, String userKey) throws EventPermissionDeniedException, InvalidKeyException {
		Space space = null;
		try {
			User user = server.getUserDatabase().getUserForKey(userKey);
			if(user != null) space = server.getSpaces().getSpace(user.getDefaultSpaceIdentifier());
			else throw new InvalidKeyException();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return space;
	}

	public static EventSenderCredentials getCredentials(Server server, String userKey) throws InvalidKeyException{
		EventSenderCredentials creds = null;
		try {
			User user = server.getUserDatabase().getUserForKey(userKey);
			if (user != null) creds = new EventSenderCredentials(user.getURI(), user.getName(), user.getKey());
			else throw new InvalidKeyException();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return creds;
	}

	public static Space getSpace(Server server, String spaceID) {
		Space space = null;
		try {
			space = server.getSpaces().getSpace(spaceID);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return space;
	}
	
	public static String getUserURIForEmail(Server server, String mailto) throws IOException{
		User user = getUserForEmail(server,mailto);
		if(user != null) return user.getURI();
		else throw new IOException("maker unknown");
	}
	
	public static User getUserForEmail(Server server, String mailto) throws IOException{
		String email = skipMailto(mailto);
		User user = server.getUserDatabase().getUserForEmail(email);
		if(user != null) return user;
		else throw new IOException("user unknown");
	}
	
	public static String getEmailForUserURI(Server server,String uri) throws IOException{
		User user = server.getUserDatabase().getUser(uri);
		if(user != null) return user.getEmail();
		else throw new IOException("user unknown");
	}
	
    public static XMLGregorianCalendar convertCalendar(Date date) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(date.getTime());
        try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
    }
	
	public static String skipMailto(String mailto){
		String toReturn = "";
		mailto = mailto.toLowerCase();
		if(mailto.startsWith("mailto:")) toReturn = mailto.replace("mailto:", "");
		return toReturn;
	}

}
