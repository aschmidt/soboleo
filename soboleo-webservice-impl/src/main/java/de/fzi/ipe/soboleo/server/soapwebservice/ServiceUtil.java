package de.fzi.ipe.soboleo.server.soapwebservice;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class ServiceUtil {

	public static LocalizedString convertLocalizedString(
			de.fzi.ipe.soboleo.beans.ontology.LocalizedString toConvert) {
		LocalizedString converted = new LocalizedString();
		converted.setLang(toConvert.getLanguage().getAcronym());
		converted.setValue(toConvert.getString());

		return converted;
	}

	public static XMLGregorianCalendar convertCalendar(Date date) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeInMillis(date.getTime());
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
