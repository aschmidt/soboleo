package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.springframework.beans.factory.annotation.Autowired;

import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsForConcept;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.server.SoboleoServiceUtils;
import de.fzi.ipe.soboleo.space.Space;

/**
 * searching persons by tags (tagURIs) and aggregates several
 * PersonSearchServices
 */
@WebService(name = "MATURE-AggregatedPersonSearchService", targetNamespace = "http://mature-ip.eu/MATURE")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso( { ObjectFactory.class })
public class MATUREAggregatedPersonSearchServiceImpl extends MATUREService
		implements MATUREAggregatedPersonSearchService {

	@Autowired protected Server server;
	/**
	 * search persons that are, e.g. annotated with given tag
	 * 
	 * @param parameters
	 * @return returns de.fzi.ipe.soboleo.server.matureservice.search.
	 *         SearchPersonsByTagsResponse
	 * @throws InvalidKeyException
	 */
	@WebMethod(action = "http://mature-ip.eu/MATURE/searchPersonsByTags")
	@WebResult(name = "searchPersonsByTagsResponse", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
	public SearchPersonsByTagsResponse searchPersonsByTags(
			@WebParam(name = "searchPersonsByTagsRequest", targetNamespace = "http://mature-ip.eu/MATURE", partName = "parameters") SearchPersonsByTagsRequest parameters)
			throws InvalidKeyException {

		getServer();
		try {
			EventSenderCredentials creds = SoboleoServiceUtils.getCredentials(server,parameters.agentKey);
			Space space = SoboleoServiceUtils.getDefaultSpace(server,parameters.agentKey);

			ArrayList<SearchPersonEntry> list = new ArrayList<SearchPersonEntry>();
			
			@SuppressWarnings("unchecked")
			List<TaggedPerson> persons = (List<TaggedPerson>) space.getEventBus().executeQuery(new SearchPersonsForConcept(parameters.tagURI.toArray(new String[0]), null), creds);
			for(TaggedPerson current : persons)	list.add(util().convertSearchPersonEntry(current, space.getClientSideTaxonomy()));
			
			SearchPersonsByTagsResponse  response=new SearchPersonsByTagsResponse();
			response.searchPersonResult=list;
			
			return response;
			
		} catch (EventPermissionDeniedException e ) {
			InvalidKeyMsg message = new InvalidKeyMsg();
			message.setInvalidKeyMsg(e.getCause().toString());
			throw new InvalidKeyException("error", message);

		} catch (java.security.InvalidKeyException e) {
			InvalidKeyMsg message = new InvalidKeyMsg();
			message.setInvalidKeyMsg(e.getCause().toString());
			throw new InvalidKeyException("error", message);
		}
	}

	public Server getServer() { return server; }
}
