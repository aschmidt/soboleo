
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tag according to SKOS concept
 * 
 * <p>Java class for tag complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tag">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tagURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="prefLabel" type="{http://mature-ip.eu/MATURE/types}LocalizedString" maxOccurs="unbounded"/>
 *         &lt;element name="altLabel" type="{http://mature-ip.eu/MATURE/types}LocalizedString" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="hiddenLabel" type="{http://mature-ip.eu/MATURE/types}LocalizedString" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="broader" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="narrower" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="related" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="description" type="{http://mature-ip.eu/MATURE/types}LocalizedString" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tag", namespace = "http://mature-ip.eu/MATURE/types", propOrder = {
    "tagURI",
    "prefLabel",
    "altLabel",
    "hiddenLabel",
    "broader",
    "narrower",
    "related",
    "description"
})
public class Tag {

    @XmlElement(required = true)
    protected String tagURI;
    @XmlElement(required = true)
    protected List<LocalizedString> prefLabel;
    protected List<LocalizedString> altLabel;
    protected List<LocalizedString> hiddenLabel;
    protected List<String> broader;
    protected List<String> narrower;
    protected List<String> related;
    protected List<LocalizedString> description;

    /**
     * Gets the value of the tagURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagURI() {
        return tagURI;
    }

    /**
     * Sets the value of the tagURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagURI(String value) {
        this.tagURI = value;
    }

    /**
     * Gets the value of the prefLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prefLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrefLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedString }
     * 
     * 
     */
    public List<LocalizedString> getPrefLabel() {
        if (prefLabel == null) {
            prefLabel = new ArrayList<LocalizedString>();
        }
        return this.prefLabel;
    }

    /**
     * Gets the value of the altLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the altLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAltLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedString }
     * 
     * 
     */
    public List<LocalizedString> getAltLabel() {
        if (altLabel == null) {
            altLabel = new ArrayList<LocalizedString>();
        }
        return this.altLabel;
    }

    /**
     * Gets the value of the hiddenLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hiddenLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHiddenLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedString }
     * 
     * 
     */
    public List<LocalizedString> getHiddenLabel() {
        if (hiddenLabel == null) {
            hiddenLabel = new ArrayList<LocalizedString>();
        }
        return this.hiddenLabel;
    }

    /**
     * Gets the value of the broader property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the broader property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBroader().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getBroader() {
        if (broader == null) {
            broader = new ArrayList<String>();
        }
        return this.broader;
    }

    /**
     * Gets the value of the narrower property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the narrower property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNarrower().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getNarrower() {
        if (narrower == null) {
            narrower = new ArrayList<String>();
        }
        return this.narrower;
    }

    /**
     * Gets the value of the related property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the related property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelated().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRelated() {
        if (related == null) {
            related = new ArrayList<String>();
        }
        return this.related;
    }

    /**
     * Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedString }
     * 
     * 
     */
    public List<LocalizedString> getDescription() {
        if (description == null) {
            description = new ArrayList<LocalizedString>();
        }
        return this.description;
    }

}
