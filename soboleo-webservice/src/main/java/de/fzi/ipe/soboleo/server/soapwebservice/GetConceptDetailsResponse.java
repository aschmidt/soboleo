
package de.fzi.ipe.soboleo.server.soapwebservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="conceptURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="prefLabel" type="{http://soboleo.com}localeString" maxOccurs="unbounded"/>
 *         &lt;element name="altLabel" type="{http://soboleo.com}localeString" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="hiddenLabel" type="{http://soboleo.com}localeString" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="broaderConcept" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="narrowerConcept" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="relatedConcept" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "conceptURI",
    "prefLabel",
    "altLabel",
    "hiddenLabel",
    "broaderConcept",
    "narrowerConcept",
    "relatedConcept"
})
@XmlRootElement(name = "getConceptDetailsResponse")
public class GetConceptDetailsResponse {

    @XmlElement(required = true)
    protected String conceptURI;
    @XmlElement(required = true)
    protected List<LocaleString> prefLabel;
    protected List<LocaleString> altLabel;
    protected List<LocaleString> hiddenLabel;
    protected List<String> broaderConcept;
    protected List<String> narrowerConcept;
    protected List<String> relatedConcept;

    /**
     * Gets the value of the conceptURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptURI() {
        return conceptURI;
    }

    /**
     * Sets the value of the conceptURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptURI(String value) {
        this.conceptURI = value;
    }

    /**
     * Gets the value of the prefLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prefLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrefLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocaleString }
     * 
     * 
     */
    public List<LocaleString> getPrefLabel() {
        if (prefLabel == null) {
            prefLabel = new ArrayList<LocaleString>();
        }
        return this.prefLabel;
    }

    /**
     * Gets the value of the altLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the altLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAltLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocaleString }
     * 
     * 
     */
    public List<LocaleString> getAltLabel() {
        if (altLabel == null) {
            altLabel = new ArrayList<LocaleString>();
        }
        return this.altLabel;
    }

    /**
     * Gets the value of the hiddenLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hiddenLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHiddenLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocaleString }
     * 
     * 
     */
    public List<LocaleString> getHiddenLabel() {
        if (hiddenLabel == null) {
            hiddenLabel = new ArrayList<LocaleString>();
        }
        return this.hiddenLabel;
    }

    /**
     * Gets the value of the broaderConcept property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the broaderConcept property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBroaderConcept().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getBroaderConcept() {
        if (broaderConcept == null) {
            broaderConcept = new ArrayList<String>();
        }
        return this.broaderConcept;
    }

    /**
     * Gets the value of the narrowerConcept property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the narrowerConcept property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNarrowerConcept().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getNarrowerConcept() {
        if (narrowerConcept == null) {
            narrowerConcept = new ArrayList<String>();
        }
        return this.narrowerConcept;
    }

    /**
     * Gets the value of the relatedConcept property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedConcept property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedConcept().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRelatedConcept() {
        if (relatedConcept == null) {
            relatedConcept = new ArrayList<String>();
        }
        return this.relatedConcept;
    }

}
