
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ratingAssignment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ratingAssignment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="makerURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="digitalResourceURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ratingScore" type="{http://mature-ip.eu/MATURE/types}ratingScoreType"/>
 *         &lt;element name="ratingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ratingAssignment", namespace = "http://mature-ip.eu/MATURE/types", propOrder = {
    "makerURI",
    "digitalResourceURI",
    "ratingScore",
    "ratingDate"
})
public class RatingAssignment {

    @XmlElement(required = true)
    protected String makerURI;
    @XmlElement(required = true)
    protected String digitalResourceURI;
    protected int ratingScore;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ratingDate;

    /**
     * Gets the value of the makerURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakerURI() {
        return makerURI;
    }

    /**
     * Sets the value of the makerURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakerURI(String value) {
        this.makerURI = value;
    }

    /**
     * Gets the value of the digitalResourceURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitalResourceURI() {
        return digitalResourceURI;
    }

    /**
     * Sets the value of the digitalResourceURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitalResourceURI(String value) {
        this.digitalResourceURI = value;
    }

    /**
     * Gets the value of the ratingScore property.
     * 
     */
    public int getRatingScore() {
        return ratingScore;
    }

    /**
     * Sets the value of the ratingScore property.
     * 
     */
    public void setRatingScore(int value) {
        this.ratingScore = value;
    }

    /**
     * Gets the value of the ratingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRatingDate() {
        return ratingDate;
    }

    /**
     * Sets the value of the ratingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRatingDate(XMLGregorianCalendar value) {
        this.ratingDate = value;
    }

}
