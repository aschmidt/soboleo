
package de.fzi.ipe.soboleo.server.soapwebservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * if language is given, only prefLabels and altLabels in this language are returned
 * 
 * <p>Java class for tag complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tag">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tagURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="prefLabel" type="{http://soboleo.com/types}LocalizedString" maxOccurs="unbounded"/>
 *         &lt;element name="altLabel" type="{http://soboleo.com/types}LocalizedString" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tag", propOrder = {
    "tagURI",
    "prefLabel",
    "altLabel"
})
public class Tag {

    @XmlElement(required = true)
    protected String tagURI;
    @XmlElement(required = true)
    protected List<LocalizedString> prefLabel;
    protected List<LocalizedString> altLabel;

    /**
     * Gets the value of the tagURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagURI() {
        return tagURI;
    }

    /**
     * Sets the value of the tagURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagURI(String value) {
        this.tagURI = value;
    }

    /**
     * Gets the value of the prefLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prefLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrefLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedString }
     * 
     * 
     */
    public List<LocalizedString> getPrefLabel() {
        if (prefLabel == null) {
            prefLabel = new ArrayList<LocalizedString>();
        }
        return this.prefLabel;
    }

    /**
     * Gets the value of the altLabel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the altLabel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAltLabel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocalizedString }
     * 
     * 
     */
    public List<LocalizedString> getAltLabel() {
        if (altLabel == null) {
            altLabel = new ArrayList<LocalizedString>();
        }
        return this.altLabel;
    }

}
