
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for tagAssignment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tagAssignment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tag" type="{http://mature-ip.eu/MATURE/types}tag"/>
 *         &lt;element name="resourceURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="makerURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="taggingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="proxyURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tagAssignment", namespace = "http://mature-ip.eu/MATURE/types", propOrder = {
    "tag",
    "resourceURI",
    "makerURI",
    "taggingDate",
    "proxyURL"
})
public class TagAssignment {

    @XmlElement(required = true)
    protected Tag tag;
    @XmlElement(required = true)
    protected String resourceURI;
    @XmlElement(required = true)
    protected String makerURI;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar taggingDate;
    protected String proxyURL;

    /**
     * Gets the value of the tag property.
     * 
     * @return
     *     possible object is
     *     {@link Tag }
     *     
     */
    public Tag getTag() {
        return tag;
    }

    /**
     * Sets the value of the tag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Tag }
     *     
     */
    public void setTag(Tag value) {
        this.tag = value;
    }

    /**
     * Gets the value of the resourceURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceURI() {
        return resourceURI;
    }

    /**
     * Sets the value of the resourceURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceURI(String value) {
        this.resourceURI = value;
    }

    /**
     * Gets the value of the makerURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakerURI() {
        return makerURI;
    }

    /**
     * Sets the value of the makerURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakerURI(String value) {
        this.makerURI = value;
    }

    /**
     * Gets the value of the taggingDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaggingDate() {
        return taggingDate;
    }

    /**
     * Sets the value of the taggingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaggingDate(XMLGregorianCalendar value) {
        this.taggingDate = value;
    }

    /**
     * Gets the value of the proxyURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxyURL() {
        return proxyURL;
    }

    /**
     * Sets the value of the proxyURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxyURL(String value) {
        this.proxyURL = value;
    }

}
