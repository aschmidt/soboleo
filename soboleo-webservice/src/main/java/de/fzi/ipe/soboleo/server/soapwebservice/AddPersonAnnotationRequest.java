
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taggedPersonURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="taggedPersonURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conceptURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="spaceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taggedPersonURI",
    "taggedPersonURL",
    "conceptURI",
    "spaceID",
    "userKey"
})
@XmlRootElement(name = "addPersonAnnotationRequest")
public class AddPersonAnnotationRequest {

    @XmlElement(required = true)
    protected String taggedPersonURI;
    protected String taggedPersonURL;
    @XmlElement(required = true)
    protected String conceptURI;
    @XmlElement(required = true)
    protected String spaceID;
    @XmlElement(required = true)
    protected String userKey;

    /**
     * Gets the value of the taggedPersonURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaggedPersonURI() {
        return taggedPersonURI;
    }

    /**
     * Sets the value of the taggedPersonURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaggedPersonURI(String value) {
        this.taggedPersonURI = value;
    }

    /**
     * Gets the value of the taggedPersonURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaggedPersonURL() {
        return taggedPersonURL;
    }

    /**
     * Sets the value of the taggedPersonURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaggedPersonURL(String value) {
        this.taggedPersonURL = value;
    }

    /**
     * Gets the value of the conceptURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptURI() {
        return conceptURI;
    }

    /**
     * Sets the value of the conceptURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptURI(String value) {
        this.conceptURI = value;
    }

    /**
     * Gets the value of the spaceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpaceID() {
        return spaceID;
    }

    /**
     * Sets the value of the spaceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpaceID(String value) {
        this.spaceID = value;
    }

    /**
     * Gets the value of the userKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserKey() {
        return userKey;
    }

    /**
     * Sets the value of the userKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserKey(String value) {
        this.userKey = value;
    }

}
