
package de.fzi.ipe.soboleo.server.matureservice;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.7-b01-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "MATURE-DigitalResourceTagAssignmentService", targetNamespace = "http://mature-ip.eu/MATURE")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface MATUREDigitalResourceTagAssignmentService {


    /**
     * 
     * @param parameters
     * @return
     *     returns de.fzi.ipe.soboleo.server.matureservice.GetTagAssignmentsResponse
     * @throws InvalidKeyException
     */
    @WebMethod(action = "http://mature-ip.eu/MATURE/getTagAssignments")
    @WebResult(name = "getTagAssignmentsResponse", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
    public GetTagAssignmentsResponse getTagAssignments(
        @WebParam(name = "getTagAssignmentsRequest", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
        GetTagAssignmentsRequest parameters)
        throws InvalidKeyException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns de.fzi.ipe.soboleo.server.matureservice.CreateTagAssignmentResponse
     * @throws InvalidKeyException
     */
    @WebMethod(action = "http://mature-ip.eu/MATURE/createTagAssignment")
    @WebResult(name = "createTagAssignmentResponse", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
    public CreateTagAssignmentResponse createTagAssignment(
        @WebParam(name = "createTagAssignmentRequest", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
        CreateTagAssignmentRequest parameters)
        throws InvalidKeyException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns de.fzi.ipe.soboleo.server.matureservice.GetAssignedTagsByFrequencyResponse
     * @throws InvalidKeyException
     */
    @WebMethod(action = "http://mature-ip.eu/MATURE/getAssignedTagsByFrequency")
    @WebResult(name = "getAssignedTagsByFrequencyResponse", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
    public GetAssignedTagsByFrequencyResponse getAssignedTagsByFrequency(
        @WebParam(name = "getAssignedTagsByFrequencyRequest", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
        GetAssignedTagsByFrequencyRequest parameters)
        throws InvalidKeyException
    ;

    /**
     * 
     * @param parameters
     * @return
     *     returns de.fzi.ipe.soboleo.server.matureservice.RemoveTagAssignmentResponse
     * @throws InvalidKeyException
     */
    @WebMethod(action = "http://mature-ip.eu/MATURE/removeTagAssignment")
    @WebResult(name = "removeTagAssignmentResponse", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
    public RemoveTagAssignmentResponse removeTagAssignment(
        @WebParam(name = "removeTagAssignmentRequest", targetNamespace = "http://mature-ip.eu/MATURE/extratypes", partName = "parameters")
        RemoveTagAssignmentRequest parameters)
        throws InvalidKeyException
    ;

}
