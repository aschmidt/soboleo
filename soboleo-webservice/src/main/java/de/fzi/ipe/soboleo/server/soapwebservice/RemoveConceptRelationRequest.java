
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fromConceptURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="skosType" type="{http://soboleo.com}skosRelationType"/>
 *         &lt;element name="toConceptURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="spaceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fromConceptURI",
    "skosType",
    "toConceptURI",
    "spaceID",
    "userKey"
})
@XmlRootElement(name = "removeConceptRelationRequest")
public class RemoveConceptRelationRequest {

    @XmlElement(required = true)
    protected String fromConceptURI;
    @XmlElement(required = true)
    protected SkosRelationType skosType;
    @XmlElement(required = true)
    protected String toConceptURI;
    @XmlElement(required = true)
    protected String spaceID;
    @XmlElement(required = true)
    protected String userKey;

    /**
     * Gets the value of the fromConceptURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromConceptURI() {
        return fromConceptURI;
    }

    /**
     * Sets the value of the fromConceptURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromConceptURI(String value) {
        this.fromConceptURI = value;
    }

    /**
     * Gets the value of the skosType property.
     * 
     * @return
     *     possible object is
     *     {@link SkosRelationType }
     *     
     */
    public SkosRelationType getSkosType() {
        return skosType;
    }

    /**
     * Sets the value of the skosType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkosRelationType }
     *     
     */
    public void setSkosType(SkosRelationType value) {
        this.skosType = value;
    }

    /**
     * Gets the value of the toConceptURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToConceptURI() {
        return toConceptURI;
    }

    /**
     * Sets the value of the toConceptURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToConceptURI(String value) {
        this.toConceptURI = value;
    }

    /**
     * Gets the value of the spaceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpaceID() {
        return spaceID;
    }

    /**
     * Sets the value of the spaceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpaceID(String value) {
        this.spaceID = value;
    }

    /**
     * Gets the value of the userKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserKey() {
        return userKey;
    }

    /**
     * Sets the value of the userKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserKey(String value) {
        this.userKey = value;
    }

}
