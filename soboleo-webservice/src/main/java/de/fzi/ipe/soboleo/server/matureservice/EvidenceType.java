
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for evidenceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="evidenceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="person-tagged-with"/>
 *     &lt;enumeration value="person-uses"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "evidenceType", namespace = "http://mature-ip.eu/MATURE/types")
@XmlEnum
public enum EvidenceType {

    @XmlEnumValue("person-tagged-with")
    PERSON_TAGGED_WITH("person-tagged-with"),
    @XmlEnumValue("person-uses")
    PERSON_USES("person-uses");
    private final String value;

    EvidenceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EvidenceType fromValue(String v) {
        for (EvidenceType c: EvidenceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
