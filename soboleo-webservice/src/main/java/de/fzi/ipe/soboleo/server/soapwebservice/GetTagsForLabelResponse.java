
package de.fzi.ipe.soboleo.server.soapwebservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prefLabelMatchURI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="altLabelMatchURI" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "prefLabelMatchURI",
    "altLabelMatchURI"
})
@XmlRootElement(name = "getTagsForLabelResponse")
public class GetTagsForLabelResponse {

    protected String prefLabelMatchURI;
    protected List<String> altLabelMatchURI;

    /**
     * Gets the value of the prefLabelMatchURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefLabelMatchURI() {
        return prefLabelMatchURI;
    }

    /**
     * Sets the value of the prefLabelMatchURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefLabelMatchURI(String value) {
        this.prefLabelMatchURI = value;
    }

    /**
     * Gets the value of the altLabelMatchURI property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the altLabelMatchURI property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAltLabelMatchURI().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAltLabelMatchURI() {
        if (altLabelMatchURI == null) {
            altLabelMatchURI = new ArrayList<String>();
        }
        return this.altLabelMatchURI;
    }

}
