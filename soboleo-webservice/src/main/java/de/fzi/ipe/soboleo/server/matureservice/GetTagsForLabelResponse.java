
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prefLabelMatchTag" type="{http://mature-ip.eu/MATURE/types}tag" minOccurs="0"/>
 *         &lt;element name="altLabelMatchTag" type="{http://mature-ip.eu/MATURE/types}tag" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="hiddenLabelMatchTag" type="{http://mature-ip.eu/MATURE/types}tag" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "prefLabelMatchTag",
    "altLabelMatchTag",
    "hiddenLabelMatchTag"
})
@XmlRootElement(name = "getTagsForLabelResponse")
public class GetTagsForLabelResponse {

    protected Tag prefLabelMatchTag;
    protected List<Tag> altLabelMatchTag;
    protected List<Tag> hiddenLabelMatchTag;

    /**
     * Gets the value of the prefLabelMatchTag property.
     * 
     * @return
     *     possible object is
     *     {@link Tag }
     *     
     */
    public Tag getPrefLabelMatchTag() {
        return prefLabelMatchTag;
    }

    /**
     * Sets the value of the prefLabelMatchTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Tag }
     *     
     */
    public void setPrefLabelMatchTag(Tag value) {
        this.prefLabelMatchTag = value;
    }

    /**
     * Gets the value of the altLabelMatchTag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the altLabelMatchTag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAltLabelMatchTag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tag }
     * 
     * 
     */
    public List<Tag> getAltLabelMatchTag() {
        if (altLabelMatchTag == null) {
            altLabelMatchTag = new ArrayList<Tag>();
        }
        return this.altLabelMatchTag;
    }

    /**
     * Gets the value of the hiddenLabelMatchTag property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hiddenLabelMatchTag property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHiddenLabelMatchTag().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tag }
     * 
     * 
     */
    public List<Tag> getHiddenLabelMatchTag() {
        if (hiddenLabelMatchTag == null) {
            hiddenLabelMatchTag = new ArrayList<Tag>();
        }
        return this.hiddenLabelMatchTag;
    }

}
