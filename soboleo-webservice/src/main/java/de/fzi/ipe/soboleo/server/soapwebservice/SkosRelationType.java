
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for skosRelationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="skosRelationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="http://www.w3.org/2004/02/skos/core#broader"/>
 *     &lt;enumeration value="http://www.w3.org/2004/02/skos/core#narrower"/>
 *     &lt;enumeration value="http://www.w3.org/2004/02/skos/core#related"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "skosRelationType")
@XmlEnum
public enum SkosRelationType {

    @XmlEnumValue("http://www.w3.org/2004/02/skos/core#broader")
    HTTP_WWW_W_3_ORG_2004_02_SKOS_CORE_BROADER("http://www.w3.org/2004/02/skos/core#broader"),
    @XmlEnumValue("http://www.w3.org/2004/02/skos/core#narrower")
    HTTP_WWW_W_3_ORG_2004_02_SKOS_CORE_NARROWER("http://www.w3.org/2004/02/skos/core#narrower"),
    @XmlEnumValue("http://www.w3.org/2004/02/skos/core#related")
    HTTP_WWW_W_3_ORG_2004_02_SKOS_CORE_RELATED("http://www.w3.org/2004/02/skos/core#related");
    private final String value;

    SkosRelationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SkosRelationType fromValue(String v) {
        for (SkosRelationType c: SkosRelationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
