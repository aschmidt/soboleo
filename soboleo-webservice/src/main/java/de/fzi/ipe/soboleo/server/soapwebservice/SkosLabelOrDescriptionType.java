
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for skosLabelOrDescriptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="skosLabelOrDescriptionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="http://www.w3.org/2004/02/skos/core#prefLabel"/>
 *     &lt;enumeration value="http://www.w3.org/2004/02/skos/core#altLabel"/>
 *     &lt;enumeration value="http://www.w3.org/2004/02/skos/core#hiddenLabel"/>
 *     &lt;enumeration value="http://www.w3.org/2004/02/skos/core#note"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "skosLabelOrDescriptionType")
@XmlEnum
public enum SkosLabelOrDescriptionType {

    @XmlEnumValue("http://www.w3.org/2004/02/skos/core#prefLabel")
    HTTP_WWW_W_3_ORG_2004_02_SKOS_CORE_PREF_LABEL("http://www.w3.org/2004/02/skos/core#prefLabel"),
    @XmlEnumValue("http://www.w3.org/2004/02/skos/core#altLabel")
    HTTP_WWW_W_3_ORG_2004_02_SKOS_CORE_ALT_LABEL("http://www.w3.org/2004/02/skos/core#altLabel"),
    @XmlEnumValue("http://www.w3.org/2004/02/skos/core#hiddenLabel")
    HTTP_WWW_W_3_ORG_2004_02_SKOS_CORE_HIDDEN_LABEL("http://www.w3.org/2004/02/skos/core#hiddenLabel"),
    @XmlEnumValue("http://www.w3.org/2004/02/skos/core#note")
    HTTP_WWW_W_3_ORG_2004_02_SKOS_CORE_NOTE("http://www.w3.org/2004/02/skos/core#note");
    private final String value;

    SkosLabelOrDescriptionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SkosLabelOrDescriptionType fromValue(String v) {
        for (SkosLabelOrDescriptionType c: SkosLabelOrDescriptionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
