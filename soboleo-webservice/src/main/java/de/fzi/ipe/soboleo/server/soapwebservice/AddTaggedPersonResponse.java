
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taggedPersonURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taggedPersonURI"
})
@XmlRootElement(name = "addTaggedPersonResponse")
public class AddTaggedPersonResponse {

    @XmlElement(required = true)
    protected String taggedPersonURI;

    /**
     * Gets the value of the taggedPersonURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaggedPersonURI() {
        return taggedPersonURI;
    }

    /**
     * Sets the value of the taggedPersonURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaggedPersonURI(String value) {
        this.taggedPersonURI = value;
    }

}
