
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="groupURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "groupURI"
})
@XmlRootElement(name = "getGroupResponse")
public class GetGroupResponse {

    @XmlElement(required = true)
    protected String groupURI;

    /**
     * Gets the value of the groupURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupURI() {
        return groupURI;
    }

    /**
     * Sets the value of the groupURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupURI(String value) {
        this.groupURI = value;
    }

}
