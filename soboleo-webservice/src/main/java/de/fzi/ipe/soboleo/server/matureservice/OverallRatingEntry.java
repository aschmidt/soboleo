
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * score that aggregates all ratings for this digital resource and the total number of ratings that are the aggregation basis
 * 
 * <p>Java class for overallRatingEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="overallRatingEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ratingScore" type="{http://mature-ip.eu/MATURE/types}ratingScoreType"/>
 *         &lt;element name="ratingFreq" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "overallRatingEntry", namespace = "http://mature-ip.eu/MATURE/types", propOrder = {
    "ratingScore",
    "ratingFreq"
})
public class OverallRatingEntry {

    protected int ratingScore;
    protected int ratingFreq;

    /**
     * Gets the value of the ratingScore property.
     * 
     */
    public int getRatingScore() {
        return ratingScore;
    }

    /**
     * Sets the value of the ratingScore property.
     * 
     */
    public void setRatingScore(int value) {
        this.ratingScore = value;
    }

    /**
     * Gets the value of the ratingFreq property.
     * 
     */
    public int getRatingFreq() {
        return ratingFreq;
    }

    /**
     * Sets the value of the ratingFreq property.
     * 
     */
    public void setRatingFreq(int value) {
        this.ratingFreq = value;
    }

}
