
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taggedPersons" type="{http://soboleo.com}taggedPersons"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taggedPersons"
})
@XmlRootElement(name = "searchSimilarPersonsResponse")
public class SearchSimilarPersonsResponse {

    @XmlElement(required = true)
    protected TaggedPersons taggedPersons;

    /**
     * Gets the value of the taggedPersons property.
     * 
     * @return
     *     possible object is
     *     {@link TaggedPersons }
     *     
     */
    public TaggedPersons getTaggedPersons() {
        return taggedPersons;
    }

    /**
     * Sets the value of the taggedPersons property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaggedPersons }
     *     
     */
    public void setTaggedPersons(TaggedPersons value) {
        this.taggedPersons = value;
    }

}
