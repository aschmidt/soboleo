
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="taggedPerson" type="{http://soboleo.com}taggedPerson"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "taggedPerson"
})
@XmlRootElement(name = "getTaggedPersonResponse")
public class GetTaggedPersonResponse {

    @XmlElement(required = true)
    protected TaggedPerson taggedPerson;

    /**
     * Gets the value of the taggedPerson property.
     * 
     * @return
     *     possible object is
     *     {@link TaggedPerson }
     *     
     */
    public TaggedPerson getTaggedPerson() {
        return taggedPerson;
    }

    /**
     * Sets the value of the taggedPerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link TaggedPerson }
     *     
     */
    public void setTaggedPerson(TaggedPerson value) {
        this.taggedPerson = value;
    }

}
