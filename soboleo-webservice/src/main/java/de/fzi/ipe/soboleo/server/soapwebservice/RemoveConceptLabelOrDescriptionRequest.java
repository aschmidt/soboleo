
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="conceptURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="skosType" type="{http://soboleo.com}skosLabelOrDescriptionType"/>
 *         &lt;element name="labelOrDescription" type="{http://soboleo.com}localeString"/>
 *         &lt;element name="spaceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="userKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "conceptURI",
    "skosType",
    "labelOrDescription",
    "spaceID",
    "userKey"
})
@XmlRootElement(name = "removeConceptLabelOrDescriptionRequest")
public class RemoveConceptLabelOrDescriptionRequest {

    @XmlElement(required = true)
    protected String conceptURI;
    @XmlElement(required = true)
    protected SkosLabelOrDescriptionType skosType;
    @XmlElement(required = true)
    protected LocaleString labelOrDescription;
    @XmlElement(required = true)
    protected String spaceID;
    @XmlElement(required = true)
    protected String userKey;

    /**
     * Gets the value of the conceptURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptURI() {
        return conceptURI;
    }

    /**
     * Sets the value of the conceptURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptURI(String value) {
        this.conceptURI = value;
    }

    /**
     * Gets the value of the skosType property.
     * 
     * @return
     *     possible object is
     *     {@link SkosLabelOrDescriptionType }
     *     
     */
    public SkosLabelOrDescriptionType getSkosType() {
        return skosType;
    }

    /**
     * Sets the value of the skosType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SkosLabelOrDescriptionType }
     *     
     */
    public void setSkosType(SkosLabelOrDescriptionType value) {
        this.skosType = value;
    }

    /**
     * Gets the value of the labelOrDescription property.
     * 
     * @return
     *     possible object is
     *     {@link LocaleString }
     *     
     */
    public LocaleString getLabelOrDescription() {
        return labelOrDescription;
    }

    /**
     * Sets the value of the labelOrDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocaleString }
     *     
     */
    public void setLabelOrDescription(LocaleString value) {
        this.labelOrDescription = value;
    }

    /**
     * Gets the value of the spaceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpaceID() {
        return spaceID;
    }

    /**
     * Sets the value of the spaceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpaceID(String value) {
        this.spaceID = value;
    }

    /**
     * Gets the value of the userKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserKey() {
        return userKey;
    }

    /**
     * Sets the value of the userKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserKey(String value) {
        this.userKey = value;
    }

}
