
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * if makerURI and resourceURI and proxURL not defined, give back anything
 * resourceURI and proxyURL are mutually exclusive
 * 
 * 
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="makerURI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resourceURI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agentKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "makerURI",
    "resourceURI",
    "proxyURL",
    "agentKey",
    "agentURI"
})
@XmlRootElement(name = "getAssignedTagsByFrequencyRequest")
public class GetAssignedTagsByFrequencyRequest {

    protected String makerURI;
    protected String resourceURI;
    protected String proxyURL;
    @XmlElement(required = true)
    protected String agentKey;
    @XmlElement(required = true)
    protected String agentURI;

    /**
     * Gets the value of the makerURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakerURI() {
        return makerURI;
    }

    /**
     * Sets the value of the makerURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakerURI(String value) {
        this.makerURI = value;
    }

    /**
     * Gets the value of the resourceURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceURI() {
        return resourceURI;
    }

    /**
     * Sets the value of the resourceURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceURI(String value) {
        this.resourceURI = value;
    }

    /**
     * Gets the value of the proxyURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxyURL() {
        return proxyURL;
    }

    /**
     * Sets the value of the proxyURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxyURL(String value) {
        this.proxyURL = value;
    }

    /**
     * Gets the value of the agentKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentKey() {
        return agentKey;
    }

    /**
     * Sets the value of the agentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentKey(String value) {
        this.agentKey = value;
    }

    /**
     * Gets the value of the agentURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentURI() {
        return agentURI;
    }

    /**
     * Sets the value of the agentURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentURI(String value) {
        this.agentURI = value;
    }

}
