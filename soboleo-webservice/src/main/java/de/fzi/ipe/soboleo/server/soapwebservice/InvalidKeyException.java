
package de.fzi.ipe.soboleo.server.soapwebservice;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.7-b01-
 * Generated source version: 2.1
 * 
 */
@WebFault(name = "invalidKeyMsg", targetNamespace = "http://soboleo.com")
public class InvalidKeyException
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private InvalidKeyMsg faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public InvalidKeyException(String message, InvalidKeyMsg faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public InvalidKeyException(String message, InvalidKeyMsg faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: de.fzi.ipe.soboleo.server.soapwebservice.InvalidKeyMsg
     */
    public InvalidKeyMsg getFaultInfo() {
        return faultInfo;
    }

}
