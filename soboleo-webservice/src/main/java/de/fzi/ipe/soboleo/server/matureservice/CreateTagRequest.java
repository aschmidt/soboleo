
package de.fzi.ipe.soboleo.server.matureservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prefLabel" type="{http://mature-ip.eu/MATURE/types}LocalizedString"/>
 *         &lt;element name="agentURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="agentKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "prefLabel",
    "agentURI",
    "agentKey"
})
@XmlRootElement(name = "createTagRequest")
public class CreateTagRequest {

    @XmlElement(required = true)
    protected LocalizedString prefLabel;
    @XmlElement(required = true)
    protected String agentURI;
    @XmlElement(required = true)
    protected String agentKey;

    /**
     * Gets the value of the prefLabel property.
     * 
     * @return
     *     possible object is
     *     {@link LocalizedString }
     *     
     */
    public LocalizedString getPrefLabel() {
        return prefLabel;
    }

    /**
     * Sets the value of the prefLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalizedString }
     *     
     */
    public void setPrefLabel(LocalizedString value) {
        this.prefLabel = value;
    }

    /**
     * Gets the value of the agentURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentURI() {
        return agentURI;
    }

    /**
     * Sets the value of the agentURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentURI(String value) {
        this.agentURI = value;
    }

    /**
     * Gets the value of the agentKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentKey() {
        return agentKey;
    }

    /**
     * Sets the value of the agentKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentKey(String value) {
        this.agentKey = value;
    }

}
