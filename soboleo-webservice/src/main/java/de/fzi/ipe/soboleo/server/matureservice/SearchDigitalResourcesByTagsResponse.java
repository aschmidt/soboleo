
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="searchDigitalResourceResult" type="{http://mature-ip.eu/MATURE/types}searchDigitalResourceEntry" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchDigitalResourceResult"
})
@XmlRootElement(name = "searchDigitalResourcesByTagsResponse")
public class SearchDigitalResourcesByTagsResponse {

    protected List<SearchDigitalResourceEntry> searchDigitalResourceResult;

    /**
     * Gets the value of the searchDigitalResourceResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the searchDigitalResourceResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSearchDigitalResourceResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SearchDigitalResourceEntry }
     * 
     * 
     */
    public List<SearchDigitalResourceEntry> getSearchDigitalResourceResult() {
        if (searchDigitalResourceResult == null) {
            searchDigitalResourceResult = new ArrayList<SearchDigitalResourceEntry>();
        }
        return this.searchDigitalResourceResult;
    }

}
