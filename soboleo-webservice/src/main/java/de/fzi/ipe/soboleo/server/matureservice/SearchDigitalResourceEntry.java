
package de.fzi.ipe.soboleo.server.matureservice;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchDigitalResourceEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchDigitalResourceEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="digitalResourceURI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="score" type="{http://mature-ip.eu/MATURE/types}searchScoreType"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tagFreqList" type="{http://mature-ip.eu/MATURE/types}tagFrequencyEntry" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="overallRating" type="{http://mature-ip.eu/MATURE/types}overallRatingEntry"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchDigitalResourceEntry", namespace = "http://mature-ip.eu/MATURE/types", propOrder = {
    "digitalResourceURI",
    "score",
    "title",
    "tagFreqList",
    "overallRating"
})
public class SearchDigitalResourceEntry {

    @XmlElement(required = true)
    protected String digitalResourceURI;
    protected float score;
    @XmlElement(required = true)
    protected String title;
    protected List<TagFrequencyEntry> tagFreqList;
    @XmlElement(required = true)
    protected OverallRatingEntry overallRating;

    /**
     * Gets the value of the digitalResourceURI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDigitalResourceURI() {
        return digitalResourceURI;
    }

    /**
     * Sets the value of the digitalResourceURI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDigitalResourceURI(String value) {
        this.digitalResourceURI = value;
    }

    /**
     * Gets the value of the score property.
     * 
     */
    public float getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     * 
     */
    public void setScore(float value) {
        this.score = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the tagFreqList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tagFreqList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTagFreqList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagFrequencyEntry }
     * 
     * 
     */
    public List<TagFrequencyEntry> getTagFreqList() {
        if (tagFreqList == null) {
            tagFreqList = new ArrayList<TagFrequencyEntry>();
        }
        return this.tagFreqList;
    }

    /**
     * Gets the value of the overallRating property.
     * 
     * @return
     *     possible object is
     *     {@link OverallRatingEntry }
     *     
     */
    public OverallRatingEntry getOverallRating() {
        return overallRating;
    }

    /**
     * Sets the value of the overallRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link OverallRatingEntry }
     *     
     */
    public void setOverallRating(OverallRatingEntry value) {
        this.overallRating = value;
    }

}
