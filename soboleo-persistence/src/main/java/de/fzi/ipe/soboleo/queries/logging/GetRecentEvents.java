/*
 * FZI - Information Process Engineering 
 * Created on 05.02.2010 by zach
 */
package de.fzi.ipe.soboleo.queries.logging;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Query event used to query for recent events. Always 20 (if that many are available). The result
 * is a List of events. 
 */
public class GetRecentEvents extends QueryEvent{

	private int offset = 0;
	private Class<? extends Event>[] classes;

	
	/**
	 * Returns the most recent 20 events (ignoring the first <offset> events). Only 
	 * events that are subclasses of one of the classes given are returned. 
	 */
	public GetRecentEvents(int offset ,Class<? extends Event>... classes) {
		this.offset = offset;
		this.classes = classes;
	}
	
	/**
	 * Returns the most recent 20 events (ignoring the first <offset> events). All
	 * stored events are returned. 
	 */
	@SuppressWarnings("unchecked")
	public GetRecentEvents(int offset) {
		this.offset = offset;
		classes =  new Class[1]; 
		classes[0] = Event.class;
	}
	
	
	public int getOffset() {
		return offset;
	}
	
	public Class<? extends Event>[] getClasses() {
		return classes;
	}
	
}
