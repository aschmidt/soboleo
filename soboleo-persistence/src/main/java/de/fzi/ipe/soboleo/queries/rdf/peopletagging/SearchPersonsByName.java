package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;


/**
 * Searches for peoples based on the string given. 
 * Searches for names of peoples not concepts.
 * Used in Browse People.
 * 
 * @author FZI - Information Process Engineering Stephan Kluge
 */
public class SearchPersonsByName extends QueryEvent
{

	private String searchString;
	private String askerURI;
	
	
	public SearchPersonsByName(String searchString) 
	{
		this.searchString = searchString;
	}
	
	public SearchPersonsByName(String searchString, String askerURI)
	{
		this.searchString = searchString;
		this.askerURI = askerURI;
	}
	
	@SuppressWarnings("unused")
	private SearchPersonsByName() 
	{;}
	
	public String getSearchString() 
	{
		return searchString;
	}
	
	public String getAskerURI()
	{
		return askerURI;
	}
	
}
