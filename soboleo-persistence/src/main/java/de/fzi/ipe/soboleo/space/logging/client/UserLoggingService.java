package de.fzi.ipe.soboleo.space.logging.client;


public interface UserLoggingService {
	/** this is the interface which is invoked by the client side. 
	 * calls the user logging services from web service von Grazern
	 * @author braun
	 *
	 */

		/**
		 * Sets specific user events by a user
		 * @param key key of the requesting use
		 * @param mailtoUri mailto-uri of the requesting user 
		 * @param space respective space name of the maker
		 * @param resourceURI the uri of the resource/object that is affected by this event
		 * @param actionType type of event to be logged 
		 * @param content additional detail information of the event
		 * @return boolean for success of operation
		 */
		public boolean addUserEventRequest(String key, String mailtoUri, String space, String resourceURI, String actionType, String content);
		
	}
