/*
 * FZI - Information Process Engineering 
 * Created on 08.07.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.dialog;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.openrdf.model.URI;

import de.fzi.ipe.soboleo.beans.dialog.WebdocumentDialog;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFTag;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;


/**
 * Represents a document in the context of the document.eventBusAdaptor. This class is 
 * intended to be used only by the AnnotationEventBus Adaptor - other parts of soboleo
 * should use events and commands to interact with annotations and documents. 
 */
public class RDFWebDocumentDialog extends RDFDialog{
	
	protected RDFWebDocumentDialog(URI uri, TripleStore tripleStore) {
		super(uri,tripleStore);
	}
	
	protected synchronized void createDocument() throws IOException{
		super.createDocument();
		URI typeURI = vf.createURI(TYPE.toString());
		URI conceptURI = vf.createURI(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
		tripleStore.addTriple(documentURI, typeURI, conceptURI, null);
		URI createdURI = vf.createURI(SoboleoNS.DATE_ADDED.toString());
		tripleStore.setTimestamp(documentURI, createdURI, null);
	}
	
	public WebdocumentDialog getClientSideDocument() throws IOException	{
		Set<RDFTag> rdfTags = getTags();
		Set<Tag> tags = new HashSet<Tag>(rdfTags.size());
		for (RDFTag rdfTag:rdfTags) {
			tags.add(rdfTag.getClientSideTag());
		}
		String uri = getURI().toString();
		String title = getTitle();
		String content = getContent();
		String initiator = getInitiator();
		Set<String> participants = getParticipants();
		String isAbout = getAbout();
		return new WebdocumentDialog(uri,title,content,isAbout, initiator, participants,tags);
	}
	
}
