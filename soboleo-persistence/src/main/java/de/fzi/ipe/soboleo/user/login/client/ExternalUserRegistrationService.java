package de.fzi.ipe.soboleo.user.login.client;

public interface ExternalUserRegistrationService {
	/** this is the interface which is invoked by the client side. 
	 * calls the user logging services from web service von Grazern
	 * @author braun
	 *
	 */
	
	/**
	 * 
	 */
	public boolean login(String userEmail, String userKey);
}
