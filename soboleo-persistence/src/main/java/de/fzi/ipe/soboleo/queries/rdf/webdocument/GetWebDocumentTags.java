/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.webdocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the PersonTags
 */
public class GetWebDocumentTags extends QueryEvent{

	private String webdocURI;
	private String conceptURI;
	private String makerURI;
	
	public GetWebDocumentTags(String resourceURI, String conceptURI, String makerURI) {
		this.webdocURI = resourceURI;
		this.conceptURI = conceptURI;
		this.makerURI = makerURI;
	}
	
	@SuppressWarnings("unused")
	private GetWebDocumentTags() {; }
	
	public String getWebDocumentURI() {
		return webdocURI;
	}
	
	public String getConceptURI(){
		return conceptURI;
	}
	
	public String getMakerURI(){
		return makerURI;
	}
}
