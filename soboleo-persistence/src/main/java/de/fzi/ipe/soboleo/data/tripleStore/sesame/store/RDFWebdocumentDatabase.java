/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.data.tripleStore.sesame.store;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;
import static de.fzi.ipe.soboleo.beans.ontology.SoboleoNS.DOCUMENT;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;

import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

/**
 * Represents the set of all documents and annotations in the context of 
 * the document.eventBusAdapter. This class is intended to be used by classes
 * in the document package - all other classes should use events, queries and commands
 * to interact with the DocumentSet.
 */
public class RDFWebdocumentDatabase extends RDFDocumentDatabase<RDFWebDocument>{
	
	public RDFWebdocumentDatabase(TripleStore tripleStore) {
		super(tripleStore);
	}
	
	protected synchronized RDFWebDocument getDocument(URI documentURI) throws IOException {
		RDFWebDocument doc = documentCache.get(documentURI);
		if (doc == null) {
			URI conceptURI = vf.createURI(DOCUMENT.toString());
			URI typeURI = vf.createURI(TYPE.toString());
			if (tripleStore.hasStatement(documentURI, typeURI, conceptURI)) {
				doc = new RDFWebDocument(documentURI,tripleStore);
				documentCache.put(documentURI, doc);
			}
		}
		return doc;
	}
	
	public synchronized void deleteDocument(String uri) throws IOException {
		URI docURI = vf.createURI(uri);
		RDFWebDocument doc = getDocument(docURI);
		if (doc != null) { //this really is a document
			Set<RDFTag> tags = doc.getTags();
			for (RDFTag tag: tags) tag.delete();
			tripleStore.removeTriples(docURI, null, null, null);
			documentCache.remove(docURI);
		}
	}
	
	public synchronized Set<RDFWebDocument> getAllDocuments() throws IOException {
		return super.getAllDocuments(DOCUMENT.toString());
	}
	
	public synchronized RDFWebDocument createDocument() throws IOException{
		URI docURI = tripleStore.getUniqueURI();
		RDFWebDocument doc = new RDFWebDocument(docURI,tripleStore);
		doc.createDocument();
		documentCache.put(docURI, doc);
		return doc;
	}
	
	/**
	 * Gets the document by URL. The create parameter specifies whether a new document 
	 * is created when none exists for this url. 
	 * @throws IOException
	 */
	
	public synchronized RDFWebDocument getDocumentByURL(String url, boolean create) throws IOException {
		URI hasURL = vf.createURI(SoboleoNS.HAS_URL.toString());
		Literal urlLiteral = vf.createLiteral(url);
		List<Statement> statements = tripleStore.getStatementList(null, hasURL, urlLiteral);
		if (create && statements.size() == 0) {
			RDFWebDocument doc = createDocument();
			tripleStore.addTriple(doc.getURI(), hasURL, urlLiteral, null);
			URI createdURI = vf.createURI(SoboleoNS.DATE_ADDED.toString());
			tripleStore.setTimestamp(doc.getURI(), createdURI, null);
			return doc;
		}
		else if (statements.size() == 0) return null;
		else {
			Statement stmt = statements.get(0);
			URI documentURI = (URI) stmt.getSubject();
			return getDocument(documentURI);
		}
	}
}
