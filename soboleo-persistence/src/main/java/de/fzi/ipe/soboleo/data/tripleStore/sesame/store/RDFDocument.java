/*
 * FZI - Information Process Engineering 
 * Created on 08.07.2009 by zach
 */
package de.fzi.ipe.soboleo.data.tripleStore.sesame.store;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.CommonTag;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Document;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;


/**
 * Represents a document in the context of the document.eventBusAdaptor. This class is 
 * intended to be used only by the AnnotationEventBus Adaptor - other parts of soboleo
 * should use events and commands to interact with annotations and documents. 
 */
public abstract class RDFDocument<A extends Document> {

	protected URI documentURI;
	protected ValueFactory vf;
	
	protected TripleStore tripleStore;
	
	protected RDFDocument(URI uri, TripleStore tripleStore) {
		this.documentURI = uri;
		this.tripleStore = tripleStore;
		this.vf = tripleStore.getValueFactory();
	}
	
	public URI getURI() {
		return documentURI;
	}
	
	public void setTitle(String title) throws IOException {
		URI predicate = vf.createURI(SoboleoNS.HAS_TITLE.toString());
		tripleStore.removeTriples(documentURI, predicate, null, null);
		tripleStore.addTriple(documentURI,predicate,vf.createLiteral(title),null);
		tripleStore.setTimestamp(documentURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	public RDFTag addTag(String userId, String conceptId) throws IOException {
		URI tagObjectID = tripleStore.getUniqueURI();
		RDFTag annotation = new RDFTag(tagObjectID,tripleStore);
		annotation.createTag(documentURI);
		annotation.setUserId(userId);
		annotation.setConceptId(conceptId);
		return annotation;
	}
	
	public void removeTag(String tagIDString) throws IOException {
		URI tagID = vf.createURI(tagIDString);
		RDFTag tag = new RDFTag(tagID,tripleStore);
		tag.delete();
	}
	
	public Set<RDFTag> getTags() throws IOException {
		Set<RDFTag> toReturn = new HashSet<RDFTag>();
		URI isTaggedWith = vf.createURI(CommonTag.IS_TAGGED_WITH.toString());
		List<Statement> statements = tripleStore.getStatementList(documentURI, isTaggedWith, null);
		for (Statement s: statements) toReturn.add(new RDFTag((URI)s.getObject(),tripleStore));
		return toReturn;
	}
	
	public String getTitle() throws IOException {
		URI predicate = vf.createURI(SoboleoNS.HAS_TITLE.toString());
		return getValue(predicate);		
	}

	protected String getValue(URI predicate) throws IOException {
		List<Statement> statements = tripleStore.getStatementList(documentURI, predicate, null);
		if (statements.size()==0) return null;
		else return (statements.get(0).getObject().stringValue());
	}
	
	public void deleteAllTags() throws IOException {
		Set<RDFTag> tags = getTags();
		for (RDFTag tag: tags) tag.delete();
	}
	
	abstract public A getClientSideDocument() throws IOException;
	
	abstract protected void createDocument() throws IOException; 
	
}
