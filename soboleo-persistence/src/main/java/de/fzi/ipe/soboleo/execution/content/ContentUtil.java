package de.fzi.ipe.soboleo.execution.content;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class ContentUtil {

	
	
	public static void cacheInputStream(InputStream is)
	{
		try {
			try {
				final byte[] thumb = new byte[is.available()];
				is.read(thumb);

				final DataOutputStream os = new DataOutputStream(
						new FileOutputStream("cache"));
				for (final byte element : thumb) {
					os.writeByte(element);
				}
				os.close();
				

				
			} catch (final Exception e) {

			}
			
		} catch (Exception e) {
			
		}
	}
	
	public static InputStream getCachedInputStream()
	{
		try {
			File f = new File("cache");
			FileInputStream fis = new FileInputStream(f);
			return fis;
		} catch (Exception e) {
			return null;
		}
	}
}
