/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;


/**
 * Searches for peoples based on the string given. 
 * The string is searched for concept references and these
 * are then used to find persons. Returns a sorted list
 * of ClientSide.TaggedPerson objects, the best matching 
 * ones are returned first. 
 */
public class SearchPersonsForConcept extends QueryEvent{

	private String[] conceptURIs;
	private String taggedPersonURI;
	
	public SearchPersonsForConcept(String conceptURI) {
		this.conceptURIs = new String[]{conceptURI};
	}
	
	public SearchPersonsForConcept(String conceptURI, String askerURI){
		this.conceptURIs = new String[]{conceptURI};
		this.taggedPersonURI = askerURI;
	}
	
	public SearchPersonsForConcept(String[] conceptURIs, String askerURI){
		this.conceptURIs = conceptURIs;
		this.taggedPersonURI = askerURI;
	}
	
	@SuppressWarnings("unused")
	private SearchPersonsForConcept() {;}
	
	public String[] getConceptURIs() {
		return conceptURIs;
	}
	
	public String getTaggedPersonURI(){
		return taggedPersonURI;
	}
	
}
