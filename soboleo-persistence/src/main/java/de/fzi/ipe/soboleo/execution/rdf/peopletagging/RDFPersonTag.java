/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.peopletagging;

import java.io.IOException;
import java.util.List;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.RDF;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.PersonTag;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

public abstract class RDFPersonTag {

	URI tagURI;
	TripleStore tripleStore;
	ValueFactory vf;
	
	RDFPersonTag(URI annotationID, TripleStore tripleStore){
		this.tagURI = annotationID;
		this.tripleStore = tripleStore;
		this.vf = tripleStore.getValueFactory();
	}

	void createTag(URI personURI) throws IOException {
		URI tagClassId = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG.toString());
		URI rdfType = vf.createURI(RDF.TYPE.toString());
		URI isTaggedWith = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAGGED_WITH.toString());
		tripleStore.addTriple(tagURI, rdfType, tagClassId, null);
		tripleStore.addTriple(personURI,isTaggedWith,tagURI,null);
		tripleStore.setTimestamp(tagURI, vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG_DATE.toString()), null);
	}

	public URI getURI()  {
		return tagURI;
	}
	
	public void setConceptId(String conceptIdString) throws IOException {
		URI conceptId = vf.createURI(conceptIdString);
		URI pred = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG_MEANS.toString());
		tripleStore.removeTriples(tagURI,pred,null,null);
		tripleStore.addTriple(tagURI,pred,conceptId,null);
	}
	
	public String getConceptId() throws IOException {
		URI pred = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG_MEANS.toString());
		List<Statement> statements = tripleStore.getStatementList(tagURI,pred,null);
		if (statements.size() == 0) return null;
		else return ((URI)statements.get(0).getObject()).toString();
	}
	
	public void delete() throws IOException {
		tripleStore.removeTriples(tagURI, null, null, null);
		tripleStore.removeTriples(null, null, tagURI, null);
	}
	
	public abstract PersonTag getClientSideTag() throws IOException;
	
	public static RDFPersonTag getPersonTag(URI uri, TripleStore tripleStore) throws IOException {
		ValueFactory vf = tripleStore.getValueFactory();
		URI rdfType = vf.createURI(RDF.TYPE.toString());
		URI manualTagURI = vf.createURI(SoboleoNS.PEOPLE_MANUAL_PERSON_TAG.toString());
		if (tripleStore.hasStatement(uri, rdfType, manualTagURI)) { //is a manual tag
			return new RDFManualPersonTag(uri,tripleStore);
		}
		else {
			return null; //TODO - Automatic Person Tag
		}
	}
		
}
