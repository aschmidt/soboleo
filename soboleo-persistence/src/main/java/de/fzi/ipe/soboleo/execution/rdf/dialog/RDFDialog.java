/*
 * FZI - Information Process Engineering 
 * Created on 08.07.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.dialog;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;

import de.fzi.ipe.soboleo.beans.dialog.Dialog;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFDocument;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;


/**
 * Represents a document in the context of the document.eventBusAdaptor. This class is 
 * intended to be used only by the AnnotationEventBus Adaptor - other parts of soboleo
 * should use events and commands to interact with annotations and documents. 
 */
public abstract class RDFDialog extends RDFDocument<Dialog>{
	
	protected RDFDialog(URI uri, TripleStore tripleStore) {
		super(uri,tripleStore);
	}
	
	public String getAbout() throws IOException {
		URI predicate = vf.createURI(SoboleoNS.IS_ABOUT.toString());
		return getValue(predicate);
	}
	
	public void setAbout(String aboutURI) throws IOException {
		URI predicate = vf.createURI(SoboleoNS.IS_ABOUT.toString());
		tripleStore.removeTriples(documentURI, predicate, null, null);
		tripleStore.addTriple(documentURI, predicate, vf.createURI(aboutURI), null);
		tripleStore.setTimestamp(documentURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	public void removeParticipant(String userId) throws IOException {
		URI userID = vf.createURI(userId);
		URI predicate = vf.createURI(SoboleoNS.HAS_PARTICIPANT.toString());
		tripleStore.removeTriples(documentURI, predicate, userID, null);
		tripleStore.setTimestamp(documentURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	public Set<String> getParticipants() throws IOException {
		Set<String> toReturn = new HashSet<String>();
		URI hasParticipant = vf.createURI(SoboleoNS.HAS_PARTICIPANT.toString());
		List<Statement> statements = tripleStore.getStatementList(documentURI, hasParticipant, null);
		for (Statement s: statements) toReturn.add(((URI)s.getObject()).stringValue());
		return toReturn;
	}
	
	public void addParticipant(String userId) throws IOException {
		URI userID = vf.createURI(userId);
		URI predicate = vf.createURI(SoboleoNS.HAS_PARTICIPANT.toString());
		tripleStore.removeTriples(documentURI, predicate, userID, null);
		tripleStore.addTriple(documentURI, predicate, userID, null);
		tripleStore.setTimestamp(documentURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	public void setInitiator(String userId) throws IOException {
		URI userID = vf.createURI(userId);
		URI predicate = vf.createURI(SoboleoNS.HAS_INITIATOR.toString());
		tripleStore.removeTriples(documentURI, predicate, null, null);
		tripleStore.addTriple(documentURI, predicate, userID, null);
		tripleStore.setTimestamp(documentURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	public String getInitiator() throws IOException {
		URI predicate = vf.createURI(SoboleoNS.HAS_INITIATOR.toString());
		return getValue(predicate);
	}
	
	public String getContent() throws IOException {
		URI predicate = vf.createURI(SoboleoNS.HAS_CONTENT.toString());
		return getValue(predicate);
	}
	
	public void setContent(String content) throws IOException {
		URI predicate = vf.createURI(SoboleoNS.HAS_CONTENT.toString());
		tripleStore.removeTriples(documentURI, predicate, null, null);
		tripleStore.addTriple(documentURI, predicate, vf.createLiteral(content), null);
		tripleStore.setTimestamp(documentURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	protected synchronized void createDocument() throws IOException{
		URI typeURI = vf.createURI(TYPE.toString());
		URI conceptURI = vf.createURI(SoboleoNS.DIALOG.toString());
		tripleStore.addTriple(documentURI, typeURI, conceptURI, null);
		URI createdURI = vf.createURI(SoboleoNS.DATE_ADDED.toString());
		tripleStore.setTimestamp(documentURI, createdURI, null);
	}
	
	public abstract Dialog getClientSideDocument() throws IOException; 
	
}
