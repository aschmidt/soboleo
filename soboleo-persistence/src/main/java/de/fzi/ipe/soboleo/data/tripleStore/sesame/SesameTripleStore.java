/*
 * FZI - Information Process Engineering 
 * Created on 15.01.2009 by zach
 * Creates a memory based SesameTripleStore based on a file. 
 */
package de.fzi.ipe.soboleo.data.tripleStore.sesame;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;
import static de.fzi.ipe.soboleo.beans.ontology.SoboleoNS.LAST_ID;
import static de.fzi.ipe.soboleo.beans.ontology.SoboleoNS.TYPE_DATASTORE;

import java.io.File;
import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.sail.memory.MemoryStore;

import de.fzi.ipe.soboleo.data.tripleStore.tools.Exporter;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

/**
 * @author FZI - Information Process Engineering 
 * Created on 15.01.2009 by zach
 * Creates a memory based SesameTripleStore based on a file. 
 *
 */
public class SesameTripleStore implements TripleStore{
	
	private RepositoryConnection con;
	private ValueFactory valueFactory;
	private URI datastoreURI;
	private DatatypeFactory datatypeFactory;
	
	/**
	 * Constructor for the implemented Interface TripleStore SesameTripleStore
	 * @param file
	 * @param datastoreName
	 * @throws IOException
	 */
	public SesameTripleStore(File file, String datastoreName) throws IOException{
		Repository repository;
		MemoryStore memStore = new MemoryStore(file);
		memStore.setSyncDelay(1000L);
		repository = new SailRepository(memStore);
		try {
			repository.initialize();
			valueFactory = repository.getValueFactory();
			this.con = repository.getConnection();
			initRepository(this.con, datastoreName);
		} catch (RepositoryException e) {
			throw new IOException(e);
		}
		try {
			this.datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException dce) {
			throw new IOException (dce);
		}
	}
	
	@Override
	public ValueFactory getValueFactory() {
		return valueFactory;
	}
	
	@Override
	public void removeTriples(Resource subject, URI predicate, Value object, Resource context) throws IOException {
		try {
			//the sesame api makes a difference between a context that is not stated and one that is null
			//with a stated "null" only statements without a context are removed. calling 'remove' without 
			//stating a context removes all statements. The second behavior is what we want here. 
			if (context == null) this.con.remove(subject,predicate,object);
			else this.con.remove(subject, predicate, object, context);
		} catch (RepositoryException re) {
			throw new IOException(re);
		}
	}
	
	@Override
	public void removeTriples(String subjectURI, String predicateURI, String objectURI, String contextURI) throws IOException {
		URI s = null;
		if (subjectURI != null) s = valueFactory.createURI(subjectURI);
		URI p = null;
		if (predicateURI != null) p = valueFactory.createURI(predicateURI);
		URI o = null;
		if (objectURI != null) o = valueFactory.createURI(objectURI);
		URI c = null;
		if (contextURI != null) c = valueFactory.createURI(contextURI);
		removeTriples(s, p, o, c);
	}
	
	@Override
	public void addTriple(Resource subject, URI predicate, Value object, Resource context) throws IOException {
		try {
			this.con.add(subject, predicate, object, context);
		} catch (RepositoryException re) {
			throw new IOException(re);
		}
	}

	@Override
	public void addTriple(String subjectURI, String predicateURI, String objectURI, String contextURI) throws IOException {
		URI s = valueFactory.createURI(subjectURI);
		URI p = valueFactory.createURI(predicateURI);
		URI o = valueFactory.createURI(objectURI);
		URI c = null;
		if (contextURI != null) c = valueFactory.createURI(contextURI);
		addTriple(s, p, o, c);
	}
	
	@Override
	public void addTriple(String subjectURI, String predicateURI, int objectValue, String contextURI) throws IOException {
		URI s = valueFactory.createURI(subjectURI);
		URI p = valueFactory.createURI(predicateURI);
		Value o = valueFactory.createLiteral(objectValue);
		URI c = null;
		if (contextURI != null) c = valueFactory.createURI(contextURI);
		addTriple(s, p, o, c);
	}	
	
	private void initRepository(RepositoryConnection con, String datastoreName) throws RepositoryException,IOException {
		URI rdfType = valueFactory.createURI(TYPE.toString());
		URI typeDatastore = valueFactory.createURI(TYPE_DATASTORE.toString());
		RepositoryResult<Statement> statements = con.getStatements(null,rdfType,typeDatastore,false);
		if (statements.hasNext()) { //repository metadata exists already
			Statement s = statements.next();
			datastoreURI = (URI) s.getSubject();
			statements.close();
		}
		else {
			statements.close();
			datastoreURI = valueFactory.createURI(SOBOLEO_NS+datastoreName);
			addTriple(datastoreURI, rdfType, typeDatastore,null);
			setValue(datastoreURI.toString(),LAST_ID.toString(),0);
		}
	}

	public void close() throws IOException {
		try {
			this.con.close();
			this.con.getRepository().shutDown();
		} catch(RepositoryException re) {
			throw new IOException(re);
		}
	}	
	
	public String getContentDump () throws IOException {	
		try {
			StringBuffer toReturn = new StringBuffer();
			RepositoryResult<Statement> statements = this.con.getStatements(null, null, null, false);
			while (statements.hasNext()){
				Statement next = statements.next();
				toReturn.append(next.getSubject() + ", "+ next.getPredicate() + ", " +  next.getObject() +", "+next.getContext());
				toReturn.append(System.getProperty("line.separator"));
			}
			statements.close();
			return toReturn.toString();
		} catch (RepositoryException re) {
			throw new IOException(re);
		}
	}
	
	public String getSKOSTaxonomyAsXML() throws IOException {
		try {
			return Exporter.exportSKOS(con);
		} catch (RepositoryException e) {
			throw new IOException(e);
		} catch (RDFHandlerException e) {
			throw new IOException(e);
		}
	}
	
	@Override
	public String getCompleteRDFDump() throws IOException {
		try {
			return Exporter.exportRDF(this.con);
		} catch (RepositoryException e) {
			throw new IOException(e);
		} catch (RDFHandlerException e) {
			throw new IOException(e);
		}
	}

	protected int getValue(String subjectURI, String predicateURI) throws IOException {
		try {
			URI s= valueFactory.createURI(subjectURI);
			URI p= valueFactory.createURI(predicateURI);
			RepositoryResult<Statement> statements = this.con.getStatements(s, p, null, false);
			Statement stmt = statements.next();
			Literal l = (Literal) stmt.getObject();
			statements.close();
			return l.intValue();
		} catch(RepositoryException re) {
			throw new IOException(re);
		}
	}
	
	protected void setValue(String subjectURI, String predicateURI, int value) throws IOException{
		addTriple(subjectURI, predicateURI, value, null);
	}
	
	@Override
	public URI getUniqueURI() throws IOException {
		int value = getValue(datastoreURI.toString(),LAST_ID.toString());
		removeTriples(datastoreURI.toString(), LAST_ID.toString(), null, null);
		value ++;
		setValue(datastoreURI.toString(), LAST_ID.toString(),value);
		return valueFactory.createURI(datastoreURI+"-gen"+value);
	}

	
	@Override
	public List<Statement> getStatementList(Resource subject, URI predicate, Value object) throws IOException {
		try {
			RepositoryResult<Statement> statements = this.con.getStatements(subject, predicate,object, false);
			List<Statement> toReturn = statements.asList();
			statements.close();
			return toReturn;
		} catch (RepositoryException re) {
			throw new IOException(re);
		}
	}
	
	@Override
	public List<Statement> getStatementList(String subject, String predicate, String object) throws IOException {
		URI s = null;
		if (subject != null) s = valueFactory.createURI(subject);
		URI p = null;
		if (predicate != null) p = valueFactory.createURI(predicate);
		URI o = null;
		if (object != null) o = valueFactory.createURI(object);
		return getStatementList(s, p, o);
	}

	@Override
	public boolean hasStatement(Resource subject, URI predicate, Value object) throws IOException {
		try {
			return this.con.hasStatement(subject, predicate, object, false);
		}catch (RepositoryException re) {
			throw new IOException(re);
		}
	}
	
	
	
     /**
     *  Execute a SELECT SPARQL query  
     * 
     * @param queryString SELECT SPARQL query
     * @return set of results, each containing a Hashmap of bindings <String, Value>
     */
	@Override
    public Set<HashMap<String,Value>> executeSelectSPARQLQuery(String queryString) throws IOException{
    	try {
    		TupleQuery query = this.con.prepareTupleQuery(org.openrdf.query.QueryLanguage.SPARQL, queryString);
    		TupleQueryResult result = query.evaluate();
    		Set<HashMap<String,Value>> resultList = new HashSet<HashMap<String,Value>>();
    		while (result.hasNext()) {
    			BindingSet bindingSet = result.next();
    			Set<String> names = bindingSet.getBindingNames();
    			HashMap<String,Value> map = new HashMap<String,Value>();
    			for (String n : names) map.put(n, bindingSet.getValue(n));
    			resultList.add(map);
    		}
    		result.close();
    		return resultList;
    	}
    	catch (RepositoryException re) {
    		throw new IOException(re);
    	} catch (MalformedQueryException e) {
    		throw new IOException(e);
		} catch (QueryEvaluationException e) {
			throw new IOException(e);
		}
    }

	@Override 
	public Set<Value> executeSelectSPARQLQueryForParameter(String queryString, String paramName) throws IOException{
		try {
    		TupleQuery query = this.con.prepareTupleQuery(org.openrdf.query.QueryLanguage.SPARQL, queryString);
    		TupleQueryResult result = query.evaluate();
    		Set<Value> resultList = new HashSet<Value>();
    		if(!result.getBindingNames().contains(paramName)) throw new IllegalArgumentException(paramName +" is not a binding name");
    		while (result.hasNext()) {
    			BindingSet bindingSet = result.next();
    			Value val = bindingSet.getValue(paramName);
    			if(val != null) resultList.add(val);
    		}
    		result.close();
    		return resultList;
    	}
    	catch (RepositoryException re) {
    		throw new IOException(re);
    	} catch (MalformedQueryException e) {
    		throw new IOException(e);
		} catch (QueryEvaluationException e) {
			throw new IOException(e);
		}
	}

	@Override
	public void setTimestamp(Resource subject, URI predicate, String contextURI) throws IOException {
		XMLGregorianCalendar xmlCal = datatypeFactory.newXMLGregorianCalendar((GregorianCalendar)GregorianCalendar.getInstance());
		Literal date = valueFactory.createLiteral(xmlCal);
		
		removeTriples(subject, predicate, null, null);
		addTriple(subject, predicate, date, null);
	}

	@Override
	public URI getDatastoreURI() {
		return datastoreURI;
	}
	
	
}
