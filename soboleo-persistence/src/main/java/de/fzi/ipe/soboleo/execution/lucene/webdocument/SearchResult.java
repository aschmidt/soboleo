package de.fzi.ipe.soboleo.execution.lucene.webdocument;

import java.util.List;
import java.util.Set;

import de.fzi.ipe.soboleo.lucene.LuceneResult;

public class SearchResult {
	
	private LuceneResult luceneResult;
	private Set<String> searchedConcepts;
	private List<String> relaxations;
	private List<String> refinements;
	private Set<String> mustConceptURIs;
	private String query;
	
	
	public SearchResult(LuceneResult result, Set<String> searchedConceptURIs) {
		this.luceneResult = result;
		this.searchedConcepts = searchedConceptURIs;
	}
	
	public LuceneResult getLuceneResult() {
		return luceneResult;
	}
	
	public Set<String> getSearchedConceptURIs() {
		return searchedConcepts;
	}

	public void setRelaxations(List<String> relaxations) {
		this.relaxations = relaxations;
	}

	public List<String> getRelaxations() {
		return relaxations;
	}

	public Set<String> getRequiredConcepts() {
		return mustConceptURIs;
	}
	
	public void setQuery(String query) {
		this.query = query;
	}
	
	public String getQuery() {
		return query;
	}
	
	public void setRequiredConceptURIs(Set<String> mustConceptUris) {
		this.mustConceptURIs = mustConceptUris;
	}

	public void setRefinements(List<String> refinements) {
		this.refinements = refinements;
	}
	
	public List<String> getRefinements() {
		return refinements;
	}
}
