/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.officedocument;

import java.util.HashSet;
import java.util.Set;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.lucene.document.ResultType;

/**
 * Event to ask the LuceneEventBusAdaptor for a search result. Returns
 * a SearchResult object. 
 */
public class GetOfficeDocumentsSearchResult extends QueryEvent{
	
	private ResultType resultType;
	private String query;
	private Set<String> mustConceptURIs;
	private Set<String> mustSubconceptURIs;
	private Set<String> extractedConceptURIs;
	private Set<String> extractedSubconceptURIs;
	
	@SuppressWarnings("unused") //needed for GWT
	private GetOfficeDocumentsSearchResult() {}
	
	/**
	 * @param query
	 * @param resultType The result type expected - if null is given, all types are returned
	 */
	public GetOfficeDocumentsSearchResult(String query,ResultType resultType) {
		this.query = query;
		this.mustConceptURIs = new HashSet<String>();
		this.resultType = resultType;
	}
	
	public GetOfficeDocumentsSearchResult(String query, ResultType resultType, Set<String> mustConceptURIs) {
		this.query = query;
		this.mustConceptURIs = mustConceptURIs;
		this.resultType = resultType;
	}
	
	public String getQuery(){
		return query; 
	}
	
	public ResultType getResultType() {
		return resultType;
	}
	
	public Set<String> getMustConceptURIs(){
		return mustConceptURIs; 
	}
	
	public Set<String> getMustSubconceptURIs (){
		return mustSubconceptURIs;
	}
	
	public Set<String> getExtractedConceptURIs(){
		return extractedConceptURIs;
	}
	
	public Set<String> getExtractedSubconceptURIs(){
		return extractedSubconceptURIs;
	}
	
	public void setMustConceptURIs(Set<String> mustConceptURIs){
		this.mustConceptURIs = mustConceptURIs;
	}
	
	public void setMustSubconceptURIs (Set<String> mustSubconceptURIs){
		this.mustSubconceptURIs = mustSubconceptURIs;
	}
	
	public void setExtractedConceptURIs(Set<String> extractedConceptURIs){
		this.extractedConceptURIs = extractedConceptURIs;
	}
	
	public void setExtractedSubconceptURIs(Set<String> extractedSubconceptURIs){
		this.extractedSubconceptURIs = extractedSubconceptURIs;
	}
}
