/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the PersonTags
 */
public class GetAggregatedPersonTags extends QueryEvent{

	private String taggedPersonURI;
	private String proxyURL;
	private String makerURI;
	
	public GetAggregatedPersonTags(String resourceURI, String makerURI, String proxyURL) {
		this.taggedPersonURI = resourceURI;
		this.proxyURL = proxyURL;
		this.makerURI = makerURI;
	}
	
	@SuppressWarnings("unused")
	private GetAggregatedPersonTags() {; }
	
	public String getTaggedPersonURI() {
		return taggedPersonURI;
	}
	
	public String getPageURL(){
		return proxyURL;
	}
	
	public String getMakerURI(){
		return makerURI;
	}
}
