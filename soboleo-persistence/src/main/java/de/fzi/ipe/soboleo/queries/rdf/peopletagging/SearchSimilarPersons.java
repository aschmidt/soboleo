/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.peopletagging;


import de.fzi.ipe.soboleo.event.execution.QueryEvent;


/**
 * Searches for peoples based on the string given. 
 * The string is searched for concept references and these
 * are then used to find persons. Returns a sorted list
 * of ClientSide.TaggedPerson objects, the best matching 
 * ones are returned first. 
 */
public class SearchSimilarPersons extends QueryEvent{

	private String taggedPersonURI;
	
	public SearchSimilarPersons(String taggedPersonURI) {
		this.taggedPersonURI = taggedPersonURI;
	}
	
	@SuppressWarnings("unused")
	private SearchSimilarPersons() {;}
	
	public String getTaggedPersonURI() {
		return taggedPersonURI;
	}
	
}
