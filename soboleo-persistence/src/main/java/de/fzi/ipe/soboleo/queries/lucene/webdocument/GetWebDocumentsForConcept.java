/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.webdocument;

import java.util.Set;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.lucene.document.ResultType;

/**
 * Event to ask the LuceneEventBusAdaptor for the SearchingLuceneWrapper. 
 */
public class GetWebDocumentsForConcept extends QueryEvent{
	
	private String conceptURI;
	private Set<String> subconceptURIs;
	private ResultType resultType;
	
	@SuppressWarnings("unused") //needed for GWT
	private GetWebDocumentsForConcept() {}
	
	public GetWebDocumentsForConcept(String conceptURI, ResultType resultType) {
		this.resultType = resultType;
		this.conceptURI = conceptURI;
	}
	
	public String getConceptURI(){
		return conceptURI; 
	}
	
	public Set<String> getSubconceptURIs(){
		return subconceptURIs; 
	}
	
	public ResultType getResultType() {
		return resultType;
	}
	
	public void setSubconceptURIs(Set<String> subconceptURIs){
		this.subconceptURIs = subconceptURIs;
	}
}
