/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.officedocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Event to ask the LuceneEventBusAdaptor for the SearchingLuceneWrapper. 
 */
public class GetOfficeDocumentByUri extends QueryEvent{
	
	private String uri;
	
	public GetOfficeDocumentByUri(String uri) {
		this.uri=uri;
	}
	
	
	@SuppressWarnings("unused")
	private GetOfficeDocumentByUri() {}
	

	public String getUri() {
		return uri;
	}


		
}
