package de.fzi.ipe.soboleo.data.documentstorage;

import java.io.InputStream;

import de.fzi.ipe.soboleo.beans.document.DocumentType;


/** the interface for the storage service to store documents.
 * 
 * @author awalter
 *
 */
public interface DocumentStorage {

	/** initialize document storage . This method must be called to set the storage path
	 * from where to load and store files.**/
	public boolean initializeStorage(String storagePath);
	 
	/** adds a document and stores is using the document key. */
	public boolean addDocument(String documentKey, DocumentType dt, InputStream is );
	
	/** loads a document based on the given documentKey and document type. */
	public InputStream loadDocument(String documentKey, String dt);
	
	/** removes a doucment based the given documentKey and document type. */
	public boolean removeDocument(String documentKey, String dt);
	
	
}
