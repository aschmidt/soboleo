/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.webdocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the TaggedPerson for the URI. Returns the TaggedPerson object
 * that has a tag with this url (or none, if no person has been tagged for 
 * this url).
 */
public class GetWebDocument extends QueryEvent{

	private String uri;
	
	public GetWebDocument(String uri) {
		this.uri = uri;
	}
	
	@SuppressWarnings("unused")
	private GetWebDocument() {; }
	
	public String getURI() {
		return uri;
	}
}
