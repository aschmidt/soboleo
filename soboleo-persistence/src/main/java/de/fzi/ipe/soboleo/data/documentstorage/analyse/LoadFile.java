package de.fzi.ipe.soboleo.data.documentstorage.analyse;

import java.io.BufferedReader;
import java.io.FileReader;

public class LoadFile {

	
	public LoadFile(String pathName)
	{
		loadFile(pathName);
	}

	String loadedFile="";
	
	public void loadFile(String pathName)
	{
		try
		{
			StringBuffer fileData = new StringBuffer(1000);
	        BufferedReader reader = new BufferedReader(
	                new FileReader(pathName));
	        char[] buf = new char[1024];
	        int numRead=0;

	        while((numRead=reader.read(buf)) != -1){
	            String readData = String.valueOf(buf, 0, numRead);
	            fileData.append(readData);
	            buf = new char[1024];
	        }
	        reader.close();
	        loadedFile= fileData.toString();

		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
	
	public String getLoadedFile()
	{
		return loadedFile;
	}
	
}
