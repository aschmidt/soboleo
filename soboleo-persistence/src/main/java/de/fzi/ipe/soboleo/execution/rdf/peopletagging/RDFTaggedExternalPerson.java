/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.peopletagging;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;

import de.fzi.ipe.soboleo.beans.ontology.RDF;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.PersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedExternalPerson;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

public class RDFTaggedExternalPerson extends RDFTaggedPerson {

	
	public RDFTaggedExternalPerson(URI personURI, TripleStore tripleStore) {
		super(personURI, tripleStore);
	}

	void createPerson() throws IOException {
		URI personClassId = vf.createURI(SoboleoNS.PEOPLE_EXTERNAL_TAGGED_PERSON.toString());
		URI rdfType = vf.createURI(RDF.TYPE.toString());
		tripleStore.addTriple(personURI, rdfType, personClassId, null);
		tripleStore.setTimestamp(personURI, vf.createURI(SoboleoNS.PEOPLE_EXTERNAL_PERSON_CREATED_DATE.toString()), null);
		tripleStore.setTimestamp(personURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	public void setEmail(String email) throws IOException {
		URI predicate = vf.createURI(SoboleoNS.PEOPLE_PERSON_EMAIL.toString());
		tripleStore.removeTriples(personURI, predicate, null, null);
		tripleStore.addTriple(personURI, predicate, vf.createLiteral(email.toLowerCase()), null);
		tripleStore.setTimestamp(personURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}
	
	@Override
	public String getEmail() throws IOException{
		URI predicate = vf.createURI(SoboleoNS.PEOPLE_PERSON_EMAIL.toString());
		return getValue(predicate);
	}

	public void setName(String name) throws IOException {
		URI predicate = vf.createURI(SoboleoNS.PEOPLE_PERSON_NAME.toString());
		tripleStore.removeTriples(personURI, predicate, null, null);
		tripleStore.addTriple(personURI, predicate, vf.createLiteral(name), null);
		tripleStore.setTimestamp(personURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);		
	}
	
	@Override
	public String getName() throws IOException {
		URI predicate = vf.createURI(SoboleoNS.PEOPLE_PERSON_NAME.toString());
		return getValue(predicate);
	}
	
	private String getValue(URI predicate) throws IOException {
		List<Statement> statements = tripleStore.getStatementList(personURI, predicate, null);
		if (statements.size()==0) return null;
		else return ((Literal)statements.get(0).getObject()).stringValue();
	}
	
	public TaggedExternalPerson getClientSideTaggedPerson() throws IOException {
		Set<RDFPersonTag> rdfPersonTags = getTags();
		Set<PersonTag> personTags = new HashSet<PersonTag>(rdfPersonTags.size());
		for (RDFPersonTag rdfPersonTag: rdfPersonTags) {
			personTags.add(RDFPersonTag.getPersonTag(rdfPersonTag.getURI(), tripleStore).getClientSideTag());
		}
		String email = getEmail();
		String name = getName();
		return new TaggedExternalPerson(personURI.toString(),email,name,personTags);
	}
	
}
