package de.fzi.ipe.soboleo.lucene;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Searcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.LuceneIndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;

public class LuceneResultImpl implements Iterable<IndexDocument>, LuceneResult {

	protected static final Logger logger = Logger
			.getLogger(LuceneResultImpl.class);

	private TopDocs hits;
	private Query query;
	private Analyzer analyzer;
	private Searcher searcher;
	
	protected LuceneResultImpl(TopDocs hits, Searcher searcher, Query query, Analyzer analyzer) {
		this.hits = hits;
		this.searcher = searcher;
		this.query = query;
		this.analyzer = analyzer;
	}

	/**
	 * Returns the nth document in this set without its content.
	 * 
	 * @throws IOException
	 */
	public IndexDocument doc(int n) throws IOException {
		return createIndexDocument(searcher.doc(hits.scoreDocs[n].doc));
	}

	/**
	 * Returns nth document in this set with its concent.
	 * 
	 * @param n
	 * @return
	 * @throws IOException
	 */
	public IndexDocument fullDoc(int n) throws IOException {
		return createFullIndexDocument(searcher.doc(hits.scoreDocs[n].doc));
	}

	public String highlightText(int i) throws IOException {
		Highlighter highlighter = new Highlighter(new QueryScorer(query));
		String text = searcher.doc(hits.scoreDocs[i].doc).get(LuceneWrapper.ATR_CONTENT);
		TokenStream tokenStream = analyzer.tokenStream(
				LuceneWrapper.ATR_CONTENT, new StringReader(text));
		try {
			return highlighter.getBestFragments(tokenStream, text, 3, " ... ");
		} catch (Exception e) {
			return null;
		}
	}

	public String getText(int i) throws IOException {
		String text = searcher.doc(hits.scoreDocs[i].doc).get(LuceneWrapper.ATR_CONTENT);
		return text;
	}

	public ResultType getType(int i) throws IOException {
		String typeString = searcher.doc(hits.scoreDocs[i].doc).get(LuceneWrapper.ATR_TYPE);
		if (typeString == null)
			return null;
		else
			return ResultType.valueOf(typeString);
	}

	/**
	 * Returns the total number of hits availlable in this result.
	 */
	public int length() {
		return hits.totalHits;
	}
	
	/**
	 * Returns the total number of hits availlable in this result.
	 */
	public int getLength() {
		return hits.totalHits;
	}

	/**
	 * Returns the score of the nth document.
	 * 
	 * @throws IOException
	 */
	public float score(int n) throws IOException {
		return hits.scoreDocs[n].score;
	}

	/**
	 * Returns the id for the nth document in this set.
	 * 
	 * @throws IOException
	 */
	public int id(int n) throws IOException {
		return hits.scoreDocs[n].doc;
	}

	/**
	 * Returns an Iterator over the results.
	 * 
	 * @param doc
	 * @return
	 */
	public Iterator<IndexDocument> iterator() {
		return new LuceneResultIterator();
	}

	private IndexDocument createIndexDocument(Document doc) {
		String title = doc.get(LuceneWrapper.ATR_TITLE);
		String url = doc.get(LuceneWrapper.ATR_URL);
		String uri = doc.get(LuceneWrapper.ATR_URI);
		String typeString = doc.get(LuceneWrapper.ATR_TYPE);
		ResultType resultType = ResultType.WEB_DOCUMENT;
		if (typeString != null)
			resultType = ResultType.valueOf(typeString);
		long time = 0;
		try {
			time = DateTools.stringToTime(doc.get(LuceneWrapper.ATR_DATE));
		} catch (ParseException e) {
			throw new RuntimeException(e);// Really shouldn't happen;
		}
		Set<SemanticAnnotation> semanticAnnotations = new HashSet<SemanticAnnotation>();

		logger.debug("load sias");
		
		logger.debug("length ut: " +doc.getValues(LuceneWrapper.ATR_CONCEPTS_UT).length );
		logger.debug("length nout: " +doc.getValues(LuceneWrapper.ATR_CONCEPTS).length );

		
		if (doc.getValues(LuceneWrapper.ATR_CONCEPTS_UT).length >= doc.getValues(LuceneWrapper.ATR_CONCEPTS).length) {

			
			
			for (String cUri : doc.getValues(LuceneWrapper.ATR_CONCEPTS_UT)) {

				logger.debug("cUri : " + cUri);

				// split the cUri value to semanticAnnotation
				try {
					String fields[] = cUri.split(" xxx ");

					// stay backward compatible
					if (fields.length == 1) {

						logger.info("this is an old index entry:" + cUri);

						SemanticAnnotation sia = new SemanticAnnotation();
						sia.setUri(cUri);
						sia.setUserId("unknown");
						semanticAnnotations.add(sia);
					} else {

						logger.debug("owner: " + fields[1]);

						SemanticAnnotation sia = new SemanticAnnotation(
								fields[0], fields[1],
								Long.parseLong(fields[2]), Long
										.parseLong(fields[3]));
						semanticAnnotations.add(sia);
					}
				} catch (Exception e) {
					logger.error(e);
				}

			}
		
	} else {
		logger.info("old index entry!");

		for (String cUri : doc.getValues(LuceneWrapper.ATR_CONCEPTS)) {

			logger.info("curi: " + cUri);

			// split the cUri value to semanticAnnotation
			try {

					SemanticAnnotation sia = new SemanticAnnotation();
					sia.setUri(cUri);
					sia.setUserId("unknown");
					semanticAnnotations.add(sia);
				
			} catch (Exception e) {

			}

		}
	}
		String docOwner = doc.get(LuceneWrapper.ATR_DOCOWNER);

		return new LuceneIndexDocument(uri, title, null, url, time, resultType,
				semanticAnnotations, docOwner);
	}

	private IndexDocument createFullIndexDocument(Document doc) {
		String title = doc.get(LuceneWrapper.ATR_TITLE);
		String url = doc.get(LuceneWrapper.ATR_URL);
		String uri = doc.get(LuceneWrapper.ATR_URI);
		String text = doc.get(LuceneWrapper.ATR_CONTENT);
		String typeString = doc.get(LuceneWrapper.ATR_TYPE);
		ResultType resultType = ResultType.WEB_DOCUMENT;
		if (typeString != null)
			resultType = ResultType.valueOf(typeString);
		long time = 0;
		try {
			time = DateTools.stringToTime(doc.get(LuceneWrapper.ATR_DATE));
		} catch (ParseException e) {
			throw new RuntimeException(e);// Really shouldn't happen;
		}

		Set<SemanticAnnotation> semanticAnnotations = new HashSet<SemanticAnnotation>();
		

		if (doc.getValues(LuceneWrapper.ATR_CONCEPTS_UT).length >= doc.getValues(LuceneWrapper.ATR_CONCEPTS).length) {

			for (String cUri : doc.getValues(LuceneWrapper.ATR_CONCEPTS_UT)) {

				logger.info("curi: " + cUri);

				// split the cUri value to semanticAnnotation
				try {
					String fields[] = cUri.split(" xxx ");

					// stay backward compatible

					SemanticAnnotation sia = new SemanticAnnotation(fields[0],
							fields[1], Long.parseLong(fields[2]), Long
									.parseLong(fields[3]));
					semanticAnnotations.add(sia);

				} catch (Exception e) {

				}

			}
		} else {
			logger.info("old index entry!");

			for (String cUri : doc.getValues(LuceneWrapper.ATR_CONCEPTS)) {

				logger.info("curi: " + cUri);

				// split the cUri value to semanticAnnotation
				try {

					SemanticAnnotation sia = new SemanticAnnotation();
					sia.setUri(cUri);
					sia.setUserId("unknown");
					semanticAnnotations.add(sia);

				} catch (Exception e) {

				}

			}

		}
		String docOwner = doc.get(LuceneWrapper.ATR_DOCOWNER);

		return new LuceneIndexDocument(uri, title, text, url, time, resultType,
				semanticAnnotations, docOwner);
	}

	private class LuceneResultIterator implements Iterator<IndexDocument> {

		int counter = 0;

		public boolean hasNext() {
			return counter < length();
		}

		public IndexDocument next() {
			try {
				counter++;
				return doc((counter - 1));
			} catch (IOException io) {
				throw new RuntimeException(io);
			}
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

	}

	@Override
	public Iterator<IndexDocument> getIterator()
	{ return iterator(); }

}
