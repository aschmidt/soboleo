/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.peopletagging;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.RDF;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

public class RDFTaggedPersonsDatabase {

	private TripleStore tripleStore;
	private ValueFactory vf;
	protected HashMap<URI,RDFTaggedPerson> peopleCache = new HashMap<URI,RDFTaggedPerson>();
	
	public RDFTaggedPersonsDatabase(TripleStore tripleStore) {
		this.tripleStore = tripleStore;
		this.vf = tripleStore.getValueFactory();
	}
	
	public Set<RDFTaggedPerson> getTaggedPersons() throws IOException {
		Set<RDFTaggedPerson> toReturn = new HashSet<RDFTaggedPerson>();
		URI typeTaggedExternalPerson = vf.createURI(SoboleoNS.PEOPLE_EXTERNAL_TAGGED_PERSON.toString());
		URI rdfType = vf.createURI(RDF.TYPE.toString());
		List<Statement> statements = tripleStore.getStatementList(null, rdfType, typeTaggedExternalPerson);
		for (Statement currentStatement: statements) {
			URI currentURI = (URI) currentStatement.getSubject();
			toReturn.add(new RDFTaggedExternalPerson(currentURI,tripleStore));
		}
		//tagged user
		return toReturn; 
	}
	
	public RDFTaggedPerson getTaggedPerson(String uri) {
		URI personURI = vf.createURI(uri);
		return new RDFTaggedExternalPerson(personURI, tripleStore);
		//TODO tagged user
	}
	
	public RDFTaggedPerson getTaggedPersonForURL(String url) throws IOException {
		URI urlPredicate = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAG_URL.toString());
		Literal urlLiteral = vf.createLiteral(url);
		List<Statement> statements = tripleStore.getStatementList(null, urlPredicate, urlLiteral);
		if (statements.size() == 0) return null;
		else {
			URI tagURI = (URI) statements.get(0).getSubject();
			URI isTaggedWith = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAGGED_WITH.toString());
			statements =  tripleStore.getStatementList(null, isTaggedWith, tagURI);
			if (statements.size() == 0) return null;
			else return new RDFTaggedExternalPerson((URI) statements.get(0).getSubject(),tripleStore);
		}
	}
	
	public RDFTaggedPerson getTaggedPersonForEmail(String email) throws IOException {
		URI hasEmail = vf.createURI(SoboleoNS.PEOPLE_PERSON_EMAIL.toString());
		Literal emailLiteral = vf.createLiteral(email.toLowerCase());
		List<Statement> statements = tripleStore.getStatementList(null, hasEmail, emailLiteral);
		if (statements.size()==0) return null;
		else {
			Statement statement = statements.get(0);
			return new RDFTaggedExternalPerson((URI)statement.getSubject(),tripleStore);
			//TODO tagged user
		}
	}
	
	public RDFTaggedPerson createTaggedPerson(String email, String name) throws IOException {
		//TODO currently without any check if there's already a tagged person with this email and name
		URI personURI = tripleStore.getUniqueURI();
		RDFTaggedExternalPerson person = new RDFTaggedExternalPerson(personURI,tripleStore);
		person.createPerson();
		person.setEmail(email);
		person.setName(name);
		return person;
	}
	
	public RDFManualPersonTag getManualPersonTag(String uri) throws IOException {
		URI tagURI = vf.createURI(uri);
		return new RDFManualPersonTag(tagURI,tripleStore);
	}
	
	
}
