/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the TaggedPerson for one URL. Returns the TaggedPerson object
 * that has a tag with this url (or none, if no person has been tagged for 
 * this url).
 */
public class GetTaggedPersonByURL extends QueryEvent{

	private String url;
	
	public GetTaggedPersonByURL(String url) {
		this.url = url;
	}
	
	@SuppressWarnings("unused")
	private GetTaggedPersonByURL() {; }
	
	public String getURL() {
		return url;
	}
	
	
}
