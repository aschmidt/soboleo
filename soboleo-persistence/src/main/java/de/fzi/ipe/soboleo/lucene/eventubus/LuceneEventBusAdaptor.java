package de.fzi.ipe.soboleo.lucene.eventubus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.data.documentstorage.DocumentStorage;
import de.fzi.ipe.soboleo.data.documentstorage.DocumentStorageImpl;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.common.ShutdownCommand;
import de.fzi.ipe.soboleo.event.dialog.AddDialogTag;
import de.fzi.ipe.soboleo.event.dialog.RemoveDialogTag;
import de.fzi.ipe.soboleo.event.dialog.SetDialogContent;
import de.fzi.ipe.soboleo.event.dialog.SetDialogTitle;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocument;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocumentTag;
import de.fzi.ipe.soboleo.event.officedocument.RemoveOfficeDocument;
import de.fzi.ipe.soboleo.event.officedocument.RemoveOfficeDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.AddWebDocument;
import de.fzi.ipe.soboleo.event.webdocument.AddWebDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.RemoveWebDocument;
import de.fzi.ipe.soboleo.event.webdocument.RemoveWebDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.SetWebDocumentContent;
import de.fzi.ipe.soboleo.event.webdocument.SetWebDocumentTitle;
import de.fzi.ipe.soboleo.event.webdocument.SetWebDocumentURL;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.execution.content.ContentUtil;
import de.fzi.ipe.soboleo.execution.content.DocumentContentExtractor;
import de.fzi.ipe.soboleo.execution.content.Fetcher;
import de.fzi.ipe.soboleo.execution.content.FetcherImplementation;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchUtil;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.LuceneResultClientImpl;
import de.fzi.ipe.soboleo.lucene.SearchingLuceneWrapper;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.LuceneIndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetAllDocuments;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsForConcept;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetAllOfficeDocuments;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentByUri;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentInputStream;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetAllWebDocuments;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetSearchingLuceneWrapper;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetWebDocumentsForConcept;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetWebDocumentsSearchResult;

/**
 * Event bus adaptor that makes watches for added documents & tags adds these to
 * the lucene index. Also answer queries
 * 
 * @author valentin
 * 
 */
public class LuceneEventBusAdaptor implements EventBusEventProcessor,
		EventBusQueryProcessor {

	protected static final Logger logger = Logger
			.getLogger(LuceneEventBusAdaptor.class);

	private SearchingLuceneWrapper luceneWrapper;
	private EventBus eventBus;
	private ClientSideTaxonomy tax;

	private Fetcher fetcher;

	private DocumentStorage documentStorage = new DocumentStorageImpl();

	private String lastAddedDocumentKey;

	public LuceneEventBusAdaptor(File indexDir, File documentStorageDir,
			EventBus eventBus, ClientSideTaxonomy tax) throws IOException {
		if (!indexDir.exists())
			indexDir.mkdirs();

		logger.info("use index dir :" + indexDir.getAbsolutePath());

		luceneWrapper = new SearchingLuceneWrapper(indexDir.getAbsolutePath());
		this.eventBus = eventBus;
		this.tax = tax;

		// fetcher is only needed by lucene event bus!
		this.fetcher = new FetcherImplementation();

		// initialize the document storage
		this.documentStorage.initializeStorage(documentStorageDir
				.getAbsolutePath());
	}

	@Override
	public void receiveEvent(Event event) {
			
		try {
			if (event instanceof ShutdownCommand) {
				luceneWrapper.close();
			} else if (event instanceof SetDialogTitle) {
				SetDialogTitle setDialogTitle = (SetDialogTitle) event;
				IndexDocument indexDoc = luceneWrapper
						.getFullDocument(setDialogTitle.getDocURI());
				if (indexDoc == null) { // do not yet know this
					indexDoc = new LuceneIndexDocument(setDialogTitle
							.getDocURI(), setDialogTitle.getTitle(), "", "",
							System.currentTimeMillis(), ResultType.DIALOG,
							new HashSet<SemanticAnnotation>(), indexDoc.getDocumentOwner());
				}
				indexDoc = new LuceneIndexDocument(setDialogTitle.getDocURI(),
						setDialogTitle.getTitle(), indexDoc.getContent(),
						indexDoc.getURL(), System.currentTimeMillis(),
						ResultType.DIALOG, new HashSet<SemanticAnnotation>(), indexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof SetDialogContent) {
				SetDialogContent setDialogContent = (SetDialogContent) event;
				IndexDocument indexDoc = luceneWrapper
						.getFullDocument(setDialogContent.getDocURI());
				if (indexDoc == null) { // do not yet know this
					indexDoc = new LuceneIndexDocument(setDialogContent
							.getDocURI(), "", setDialogContent.getContent(),
							"", System.currentTimeMillis(), ResultType.DIALOG,
							new HashSet<SemanticAnnotation>(), indexDoc.getDocumentOwner());
				}
				indexDoc = new LuceneIndexDocument(
						setDialogContent.getDocURI(), indexDoc.getTitle(),
						setDialogContent.getContent(), indexDoc.getURL(),
						System.currentTimeMillis(), ResultType.DIALOG,
						new HashSet<SemanticAnnotation>(), indexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof AddDialogTag) {
				AddDialogTag addTag = (AddDialogTag) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(addTag.getDocumentURI());
				Set<SemanticAnnotation> oldConceptURIs = oldIndexDoc.getSemanticAnnotations();
				Set<SemanticAnnotation> conceptURIs = new HashSet<SemanticAnnotation>(oldConceptURIs);
				conceptURIs.add(addTag.getSia());
				IndexDocument indexDoc = new LuceneIndexDocument(addTag
						.getDocumentURI(), oldIndexDoc.getTitle(), oldIndexDoc
						.getContent(), oldIndexDoc.getURL(), System
						.currentTimeMillis(), ResultType.DIALOG, conceptURIs, oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof RemoveDialogTag) {
				RemoveDialogTag removeTag = (RemoveDialogTag) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(removeTag.getDocURI());
				Set<SemanticAnnotation> oldConceptURIs = oldIndexDoc.getSemanticAnnotations();
				Set<SemanticAnnotation> conceptURIs = new HashSet<SemanticAnnotation>(oldConceptURIs);

				conceptURIs=this.removeDeletedAnnotation(conceptURIs, removeTag.getConceptURI());
				//conceptURIs.remove(removeTag.getConceptURI());
				
				IndexDocument indexDoc = new LuceneIndexDocument(removeTag
						.getDocURI(), oldIndexDoc.getTitle(), oldIndexDoc
						.getContent(), oldIndexDoc.getURL(), System
						.currentTimeMillis(), ResultType.DIALOG, conceptURIs, oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof AddWebDocument) {
				AddWebDocument addDocument = (AddWebDocument) event;
				fetcher.fetch(addDocument.getURI(), addDocument.getURL(),
						eventBus);
				IndexDocument indexDoc = new LuceneIndexDocument(addDocument
						.getURI(), addDocument.getTitle(), "", addDocument
						.getURL(), System.currentTimeMillis(),
						ResultType.WEB_DOCUMENT, new HashSet<SemanticAnnotation>(),addDocument.getDocOwner() );
				luceneWrapper.index(indexDoc);
			} else if (event instanceof RemoveWebDocument) {
				RemoveWebDocument removeDocument = (RemoveWebDocument) event;
				luceneWrapper.deleteFromIndex(removeDocument.getDocURI());
			} else if (event instanceof SetWebDocumentTitle) {
				SetWebDocumentTitle setDocumentTitle = (SetWebDocumentTitle) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(setDocumentTitle.getDocURI());
				IndexDocument indexDoc = new LuceneIndexDocument(
						setDocumentTitle.getDocURI(), setDocumentTitle
								.getTitle(), oldIndexDoc.getContent(),
						oldIndexDoc.getURL(), System.currentTimeMillis(),
						ResultType.WEB_DOCUMENT, oldIndexDoc.getSemanticAnnotations(), oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof SetWebDocumentURL) {
				SetWebDocumentURL setDocumentURL = (SetWebDocumentURL) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(setDocumentURL.getDocURI());
				IndexDocument indexDoc = new LuceneIndexDocument(setDocumentURL
						.getDocURI(), oldIndexDoc.getTitle(), oldIndexDoc
						.getContent(), setDocumentURL.getURL(), System
						.currentTimeMillis(), ResultType.WEB_DOCUMENT,
						oldIndexDoc.getSemanticAnnotations(), oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof SetWebDocumentContent) {
				SetWebDocumentContent setDocumentContent = (SetWebDocumentContent) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(setDocumentContent.getDocURI());
				IndexDocument indexDoc = new LuceneIndexDocument(
						setDocumentContent.getDocURI(), oldIndexDoc.getTitle(),
						setDocumentContent.getContent(), oldIndexDoc.getURL(),
						System.currentTimeMillis(), ResultType.WEB_DOCUMENT,
						oldIndexDoc.getSemanticAnnotations(), oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof AddWebDocumentTag) {
				AddWebDocumentTag addTag = (AddWebDocumentTag) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(addTag.getDocumentURI());
				Set<SemanticAnnotation> oldConceptURIs = oldIndexDoc.getSemanticAnnotations();
				Set<SemanticAnnotation> conceptURIs = new HashSet<SemanticAnnotation>(oldConceptURIs);
				conceptURIs.add(addTag.getSia());
				IndexDocument indexDoc = new LuceneIndexDocument(addTag
						.getDocumentURI(), oldIndexDoc.getTitle(), oldIndexDoc
						.getContent(), oldIndexDoc.getURL(), System
						.currentTimeMillis(), ResultType.WEB_DOCUMENT,
						conceptURIs, oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			} else if (event instanceof RemoveWebDocumentTag) {
				RemoveWebDocumentTag removeTag = (RemoveWebDocumentTag) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(removeTag.getDocURI());
				Set<SemanticAnnotation> oldConceptURIs = oldIndexDoc.getSemanticAnnotations();
				Set<SemanticAnnotation> conceptURIs = new HashSet<SemanticAnnotation>(oldConceptURIs);
				//conceptURIs.remove(removeTag.getConceptURI());
				
				conceptURIs=this.removeDeletedAnnotation(conceptURIs, removeTag.getConceptURI());

				
				IndexDocument indexDoc = new LuceneIndexDocument(removeTag
						.getDocURI(), oldIndexDoc.getTitle(), oldIndexDoc
						.getContent(), oldIndexDoc.getURL(), System
						.currentTimeMillis(), ResultType.WEB_DOCUMENT,
						conceptURIs, oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);
			}
			// adding actions for officedocument
			else if (event instanceof AddOfficeDocument) {
				// add the add actions
				AddOfficeDocument addOfficeDocument = (AddOfficeDocument) event;
				logger.info("add office document : "
						+ addOfficeDocument.getDocumentType());

				logger.info("owner of doc is :" +addOfficeDocument.getDocOwner());
				
				// create a key for this document
				String documentKey = "doc" + System.currentTimeMillis();

				// cache the is
				ContentUtil
						.cacheInputStream(addOfficeDocument.getInputStream());

				// try to extract text content
				// if this fails , ioe error comes back
				String textContent = DocumentContentExtractor
						.getExtractContentFromFile(ContentUtil
								.getCachedInputStream(), addOfficeDocument
								.getDocumentType());

				// try to save the file
				boolean status = this.documentStorage.addDocument(documentKey,
						addOfficeDocument.getDocumentType(), ContentUtil
								.getCachedInputStream());

				if (!status) {
					throw new IOException();
				}

				// add the document to index
				IndexDocument indexDoc = new LuceneIndexDocument(documentKey,
						addOfficeDocument.getTitle(), textContent,
						addOfficeDocument.getDocumentType().toString(), System
								.currentTimeMillis(),
						ResultType.OFFICE_DOCUMENT, new HashSet<SemanticAnnotation>(),addOfficeDocument.getDocOwner());
				luceneWrapper.index(indexDoc);

				lastAddedDocumentKey = documentKey;
			} else if (event instanceof AddOfficeDocumentTag) {
				// add the add office document tag action

				logger.info("AddOfficeDocumentTag..");
				
				AddOfficeDocumentTag addTag = (AddOfficeDocumentTag) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(addTag.getDocumentURI());
				Set<SemanticAnnotation> oldConceptURIs = oldIndexDoc.getSemanticAnnotations();
				
				logger.info("size of concept uris old doc:" +oldConceptURIs.size());
				
				Set<SemanticAnnotation> conceptURIs = new HashSet<SemanticAnnotation>(oldConceptURIs);
				conceptURIs.add(addTag.getSia());
				
				logger.info("size of concept uris:" +conceptURIs.size());
				
				IndexDocument indexDoc = new LuceneIndexDocument(addTag
						.getDocumentURI(), oldIndexDoc.getTitle(), oldIndexDoc
						.getContent(), oldIndexDoc.getURL(), System
						.currentTimeMillis(), ResultType.OFFICE_DOCUMENT,
						conceptURIs, oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);

			} else if (event instanceof RemoveOfficeDocumentTag) {
				// remove a tag at office document

				RemoveOfficeDocumentTag removeTag = (RemoveOfficeDocumentTag) event;
				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(removeTag.getDocURI());
				Set<SemanticAnnotation> oldConceptURIs = oldIndexDoc.getSemanticAnnotations();


				Set<SemanticAnnotation> conceptURIs = new HashSet<SemanticAnnotation>(oldConceptURIs);

				// updated remove operation: needs to search the correct one
				conceptURIs=this.removeDeletedAnnotation(conceptURIs, removeTag.getConceptURI());
				//conceptURIs.remove(removeTag.getConceptURI());

				IndexDocument indexDoc = new LuceneIndexDocument(removeTag
						.getDocURI(), oldIndexDoc.getTitle(), oldIndexDoc
						.getContent(), oldIndexDoc.getURL(), System
						.currentTimeMillis(), ResultType.OFFICE_DOCUMENT,
						conceptURIs,oldIndexDoc.getDocumentOwner());
				luceneWrapper.index(indexDoc);

			} else if (event instanceof RemoveOfficeDocument) {

				// remove office document
				RemoveOfficeDocument removeDocument = (RemoveOfficeDocument) event;

				IndexDocument oldIndexDoc = luceneWrapper
						.getFullDocument(removeDocument.getDocURI());

				logger.info("document to remove: type : "
						+ oldIndexDoc.getURL());

				luceneWrapper.deleteFromIndex(removeDocument.getDocURI());

				// remove document from storage
				boolean status = this.documentStorage.removeDocument(
						removeDocument.getDocURI(), oldIndexDoc.getURL());
				logger.info("document deleted: " + status);
			}

		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	
	private Set<SemanticAnnotation> removeDeletedAnnotation(Set<SemanticAnnotation> sias, String uri)
	{
		// removes the sia with same uri such as the given one
		for (SemanticAnnotation sia:sias)
		{
			if (sia.getUri().equals(uri))
			{
				sias.remove(sia);
				break;
			}
		}
		
		return sias;
	}
	
	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}

	@Override
	public Object process(QueryEvent query) {
		if (query instanceof GetSearchingLuceneWrapper) {
			return luceneWrapper;
		} else if (query instanceof GetAllWebDocuments) {
			GetAllWebDocuments getAllDocuments = (GetAllWebDocuments) query;
			try {
				if (getAllDocuments.getResultType() != null)
					return new LuceneResultClientImpl(luceneWrapper
							.getAllDocuments(getAllDocuments.getResultType()));
				else
					return new LuceneResultClientImpl(luceneWrapper
							.getAllDocuments());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else if (query instanceof GetWebDocumentsForConcept) {
			GetWebDocumentsForConcept getDocumentsForConcept = (GetWebDocumentsForConcept) query;
			try {
				return new LuceneResultClientImpl(luceneWrapper
						.getWebDocumentsForConcept(getDocumentsForConcept
								.getConceptURI(), getDocumentsForConcept
								.getSubconceptURIs(), getDocumentsForConcept
								.getResultType()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else if (query instanceof GetWebDocumentsSearchResult) {
			try {
				GetWebDocumentsSearchResult getSearchResult = (GetWebDocumentsSearchResult) query;

				Set<String> mustWithSubconceptURIs = new HashSet<String>();
				mustWithSubconceptURIs.addAll(getSearchResult
						.getMustConceptURIs());
				mustWithSubconceptURIs.addAll(getSearchResult
						.getMustSubconceptURIs());
				LuceneResult result = luceneWrapper.searchForContent(
						getSearchResult.getQuery(), getSearchResult
								.getResultType(), getSearchResult
								.getExtractedConceptURIs(), getSearchResult
								.getExtractedSubconceptURIs(),
						mustWithSubconceptURIs);
				result = new LuceneResultClientImpl(result);
				List<String> relaxations = SearchUtil.createRelaxations(
						getSearchResult.getExtractedConceptURIs(), result, tax);
				List<String> refinements = SearchUtil.createRefinements(
						getSearchResult.getExtractedConceptURIs(), result, tax);
				SearchResult resultBean = new SearchResult(result,
						getSearchResult.getExtractedConceptURIs());
				resultBean.setQuery(getSearchResult.getQuery());
				resultBean.setRelaxations(relaxations);
				resultBean.setRequiredConceptURIs(getSearchResult
						.getMustConceptURIs());
				resultBean.setRefinements(refinements);
				return resultBean;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		// methods for officedocuments
		else if (query instanceof GetAllOfficeDocuments) {
			logger.info("query: getAllOfficeDocuments");
			GetAllOfficeDocuments getAllOfficeDocuments = (GetAllOfficeDocuments) query;

			try {
				if (getAllOfficeDocuments.getResultType() != null) {
					return new LuceneResultClientImpl(luceneWrapper
							.getAllDocuments(getAllOfficeDocuments
									.getResultType()));
				} else {
					return new LuceneResultClientImpl(luceneWrapper
							.getAllDocuments());
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		// search for office document by uri
		else if (query instanceof GetOfficeDocumentByUri) {
			GetOfficeDocumentByUri getByUri = (GetOfficeDocumentByUri) query;

			try {
				return new LuceneResultClientImpl(luceneWrapper
						.getOfficeDocumentByUri(getByUri.getUri()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		// return the input stream of an office document
		else if (query instanceof GetOfficeDocumentInputStream) {
			GetOfficeDocumentInputStream gis = (GetOfficeDocumentInputStream) query;

			InputStream is = this.documentStorage.loadDocument(gis.getUri(),
					gis.getDocumentType());

			if (is == null) {
				throw new RuntimeException("file not found");
			}
			return is;
		}
		// methods to search for webdocuments AND officedocuments
		else if (query instanceof GetAllDocuments) {
			try {
				// returns all stored documents
				logger.info("query: GetAllDocuments");

				return new LuceneResultClientImpl(luceneWrapper
						.getAllDocuments());

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		} else if (query instanceof GetDocumentsForConcept) {

			GetDocumentsForConcept getDocumentsForConcept = (GetDocumentsForConcept) query;
			try {

				try {
					logger
							.info("subconcepts: "
									+ getDocumentsForConcept
											.getSubconceptURIs().size());
				} catch (Exception e) {
					logger.error(e);
				}
				return new LuceneResultClientImpl(luceneWrapper
						.getAllDocumentsForConcept(getDocumentsForConcept
								.getConceptURI(), getDocumentsForConcept
								.getSubconceptURIs()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else if (query instanceof GetDocumentsSearchResult) {

			try {
				GetDocumentsSearchResult getSearchResult = (GetDocumentsSearchResult) query;

				Set<String> mustWithSubconceptURIs = new HashSet<String>();

				try {
					mustWithSubconceptURIs.addAll(getSearchResult
							.getMustConceptURIs());
				} catch (Exception e) {
					logger.info("no must concept uris provided");
				}

				try {
					mustWithSubconceptURIs.addAll(getSearchResult
							.getMustSubconceptURIs());
				} catch (Exception e) {
					logger.info("no must sub-concept uris provided");
				}

				LuceneResult result = luceneWrapper
						.searchForAllDocumentTypesContent(getSearchResult
								.getQuery(), getSearchResult
								.getExtractedConceptURIs(), getSearchResult
								.getExtractedSubconceptURIs(),
								mustWithSubconceptURIs, getSearchResult.getDocOwner(), getSearchResult.getAnnotationOwner());

				logger.info("results: " + result.length());

				result = new LuceneResultClientImpl(result);

				SearchResult resultBean = new SearchResult(result,
						getSearchResult.getExtractedConceptURIs());
				resultBean.setQuery(getSearchResult.getQuery());

				if (getSearchResult.getExtractedConceptURIs() != null) {
					try {
						List<String> relaxations = SearchUtil
								.createRelaxations(getSearchResult
										.getExtractedConceptURIs(), result, tax);
						resultBean.setRelaxations(relaxations);
					} catch (Exception e) {
						logger.error("failed setting relaxations");
					}
				}

				if (getSearchResult.getExtractedConceptURIs() != null) {
					try {
						List<String> refinements = SearchUtil
								.createRefinements(getSearchResult
										.getExtractedConceptURIs(), result, tax);
						resultBean.setRefinements(refinements);
					} catch (Exception e) {
						logger.error("failed setting refinements");
					}
				}

				if (getSearchResult.getMustConceptURIs() != null) {
					resultBean.setRequiredConceptURIs(getSearchResult
							.getMustConceptURIs());
				}

				return resultBean;
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}

		return null;
	}

	public String getLastAddedDocumentKey() {
		return lastAddedDocumentKey;
	}

	public void setLastAddedDocumentKey(String lastAddedDocumentKey) {
		this.lastAddedDocumentKey = lastAddedDocumentKey;
	}

}
