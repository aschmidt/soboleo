 
package de.fzi.ipe.soboleo.queries.rdf.dialog;


/**
 * @author FZI - Information Process Engineering by zach
 * Created on 20.08.2009 
 * Retrieves the TaggedPerson for the URI. Returns the TaggedPerson object
 * that has a tag with this url (or none, if no person has been tagged for 
 * this url).
 */
public class GetConceptDialog extends GetDialog{

	/**
	 * @param uri
	 */
	public GetConceptDialog(String uri) {
		super(uri);
	}
	
	@SuppressWarnings("unused")
	private GetConceptDialog(){;}
}
