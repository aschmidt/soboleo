/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.dialog;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;
import static de.fzi.ipe.soboleo.beans.ontology.SoboleoNS.DIALOG;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;

import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFDocumentDatabase;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFTag;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

/**
 * Represents the set of all documents and annotations in the context of 
 * the document.eventBusAdapter. This class is intended to be used by classes
 * in the document package - all other classes should use events, queries and commands
 * to interact with the DocumentSet.
 */
public class RDFDialogDatabase extends RDFDocumentDatabase<RDFDialog>{
	
	public RDFDialogDatabase(TripleStore tripleStore) {
		super(tripleStore);
	}
	
	public synchronized RDFDialog getDocument(URI documentURI) throws IOException {
		RDFDialog doc = documentCache.get(documentURI);
		if (doc == null) {
			URI webdocumentDialogURI = vf.createURI(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
			URI conceptDialogURI = vf.createURI(SoboleoNS.DIALOG_CONCEPT.toString());
			URI typeURI = vf.createURI(TYPE.toString());
			if (tripleStore.hasStatement(documentURI, typeURI, webdocumentDialogURI)) {
				doc = new RDFWebDocumentDialog(documentURI,tripleStore);
				documentCache.put(documentURI, doc);
			}
			else if (tripleStore.hasStatement(documentURI, typeURI, conceptDialogURI)) {
				doc = new RDFConceptDialog(documentURI,tripleStore);
				documentCache.put(documentURI, doc);
			}
		}
		return doc;
	}
	
	public synchronized void deleteDocument(String uri) throws IOException {
		URI docURI = vf.createURI(uri);
		RDFDialog doc = getDocument(docURI);
		if (doc != null) { //this really is a document
			Set<RDFTag> tags = doc.getTags();
			for (RDFTag tag: tags) tag.delete();
			tripleStore.removeTriples(docURI, null, null, null);
			documentCache.remove(docURI);
		}
	}
	
	
	public synchronized RDFDialog createDocument(String type) throws IOException{
		if(type.equals(SoboleoNS.DIALOG_CONCEPT.toString())) return createConceptDialog();
		else if (type.equals(SoboleoNS.DIALOG_WEBDOCUMENT.toString())) return createWebDocumentDialog();
		else throw new IllegalArgumentException("Type must be either SoboleoNS.DIALOG_CONCEPT or SoboleoNS.DIALOG_WEBDOCUMENT");
	}
	
	private synchronized RDFConceptDialog createConceptDialog() throws IOException{
		URI docURI = tripleStore.getUniqueURI();
		RDFConceptDialog doc = new RDFConceptDialog(docURI, tripleStore);
		doc.createDocument();
		documentCache.put(docURI, doc);
		return doc;
	}
	
	
	private synchronized RDFWebDocumentDialog createWebDocumentDialog() throws IOException{
		URI docURI = tripleStore.getUniqueURI();
		RDFWebDocumentDialog doc = new RDFWebDocumentDialog(docURI, tripleStore);
		doc.createDocument();
		documentCache.put(docURI, doc);
		return doc;
	}
	
	public synchronized Set<RDFDialog> getAllDocuments() throws IOException {
		return super.getAllDocuments(DIALOG.toString());
	}
	
	public synchronized Set<RDFDialog> getDialogsByAbout(String aboutURI) throws IOException {
		Set<RDFDialog> toReturn = new HashSet<RDFDialog>();
		URI isAbout = vf.createURI(SoboleoNS.IS_ABOUT.toString());
		URI about = vf.createURI(aboutURI);
		List<Statement> statements = tripleStore.getStatementList(null, isAbout, about);
		for (Statement s: statements) toReturn.add(getDocument((URI) s.getSubject()));
		return toReturn;
	}
	
	public synchronized Set<RDFDialog> getDialogsByType(String type) throws IOException{
		Set<RDFDialog> toReturn = new HashSet<RDFDialog>();
		URI typeURI = vf.createURI(TYPE.toString());
		URI typeClass = vf.createURI(type);
		List<Statement> statements = tripleStore.getStatementList(null, typeURI, typeClass);
		for (Statement s: statements) toReturn.add(getDocument((URI) s.getSubject()));
		return toReturn;
	}
	
	
	public synchronized Set<RDFDialog> getDialogsByAbout(String aboutURI, String type) throws IOException{
		Set<RDFDialog> toReturn = new HashSet<RDFDialog>();
		String bindingName = "dialog";
		String query = "SELECT ?" +bindingName +
        " WHERE { " +
            "?" + bindingName +" <" + TYPE.toString() +"> <" +type + ">. " +
            "?" + bindingName +" <" + SoboleoNS.IS_ABOUT.toString() +"> <" +aboutURI +"> ." +
        "}";
		Set<Value> result = tripleStore.executeSelectSPARQLQueryForParameter(query, bindingName);
		for(Value docURI : result) toReturn.add(getDocument((URI) docURI));
		return toReturn;
	}
}
