/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.dialog;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the TaggedPerson for the URI. Returns the TaggedPerson object
 * that has a tag with this url (or none, if no person has been tagged for 
 * this url).
 */
public class GetDialog extends QueryEvent{

	private String uri;
	
	public GetDialog(String uri) {
		this.uri = uri;
	}
	
	protected GetDialog() {; }
	
	public String getURI() {
		return uri;
	}
}
