/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.dialog;


/**
 * Retrieves the TaggedPerson for the URI. Returns the TaggedPerson object
 * that has a tag with this url (or none, if no person has been tagged for 
 * this url).
 */
public class GetWebDocumentDialog extends GetDialog{

	public GetWebDocumentDialog(String uri) {
		super(uri);
	}
	
	@SuppressWarnings("unused")
	private GetWebDocumentDialog(){;}
}
