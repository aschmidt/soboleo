/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.dialog;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * 
 */
public class GetDialogsForWebDocument extends QueryEvent{
	
	private String webdocumentURI;
	
	@SuppressWarnings("unused") //needed for GWT
	private GetDialogsForWebDocument() {}
	
	public GetDialogsForWebDocument(String webdocumentURI) {
		this.webdocumentURI = webdocumentURI;
	}
	
	public String getWebDocumentURI(){
		return webdocumentURI; 
	}
}
