/*
 * FZI - Information Process Engineering 
 * Created on 14.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.content;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.LinkedBlockingQueue;

import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.event.webdocument.SetWebDocumentContent;
import de.fzi.ipe.soboleo.eventbus.EventBus;

public class FetcherImplementation extends Thread implements Fetcher {

	static class FetchTask {
		String URI;
		String URL;
		EventBus eventBus;
		
		FetchTask(String URI, String URL, EventBus eventBus) {
			this.URI = URI;
			this.URL = URL;
			this.eventBus = eventBus;
		}
	}
	

	private LinkedBlockingQueue<FetchTask> taskQueue = new LinkedBlockingQueue<FetchTask>();
	private boolean stopped = false;
	
	@Override
	public void fetch(String documentURI, String documentURL, EventBus eventBus) {
		taskQueue.add(new FetchTask(documentURI,documentURL,eventBus));
	}

	public FetcherImplementation() {
		this.start();
	}
	
	public synchronized void stopFetcher() {
		stopped = true;
		this.interrupt();
	}
	
	public void run() {
		while (!stopped) {
			try {
				FetchTask task = taskQueue.take();
				process(task);
			} catch(InterruptedException ie) { ; 
			} catch(Throwable e) {
				e.printStackTrace();
			}
			
		}
		
	}

	private void process(FetchTask task) {
		String content = fetchDoc(task.URL);
		if (content != null) {
			SetWebDocumentContent docContentCommand = new SetWebDocumentContent(task.URI,content);
			task.eventBus.executeCommand(docContentCommand, EventSenderCredentials.SERVER);
		}
	}
	
	private String fetchDoc(String URLstring) {
		try {
			URL url = new URL(URLstring);
			return DocumentContentExtractor.getDownloadDocumentAndExtractContent(url);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return null; 
	}
}
