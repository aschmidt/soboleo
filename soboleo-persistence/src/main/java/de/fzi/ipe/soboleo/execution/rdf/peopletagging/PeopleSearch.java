/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.peopletagging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.AggregatedConceptTags;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;

public class PeopleSearch {

	public static final float RESULT_THRESHOLD = 0.2f;
	
	public static List<TaggedPerson> findPersons(String searchString, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		WeightedConceptCloud searchCloud = makeConceptCloud(searchString, tax);
		Set<WeightedConceptCloud> personClouds = makeConceptClouds(database, tax);
		return findBestMatches(searchCloud, personClouds);
	}
	
	public static List<TaggedPerson> findPersons(String searchString, RDFTaggedPerson asker, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		WeightedConceptCloud searchCloud = makeConceptCloud(searchString, asker.getClientSideTaggedPerson(), tax);
		Set<WeightedConceptCloud> personClouds = makeConceptClouds(database, tax);
		return findBestMatches(searchCloud, personClouds);
	}
	
	public static List<TaggedPerson> findPersons(RDFTaggedPerson taggedPerson, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		WeightedConceptCloud searchCloud = makeConceptCloud(taggedPerson.getClientSideTaggedPerson(), tax);
		Set<WeightedConceptCloud> personClouds = makeConceptClouds(database, tax);
		return findBestMatches(searchCloud, personClouds);
	}
	/**
	 * Returns a List of all people surname or name as the search String
	 * @param searchString 
	 * @param asker 
	 * @param database
	 * @param tax
	 * @return List<TaggedPerson> 
	 * @throws IOException
	 */
	public static List<TaggedPerson> findPersonsByName(String searchString, RDFTaggedPerson asker, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		Set<RDFTaggedPerson> allPersons = database.getTaggedPersons();
		List<TaggedPerson> foundPersons = new ArrayList<TaggedPerson>();
		for(RDFTaggedPerson person : allPersons)
		{
		    if(person.getName().toLowerCase().contains(searchString.toLowerCase()))
		    {
		    	if(!person.getName().contentEquals("Anonymous"))
		    	{
		    		foundPersons.add(person.getClientSideTaggedPerson());
		    	}
		    }
		}
		return foundPersons;
	}
	/**
	 * Returns a List of all people surname or name as the search String
	 * @param searchString 
	 * @param database
	 * @param tax
	 * @return List<TaggedPerson> 
	 * @throws IOException
	 */
	public static List<TaggedPerson> findPersonsByName(String searchString, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		Set<RDFTaggedPerson> allPersons = database.getTaggedPersons();
		List<TaggedPerson> foundPersons = new ArrayList<TaggedPerson>();
		for(RDFTaggedPerson person : allPersons)
		{
		    if(person.getName().toLowerCase().contains(searchString.toLowerCase()))
		    {
		    	if(!person.getName().contentEquals("Anonymous"))
		    	{
		    		foundPersons.add(person.getClientSideTaggedPerson());
		    	}
		    }
		}
		return foundPersons;
	}
	/**
	 * Returns a List of all people surname or name as the search String and if they have a toppic
	 * as the search string.
	 * @param searchString 
	 * @param database
	 * @param tax
	 * @return List<TaggedPerson> 
	 * @throws IOException
	 */
	public static List<TaggedPerson> findPersonsByNameAndTopic(String searchString, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		Set<RDFTaggedPerson> allPersons = database.getTaggedPersons();
		List<TaggedPerson> foundPersons = new ArrayList<TaggedPerson>();
		for(RDFTaggedPerson person : allPersons)
		{
		    if(person.getName().toLowerCase().contains(searchString.toLowerCase()))
		    {
		    	if(!person.getName().contentEquals("Anonymous"))
		    	{
		    		foundPersons.add(person.getClientSideTaggedPerson());
		    	}
		    }
		}
		List<TaggedPerson> personsToAdd = new ArrayList<TaggedPerson>();
		
		for(TaggedPerson personTagged: findPersons(searchString,database,tax))
		{
			if(!(foundPersons.contains(personTagged)))
			{
				personsToAdd.add(personTagged);
			}
		}
		foundPersons.addAll(personsToAdd);
		return foundPersons;
	}
	/**
	 * Returns a List of all people surname or name as the search String and if they have a toppic
	 * as the search string.
	 * @param searchString 
	 * @param asker 
	 * @param database
	 * @param tax
	 * @return List<TaggedPerson> 
	 * @throws IOException
	 */
	public static List<TaggedPerson> findPersonsByNameAndTopic(String searchString, RDFTaggedPerson asker, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		Set<RDFTaggedPerson> allPersons = database.getTaggedPersons();
		List<TaggedPerson> foundPersons = new ArrayList<TaggedPerson>();
		//Find Persons by name
		for(RDFTaggedPerson person : allPersons)
		{
		    if(person.getName().toLowerCase().contains(searchString.toLowerCase()))
		    {
		    	if(!person.getName().contentEquals("Anonymous"))
		    	{
		    		foundPersons.add(person.getClientSideTaggedPerson());
		    	}
		    }
		}
		List<TaggedPerson> personsToAdd = new ArrayList<TaggedPerson>();
		//Find persons by Toppic
		for(TaggedPerson personTagged: findPersons(searchString,asker,database,tax))
		{
			if(!(foundPersons.contains(personTagged)))
			{
				personsToAdd.add(personTagged);
			}
		}
		//merge the two lists to one
		foundPersons.addAll(personsToAdd);
		return foundPersons;
	}
	
	public static List<TaggedPerson> findPersonsForConcept(RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax, String... conceptIDs) throws IOException {
		  WeightedConceptCloud searchCloud = new WeightedConceptCloud();
		  for(String conceptID : conceptIDs)  searchCloud.addConcept(conceptID, 1f);
		  Set<WeightedConceptCloud> personClouds = makeConceptClouds(database, tax);
		  return findBestMatches(searchCloud, personClouds);
	}
	
	public static List<TaggedPerson> findPersonsForConcept(RDFTaggedPerson asker, RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax, String... conceptIDs) throws IOException {
		  WeightedConceptCloud searchCloud = new WeightedConceptCloud(asker.getClientSideTaggedPerson());
		  for(String conceptID : conceptIDs)  searchCloud.addConcept(conceptID, 1f);
		  Set<WeightedConceptCloud> personClouds = makeConceptClouds(database, tax);
		  return findBestMatches(searchCloud, personClouds);
	}
	
	public static List<TaggedPerson> findBestMatches(WeightedConceptCloud conceptCloud, Collection<WeightedConceptCloud> otherClouds) {
		ArrayList<WeightedConceptCloudContainer> cloudContainers = new ArrayList<WeightedConceptCloudContainer>(otherClouds.size());
		for (WeightedConceptCloud currentCloud: otherClouds) {
			if(conceptCloud.getTaggedPerson() != null && currentCloud.getTaggedPerson().getURI().equals(conceptCloud.getTaggedPerson().getURI())){
				continue; //thats the person asking the question himself
			}
			else {
				float compareValue = conceptCloud.compare(currentCloud);
				cloudContainers.add(new WeightedConceptCloudContainer(compareValue,currentCloud));
			}
		}
		Collections.sort(cloudContainers);
		ArrayList<TaggedPerson> toReturn = new ArrayList<TaggedPerson>(cloudContainers.size());
		for (WeightedConceptCloudContainer container: cloudContainers) {
			if (container.compareValue>RESULT_THRESHOLD) toReturn.add(container.conceptCloud.getTaggedPerson());
		}
		return toReturn;
	}
	
	public static Set<WeightedConceptCloud> makeConceptClouds(RDFTaggedPersonsDatabase database, ClientSideTaxonomy tax) throws IOException {
		Set<WeightedConceptCloud> toReturn = new HashSet<WeightedConceptCloud>();
		Set<RDFTaggedPerson> taggedPersons = database.getTaggedPersons();
		for (RDFTaggedPerson person: taggedPersons) {
			toReturn.add(makeConceptCloud(person.getClientSideTaggedPerson(),tax));
		}
		return toReturn;
	}
	
	public static WeightedConceptCloud makeConceptCloud(TaggedPerson taggedPerson, ClientSideTaxonomy tax) {
		WeightedConceptCloud conceptCloud = new WeightedConceptCloud(taggedPerson);
		Collection<AggregatedConceptTags> aggregatedTags = taggedPerson.getAggregatedTags();
		for (AggregatedConceptTags tag: aggregatedTags) {
			conceptCloud.addConcept(tag.getConceptID(), tag.getWeight());
		}
		extendWithSuperconcepts(conceptCloud, tax);
		return conceptCloud;
	}

	public static WeightedConceptCloud makeConceptCloud(String searchString, TaggedPerson asker, ClientSideTaxonomy tax) {
		WeightedConceptCloud conceptCloud = new WeightedConceptCloud(asker);
		Set<String> extractedConceptURIs = tax.getExtractedConceptURIs(searchString);
		for (String conceptURI: extractedConceptURIs) {
			conceptCloud.addConcept(conceptURI, 1f);
		}
//		extendWithSuperconcepts(conceptCloud, tax);
		return conceptCloud;
	}
	
	public static WeightedConceptCloud makeConceptCloud(String searchString, ClientSideTaxonomy tax) {
		WeightedConceptCloud conceptCloud = new WeightedConceptCloud();
		Set<String> extractedConceptURIs = tax.getExtractedConceptURIs(searchString);
		for (String conceptURI: extractedConceptURIs) {
			conceptCloud.addConcept(conceptURI, 1f);
		}
//		extendWithSuperconcepts(conceptCloud, tax);
		return conceptCloud;
	}
	
	private static void extendWithSuperconcepts(WeightedConceptCloud cloud, ClientSideTaxonomy tax) {
		HashSet<String> conceptURIs = new HashSet<String>();
		conceptURIs.addAll(cloud.getConcepts()); //copy necessary, otherwise ConcurrentModificationException
		for (String conceptURI: conceptURIs) {
			float value = cloud.getValue(conceptURI);
			Set<String> allSuperconceptURIs = tax.getAllSuperconceptURIs(conceptURI);
			for (String superconceptURI:allSuperconceptURIs) {
				cloud.addConcept(superconceptURI, value / 3f);
			}
		}
	}
	
	
	private static class WeightedConceptCloudContainer implements Comparable<WeightedConceptCloudContainer>{
		float compareValue;
		WeightedConceptCloud conceptCloud;
		
		public WeightedConceptCloudContainer(float compareValue, WeightedConceptCloud conceptCloud) {
			this.compareValue = compareValue;
			this.conceptCloud = conceptCloud;
		}

		@Override
		public int compareTo(WeightedConceptCloudContainer other) {
			return (int) Math.round((other.compareValue *1000) - (compareValue*1000));
		}
		
	}
	
	
}
