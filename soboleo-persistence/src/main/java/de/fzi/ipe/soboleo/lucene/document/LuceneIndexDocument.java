package de.fzi.ipe.soboleo.lucene.document;

import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;

public class LuceneIndexDocument implements IndexDocument, IsSerializable{
	
	private Set<SemanticAnnotation> semanticAnnotations;
	private String title;
	private String content; 
	private String url;
	private String uri;
	private Long date;
	private ResultType type;
	private String documentOwner;
	
	public LuceneIndexDocument()
	{
		;
	}
	
	public LuceneIndexDocument(String uri,String title, String content,String url,long date,ResultType resultType, Set<SemanticAnnotation> semanticAnnotations, String documentOwner) {
		this.uri = uri;
		this.title = title;
		this.content = content;
		this.url = url;
		//this.conceptUris = new HashSet<String>();
		//this.conceptUris.addAll(conceptUris);
		this.semanticAnnotations = semanticAnnotations;
		this.date = date;
		this.type = resultType;
		this.documentOwner=documentOwner;
	}
	
	public long getDate() {
		return date;
	}
	
	public String getTitle() {
		return title;
	}
	
	public ResultType getResultType() {
		return type;
	}
	
	
	public String getContent() {
		return content;
	}
	
	public String getUri() {
		return getURI();
	}
	
	public String getURI() {
		return uri;
	}

	public String getUrl() {
		return getURL();
	}
	
	public String getURL() {
		return url;
	}
	
	public Set<SemanticAnnotation> getSemanticAnnotations() {
		return semanticAnnotations;
	}
	
	
	public int hashCode() {
		return url.hashCode();
	}
	
	public boolean equals(Object other) {
		if (other instanceof LuceneIndexDocument) {
			return ((IndexDocument) other).getURL().equals(url);
		}
		else {
			return false;
		}
	}
	
	public String toString() {
		StringBuilder toReturn = new StringBuilder();
		toReturn.append("de.fzi.ipe.soboleo.webdocument.IndexDocument\n");
		toReturn.append(" URI: "+uri);
		toReturn.append(" Title: "+title);
		toReturn.append(" URL: "+url);
		toReturn.append(" Concepts: ");
		for (SemanticAnnotation sia: semanticAnnotations) toReturn.append(sia.getUri() + ", ");
		toReturn.append(" Content: "+content);
		return toReturn.toString();
	}

	@Override
	public ResultType getType() {
		return type;
	}

	public SemanticAnnotation hasAnnotation(String userID, String conceptID){
		for(SemanticAnnotation t : semanticAnnotations){
			if (t.getUri().equals(conceptID))
			{
				return t;
			}
		}
		return null;
	}


	// the id of the user who created the doc
	public String getDocumentOwner()
	{
		return this.documentOwner;
	}
}
