/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.officedocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.lucene.document.ResultType;

/**
 * Event to ask the LuceneEventBusAdaptor for the SearchingLuceneWrapper. 
 */
public class GetAllOfficeDocuments extends QueryEvent{
	
	private ResultType resultType;
	
	public GetAllOfficeDocuments(ResultType resultType) {
		this.resultType = resultType;
	}
	
	
	@SuppressWarnings("unused")
	private GetAllOfficeDocuments() {}
	
	
	public ResultType getResultType() {
		return resultType;
	}
}
