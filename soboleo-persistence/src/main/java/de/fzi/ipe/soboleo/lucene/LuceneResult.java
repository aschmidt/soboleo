package de.fzi.ipe.soboleo.lucene;

import java.io.IOException;
import java.util.Iterator;

import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;



public interface LuceneResult extends Iterable<IndexDocument>{
	
	
	public IndexDocument doc(int n) throws IOException;	

	public String highlightText(int i) throws IOException;
	
	public String getText (int i) throws IOException;
	
	public ResultType getType(int i) throws IOException;
	
	/**
	 * Returns the total number of hits availlable in this result.
	 */
	public int length();
	public int getLength();
	
	/**
	 * Returns the score of the nth document.
	 * @throws IOException 
	 */
	public float score(int n) throws IOException;
	
	/**
	 * Returns an Iterator over the results.
	 * @param doc
	 * @return
	 */
	public Iterator<IndexDocument> iterator();
	public Iterator<IndexDocument> getIterator();
}
