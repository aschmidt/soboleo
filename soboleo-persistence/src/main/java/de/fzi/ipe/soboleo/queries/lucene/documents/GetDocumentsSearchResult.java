/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.documents;

import java.util.HashSet;
import java.util.Set;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Event to ask the LuceneEventBusAdaptor for a search result. Returns
 * a SearchResult object. 
 */
public class GetDocumentsSearchResult extends QueryEvent{
	
	private String query;
	private Set<String> mustConceptURIs;
	private Set<String> mustSubconceptURIs;
	private Set<String> extractedConceptURIs;
	private Set<String> extractedSubconceptURIs;
	
	// added for search of documents of a user
	// search for documents of a user
	private String docOwner=null;
	// search for documents annotated by a user
	private String annotationOwner=null;
	
	
	@SuppressWarnings("unused") //needed for GWT
	private GetDocumentsSearchResult() {}
	
	/**
	 * @param query
	 * @param resultType The result type expected - if null is given, all types are returned
	 */
	public GetDocumentsSearchResult(String query) {
		this.query = query;
		this.mustConceptURIs = new HashSet<String>();

	}
	
	public GetDocumentsSearchResult(String query, Set<String> mustConceptURIs) {
		this.query = query;
		this.mustConceptURIs = mustConceptURIs;

	}
	
	public String getQuery(){
		return query; 
	}
	

	
	public Set<String> getMustConceptURIs(){
		return mustConceptURIs; 
	}
	
	public Set<String> getMustSubconceptURIs (){
		return mustSubconceptURIs;
	}
	
	public Set<String> getExtractedConceptURIs(){
		return extractedConceptURIs;
	}
	
	public Set<String> getExtractedSubconceptURIs(){
		return extractedSubconceptURIs;
	}
	
	public void setMustConceptURIs(Set<String> mustConceptURIs){
		this.mustConceptURIs = mustConceptURIs;
	}
	
	public void setMustSubconceptURIs (Set<String> mustSubconceptURIs){
		this.mustSubconceptURIs = mustSubconceptURIs;
	}
	
	public void setExtractedConceptURIs(Set<String> extractedConceptURIs){
		this.extractedConceptURIs = extractedConceptURIs;
	}
	
	public void setExtractedSubconceptURIs(Set<String> extractedSubconceptURIs){
		this.extractedSubconceptURIs = extractedSubconceptURIs;
	}

	public String getDocOwner() {
		return docOwner;
	}

	public void setDocOwner(String docOwner) {
		this.docOwner = docOwner;
	}

	public String getAnnotationOwner() {
		return annotationOwner;
	}

	public void setAnnotationOwner(String annotationOwner) {
		this.annotationOwner = annotationOwner;
	}
	
	
}
