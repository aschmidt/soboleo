/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.rdf.peopletagging;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

public abstract class RDFTaggedPerson {

	URI personURI;

	ValueFactory vf;
	TripleStore tripleStore;
	
	RDFTaggedPerson(URI personURI, TripleStore tripleStore) {
		this.personURI = personURI;
		this.tripleStore = tripleStore;
		this.vf = tripleStore.getValueFactory();
	}
		
	public abstract String getEmail() throws IOException; 
	public abstract String getName() throws IOException;

	public URI getURI() {
		return personURI;
	}
	
	public RDFManualPersonTag addManualTag(String userID, String conceptID, String url) throws IOException {
		URI tagObjectID = tripleStore.getUniqueURI();
		RDFManualPersonTag tag = new RDFManualPersonTag(tagObjectID,tripleStore);
		tag.createTag(personURI);
		tag.setUserId(userID);
		tag.setConceptId(conceptID);
		tag.setURL(url);
		return tag;
	}
	
	public void removeTag(String tagIDString) throws IOException {
		URI tagID = vf.createURI(tagIDString);
		RDFManualPersonTag tag = new RDFManualPersonTag(tagID,tripleStore);
		tag.delete();
	}
	
	public Set<RDFPersonTag> getTags() throws IOException {
		Set<RDFPersonTag> toReturn = new HashSet<RDFPersonTag>();
		URI isTaggedWith = vf.createURI(SoboleoNS.PEOPLE_PERSON_TAGGED_WITH.toString());
		List<Statement> statements = tripleStore.getStatementList(personURI, isTaggedWith, null);
		for (Statement s: statements) toReturn.add(RDFPersonTag.getPersonTag((URI)s.getObject(),tripleStore));
		return toReturn;
	}
	
	public void deleteAllTags() throws IOException {
		Set<RDFPersonTag> tags = getTags();
		for (RDFPersonTag tag: tags) tag.delete();
	}
	
	public abstract TaggedPerson getClientSideTaggedPerson() throws IOException;
	
	// removes a person
	public Boolean removePerson(URI personURI) throws IOException
	{
		System.out.println(">> remove person :" +personURI.getLocalName());
		// removes a person
	
		System.out.println("(class RDFManualPersonTag) >> remove person :" +personURI.getLocalName());
		tripleStore.removeTriples(personURI, null, null, null);
		
		return true;
		
	}
}
