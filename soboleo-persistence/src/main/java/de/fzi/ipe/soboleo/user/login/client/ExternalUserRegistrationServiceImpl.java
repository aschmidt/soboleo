package de.fzi.ipe.soboleo.user.login.client;

import org.apache.log4j.Logger;

import at.tug.mature.clientws.MatureClientWSUtils;

import de.fzi.ipe.soboleo.space.logging.client.UserLoggingServiceImpl;

public class ExternalUserRegistrationServiceImpl implements
		ExternalUserRegistrationService {

	static Logger logger = Logger.getLogger(UserLoggingServiceImpl.class);
	
	@Override
	public boolean login(String userEmail, String userKey) {
		boolean success = false;
		try{
			logger.info("external login: " +userEmail + " " +userKey);
			// instance of the service
				logger.info("external login CN");
				at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy clientCN = new at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy();
				try {
					success = clientCN.userLogin("mailto:" + userEmail, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("status for " + userEmail+ ": " + success);
					System.out.println("status for " + userEmail+ ": " + success);
				} catch (at.tug.mature.clientws.cn.entities.InvalidKeyException e) {
					logger.error(e);
					success=false;
				}
				
				logger.info("external login kent");
				at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy clientKent = new at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy();
				try {
					success = clientKent.userLogin("mailto:" + userEmail, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("status for " + userEmail+ ": " + success);
				} catch (at.tug.mature.clientws.kent.entities.InvalidKeyException e) {
					logger.error(e);
					success=false;
				}
		} catch (Exception e) {
			logger.error(e);
			success=false;
		}
		return success;
	}

}
