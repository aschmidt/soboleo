package de.fzi.ipe.soboleo.data.documentstorage.analyse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;

public class AnalyseSearchRequests {

	// the lists and counters for concepts
	ArrayList<String> extractedConcepts = new ArrayList<String>();
	Hashtable<String, Integer> conceptsCount = new Hashtable<String, Integer>();

	int days;
	long daysInMilliSeconds = 0;
	long earliestLogDateAllowed = 0;

	
	public AnalyseSearchRequests()
	{

		
	}
	
	
	
	
	/** creates the statistic from files and returns conceptIds in ConceptStatistic
	 * 
	 * @param path
	 * @param spaceId
	 * @param days
	 * @return the statistic list
	 */
	public List<ConceptStatistic> createAndReturnStatistic(String path, String spaceId, int days)
	{
		this.days = days;
		calculateDaysInMilliSeconds();
		calculatedEarliestLogDateAllowed();
		loadLogFilesFromDirectory(path + File.separator + spaceId
				+ File.separator + "log");

		
		List<ConceptStatistic> statistic=new ArrayList<ConceptStatistic>();
		for (String concept : extractedConcepts) {
			ConceptStatistic cs=new ConceptStatistic();
			cs.setConceptId(concept);
			cs.setCount(conceptsCount.get(concept));
			statistic.add(cs);
		}
		
		return statistic;
	}
	
	/** initalize the creation of a log file on disk
	 * use this method, if analiyzes are done via cronjob
	 * @param path
	 * @param spaceId
	 * @param days
	 */
	public AnalyseSearchRequests(String path, String spaceId, int days) {
		this.days = days;
		calculateDaysInMilliSeconds();
		calculatedEarliestLogDateAllowed();
		loadLogFilesFromDirectory(path + File.separator + spaceId
				+ File.separator + "log");
		writeLogFile(path + File.separator + spaceId);
	}

	private void writeLogFile(String path) {
		// spaeter muesste man da mal top n generieren, aktuell alles einfach
		// schreiben und gut.
		File outdir = new File(path + File.separator + "analyzes");
		if (!outdir.exists()) {
			outdir.mkdir();
		}

		String output = "";
		for (String concept : extractedConcepts) {
			output += concept + "\t" + conceptsCount.get(concept) + "\n";
		}
		System.out.println("output: \n" + output);

		// save file

		String filename = path + File.separator + "analyzes" + File.separator
				+ "queriedconcepts.txt";
		// Create file

		try {
			BufferedWriter out;
			FileWriter fstream = new FileWriter(filename);
			out = new BufferedWriter(fstream);
			out.write(output);
			out.close();
		} catch (Exception e) {
			System.out.println("error writing file: " +e.toString());
		}

	}

	private void loadLogFilesFromDirectory(String path) {
		// get the files in the directory
		File dir = new File(path);

		String[] children = dir.list();
		if (children == null) {
			// Either dir does not exist or is not a directory
		} else {
			for (int i = 0; i < children.length; i++) {
				// Get filename of file or directory
				String filename = children[i];
				// System.out.println("file name : " +filename);
				readLogfile(path + File.separator + filename);
			}
		}
	}

	private void readLogfile(String path) {
		// System.out.println("file name to load : " +path);
		LoadFile lf = new LoadFile(path);
		parseLogfile(lf.loadedFile);
	}

	private void parseLogfile(String fileContent) {

		if (fileContent
				.startsWith("<de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult>")) {
			// System.out.println("search protocol: " +fileContent);

			String query = extractContent(fileContent, "query");
			String creationTime = extractContent(fileContent, "creationTime");
			String extractedConceptURIs = extractContent(fileContent,
					"extractedConceptURIs");
			List<String> conceptUris = extractContents(extractedConceptURIs,
					"string");
			String extractedSubconceptURIs = extractContent(fileContent,
					"extractedSubconceptURIs");
			List<String> subconceptUris = extractContents(
					extractedSubconceptURIs, "string");

			System.out.println("creation time: " + creationTime + " | query: "
					+ query + " concepts : " + conceptUris.size()
					+ " subconcepts : " + subconceptUris.size());

			// check creation time
			boolean addConceptsAllowed = false;

			if (days == -1) {
				addConceptsAllowed = true;
			} else {
				// check distance of time
				long logCreationTime = Long.parseLong(creationTime);

				if (logCreationTime > earliestLogDateAllowed) {
					System.out.println("logCreationTime : " + logCreationTime
							+ " | allowed: " + earliestLogDateAllowed);
					addConceptsAllowed = true;
				}
			}

			if (addConceptsAllowed) {
				// add concepts
				addConceptsToList(conceptUris);
				addConceptsToList(subconceptUris);

			}

		}

	}

	private void addConceptsToList(List<String> concepts) {
		// add concepts
		for (String concept : concepts) {
			if (conceptsCount.get(concept) == null) {
				System.out.println("add new concept: " + concept);
				conceptsCount.put(concept, 1);
				this.extractedConcepts.add(concept);
			} else {
				System.out.println("increase existing concept count: "
						+ concept);
				int count = conceptsCount.get(concept);
				count++;
				conceptsCount.put(concept, count);
			}
		}
	}

	private List<String> extractContents(String fileContent, String tag) {
		List<String> elements = new ArrayList<String>();
		fileContent += " ";
		try {
			int startAt = -1;
			int endAt = 0;
			while (startAt < fileContent.length()) {

				startAt = fileContent.indexOf("<" + tag + ">", startAt);
				// System.out.println("extractContents: startat: " +startAt +
				// " tag - " +tag);
				if (startAt > -1) {
					startAt = startAt + tag.length() + 2;
					endAt = fileContent.indexOf("</" + tag, startAt)
							+ tag.length() - tag.length();

					// System.out.println("endAt: " +endAt);

					String tagContent = fileContent.substring(startAt, endAt);
					elements.add(tagContent);
					startAt = endAt;

					// System.out.println("tag extracted : " +tag+
					// " - content : " +tagContent);
				} else {
					startAt = fileContent.length() + 1;
				}
			}
		} catch (Exception e) {
			// System.out.println("error : " +e);
		}
		return elements;
	}

	private String extractContent(String fileContent, String tag) {
		try {
			int startAt = fileContent.indexOf("<" + tag + ">") + tag.length()
					+ 2;
			int endAt = fileContent.indexOf("</" + tag + ">") + tag.length()
					- tag.length();
			String tagContent = fileContent.substring(startAt, endAt);
			// System.out.println("tag extracted : " +tag+ " - content : "
			// +tagContent);
			return tagContent;
		} catch (Exception e) {
			// System.out.println("error : " +e);
		}
		return null;
	}

	private void calculatedEarliestLogDateAllowed() {
		earliestLogDateAllowed = System.currentTimeMillis()
				- daysInMilliSeconds;
	}

	private void calculateDaysInMilliSeconds() {
		long millisADay = 1000 * 60 * 60 * 24;
		daysInMilliSeconds = days * millisADay;
	}

}
