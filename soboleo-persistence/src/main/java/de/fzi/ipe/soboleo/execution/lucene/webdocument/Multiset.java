/*
 * Ontoprise code for project ksi_underground
 * Created on 23.07.2006 by zach
 */
package de.fzi.ipe.soboleo.execution.lucene.webdocument;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * A set that also stores a count for each object
 */
public class Multiset<A> implements Iterable<A>{

	private Map<A,Entry<A>> map = new HashMap<A,Entry<A>>();
	private List<Entry<A>> list = new ArrayList<Entry<A>>();
	
	private Comparator<Entry<A>> comp = new Comparator<Entry<A>>() {
		public int compare(Entry<A> arg0, Entry<A> arg1) {
			return arg1.i - arg0.i;
		}
	};
	
	public static class Entry<A> {
		private int i = 0;
		private A object;
		
		Entry(A object) {
			this.object = object;
		}
		
		public int getCount() {
			return i;
		}
		
		public A getObject() {
			return object;
		}
	}

	
	public void add(A object) {
		add(object,1);
	}
	
	public void add(A object, int boost) {
		Entry<A> c = map.get(object);
		if (c==null) {
			c = new Entry<A>(object);
			map.put(object,c);
			list.add(c);
		}
		c.i+=boost;
	}
	
	public void addAll(Collection<? extends A> toAdd) {
		for (A a: toAdd) {
			add(a);
		}
	}
	
	public int getCount(A object) {
		Entry<A> entry = map.get(object);
		if (entry == null) return 0;
		else return entry.i;
	}
	
	public Entry<A> getEntry(A object) {
		return map.get(object);
	}
	
	public List<A> getSortedValues() {
		Collections.sort(list,comp);
		List<A> toReturn = new ArrayList<A>(list.size());
		for (Entry<A> entry:list) toReturn.add(entry.object);
		return toReturn;
	}
	
	public Iterator<A> iterator() {
		return getSortedValues().iterator();
	}

	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (A a: this) {
			builder.append(a.toString());
			builder.append(":");
			builder.append(getCount(a));
			builder.append(", ");
		}
		return builder.toString();
	}
	
	public static void main(String[] args) {
		Multiset<String> m = new Multiset<String>();
		m.add("something");
		m.add("hallo");
		m.add("test");
		m.add("test");
		m.add("hallo");
		m.add("test");
		m.add("test");
		for (String s: m) {
			System.out.println(s);
		}
	}

}
