package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Retrieves the a PersonTag for the URI.
 */
public class GetPersonTag extends QueryEvent{

	private String uri;
	
	public GetPersonTag(String uri) {
		this.uri = uri;
	}
	
	@SuppressWarnings("unused")
	private GetPersonTag() {; }
	
	public String getURI() {
		return uri;
	}
}
