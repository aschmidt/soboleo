/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

public class GetTaggedPersonForEmail extends QueryEvent implements IsSerializable {

	private String email;
	
	public GetTaggedPersonForEmail(String email) {
		this.email = email;
	}
	
	@SuppressWarnings("unused")
	private GetTaggedPersonForEmail() { ; } 
	
	public String getEmail() {
		return email;
	}
	
	
	
}
