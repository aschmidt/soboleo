package de.fzi.ipe.soboleo.execution.lucene.webdocument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;

public class SearchUtil{
	
	public static final int N = 10; //Number of documents considered for calculation of refinements and relaxations
	
//	public static SearchPeopleResult getPeopleForConcept(Concept concept, String username){
//		Taxonomy tax = Configuration.getDefaultTaxonomy(); 
//		SearchPeople sp;			
//		Set<Concept> mustConcepts = new HashSet<Concept>();
//		mustConcepts.add(concept);
//		sp = new SearchPeople(tax, mustConcepts, username);
//		SearchPeopleResult resultBean = sp.getResult();
//		return sp.getResult();
//	}
//	
//	public static SearchPeopleResult getPeopleForUser(String username){
//		Taxonomy tax = Configuration.getDefaultTaxonomy(); 
//		SearchPeople sp = new SearchPeople(tax, username);
//		SearchPeopleResult resultBean = sp.getResult();
//		return sp.getResult();
//	}
//	
	public static List<String> createRefinements(Set<String> conceptsForSearch, LuceneResult searchResult,ClientSideTaxonomy tax) throws IOException {
		Multiset<String> refinementGenerator = new Multiset<String>();
		Multiset<String> refinementFilter = new Multiset<String>();
		
		for (String currentURI:conceptsForSearch) {
			Set<String> subconceptURIs = tax.get(currentURI).getConnected(SKOS.HAS_NARROWER);
			for (String candidate: subconceptURIs) refinementGenerator.add(candidate,N);
		}
		
		int until = Math.min(searchResult.length(),N);
		for (int i=0;i<until;i++) {
			IndexDocument doc = searchResult.doc(i);
			for (SemanticAnnotation sia:doc.getSemanticAnnotations()) {
				ClientSideConcept currentConcept = tax.get(sia.getUri());
				if (currentConcept != null) {
					refinementGenerator.add(sia.getUri(),N-i);
					refinementFilter.add(sia.getUri());
				}
			}
		}
		List<String> toReturn = new ArrayList<String>();
		for (String c: refinementGenerator) {
			int currentCount = refinementFilter.getCount(c);
			if (currentCount >0 && ((until/ currentCount) >= 2) ) toReturn.add(c);
		}
		
		return toReturn;
	}
	
	
	public static List<String> createRelaxations(Set<String> conceptsForSearch, LuceneResult searchResult,ClientSideTaxonomy tax) throws IOException {
		Multiset<String> candidates = new Multiset<String>();
		for (String currentURI:conceptsForSearch) {
			Set<String> superconceptURIs = tax.get(currentURI).getConnected(SKOS.HAS_BROADER);
			for (String candidate: superconceptURIs) candidates.add(candidate,2);
		}
		//add all conceptURIs from the found documents
		int until = Math.min(searchResult.length(),N);
		for (int i=0;i<until;i++) {
			IndexDocument doc = searchResult.doc(i);
			HashSet<String> temp = new HashSet<String>();
			for (SemanticAnnotation sia:doc.getSemanticAnnotations()) {
				ClientSideConcept currentConcept = tax.get(sia.getUri());
				if (currentConcept != null) {
					if (!conceptsForSearch.contains(sia.getUri())) temp.add(sia.getUri());
					else temp.addAll(tax.get(sia.getUri()).getConnected(SKOS.HAS_BROADER));
				}
			}
			candidates.addAll(temp);
		}
		
		return candidates.getSortedValues();
	}
}
