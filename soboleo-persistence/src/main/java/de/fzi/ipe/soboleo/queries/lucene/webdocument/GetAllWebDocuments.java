/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.webdocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.lucene.document.ResultType;

/**
 * Event to ask the LuceneEventBusAdaptor for the SearchingLuceneWrapper. 
 */
public class GetAllWebDocuments extends QueryEvent{
	
	private ResultType resultType;
	
	public GetAllWebDocuments(ResultType resultType) {
		this.resultType = resultType;
	}
	
	
	@SuppressWarnings("unused")
	private GetAllWebDocuments() {}
	
	
	public ResultType getResultType() {
		return resultType;
	}
}
