package de.fzi.ipe.soboleo.lucene;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.index.Term;
import org.apache.lucene.misc.ChainedFilter;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.QueryFilter;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.LuceneIndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;



public class SearchingLuceneWrapper extends LuceneWrapper {

	protected static final Logger logger = Logger
	.getLogger(SearchingLuceneWrapper.class);

	
	public SearchingLuceneWrapper(String indexDir) throws IOException {
		super(indexDir);
		
		updateIndex();
	}

	/**
	 * Method used to update all indices (adding the type field). It checks any document and 
	 * if that does not have a type field, then the type field is added to all documents. This
	 * may take a moment. 
	 * @throws IOException
	 */
	private void updateIndex() throws IOException {
		LuceneResult allDocuments = getAllDocuments();
		if (allDocuments.length()>0) {
			if (allDocuments.getType(0) == null) {
				System.out.println("Need to do an index update of "+allDocuments.length()+" documents");
				for (int i=0;i<allDocuments.length();i++) {
					IndexDocument doc = ((LuceneResultImpl)allDocuments).fullDoc(i);
					IndexDocument newDoc = new LuceneIndexDocument(doc.getURI(),doc.getTitle(),doc.getContent(),doc.getURL(),doc.getDate(),ResultType.WEB_DOCUMENT,doc.getSemanticAnnotations(), doc.getDocumentOwner());
					index(newDoc);
					if (i%5 == 0) System.out.println("Done "+i+" of "+allDocuments.length());
				}
				System.out.println("Index updated.");
			}
		}
	}
	
	protected List<Term> tokenizeString(String text, String field) throws IOException {
		List<Term> toReturn = new ArrayList<Term>();
		StringReader reader = new StringReader(text);
		TokenStream stream = getAnalyzer().tokenStream(field,reader);
		Token currentToken = stream.next();
		while (currentToken != null) {
			Term currentTerm = new Term(field,currentToken.termText());
			toReturn.add(currentTerm);
			currentToken = stream.next();
		}
		return toReturn;
	}
	
	public LuceneResult searchForContent(String content) throws IOException {
		BooleanQuery query = createContentQuery(content);
		return getResults(query);
	}
	
	public LuceneResult searchForContent(String content, ResultType resultType, Set<String> conceptUris, Set<String> subconceptUris, Set<String> mustConceptUris) throws IOException {
		BooleanQuery query = createContentQuery(content);
		addToQuery(conceptUris, 10f,query);
		addToQuery(subconceptUris,4f,query);
//		QueryFilter filter = null;
		QueryFilter conceptsFilter = createConceptsFilter(mustConceptUris);
		QueryFilter resultTypeFilter = createResultTypeFilter(resultType);
	    if(resultTypeFilter != null && conceptsFilter != null){
			Filter[] filters = new Filter[] {conceptsFilter, resultTypeFilter};
			ChainedFilter filter = new ChainedFilter(filters, ChainedFilter.AND);
			return getResults(query,createContentQuery(content),filter);
		}
		else if(resultTypeFilter != null) return getResults(query, createContentQuery(content), resultTypeFilter);
		else return getResults(query,createContentQuery(content),conceptsFilter);
	}
	
	public LuceneResult searchForAllDocumentTypesContent(String content,  Set<String> conceptUris, Set<String> subconceptUris, Set<String> mustConceptUris, String docOwner, String annotationOwner) throws IOException {
		BooleanQuery query = createContentQuery(content);
		
		if (conceptUris != null)
		{
			addToQuery(conceptUris, 10f,query);
		}
		
		if (subconceptUris != null)
		{
			addToQuery(subconceptUris,4f,query);
		}
		
		// if owner not null, add docOwner
		if (docOwner != null)
		{
			logger.info("add owner to query: " +docOwner);
			addToQueryDocOwner(docOwner, query);
		}

		// if annotationOwner not null, add annotationOwner
		if (annotationOwner != null)
		{
			logger.info("add annotation owner to query: " +annotationOwner);
			addToQueryAnnotationOwner(annotationOwner, query);
		}

		QueryFilter conceptsFilter = createConceptsFilter(mustConceptUris);
		QueryFilter resultTypeFilter =createResultTypeFilterAllDocumentTypes();
	    if(resultTypeFilter != null && conceptsFilter != null){
			Filter[] filters = new Filter[] {conceptsFilter, resultTypeFilter};
			ChainedFilter filter = new ChainedFilter(filters, ChainedFilter.AND);
	
			return getResults(query,createContentQuery(content),filter);
		}
		else if(resultTypeFilter != null) 
		{
			logger.info("query: " +query.toString());
			
			return getResults(query, createContentQuery(content), resultTypeFilter);
		}
		else 
			{
				return getResults(query,createContentQuery(content),conceptsFilter);
			
			}
	}
	
	
	public LuceneResult searchForConceptUri(String conceptUri) throws IOException {
		BooleanQuery query = new BooleanQuery();
		
		Term currentTerm = new Term(ATR_CONCEPTS,conceptUri);
		TermQuery termQuery = new TermQuery(currentTerm);
		return getResults(query);
	}

	public LuceneResult getAllDocuments() throws IOException {
		MatchAllDocsQuery query= new MatchAllDocsQuery();
		Sort s = new Sort(new SortField(ATR_DATE,SortField.STRING,true));
		return getResults(query,null,s);
	}

	/** searches for all web documents */
	public LuceneResult getAllWebDocuments(ResultType resultType) throws IOException {
		BooleanQuery query = new BooleanQuery();
		Term typeTerm = new Term(ATR_TYPE,resultType.name());
		TermQuery typeQuery = new TermQuery(typeTerm);
		query.add(typeQuery,BooleanClause.Occur.MUST);
		Sort s = new Sort(new SortField(ATR_DATE,SortField.STRING,true));
		return getResults(query,null,s);
	}

	
	/** returns documents of type web or office */
	public LuceneResult getAllDocuments(ResultType resultType) throws IOException {
		
		// create the query  MUST(should office - should webdocument)
		
		BooleanQuery queryDocType = new BooleanQuery();
		BooleanQuery query = new BooleanQuery();

		Term typeTermOffice = new Term(ATR_TYPE,ResultType.OFFICE_DOCUMENT.toString());
		TermQuery typeQueryOffice = new TermQuery(typeTermOffice);		
		queryDocType.add(typeQueryOffice,BooleanClause.Occur.SHOULD);
		
		Term typeTermWebOffice = new Term(ATR_TYPE,ResultType.WEB_DOCUMENT.toString());
		TermQuery typeQueryWeb = new TermQuery(typeTermWebOffice);		
		queryDocType.add(typeQueryWeb,BooleanClause.Occur.SHOULD);
		
		// add the query part
		query.add(queryDocType,BooleanClause.Occur.MUST);

		
		Sort s = new Sort(new SortField(ATR_DATE,SortField.STRING,true));
		return getResults(query,null,s);
	}
	
	/**
	 * Returns the documents for the concept and all of its subconcepts. 
	 */
	public LuceneResult getAllDocumentsForConcept(String conceptURI, Set<String> subconceptURIs) throws IOException{
		
		BooleanQuery query = new BooleanQuery();
		Set<String> conceptURIs = new HashSet<String>();
	
		conceptURIs.add(conceptURI);
		addToQuery(conceptURIs,10f,query);

		
		logger.info("get all documents for concept : " +conceptURI );
		try
		{
			logger.info("subconcept size: " +subconceptURIs.size());
			addToQuery(subconceptURIs,1f,query);
		}
		catch(Exception e)
		{
			logger.info("no subconcept uris provided");
		}
		
		// .
		
		//New
		QueryFilter filter = createResultTypeFilterAllDocumentTypes();
		//
		Sort s = new Sort(new SortField(ATR_DATE,SortField.STRING,true));
		return getResults(query,filter,s);
	}
	
	
	
	/** returns the documents of a type by its uri **/
	public LuceneResult getOfficeDocumentByUri(String uri) throws IOException {
		BooleanQuery query = new BooleanQuery();

		Term currentTerm = new Term(ATR_URI,uri);
		TermQuery termQuery = new TermQuery(currentTerm);
		query.add(termQuery,BooleanClause.Occur.MUST);
		
		return getResults(query);
	}
	
	/**
	 * Returns the web documents for the concept and all of its subconcepts. 
	 */
	public LuceneResult getWebDocumentsForConcept(String conceptURI, Set<String> subconceptURIs, ResultType resultType) throws IOException{
		BooleanQuery query = new BooleanQuery();
		Set<String> conceptURIs = new HashSet<String>();
		conceptURIs.add(conceptURI);
		addToQuery(conceptURIs,10f,query);
		addToQuery(subconceptURIs,1f,query);
		//New
		QueryFilter filter = createResultTypeFilter(resultType);
		//
		Sort s = new Sort(new SortField(ATR_DATE,SortField.STRING,true));
		return getResults(query,filter,s);
	}
	
	private BooleanQuery createContentQuery(String content) throws IOException {
		List<Term> terms = tokenizeString(content,ATR_CONTENT);
		BooleanQuery query = new BooleanQuery();
		for (Term term: terms) {
			query.add(new TermQuery(term),BooleanClause.Occur.SHOULD);
		}
		
		List<Term> titleTerms = tokenizeString(content,ATR_TITLE);
		for (Term term: titleTerms) {
			TermQuery titleQuery = new TermQuery(term);
			titleQuery.setBoost(2);
			query.add(titleQuery,BooleanClause.Occur.SHOULD);
		}
		return query;
	}

	private QueryFilter createConceptsFilter(Set<String> conceptUris) {
		if (conceptUris.size() == 0) return null;
		else {
			BooleanQuery q = new BooleanQuery();
			for (String conceptUri: conceptUris) {
				Term currentTerm = new Term(ATR_CONCEPTS, conceptUri);
				TermQuery termQuery = new TermQuery(currentTerm);
				q.add(termQuery,BooleanClause.Occur.SHOULD);	
			}
			return new QueryFilter(q);
		}
	}
	
	private QueryFilter createResultTypeFilter(ResultType resultType){
		if(resultType == null) return null;
		else {
			BooleanQuery q = new BooleanQuery();
			Term typeTerm = new Term(ATR_TYPE,resultType.name());
			TermQuery typeQuery = new TermQuery(typeTerm);
			q.add(typeQuery,BooleanClause.Occur.MUST);
			return new QueryFilter(q);
		}
	}
	
	
	private QueryFilter createResultTypeFilterAllDocumentTypes(){
		
		BooleanQuery query = new BooleanQuery();
		BooleanQuery queryDocType = new BooleanQuery();
		
		Term typeTermOffice = new Term(ATR_TYPE,ResultType.OFFICE_DOCUMENT.toString());
		TermQuery typeQueryOffice = new TermQuery(typeTermOffice);		
		queryDocType.add(typeQueryOffice,BooleanClause.Occur.SHOULD);
		
		Term typeTermWebOffice = new Term(ATR_TYPE,ResultType.WEB_DOCUMENT.toString());
		TermQuery typeQueryWeb = new TermQuery(typeTermWebOffice);		
		queryDocType.add(typeQueryWeb,BooleanClause.Occur.SHOULD);
		
		// add the query part
		query.add(queryDocType,BooleanClause.Occur.MUST);
		
		

		return new QueryFilter(query);
		
	}
	
	
	
	private void addToQuery(Set<String> conceptUris, float boost, BooleanClause.Occur occur, BooleanQuery query) {
		
		
		
		for (String conceptUri: conceptUris) {
			
			String conceptUriUT=conceptUri;
			
//			logger.info("add concept uri: " +conceptUri);
			
			// only use part behind # without -
			String fields[]=conceptUri.split("#");
			conceptUri=fields[1];
			conceptUri=conceptUri.replaceAll("-", "");
			
//			logger.info("tokenized concept uri: " +conceptUri);
			
			
			Term currentTerm = new Term(ATR_CONCEPTS,conceptUri);
			TermQuery termQuery = new TermQuery(currentTerm);
			termQuery.setBoost(boost);
			query.add(termQuery,occur);
			
			Term currentTermUT = new Term(ATR_CONCEPTS,conceptUriUT);
			TermQuery termQueryUT = new TermQuery(currentTermUT);
			termQueryUT.setBoost(boost);
			query.add(termQueryUT,occur);

			
		}		
	}
	
	private void addToQuery(Set<String> conceptUris, float boost, BooleanQuery query) {
		
		
		
		addToQuery(conceptUris,boost,BooleanClause.Occur.SHOULD,query);
	}

	private void addToQueryDocOwner(String docOwner, BooleanQuery query) {
		Term currentTerm = new Term(LuceneWrapper.ATR_DOCOWNER,docOwner);
		TermQuery termQuery = new TermQuery(currentTerm);
		
		query.add(termQuery,BooleanClause.Occur.MUST);
		
	}	

	private void addToQueryAnnotationOwner(String annotationOwner, BooleanQuery query) {

		// important: the lucene tokenizer changes the key to lower case string
		// => input must be also lower case
		
		Term currentTerm = new Term(LuceneWrapper.ATR_CONCEPTS,annotationOwner.toLowerCase());
		TermQuery termQuery = new TermQuery(currentTerm);
		query.add(termQuery,BooleanClause.Occur.MUST);
		
	}	

}
