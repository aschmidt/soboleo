/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.webdocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Event to ask the LuceneEventBusAdaptor for the SearchingLuceneWrapper. 
 */
public class GetSearchingLuceneWrapper extends QueryEvent{
	
	public GetSearchingLuceneWrapper() {}
	
}
