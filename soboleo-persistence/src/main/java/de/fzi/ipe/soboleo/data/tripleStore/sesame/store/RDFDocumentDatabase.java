/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.data.tripleStore.sesame.store;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;


/**
 * Represents the set of all documents and annotations in the context of 
 * the document.eventBusAdapter. This class is intended to be used by classes
 * in the document package - all other classes should use events, queries and commands
 * to interact with the DocumentSet.
 */
public abstract class RDFDocumentDatabase<A extends RDFDocument<?>> {

	protected TripleStore tripleStore;
	protected ValueFactory vf;
	protected HashMap<URI,A> documentCache = new HashMap<URI,A>();
	
	
	public RDFDocumentDatabase(TripleStore tripleStore) {
		this.tripleStore = tripleStore;
		this.vf = tripleStore.getValueFactory();
	}
	
	/**
	 * Gets the document by URI (this is the automatically generated URI of the document, *not* its url)
	 * @throws IOException 
	 */
	public  A getDocument(String uri) throws IOException {
		URI documentURI = vf.createURI(uri);
		return getDocument(documentURI);
	}

	public synchronized RDFTag getAnnotation(String tagUriString) throws IOException {
		URI tagURI = vf.createURI(tagUriString);
		return new RDFTag(tagURI,tripleStore);
	}
	
	protected abstract A getDocument(URI documentURI) throws IOException; 
	
	protected synchronized Set<A> getAllDocuments(String type) throws IOException {
		URI typeURI = vf.createURI(TYPE.toString());
		URI conceptURI = vf.createURI(type);
		List<Statement> statementList = tripleStore.getStatementList(null, typeURI, conceptURI);
		Set<A> toReturn = new HashSet<A>(statementList.size());
		for (Statement s:statementList) toReturn.add(getDocument((URI)s.getSubject()));
		return toReturn;
	}
	
	public abstract void deleteDocument(String uri) throws IOException;
}
