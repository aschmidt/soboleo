/*
 * FZI - Information Process Engineering 
 * Created on 23.07.2009 by zach
 */
package de.fzi.ipe.soboleo.data.tripleStore.sesame.store;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.CommonTag;
import de.fzi.ipe.soboleo.beans.ontology.FOAF;
import de.fzi.ipe.soboleo.beans.ontology.RDF;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;

public class RDFTag {

	private URI uri;
	private TripleStore tripleStore;
	private ValueFactory vf;
	
	public RDFTag(URI annotationID, TripleStore tripleStore){
		this.uri = annotationID;
		this.tripleStore = tripleStore;
		this.vf = tripleStore.getValueFactory();
	}

	public void createTag(URI documentURI) throws IOException {
		URI tagClassId = this.vf.createURI(CommonTag.TAG.toString());
		URI rdfType = this.vf.createURI(RDF.TYPE.toString());
		URI isTaggedWith = this.vf.createURI(CommonTag.IS_TAGGED_WITH.toString());
		this.tripleStore.addTriple(this.uri, rdfType, tagClassId, null);
		this.tripleStore.addTriple(documentURI,isTaggedWith,this.uri,null);
		this.tripleStore.setTimestamp(this.uri, this.vf.createURI(CommonTag.HAS_TAGGINGDATE.toString()), null);
	}
	
	public void setUserId(String userId) throws IOException {
		URI userID = this.vf.createURI(userId);
		URI pred = this.vf.createURI(FOAF.MAKER.toString());
		this.tripleStore.removeTriples(this.uri, pred, null, null);
		this.tripleStore.addTriple(this.uri, pred, userID, null);
	}
	
	public String getUserId() throws IOException {
		URI pred = this.vf.createURI(FOAF.MAKER.toString());
		List<Statement> statements = this.tripleStore.getStatementList(this.uri,pred,null);
		if (statements.size() ==0) return null;
		else return ((URI)statements.get(0).getObject()).toString();
	}	
	
	public void setConceptId(String conceptIdString) throws IOException {
		URI conceptId = this.vf.createURI(conceptIdString);
		URI pred = this.vf.createURI(CommonTag.TAG_MEANS.toString());
		this.tripleStore.removeTriples(this.uri,pred,null,null);
		this.tripleStore.addTriple(this.uri,pred,conceptId,null);
	}
	
	public String getConceptId() throws IOException {
		URI pred = this.vf.createURI(CommonTag.TAG_MEANS.toString());
		List<Statement> statements = this.tripleStore.getStatementList(this.uri,pred,null);
		if (statements.size() == 0) return null;
		else return ((URI)statements.get(0).getObject()).toString();
	}
	
	public URI getURI() {
		return this.uri;
	}
	
	public void delete() throws IOException {
		this.tripleStore.removeTriples(this.uri, null, null, null);
		this.tripleStore.removeTriples(null, null, this.uri, null);
	}
	
	
	/** a slightly optimized method to create a tag object - a copy of the data
	 * in the tripleStore about a tag. It works with just one call to the triple store -
	 * only a third of what would be needed if the other methods where to be used. 
	 */
	public Tag getClientSideTag() throws IOException {
		String userID = null, conceptID = null;
		Calendar cal = null;
		List<Statement> statements = this.tripleStore.getStatementList(this.uri, null, null);
		for (Statement s:statements) {
			if (s.getPredicate().toString().equals(FOAF.MAKER.toString())) userID = s.getObject().toString();
			else if (s.getPredicate().toString().equals(CommonTag.TAG_MEANS.toString())) conceptID = s.getObject().toString();
			else if (s.getPredicate().toString().equals(CommonTag.HAS_TAGGINGDATE.toString())) {
				Literal l = (Literal) s.getObject();
				cal = l.calendarValue().toGregorianCalendar();
			}
		}
		return new Tag(this.uri.toString(),userID,conceptID,cal.getTime());
	}
	
}
