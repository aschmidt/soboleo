package de.fzi.ipe.soboleo.queries.rdf.peopletagging;

import java.util.Set;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;


/**
 * Searches for peoples based on the given string . 
 * Searches for names of peoples and concepts.
 * Used in Browse People.
 * 
 * @author FZI - Information Process Engineering Stephan Kluge
 */
public class SearchPersonsByNameAndTopic extends QueryEvent{

	private String searchString;
	private String askerURI;
	private Set<String> extractedConceptURIs;
	
	public SearchPersonsByNameAndTopic(String searchString) {
		this.searchString = searchString;
	}
	
	public SearchPersonsByNameAndTopic(String searchString, String askerURI){
		this.searchString = searchString;
		this.askerURI = askerURI;
	}
	
	@SuppressWarnings("unused")
	private SearchPersonsByNameAndTopic() {;}
	
	public String getSearchString() {
		return searchString;
	}
	
	public String getAskerURI(){
		return askerURI;
	}
	
	public Set<String> getExtractedConceptURIs(){
		return extractedConceptURIs;
	}
	
	public void setExtractedConceptURIs(Set<String> extractedConceptURIs){
		this.extractedConceptURIs = extractedConceptURIs;
	}
}
