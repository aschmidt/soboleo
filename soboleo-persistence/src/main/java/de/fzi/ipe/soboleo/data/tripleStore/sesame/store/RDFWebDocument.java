/*
 * FZI - Information Process Engineering 
 * Created on 08.07.2009 by zach
 */
package de.fzi.ipe.soboleo.data.tripleStore.sesame.store;

import static de.fzi.ipe.soboleo.beans.ontology.RDF.TYPE;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.openrdf.model.URI;

import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;


/**
 * Represents a document in the context of the document.eventBusAdaptor. This class is 
 * intended to be used only by the AnnotationEventBus Adaptor - other parts of soboleo
 * should use events and commands to interact with annotations and documents. 
 */
public class RDFWebDocument extends RDFDocument<WebDocument>{
	
	protected RDFWebDocument(URI uri, TripleStore tripleStore) {
		super(uri,tripleStore);
	}
	
	protected synchronized void createDocument() throws IOException{
		URI typeURI = vf.createURI(TYPE.toString());
		URI conceptURI = vf.createURI(SoboleoNS.DOCUMENT.toString());
		tripleStore.addTriple(documentURI, typeURI, conceptURI, null);
		URI createdURI = vf.createURI(SoboleoNS.DATE_ADDED.toString());
		tripleStore.setTimestamp(documentURI, createdURI, null);
	}
	
	public String getURL() throws IOException {
		URI predicate = vf.createURI(SoboleoNS.HAS_URL.toString());
		return getValue(predicate);
	}
	
	public void setURL(String url) throws IOException {
		URI predicate = vf.createURI(SoboleoNS.HAS_URL.toString());
		tripleStore.removeTriples(documentURI, predicate, null, null);
		tripleStore.addTriple(documentURI, predicate, vf.createLiteral(url), null);
		tripleStore.setTimestamp(documentURI, vf.createURI(SoboleoNS.DATE_LAST_MODIFIED.toString()), null);
	}

	
	public WebDocument getClientSideDocument() throws IOException {
		Set<RDFTag> rdfTags = getTags();
		Set<Tag> tags = new HashSet<Tag>(rdfTags.size());
		for (RDFTag rdfTag:rdfTags) {
			tags.add(rdfTag.getClientSideTag());
		}
		String uri = getURI().toString();
		String url = getURL();
		String title = getTitle();
		return new WebDocument(uri,url,title,tags);
	}
	
}
