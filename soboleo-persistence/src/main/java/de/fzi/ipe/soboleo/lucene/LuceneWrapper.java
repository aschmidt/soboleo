package de.fzi.ipe.soboleo.lucene;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;


/**
 * A wrapper that handles the storage of documents in a Lucene Index. Please note that in cases with concurrent writes 
 * and reads the IndexSearcher may be closed without further notice and thereby invalidate all LuceneResults that may still be
 * in use.
 */
public class LuceneWrapper {

	protected static final Logger logger = Logger
	.getLogger(LuceneWrapper.class);

	protected static final int MAX = 1000;
	
	protected static final String ATR_TITLE = "title";
	protected static final String ATR_URI = "uri";
	protected static final String ATR_URL = "url";
	protected static final String ATR_CONTENT = "content";
	protected static final String ATR_CONCEPTS = "concepts";
	protected static final String ATR_CONCEPTS_UT = "conceptsut";

	protected static final String ATR_DATE = "date";
	protected static final String ATR_TYPE = "type";
	protected static final String ATR_DOCOWNER = "docowner";
	
	
	private IndexWriter writer; 
	private IndexSearcher searcher;
	
	private Directory indexDir;
	private static Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_29);
	
	
	/**
	 * @param indexDir
	 * @param createNewIndex True if a new index should be created. If an index already exists it is overwritten. 
	 * @throws IOException
	 */
	public LuceneWrapper(String indexDir) throws IOException {
		this.indexDir = new SimpleFSDirectory(new File(indexDir));
		if (this.indexDir.getLockID() != null) {
			this.indexDir.clearLock(this.indexDir.getLockID());
		}
		this.writer = new IndexWriter(this.indexDir,analyzer,IndexWriter.MaxFieldLength.LIMITED);
		this.writer.commit();
		searcher = new IndexSearcher(writer.getReader());
	}
	
	/**
	 * Adds a document to the index. If a document with the same uri exists already its overwritten. 
	 * @param doc
	 * @throws IOException
	 */
	public synchronized void index(IndexDocument doc) throws IOException {
		if (getDocument(doc.getURI()) != null) { 
			deleteFromIndex(doc.getURI());
		}
		Document luceneDoc = createLuceneDocument(doc);
		writer.addDocument(luceneDoc);
		writer.commit();
	}
	
	/**
	 * Deletes the document with the uri "uri".
	 * @param uri
	 * @throws IOException
	 */
	public synchronized final void deleteFromIndex(String uri) throws IOException {
		writer.deleteDocuments(new Term(ATR_URI,uri));
		writer.commit();
	}
	
	public void delete(IndexDocument doc) throws IOException {
		deleteFromIndex(doc.getURI());
	}
	
	/**
	 * Returns the document with the uri "uri" or null if none exists.
	 * @param uri
	 * @return
	 * @throws IOException
	 */
	public IndexDocument getDocument(String uri) throws IOException {
		TermQuery uriQuery = new TermQuery(new Term(ATR_URI,uri));
		LuceneResultImpl result = getResults(uriQuery);
		if (result.length() == 0) return null;
		else return result.doc(0);
	}
	
	public IndexDocument getFullDocument(String uri)throws IOException {
		TermQuery uriQuery = new TermQuery(new Term(ATR_URI,uri));
		LuceneResultImpl result = getResults(uriQuery);
		if (result.length() == 0) return null;
		else return result.fullDoc(0);
	}

	
	protected synchronized LuceneResult getResults(Query query, Filter filter, Sort sort) throws IOException {
		IndexSearcher searcher = getSearcher();
		TopDocs hits = searcher.search(query,filter,MAX,sort);
		return new LuceneResultImpl(hits,searcher,query,getAnalyzer());  
	}
	
	/**
	 * Runs the query and returns the results. 
	 * @param query
	 * @return
	 * @throws IOException
	 */
	protected synchronized LuceneResultImpl getResults(Query query) throws IOException {
		IndexSearcher searcher = getSearcher();
		TopDocs hits = searcher.search(query,MAX);
		return new LuceneResultImpl(hits,searcher,query,getAnalyzer());
	}

	/**
	 * Runs the query and returns the results. The contentQuery is used
	 * only as initalizer for the highlighting tool.
	 * @param query
	 * @return
	 * @throws IOException
	 */
	protected synchronized LuceneResult getResults(Query query, Query contentQuery, Filter filter) throws IOException {
		
		IndexSearcher searcher = getSearcher(); 
		TopDocs hits = searcher.search(query,filter,MAX,Sort.RELEVANCE);
		return new LuceneResultImpl(hits,searcher,contentQuery,getAnalyzer());
	}

	
	/**
	 * Runs the query and returns the results. The contentQuery is used
	 * only as initalizer for the highlighting tool.
	 * @param query
	 * @return
	 * @throws IOException
	 */
	protected synchronized LuceneResult getResults(Query query, Query contentQuery) throws IOException {
		IndexSearcher searcher = getSearcher();
		TopDocs hits = searcher.search(query,MAX);
		return new LuceneResultImpl(hits,searcher,contentQuery,getAnalyzer());
	}
	
	
	/**
	 * Returns a current searcher for this LuceneWrapper's index. 
	 * @return
	 * @throws IOException
	 */
	protected IndexSearcher getSearcher() throws IOException {
		if (searcher.getIndexReader().isCurrent()) return searcher;
		else return new IndexSearcher(writer.getReader());
	}
	
	/**
	 * Returns the analyzer used for the fields title and content. 
	 * @param doc
	 * @return
	 */
	protected Analyzer getAnalyzer() {
		return analyzer;
	}
	
	
	private Document createLuceneDocument(IndexDocument doc) {
		Document toReturn = new Document();
		toReturn.add(new Field(ATR_TITLE, doc.getTitle(), Field.Store.YES, Field.Index.ANALYZED));
		toReturn.add(new Field(ATR_CONTENT, doc.getContent(), Field.Store.YES, Field.Index.ANALYZED));
		toReturn.add(new Field(ATR_URI, doc.getURI(),Field.Store.YES, Field.Index.NOT_ANALYZED));
		toReturn.add(new Field(ATR_URL, doc.getURL(),Field.Store.YES, Field.Index.NOT_ANALYZED));
		toReturn.add(new Field(ATR_DATE,DateTools.timeToString(doc.getDate(),DateTools.Resolution.SECOND),Field.Store.YES,Field.Index.NOT_ANALYZED));
		toReturn.add(new Field(ATR_TYPE, doc.getType().name(), Field.Store.YES, Field.Index.NOT_ANALYZED));

		try
		{
			toReturn.add(new Field(ATR_DOCOWNER, doc.getDocumentOwner(), Field.Store.YES, Field.Index.NOT_ANALYZED));
		}
		catch(Exception e)
		{
			logger.error("unknown doc owner!");
			toReturn.add(new Field(ATR_DOCOWNER, "unknown", Field.Store.YES, Field.Index.NOT_ANALYZED));
		}

		
		for(SemanticAnnotation sia: doc.getSemanticAnnotations()) {
			// format in store is uri | userid | createdDate | currentDate
			
			// the uri needs some manipulation: only index the part behind # without -
			String uri=sia.getUri();
			
			logger.info("uri: " +uri);
			
			String fields[]=uri.split("#");
			
			try
			{
				uri=fields[1];
			}
			catch(Exception e)
			{
				
			}
			
			uri=uri.replaceAll("-", "");
			logger.info("uri in index: " +uri);
			
			String indexFieldUT=sia.getUri() + " xxx " +sia.getUserId() + " xxx ";
			String indexField=uri + " xxx " +sia.getUserId() + " xxx ";
			if (sia.getCreatedDate() == -1) 
			{
				indexField += System.currentTimeMillis() + " xxx ";
				indexFieldUT += System.currentTimeMillis() + " xxx ";
			}
			else
			{
				indexField += sia.getCreatedDate() + " xxx ";
				indexFieldUT += sia.getCreatedDate() + " xxx ";
			}
			// last changed date = current date
			indexField += System.currentTimeMillis();
			indexFieldUT += System.currentTimeMillis();
			
			logger.debug("indexField : " +indexField);
			toReturn.add(new Field(ATR_CONCEPTS,indexField,Field.Store.YES, Field.Index.ANALYZED));		
			toReturn.add(new Field(ATR_CONCEPTS_UT,indexFieldUT,Field.Store.YES, Field.Index.NOT_ANALYZED));
		}
		
		return toReturn;		
	}
	
	/**
	 * Closes the this lucene wrapper. Only use this when the space is closing. 
	 * @throws IOException 
	 */
	public void close() throws IOException {
		searcher.close();
		writer.close();
		indexDir.clearLock(indexDir.getLockID());
	}
}

