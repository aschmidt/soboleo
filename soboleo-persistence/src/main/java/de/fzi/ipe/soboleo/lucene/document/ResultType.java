package de.fzi.ipe.soboleo.lucene.document;

public enum ResultType {

	 WEB_DOCUMENT, DIALOG, OFFICE_DOCUMENT
}
