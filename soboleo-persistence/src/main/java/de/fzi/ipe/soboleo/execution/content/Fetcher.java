/*
 * FZI - Information Process Engineering 
 * Created on 14.08.2009 by zach
 */
package de.fzi.ipe.soboleo.execution.content;

import de.fzi.ipe.soboleo.eventbus.EventBus;

/**
 * Class Responsible for getting documents from URLS in order to add them to the index. 
 */
public interface Fetcher {

	public void fetch(String documentURI, String documentURL, EventBus eventBus); 
	
}
