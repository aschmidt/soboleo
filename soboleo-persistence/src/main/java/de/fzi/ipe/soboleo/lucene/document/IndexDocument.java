package de.fzi.ipe.soboleo.lucene.document;

import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;


public interface IndexDocument {
	
	public String getTitle();
	public String getContent();
	public long getDate();

	
	public String getUri();
	public String getURI();

	public String getURL();
	public String getUrl();
	
	public Set<SemanticAnnotation> getSemanticAnnotations();
	public ResultType getType();
	
	// the id of the user who created the doc
	public String getDocumentOwner();

}

