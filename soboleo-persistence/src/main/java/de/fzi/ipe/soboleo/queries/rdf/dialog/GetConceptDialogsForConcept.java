/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.rdf.dialog;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * 
 */
public class GetConceptDialogsForConcept extends QueryEvent{
	
	private String conceptURI;
	
	@SuppressWarnings("unused") //needed for GWT
	private GetConceptDialogsForConcept() {}
	
	public GetConceptDialogsForConcept(String conceptURI) {
		this.conceptURI = conceptURI;
	}
	
	public String getConceptURI(){
		return conceptURI; 
	}
}
