/*
 * FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
package de.fzi.ipe.soboleo.queries.lucene.officedocument;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

/**
 * Event to ask the LuceneEventBusAdaptor for the SearchingLuceneWrapper. 
 */
public class GetOfficeDocumentInputStream extends QueryEvent{
	
	private String uri;
	private String documentType;
	
	public GetOfficeDocumentInputStream(String documentType, String uri) {
		this.uri=uri;
		this.documentType=documentType;
	}
	
	
	@SuppressWarnings("unused")
	private GetOfficeDocumentInputStream() {}
	

	public String getUri() {
		return uri;
	}


	public String getDocumentType() {
		return documentType;
	}
	
	


		
}
