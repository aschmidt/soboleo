package de.fzi.ipe.soboleo.space.logging.client;

import org.apache.log4j.Logger;

import at.tug.mature.clientws.MatureClientWSUtils;





public class UserLoggingServiceImpl implements UserLoggingService {
	
	static Logger logger = Logger.getLogger(UserLoggingServiceImpl.class);

	@Override
	public boolean addUserEventRequest(String key, String mailtoUri, String space, String resourceURI, String actionType, String content) {
		try{
			logger.info("external logging: " +mailtoUri + " " +actionType + " " +content + " " +resourceURI + " " +key);
			// instance of the service
			if(!space.equals(MatureClientWSUtils.KENT_SPACE)){
				logger.info("external logging CN");
				at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessProxy();
				try {
					boolean success = client.addUserEvent(mailtoUri, actionType, content, resourceURI, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("status for " + resourceURI
							+ ": " + success);
					return success;
				} catch (at.tug.mature.clientws.cn.entities.InvalidKeyException e) {
					logger.error(e);
					return false;
				}
			}else{
				logger.info("external logging kent");
				
				at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy client = new at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessProxy();
				try {
					boolean success = client.addUserEvent(mailtoUri, actionType, content, resourceURI, MatureClientWSUtils.UNIVERSAL_KEY);
					logger.info("status for " + resourceURI
							+ ": " + success);
					return success;
				} catch (at.tug.mature.clientws.kent.entities.InvalidKeyException e) {
					logger.error(e);
					return false;
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return false;
	}

}
