/*
 * 
 */
package de.fzi.ipe.soboleo.queries.rdf.skos;

import de.fzi.ipe.soboleo.event.execution.QueryEvent;

 
/**
 * Event to ask the SkosEventBusAdaptor for a client side taxonomy. If "update" 
 * is set true, the returned clientsidetaxonomy will get updated 
 * automatically (note, however, that this does not work if it is send to the 
 * browser. 
 * @author FZI - Information Process Engineering 
 * Created on 18.02.2009 by zach
 */
public class GetClientSideTaxonomy extends QueryEvent{
	
	private boolean update = false;
	
	public GetClientSideTaxonomy(boolean update) {
		this.update = update;
	}
	
	@SuppressWarnings("unused")
	private GetClientSideTaxonomy() {;}
	
	public boolean getUpdate() {
		return update;
	}
	
	
}
