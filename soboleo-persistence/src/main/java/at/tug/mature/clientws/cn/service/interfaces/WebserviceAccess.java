/**
 * WebserviceAccess.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.tug.mature.clientws.cn.service.interfaces;

public interface WebserviceAccess extends java.rmi.Remote {
    public boolean setUserRating(java.lang.String agentURI, int ratingScore, java.lang.String digitalResourceURI, java.lang.String agentKey) throws java.rmi.RemoteException, at.tug.mature.clientws.cn.entities.InvalidKeyException;
    public at.tug.mature.clientws.cn.entities.RatingAssignment[] getUserRatings(java.lang.String makerURI, java.lang.String digitalResourceURI, java.lang.String agentURI, java.lang.String agentKey) throws java.rmi.RemoteException, at.tug.mature.clientws.cn.entities.InvalidKeyException;
    public at.tug.mature.clientws.cn.entities.OverallRatingEntry getOverallRating(java.lang.String makerURI, java.lang.String digitalResourceURI, java.lang.String agentURI, java.lang.String agentKey) throws java.rmi.RemoteException, at.tug.mature.clientws.cn.entities.InvalidKeyException;
    public boolean userLogin(java.lang.String user, java.lang.String key) throws java.rmi.RemoteException, at.tug.mature.clientws.cn.entities.InvalidKeyException;
    public boolean addUserEvent(java.lang.String user, java.lang.String actionType, java.lang.String content, java.lang.String uri, java.lang.String key) throws java.rmi.RemoteException, at.tug.mature.clientws.cn.entities.InvalidKeyException;
}
