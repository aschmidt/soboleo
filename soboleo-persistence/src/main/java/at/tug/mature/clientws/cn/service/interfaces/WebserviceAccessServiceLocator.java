/**
 * WebserviceAccessServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.tug.mature.clientws.cn.service.interfaces;

public class WebserviceAccessServiceLocator extends org.apache.axis.client.Service implements at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessService {

    public WebserviceAccessServiceLocator() {
    }


    public WebserviceAccessServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WebserviceAccessServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WebserviceAccess
    private java.lang.String WebserviceAccess_address = "http://tug.mature-ip.eu:8080/socialservernorthumber/services/WebserviceAccess";

    public java.lang.String getWebserviceAccessAddress() {
        return WebserviceAccess_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WebserviceAccessWSDDServiceName = "WebserviceAccess";

    public java.lang.String getWebserviceAccessWSDDServiceName() {
        return WebserviceAccessWSDDServiceName;
    }

    public void setWebserviceAccessWSDDServiceName(java.lang.String name) {
        WebserviceAccessWSDDServiceName = name;
    }

    public at.tug.mature.clientws.cn.service.interfaces.WebserviceAccess getWebserviceAccess() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WebserviceAccess_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWebserviceAccess(endpoint);
    }

    public at.tug.mature.clientws.cn.service.interfaces.WebserviceAccess getWebserviceAccess(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessSoapBindingStub _stub = new at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessSoapBindingStub(portAddress, this);
            _stub.setPortName(getWebserviceAccessWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWebserviceAccessEndpointAddress(java.lang.String address) {
        WebserviceAccess_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (at.tug.mature.clientws.cn.service.interfaces.WebserviceAccess.class.isAssignableFrom(serviceEndpointInterface)) {
                at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessSoapBindingStub _stub = new at.tug.mature.clientws.cn.service.interfaces.WebserviceAccessSoapBindingStub(new java.net.URL(WebserviceAccess_address), this);
                _stub.setPortName(getWebserviceAccessWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WebserviceAccess".equals(inputPortName)) {
            return getWebserviceAccess();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://interfaces.service.mature.tug.at", "WebserviceAccessService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://interfaces.service.mature.tug.at", "WebserviceAccess"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WebserviceAccess".equals(portName)) {
            setWebserviceAccessEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
