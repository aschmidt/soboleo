package at.tug.mature.clientws.kent.service.interfaces;

public class WebserviceAccessProxy implements at.tug.mature.clientws.kent.service.interfaces.WebserviceAccess {
  private String _endpoint = null;
  private at.tug.mature.clientws.kent.service.interfaces.WebserviceAccess webserviceAccess = null;
  
  public WebserviceAccessProxy() {
    _initWebserviceAccessProxy();
  }
  
  public WebserviceAccessProxy(String endpoint) {
    _endpoint = endpoint;
    _initWebserviceAccessProxy();
  }
  
  private void _initWebserviceAccessProxy() {
    try {
      webserviceAccess = (new at.tug.mature.clientws.kent.service.interfaces.WebserviceAccessServiceLocator()).getWebserviceAccess();
      if (webserviceAccess != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)webserviceAccess)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)webserviceAccess)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (webserviceAccess != null)
      ((javax.xml.rpc.Stub)webserviceAccess)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public at.tug.mature.clientws.kent.service.interfaces.WebserviceAccess getWebserviceAccess() {
    if (webserviceAccess == null)
      _initWebserviceAccessProxy();
    return webserviceAccess;
  }
  
  public boolean setUserRating(java.lang.String agentURI, int ratingScore, java.lang.String digitalResourceURI, java.lang.String agentKey) throws java.rmi.RemoteException, at.tug.mature.clientws.kent.entities.InvalidKeyException{
    if (webserviceAccess == null)
      _initWebserviceAccessProxy();
    return webserviceAccess.setUserRating(agentURI, ratingScore, digitalResourceURI, agentKey);
  }
  
  public at.tug.mature.clientws.kent.entities.RatingAssignment[] getUserRatings(java.lang.String makerURI, java.lang.String digitalResourceURI, java.lang.String agentURI, java.lang.String agentKey) throws java.rmi.RemoteException, at.tug.mature.clientws.kent.entities.InvalidKeyException{
    if (webserviceAccess == null)
      _initWebserviceAccessProxy();
    return webserviceAccess.getUserRatings(makerURI, digitalResourceURI, agentURI, agentKey);
  }
  
  public at.tug.mature.clientws.kent.entities.OverallRatingEntry getOverallRating(java.lang.String makerURI, java.lang.String digitalResourceURI, java.lang.String agentURI, java.lang.String agentKey) throws java.rmi.RemoteException, at.tug.mature.clientws.kent.entities.InvalidKeyException{
    if (webserviceAccess == null)
      _initWebserviceAccessProxy();
    return webserviceAccess.getOverallRating(makerURI, digitalResourceURI, agentURI, agentKey);
  }
  
  public boolean userLogin(java.lang.String user, java.lang.String key) throws java.rmi.RemoteException, at.tug.mature.clientws.kent.entities.InvalidKeyException{
    if (webserviceAccess == null)
      _initWebserviceAccessProxy();
    return webserviceAccess.userLogin(user, key);
  }
  
  public boolean addUserEvent(java.lang.String user, java.lang.String actionType, java.lang.String content, java.lang.String uri, java.lang.String key) throws java.rmi.RemoteException, at.tug.mature.clientws.kent.entities.InvalidKeyException{
    if (webserviceAccess == null)
      _initWebserviceAccessProxy();
    return webserviceAccess.addUserEvent(user, actionType, content, uri, key);
  }
  
  
}