/**
 * OverallRatingEntry.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.tug.mature.clientws.kent.entities;

public class OverallRatingEntry  implements java.io.Serializable {
    private int ratinFreq;

    private double ratingScore;

    public OverallRatingEntry() {
    }

    public OverallRatingEntry(
           int ratinFreq,
           double ratingScore) {
           this.ratinFreq = ratinFreq;
           this.ratingScore = ratingScore;
    }


    /**
     * Gets the ratinFreq value for this OverallRatingEntry.
     * 
     * @return ratinFreq
     */
    public int getRatinFreq() {
        return ratinFreq;
    }


    /**
     * Sets the ratinFreq value for this OverallRatingEntry.
     * 
     * @param ratinFreq
     */
    public void setRatinFreq(int ratinFreq) {
        this.ratinFreq = ratinFreq;
    }


    /**
     * Gets the ratingScore value for this OverallRatingEntry.
     * 
     * @return ratingScore
     */
    public double getRatingScore() {
        return ratingScore;
    }


    /**
     * Sets the ratingScore value for this OverallRatingEntry.
     * 
     * @param ratingScore
     */
    public void setRatingScore(double ratingScore) {
        this.ratingScore = ratingScore;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OverallRatingEntry)) return false;
        OverallRatingEntry other = (OverallRatingEntry) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.ratinFreq == other.getRatinFreq() &&
            this.ratingScore == other.getRatingScore();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getRatinFreq();
        _hashCode += new Double(getRatingScore()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OverallRatingEntry.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities.mature.tug.at", "OverallRatingEntry"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratinFreq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.mature.tug.at", "ratinFreq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratingScore");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.mature.tug.at", "ratingScore"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
