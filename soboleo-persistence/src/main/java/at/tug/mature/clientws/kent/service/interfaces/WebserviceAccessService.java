/**
 * WebserviceAccessService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package at.tug.mature.clientws.kent.service.interfaces;

public interface WebserviceAccessService extends javax.xml.rpc.Service {
    public java.lang.String getWebserviceAccessAddress();

    public at.tug.mature.clientws.kent.service.interfaces.WebserviceAccess getWebserviceAccess() throws javax.xml.rpc.ServiceException;

    public at.tug.mature.clientws.kent.service.interfaces.WebserviceAccess getWebserviceAccess(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
