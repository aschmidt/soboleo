package de.fzi.ipe.soboleo.lucene.eventbus.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.beans.document.DocumentType;
import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocument;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocumentTag;
import de.fzi.ipe.soboleo.event.officedocument.RemoveOfficeDocument;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;
import de.fzi.ipe.soboleo.lucene.eventubus.LuceneEventBusAdaptor;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetAllDocuments;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetAllOfficeDocuments;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentByUri;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentInputStream;

public class TestSearchTestspace {

	private static LuceneEventBusAdaptor luceneeventbus;

	private static String directoryTestLucene = "C:\\soboleo\\spaces\\testspace\\";
	private static String directoryStorage = "c:\\soboleo\\spaces\\testspace\\storage\\";

	// private static String directoryTestLucene = "C:\\soboleo\\spaces\\fzi\\";
	// private static String directoryStorage =
	// "c:\soboleo\spaces\fzi\storage\";

	private static EventBus eventBus;

	protected static final Logger logger = Logger
			.getLogger(TestSearchTestspace.class);

	private static String documentUri = "";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		try {
			logger.info("setup eventbus");
			eventBus = new EventBus();
			// set eventbus
			luceneeventbus = new LuceneEventBusAdaptor(new File(
					directoryTestLucene, "index"), new File(directoryStorage),
					eventBus, null);
			eventBus.addListener(luceneeventbus);
		} catch (Exception e) {
			logger.error(e);
		}

	}


		
	@Test
	public void searchAllDocuments() {
		// searches only the added document from index


		GetDocumentsSearchResult gu = new GetDocumentsSearchResult("");
		
		
		
		Set<String> conceptUris=new HashSet<String>();
		conceptUris.add("http://soboleo.com/ns/1.0#space-newroom-gen115");
		
		gu.setExtractedConceptURIs(conceptUris);
		
		//gu.setAnnotationOwner("annalabrador");
		SearchResult result = (SearchResult) luceneeventbus.process(gu);

		// one result is expected only
		logger.info("result size for owner annalabrador: " + result.getLuceneResult().length());

		logger.info("search by uri was successful!");

		try {
			// the following infomation is needed by the downloader servlet
			IndexDocument id = (IndexDocument) result.getLuceneResult().doc(0);

			logger.info("id: " + id.getURL() + " - uri: " + id.getURI());
			
			logger.info("doc is owned by: " +id.getDocumentOwner());
			
			// semantic annotations
			logger.info("sem annotations :" +id.getSemanticAnnotations().size());
			
			for (SemanticAnnotation sia:id.getSemanticAnnotations())
			{
				logger.info("sia: " +sia.getUri() + " is owned by " +sia.getUserId());
			}
			
		} catch (Exception e) {
			logger.error(e);
		}

	}

	
	
}
