package de.fzi.ipe.soboleo.lucene.eventbus.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fzi.ipe.soboleo.beans.document.DocumentType;
import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocument;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocumentTag;
import de.fzi.ipe.soboleo.event.officedocument.RemoveOfficeDocument;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;
import de.fzi.ipe.soboleo.lucene.eventubus.LuceneEventBusAdaptor;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetAllDocuments;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetAllOfficeDocuments;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentByUri;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentInputStream;

public class TestAddSemanticAnnotation {

	private static LuceneEventBusAdaptor luceneeventbus;

	private static String directoryTestLucene = "C:\\soboleo\\spaces\\fzi\\";
	private static String directoryStorage = "c:\\soboleo\\spaces\\fzi\\storage\\";

	// private static String directoryTestLucene = "C:\\soboleo\\spaces\\fzi\\";
	// private static String directoryStorage =
	// "c:\soboleo\spaces\fzi\storage\";

	private static EventBus eventBus;

	protected static final Logger logger = Logger
			.getLogger(TestAddSemanticAnnotation.class);

	private static String documentUri = "";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		try {
			logger.info("setup eventbus");
			eventBus = new EventBus();
			// set eventbus
			luceneeventbus = new LuceneEventBusAdaptor(new File(
					directoryTestLucene, "index"), new File(directoryStorage),
					eventBus, null);
			eventBus.addListener(luceneeventbus);
		} catch (Exception e) {
			logger.error(e);
		}

	}

	@Test
	public void testAddOfficeDocument() {

		logger.info("add office document testwordx.doc");

		File f = new File("src/test/resources/files/testwordx.docx");

		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " + fis.available());

			AddOfficeDocument aod = new AddOfficeDocument(DocumentType.doc,
					fis, "testwordx.docx", "testuser");

			luceneeventbus.receiveEvent(aod);

			logger.info("uri of added document : "
					+ luceneeventbus.getLastAddedDocumentKey());

		} catch (Exception e) {
			logger.error(e);
		}

	}

		
	@Test
	public void addSemanticAnnotations()
	{
		logger.info("add semantic annotation for " +luceneeventbus.getLastAddedDocumentKey());
		
		SemanticAnnotation sia=new SemanticAnnotation();
		sia.setUri("testuri#test");
		sia.setUserId("annalabrador");
		

		AddOfficeDocumentTag aodt=new AddOfficeDocumentTag(luceneeventbus.getLastAddedDocumentKey(), sia);
		luceneeventbus.receiveEvent(aodt);
		
		try
		{
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			
		}
				
		
		SemanticAnnotation sia2=new SemanticAnnotation();
		sia2.setUri("testuri2");
		sia2.setUserId("annalabrador");
		
		AddOfficeDocumentTag aodt2=new AddOfficeDocumentTag(luceneeventbus.getLastAddedDocumentKey(), sia2);
		luceneeventbus.receiveEvent(aodt2);
		
		logger.info("add second annotation");
		
		try
		{
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			
		}
		
	}

	
	@Test
	public void searchOfficeDocumentByUri() {
		// searches only the added document from index

		logger.info("search document by uri :"
				+ luceneeventbus.getLastAddedDocumentKey());

		GetOfficeDocumentByUri gu = new GetOfficeDocumentByUri(luceneeventbus
				.getLastAddedDocumentKey());
		LuceneResult result = (LuceneResult) luceneeventbus.process(gu);

		// one result is expected only
		logger.info("result size: " + result.length());

		Assert.assertTrue(result.length() == 1);

		logger.info("search by uri was successful!");

		try {
			// the following infomation is needed by the downloader servlet
			IndexDocument id = (IndexDocument) result.doc(0);

			logger.info("id: " + id.getURL() + " - uri: " + id.getURI());
			
			logger.info("doc is owned by: " +id.getDocumentOwner());
			
			// semantic annotations
			logger.info("sem annotations :" +id.getSemanticAnnotations().size());
			
			for (SemanticAnnotation sia:id.getSemanticAnnotations())
			{
				logger.info("sia: " +sia.getUri() + " is owned by " +sia.getUserId());
			}
			
		} catch (Exception e) {
			logger.error(e);
		}

	}

	
	
	@Test
	public void searchOfficeDocumentByDocumentOwner() {
		// searches only the added document from index

		logger.info("search document by uri :"
				+ luceneeventbus.getLastAddedDocumentKey());

		GetDocumentsSearchResult gu = new GetDocumentsSearchResult("");
		gu.setDocOwner("testuser");
		SearchResult result = (SearchResult) luceneeventbus.process(gu);

		// one result is expected only
		logger.info("result size for owner annalabrador: " + result.getLuceneResult().length());

		logger.info("search by uri was successful!");

		try {
			// the following infomation is needed by the downloader servlet
			IndexDocument id = (IndexDocument) result.getLuceneResult().doc(0);

			logger.info("id: " + id.getURL() + " - uri: " + id.getURI());
			
			logger.info("doc is owned by: " +id.getDocumentOwner());
			
			// semantic annotations
			logger.info("sem annotations :" +id.getSemanticAnnotations().size());
			
			for (SemanticAnnotation sia:id.getSemanticAnnotations())
			{
				logger.info("sia: " +sia.getUri() + " is owned by " +sia.getUserId());
			}
			
		} catch (Exception e) {
			logger.error(e);
		}

	}

	
	
	@Test
	public void searchOfficeDocumentByAnnotationOwner() {
		// searches only the added document from index

		logger.info("search document by uri :"
				+ luceneeventbus.getLastAddedDocumentKey());

		GetDocumentsSearchResult gu = new GetDocumentsSearchResult("");
		
		Set<String> conceptUris=new HashSet<String>();
		conceptUris.add("http://soboleo.com/ns/1.0#space-fzi-gen10");
		
		gu.setExtractedConceptURIs(conceptUris);
		
		//gu.setAnnotationOwner("annalabrador");
		SearchResult result = (SearchResult) luceneeventbus.process(gu);

		// one result is expected only
		logger.info("result size for owner annalabrador: " + result.getLuceneResult().length());

		logger.info("search by uri was successful!");

		try {
			// the following infomation is needed by the downloader servlet
			IndexDocument id = (IndexDocument) result.getLuceneResult().doc(0);

			logger.info("id: " + id.getURL() + " - uri: " + id.getURI());
			
			logger.info("doc is owned by: " +id.getDocumentOwner());
			
			// semantic annotations
			logger.info("sem annotations :" +id.getSemanticAnnotations().size());
			
			for (SemanticAnnotation sia:id.getSemanticAnnotations())
			{
				logger.info("sia: " +sia.getUri() + " is owned by " +sia.getUserId());
			}
			
		} catch (Exception e) {
			logger.error(e);
		}

	}

	
	@Test
	public void searchOfficeDocumentByAnnotationOwnerAndConcept() {
		// searches only the added document from index

		logger.info("search document by annotation owner and concept :"
				+ luceneeventbus.getLastAddedDocumentKey());

		GetDocumentsSearchResult gu = new GetDocumentsSearchResult("");

		Set<String> mustConceptURIs=new HashSet<String>();
		mustConceptURIs.add("testuri");
		gu.setMustConceptURIs(mustConceptURIs);
		gu.setAnnotationOwner("annalabrador");
		SearchResult result = (SearchResult) luceneeventbus.process(gu);

		// one result is expected only
		logger.info("result size for owner annalabrador: " + result.getLuceneResult().length());

		logger.info("search by uri was successful!");

		try {
			// the following infomation is needed by the downloader servlet
			IndexDocument id = (IndexDocument) result.getLuceneResult().doc(0);

			logger.info("id: " + id.getURL() + " - uri: " + id.getURI());
			
			logger.info("doc is owned by: " +id.getDocumentOwner());
			
			// semantic annotations
			logger.info("sem annotations :" +id.getSemanticAnnotations().size());
			
			for (SemanticAnnotation sia:id.getSemanticAnnotations())
			{
				logger.info("sia: " +sia.getUri() + " is owned by " +sia.getUserId());
			}
			
		} catch (Exception e) {
			logger.error(e);
		}

	}

	
	
	//@Test
	public void deleteOfficeDocument() {
		logger.info("documentUri: " + luceneeventbus.getLastAddedDocumentKey());

		RemoveOfficeDocument rod = new RemoveOfficeDocument(luceneeventbus
				.getLastAddedDocumentKey());
		luceneeventbus.receiveEvent(rod);
	}

}
