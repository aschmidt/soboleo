package de.fzi.ipe.soboleo.user.login.client.test;

import org.apache.log4j.Logger;
import org.junit.Test;

import de.fzi.ipe.soboleo.user.login.client.ExternalUserRegistrationService;
import de.fzi.ipe.soboleo.user.login.client.ExternalUserRegistrationServiceImpl;

public class TestExternalUserRegistrationService {

	protected static final Logger logger = Logger
	.getLogger(TestExternalUserRegistrationService.class);

	@Test
	public void testLogin()
	{
		ExternalUserRegistrationService userRegistrationService=new ExternalUserRegistrationServiceImpl();
		boolean success = userRegistrationService.login("mailto:braun@fzi.de", "FischersFritzFischtFrischeFische");
		logger.info("login: " +success);
		System.out.println("login: " +success);
	}
	
}
