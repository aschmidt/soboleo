package de.fzi.ipe.soboleo.data.documentstorage.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import de.fzi.ipe.soboleo.beans.document.DocumentType;
import de.fzi.ipe.soboleo.data.documentstorage.DocumentStorage;
import de.fzi.ipe.soboleo.data.documentstorage.DocumentStorageImpl;

public class TestDocumentStorage {

	protected static final Logger logger = Logger
			.getLogger(TestDocumentStorage.class);

	String storagePath = "src/test/resources/storage";

	@Test
	public void testInitialization() {
		DocumentStorage storage = new DocumentStorageImpl();
		boolean status = storage.initializeStorage(storagePath);
		logger.info("status : " + status);

		Assert.assertTrue(status);

	}

	@Test
	public void testAddQueryAndRemoveFile() {
		DocumentStorage storage = new DocumentStorageImpl();
		boolean status = storage.initializeStorage(storagePath);

		// load a file from resources/files to store it.
		try {
			File f = new File("src/test/resources/files/test.pdf");
			FileInputStream fis = new FileInputStream(f);
			logger.info("file test.pdf loaded");

			String key = "" + System.currentTimeMillis();
			logger.info("key : " + key);

			status = storage.addDocument(key, DocumentType.pdf, fis);
			logger.info("addDocument status: " + status);
			Assert.assertTrue(status);

			logger.info("load the file: " + key);
			InputStream in = storage.loadDocument(key, DocumentType.pdf.toString());
			Assert.assertNotNull(in);
			in.close();

			logger.info("delete the file: " + key);
			status = storage.removeDocument(key, DocumentType.pdf.toString());
			logger.info("removeDocument status: " + status);
			Assert.assertTrue(status);

		} catch (Exception e) {
			logger.error("setup error: file test.pdf not available");
			Assert.assertTrue(false);
		}

	}

}
