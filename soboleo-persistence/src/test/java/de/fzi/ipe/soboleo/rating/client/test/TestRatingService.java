package de.fzi.ipe.soboleo.rating.client.test;

import org.apache.log4j.Logger;
import org.junit.Test;


import de.fzi.ipe.soboleo.beans.rating.Rating;
import de.fzi.ipe.soboleo.rating.client.RatingService;
import de.fzi.ipe.soboleo.rating.client.RatingServiceImpl;

public class TestRatingService {

	
	protected static final Logger logger = Logger
	.getLogger(TestRatingService.class);

	@Test 
	public void testSetUserRating(){
		RatingService ratingService=new RatingServiceImpl();
		boolean success = ratingService.setUserRating("FischersFritzFischtFrischeFische", "mailto:braun@fzi.de", "default", "http://www.fzi.de", 3);
		logger.info("set rating: " +success);
		System.out.println("set rating: " +success);
	}
	
	@Test 
	public void testGetUserRating(){
		RatingService ratingService=new RatingServiceImpl();
		Rating rating = ratingService.getUserRating("FischersFritzFischtFrischeFische", "mailto:braun@fzi.de", "default", "mailto:braun@fzi.de", "http://www.fzi.de");
		logger.info("rating: " +rating.getScore());
		System.out.println("rating: " +rating.getScore());
	}	
	
	@Test
	public void testGetOverallRating()
	{
		RatingService ratingService=new RatingServiceImpl();
		Rating rating=ratingService.getOverallRating("FischersFritzFischtFrischeFische", "mailto:braun@fzi.de", "default","http://www.fzi.de");
		logger.info("rating: " +rating.getScore());
		System.out.println("rating: " +rating.getScore());
	}
	
}
