package de.fzi.ipe.soboleo.execution.content.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import de.fzi.ipe.soboleo.beans.document.DocumentType;
import de.fzi.ipe.soboleo.execution.content.DocumentContentExtractor;

public class TestDocumentContentExtractor {

	protected static final Logger logger = Logger
			.getLogger(TestDocumentContentExtractor.class);

	
	@Test
	public void extractTextFromWordFile() {
		File f = new File("src/test/resources/files/testword.doc");
		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " +fis.available());
			
			String text=DocumentContentExtractor.getExtractContentFromFile(fis, DocumentType.doc);
			
			logger.info("text in word : " +text);
			
			Assert.assertTrue(text.length() > 0);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}

	
	@Test
	public void extractTextFromWordDocxFile() {
		File f = new File("src/test/resources/files/testwordx.docx");
		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " +fis.available());
			
			String text=DocumentContentExtractor.getExtractContentFromFile(fis, DocumentType.doc);
			
			logger.info("text in word docx : " +text);
			
			Assert.assertTrue(text.length() > 0);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}
	
	
	@Test
	public void extractTextFromPDFFile() {
		File f = new File("src/test/resources/files/testa.pdf");
		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " +fis.available());
			
			String text=DocumentContentExtractor.getExtractContentFromFile(fis, DocumentType.pdf);
			
			logger.info("text in pdf : " +text);
			
			Assert.assertTrue(text.length() > 0);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}
	
	
	@Test
	public void extractTextFromExcelFile() {
		File f = new File("src/test/resources/files/test.xls");
		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " +fis.available());
			
			String text=DocumentContentExtractor.getExtractContentFromFile(fis, DocumentType.xls);
			
			logger.info("text in xls file : " +text);
			
			Assert.assertTrue(text.length() > 0);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}

	
	@Test
	public void extractTextFromExcelxFile() {
		File f = new File("src/test/resources/files/test.xlsx");
		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " +fis.available());
			
			String text=DocumentContentExtractor.getExtractContentFromFile(fis, DocumentType.xls);
			
			logger.info("text in pdf : " +text);
			
			Assert.assertTrue(text.length() > 0);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}


	@Test
	public void extractTextFromPPTFile() {
		File f = new File("src/test/resources/files/text.ppt");
		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " +fis.available());
			
			String text=DocumentContentExtractor.getExtractContentFromFile(fis, DocumentType.ppt);
			
			logger.info("text in ppt file : " +text);
			
			Assert.assertTrue(text.length() > 0);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}

	
	@Test
	public void extractTextFromPPTxFile() {
		File f = new File("src/test/resources/files/textx.pptx");
		try {
			FileInputStream fis = new FileInputStream(f);
			logger.info("fis stream : " +fis.available());
			
			String text=DocumentContentExtractor.getExtractContentFromFile(fis, DocumentType.ppt);
			
			logger.info("text in pdf : " +text);
			
			Assert.assertTrue(text.length() > 0);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}

	}



}
