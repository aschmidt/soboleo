package de.fzi.ipe.soboleo.tagcloud.rpc.service.test;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.tagcloud.rpc.TagCloudService;
import de.fzi.ipe.soboleo.tagcloud.service.TagCloudServiceImpl;

@Ignore
public class TestTagCloudService {

	
	private final Logger logger = Logger.getLogger(this.getClass());

	
	@Test
	public void testGetSearchRequestsOverall()
	{
		logger.info("testGetSearchRequestsOverall");
		TagCloudService tcsi=new TagCloudServiceImpl();		
		
		List<ConceptStatistic> statistic=tcsi.getSearchRequestsOverall("test", 30, Language.en);
		
		for (ConceptStatistic concept:statistic)
		{
			LocalizedString prefLabel = concept.getConcept().getBestFitText(SKOS.PREF_LABEL,Language.en);
			logger.info("pref label : " +prefLabel.getString());
	
		}
	}
	
	@Test
	public void testGetUsedConceptsInPersonAnnotations(){
		logger.info("testGetUsedConceptsInPersonAnnotations");
		TagCloudService tcsi=new TagCloudServiceImpl();
		
		List<ConceptStatistic> statistic=tcsi.getUsedConceptsInPersonAnnotations("test", 30, Language.en, EventSenderCredentials.SERVER);
		
		for (ConceptStatistic concept:statistic)
		{
			LocalizedString prefLabel = concept.getConcept().getBestFitText(SKOS.PREF_LABEL,Language.en);
			logger.info("pref label : " +prefLabel.getString());
	
		}
	}
}
