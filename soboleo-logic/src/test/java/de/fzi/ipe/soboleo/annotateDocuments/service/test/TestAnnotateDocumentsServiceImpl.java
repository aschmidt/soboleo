package de.fzi.ipe.soboleo.annotateDocuments.service.test;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import de.fzi.ipe.soboleo.annotateDocuments.client.rpc.AnnotateDocumentsService;
import de.fzi.ipe.soboleo.annotateDocuments.service.AnnotateDocumentsServiceImpl;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.user.User;

@Ignore
public class TestAnnotateDocumentsServiceImpl {

	@Autowired private Server server;
	private static final Logger logger = Logger.getLogger(TestAnnotateDocumentsServiceImpl.class);
	
	
	static String userkey="3JB12156L225E512B7546S4V416M766O3L7762";
	
	static EventSenderCredentials creds;
	
	static AnnotateDocumentsService service;
	
	@Before
	public void setUp() throws Exception {

		User user = server.getUserDatabase().getUserForKey(userkey);
		if (user != null) creds = new EventSenderCredentials(user.getURI(), user.getName(), user.getKey());

		logger.info("creds: " +creds.getSenderName());
		
		service=new AnnotateDocumentsServiceImpl();
	}
	
	@Test
	public void testGetOfficeDocumentByURI()
	{
		logger.info("getOfficeDocumentByURI");
		
		try {
			service.getOfficeDocumentByURI("fzi", creds, null, "doc1301334488040");
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
}
