package de.fzi.ipe.soboleo.webdocument.officedocument.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import de.fzi.ipe.soboleo.webdocument.officedocument.DocumentSaver;

@Ignore
public class TestDocumentUploader {

	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Test
	public void testUpload()
	{
		
		File f = new File("src/test/resources/files/testwordx.docx");
		String userkey="3JB12156L225E512B7546S4V416M766O3L7762";
		
		
		try
		{
			InputStream fis = new FileInputStream(f);
		
			DocumentSaver ds=new DocumentSaver();
			String docuri=ds.getUploadResult(userkey, "testdoc1.doc", "doc", fis);
			logger.info("docuri:" +docuri);
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		
	}
	
}
