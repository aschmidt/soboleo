package de.fzi.ipe.soboleo.tagcloud.rpc.service.test;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.tagcloud.rpc.TagCloudService;
import de.fzi.ipe.soboleo.tagcloud.service.TagCloudServiceImpl;

@Ignore
public class TestTagCloudServiceUserAnnotations {

	
	private final Logger logger = Logger.getLogger(this.getClass());

	
	@Test
	public void testGetUserAnnotations()
	{
		logger.info("testGetUserAnnotations");
		TagCloudService tcsi=new TagCloudServiceImpl();
		
		List<ConceptStatistic> statistic=tcsi.getUserAnnotations("681V454J1P3H4W3B367BB79615U184N22356I3E", "test", Language.de, EventSenderCredentials.SERVER);
//		List<ConceptStatistic> statistic=tcsi.getUserAnnotations("3QH1N6V6P2T536E3OJ5K5R1GJ28155B1X6E1V", "fzi", Language.de, EventSenderCredentials.SERVER);
		
		for (ConceptStatistic concept:statistic)
		{
			LocalizedString prefLabel = concept.getConcept().getBestFitText(SKOS.PREF_LABEL,Language.de);
			logger.info("pref label : " +prefLabel.getString() + " count : " +concept.getConceptId());
	
		}
	}
}
