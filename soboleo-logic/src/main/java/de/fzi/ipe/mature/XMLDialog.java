package de.fzi.ipe.mature;

import java.util.ArrayList;
import java.util.List;

/**
 * @deprecated
 */
public class XMLDialog {
	private String dialogID, dialogUri,dialogTitle,dialogContent,dialogType;
	private XMLConcept concept;
	private XMLResource resource;
	private XMLUser[] initiator;
	private String spaceUri;
	private List<XMLUser> participants = new ArrayList<XMLUser>();
	
	public XMLDialog(String dialogID, String dialogURI, String dialogTitle, String dialogContent, DialogType dialogType, String spaceURI, XMLUser initiator) {
		this.dialogID = dialogID;
		this.dialogUri = dialogURI;
		this.dialogTitle = dialogTitle;
		this.dialogContent = dialogContent;
		this.dialogType = dialogType.toString();
		this.spaceUri = spaceURI;
		this.initiator = new XMLUser[] {initiator.clone()}; //clone is important, because the initiator 
		// must not be the same object as one of the participants. Because in such a case
		// Xstream would not print the user twice - as defined in the XML - but rather write
		// a reference. The array of length one is important, because the <user> tag needs to be nested in an
		// initiator tag.
	}
	
	public String getSpaceURI(){
		return spaceUri;
	}
	public void setSpaceURI(String spaceUri){
		this.spaceUri = spaceUri;
	}
	public String getDialogURI(){
		return dialogUri;
	}
	public void setDialogURI(String dialogUri){
		this.dialogUri = dialogUri;
	}
	public String getDialogID(){
		return dialogID;
	}
	public void setDialogID(String dialogID){
		this.dialogID = dialogID;
	}
	public String getDialogTitle(){
		return dialogTitle;
	}
	public void setDialogTitle(String dialogTitle){
		this.dialogTitle = dialogTitle;
	}
	public String getDialogContent(){
		return dialogContent;
	}
	public void setDialogContent(String dialogContent){
		this.dialogContent = dialogContent;
	}
	public String getDialogType(){
		return dialogType;
	}
	public void setDialogType(DialogType dialogType){
		this.dialogType = dialogType.toString();
	}
	public XMLConcept getConcept(){
		return concept;
	}
	public void setConcept(XMLConcept concept){
		this.concept = concept;
	}
	public XMLResource getResource(){
		return resource;
	}
	public void setResource(XMLResource resource){
		this.resource = resource;
	}
	public XMLUser getInitiator(){
		if(initiator.length > 0) return initiator[0];
		else return null;
	}
	public void setInitiator(XMLUser initiator){
		this.initiator = new XMLUser[] {initiator.clone()};
	}
	public List<XMLUser> getParticipants(){
		return participants;
	}
	public void addParticipant(XMLUser participant){
		participants.add(participant);
	}
	

	public static class XMLConcept {
		public String conceptUri;
		public XMLLabel prefLabel;
		
		public XMLConcept(String conceptURI, XMLLabel prefLabel) {
			this.conceptUri = conceptURI;
			this.prefLabel = prefLabel;
		}
	}
	
	public static class XMLResource {
		public String resourceUri, resourceUrl;
		
		public XMLResource(String resourceUri, String resourceUrl) {
			this.resourceUri = resourceUri;
			this.resourceUrl = resourceUrl;
		}
	}
	
	public static class XMLLabel {
		String lang,content;
		
		public XMLLabel(String lang, String content) {
			this.lang = lang;
			this.content = content;
		}
	}
	
	public static class XMLUser {
		private String userUri, userEmail,userName;
		
		public XMLUser(String userURI, String userEmail, String userName) {
			this.userUri = userURI;
			this.userEmail = userEmail;
			this.userName = userName;
		}
		
		public XMLUser clone() { return new XMLUser(userUri, userEmail, userName); }
		public String getUserURI(){
			return this.userUri;
		}
		public String getUserEmail(){
			return this.userEmail;
		}
		public String getUserName(){
			return this.userName;
		}
	}
	
	public enum DialogType {
		KM_DG("KM-DG"),
		CDR_DG("CDR-DG");
		
		private final String type;
		
		DialogType(String type) {
			this.type = type;
		}
		
		public String toString() {
			return type;
		}
	}
	
}

