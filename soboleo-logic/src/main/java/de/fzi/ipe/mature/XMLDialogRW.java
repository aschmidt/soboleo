package de.fzi.ipe.mature;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

import de.fzi.ipe.mature.XMLDialog.DialogType;
import de.fzi.ipe.mature.XMLDialog.XMLConcept;
import de.fzi.ipe.mature.XMLDialog.XMLLabel;
import de.fzi.ipe.mature.XMLDialog.XMLResource;
import de.fzi.ipe.mature.XMLDialog.XMLUser;
import de.fzi.ipe.soboleo.beans.dialog.ConceptDialog;
import de.fzi.ipe.soboleo.beans.dialog.WebdocumentDialog;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.user.User;

/**
 * A small class to aid in reading and writing the Dialog XML representation.
 * 
 * @author zacharias, braun
 * @deprecated
 */
public class XMLDialogRW {

	private XStream xstream;

	public XMLDialogRW() {
		initialize(false);
	}

	public XMLDialogRW(boolean writeCDATA) {
		initialize(writeCDATA);
	}

	private void initialize(boolean writeCDATA) {
		if (writeCDATA) {
			xstream = new XStream(new DomDriver() {
				public HierarchicalStreamWriter createWriter(Writer out) {
					return new PrettyPrintWriter(out) {
						boolean cdata = false;

						@Override
						public void startNode(String name, @SuppressWarnings("rawtypes") Class clazz) {
							super.startNode(name, clazz);
							cdata = (name.equals("dialogContent"));
						}

						protected void writeText(QuickWriter writer, String text) {
							if (cdata) {
								writer.write("<![CDATA[");
								writer.write(text);
								writer.write("]]>");
							} else {
								writer.write(text);
							}
						}
					};
				}
			});
		} else
			xstream = new XStream();
		xstream.alias("dialog", XMLDialog.class);
		xstream.alias("user", XMLUser.class);
		xstream.registerConverter(new XMLLabelConverter());
		xstream.alias("resource", XMLResource.class);
	}

	public String toString(XMLDialog dialog) {
		return xstream.toXML(dialog);
	}

	public XMLDialog fromString(String string) {
		return (XMLDialog) xstream.fromXML(string);
	}

	public XMLDialog createXMLConceptDialog(ConceptDialog cd, String spaceURI,
			LocalizedString prefLabel, User initiator, Set<User> participants)
			throws IOException {
		String dialogID = cd.getURI().split("#")[1];
		XMLUser xmlInitiator = new XMLUser(initiator.getURI(),
				initiator.getEmail(), initiator.getName());
		XMLLabel label = new XMLLabel(prefLabel.getLanguage().getAcronym(),
				prefLabel.getString());
		XMLConcept concept = new XMLConcept(cd.getAbout(), label);
		XMLDialog dialog = new XMLDialog(dialogID, cd.getURI(), cd.getTitle(),
				"", DialogType.KM_DG, spaceURI, xmlInitiator);
		dialog.setConcept(concept);
		for (User user : participants) {
			XMLUser participant = new XMLUser(user.getURI(), user.getEmail(),
					user.getName());
			dialog.addParticipant(participant);
		}
		return dialog;
	}

	public XMLDialog createXMLWebDocumentDialog(WebdocumentDialog wd,
			String spaceURI, String webUrl, User initiator,
			Set<User> participants) throws IOException {
		String dialogID = wd.getURI().split("#")[1];
		XMLUser xmlInitiator = new XMLUser(initiator.getURI(),
				initiator.getEmail(), initiator.getName());
		XMLResource resource = new XMLResource(wd.getAbout(), webUrl);
		XMLDialog dialog = new XMLDialog(dialogID, wd.getURI(), wd.getTitle(),
				"", DialogType.CDR_DG, spaceURI, xmlInitiator);
		dialog.setResource(resource);
		for (User user : participants) {
			XMLUser participant = new XMLUser(user.getURI(), user.getEmail(),
					user.getName());
			dialog.addParticipant(participant);
		}
		return dialog;
	}

	public static XMLDialog createTestXMLConceptDialog() {
		XMLUser user1 = new XMLUser("http://soboleo.com/ns/1.0#users-db-gen1",
				"braun@fzi.de", "Simone Braun");
		XMLUser user2 = new XMLUser("http://soboleo.com/ns/1.0#users-db-gen2",
				"a.ravenscroft@londonmet.ac.uk", "Andrew Ravenscroft");
		XMLLabel label = new XMLLabel("en", "Java Programming");
		XMLConcept concept = new XMLConcept(
				"http://soboleo.com/ns/1.0#space-default-gen2", label);
		XMLDialog dialog = new XMLDialog("space-default-gen1",
				"http://soboleo.com/ns/1.0#space-default-gen1",
				"Java Programming", "Some content", DialogType.KM_DG,
				"default", user1);
		dialog.setConcept(concept);
		dialog.addParticipant(user1);
		dialog.addParticipant(user2);
		return dialog;
	}

	public static XMLDialog createTestXMLWebDocumentDialog() {
		XMLUser user1 = new XMLUser("http://soboleo.com/ns/1.0#users-db-gen1",
				"braun@fzi.de", "Simone Braun");
		XMLUser user2 = new XMLUser("http://soboleo.com/ns/1.0#users-db-gen2",
				"a.ravenscroft@londonmet.ac.uk", "Andrew Ravenscroft");
		XMLResource resource = new XMLResource(
				"http://soboleo.com/ns/1.0#space-default-gen2",
				"http://www.fzi.de");
		XMLDialog dialog = new XMLDialog("space-default-gen1",
				"http://soboleo.com/ns/1.0#space-default-gen1",
				"Java Programming", "Some content", DialogType.CDR_DG,
				"default", user1);
		dialog.setResource(resource);
		dialog.addParticipant(user1);
		dialog.addParticipant(user2);
		return dialog;
	}

	public static String readFileToString(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		StringBuilder builder = new StringBuilder();
		String temp = reader.readLine();
		while (temp != null) {
			builder.append(temp);
			temp = reader.readLine();
		}
		reader.close();
		return builder.toString();
	}

	public static void main(String[] args) throws IOException {
		XMLDialogRW rw = new XMLDialogRW();
		System.out.println(rw.toString(createTestXMLConceptDialog()));
		//
		// System.out.println("==================");System.out.println("==================");System.out.println("==================");
		//
		// System.out.println(rw.toString(createTestXMLWebDocumentDialog()));
		//
		// System.out.println("==================");System.out.println("==================");System.out.println("==================");
		//
		// XMLDialog d2 = rw.fromString(readFileToString(new File("file.txt")));
		// System.out.println(rw.toString(d2));
		//
		// System.out.println("==================");System.out.println("==================");System.out.println("==================");
		//
		// XMLDialog d3 = rw.fromString(readFileToString(new
		// File("file2.txt")));
		// System.out.println(rw.toString(d3));

		XMLDialog d4 = rw.fromString(readFileToString(new File("file3.txt")));
		System.out.println(d4.getDialogContent());

	}

	private static class XMLLabelConverter implements Converter {

		public boolean canConvert(@SuppressWarnings("rawtypes") Class clazz) {
			return clazz.equals(XMLLabel.class);
		}

		public void marshal(Object value, HierarchicalStreamWriter writer,
				MarshallingContext context) {
			XMLLabel label = (XMLLabel) value;
			writer.addAttribute("lang", label.lang);
			writer.setValue(label.content);
		}

		public Object unmarshal(HierarchicalStreamReader reader,
				UnmarshallingContext context) {
			String lang = reader.getAttribute("lang");
			String content = reader.getValue();
			return new XMLLabel(lang, content);
		}
	}
}
