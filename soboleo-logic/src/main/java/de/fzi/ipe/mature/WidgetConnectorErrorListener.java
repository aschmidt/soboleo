package de.fzi.ipe.mature;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.upb.mature.api.java.ReconnectingConnectionErrorListener;
import de.upb.mature.api.java.UserCredentials;
import de.upb.mature.api.java.UserCredentialsProvider;
import de.upb.mature.api.java.WidgetServerConnector;
import de.upb.mature.api.java.WidgetServerConnectorException;

	/**
	 * ConnectionErrorListener which tries to reconnect to server in case of
	 * connection errors.
	 * 
	 * @author iceman
	 * @deprecated
	 * 
	 */
	public class WidgetConnectorErrorListener extends ReconnectingConnectionErrorListener {

		private final Log log = LogFactory.getLog(getClass());

		private WidgetServerConnector widgetServerConnector = null;
		private UserCredentialsProvider userCredentialsProvider = null;
		private WidgetConnector widgetConnector = null;

		private int numberOfAttempts = 0;
		private int delayBetweenAttempts = 0;

		public WidgetConnectorErrorListener(WidgetConnector widgetConnector, WidgetServerConnector widgetServerConnector, UserCredentialsProvider userCredentialsProvider, int numberOfAttempts, int delayBetweenAttempts) {
			super(widgetServerConnector, userCredentialsProvider, numberOfAttempts, delayBetweenAttempts);
			this.widgetConnector = widgetConnector;
			this.widgetServerConnector = widgetServerConnector;
			this.userCredentialsProvider = userCredentialsProvider;
			this.numberOfAttempts = numberOfAttempts;
			this.delayBetweenAttempts = delayBetweenAttempts;
		}

		@Override
		public void onConnectionError(Exception cause) throws WidgetServerConnectorException {
			UserCredentials userCredentials = userCredentialsProvider.getUserCredentials();
			boolean failed = true;
			for (int i = 0; (i < numberOfAttempts) && failed; i++) {
				try {
					log.info("Reconnection attempt " + (i + 1) + " of " + numberOfAttempts);
					widgetServerConnector.connect(userCredentials, this);
					log.info("Reconnection successful.");
					failed = false;
				} catch (WidgetServerConnectorException e) {
					log.info("Reconnection attempt failed.");
				}
				synchronized (this) {
					try {
						wait(delayBetweenAttempts * 1000);
					} catch (InterruptedException e) {
					}
				}
			}

			if (failed) {
				widgetConnector.setConnected(false);
			}
		}
	}

