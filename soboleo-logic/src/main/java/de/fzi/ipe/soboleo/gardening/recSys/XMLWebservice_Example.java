package de.fzi.ipe.soboleo.gardening.recSys;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetAllWebDocuments;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetWebDocument;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceException;
import de.fzi.ipe.soboleo.server.xmlWebservice.client.SOBOLEORemoteSpace;


/**
 * Tests the XML Webservice - note that this test requires a running tomcat. 
 */
public class XMLWebservice_Example{

	private EventSenderCredentials credentials = new EventSenderCredentials("http://soboleo.com/ns/1.0#users-db-gen1","Anonymous", "N4W5I4N334M6X45C6K1H2B6147233H7E3H6H79");
	
	static final String SERVER_URL = "http://tool.soboleo.com/xmlService";
	
	
	
	public void testSoboleoRemoteSpace() throws Exception  {
//		public XMLWebservice_Example throws Exception {
		SOBOLEORemoteSpace remoteSpace = new SOBOLEORemoteSpace(SERVER_URL,"default",1,credentials);
		ClientSideTaxonomy tax = remoteSpace.getTaxonomy();
		
		//get root concepts
		// Set<ClientSideConcept> roots = tax.getRootConcepts();
	//for (ClientSideConcept r:roots){
	//	testconcepts=tax.getConceptsForLabel("fzi");
	
		
	//	for (ClientSideConcept c:roots)
	//		System.out.println(c.getURI());
		
		//get prototypical concepts which contain the concepts to position
		ClientSideConcept proto = tax.getConceptForPrefLabel(new LocalizedString("fzi", Language.en));
		Set<ClientSideConcept> protos= new HashSet<ClientSideConcept>();
		protos.add(proto);
		//System.out.println("proto="+proto.getText(SKOS.PREF_LABEL));
	//	String uri=proto.getURI();
	//	Set<ClientSideConcept> testcon=tax.getExtractedConcepts(uri);
	//	for (ClientSideConcept c:testcon)
	//		System.out.println("testcon="+c.getText(SKOS.PREF_LABEL));
		/*
		//get URIs of concepts to position (or the narrower concepts of "prototypical concepts")
		Set<String> conceptURIsToPosition = proto.getConnected(SKOS.HAS_NARROWER);
		// or as ClientSideConcept
		Set<ClientSideConcept> conceptsToPosition = proto.getConnectedAsConcepts(SKOS.HAS_NARROWER, tax);
		
		//get all preferred labels of a concept
		Set<LocalizedString> prefLabels = proto.getTexts(SKOS.PREF_LABEL);
		//get English preferred label of a concept
		LocalizedString englishPrefLabel = proto.getText(SKOS.PREF_LABEL, Language.en);		
		//get all alternative labels of a concept
		Set<LocalizedString> altLabels = proto.getTexts(SKOS.ALT_LABEL);
		//get all English alternative labels of a concept
		Set<LocalizedString> englishAltLabels = proto.getTexts(SKOS.ALT_LABEL, Language.en);
		*/
		//get all annotated document (for your case a little bit complicated)
		GetAllWebDocuments query =  new GetAllWebDocuments(ResultType.WEB_DOCUMENT);

		LuceneResult result = (LuceneResult) remoteSpace.sendEvent(query); 
		Set<WebDocument> webDocs = new HashSet<WebDocument>();

		try{
			for (IndexDocument current : result)
			{
				WebDocument doc = (WebDocument) remoteSpace.sendEvent(new GetWebDocument(current.getURI()));
				webDocs.add(doc);
				//	System.out.println("added");		
			}
		}
		catch(XMLWebserviceException exp){System.out.println("next line");}
		//remoteSpace.close();
		
		//
		//for(ClientSideConcept r:roots){
		//	System.out.println(r.getText(SKOS.PREF_LABEL).getString());
		//} */
		// Recommendation recsys=new Recommendation();
		//List<GardeningRecommendation> stringrec=recsys.stringBasedSuperRecommendations(tax, roots);
	//	List<GardeningRecommendation> stringrecsuper=recsys.hybridSuperRecommendations(tax, roots,webDocs);
	
	//	for (GardeningRecommendation a:rec)
	//		System.out.println(a.toString());
	//List<GardeningRecommendation> contextrec=recsys.contextualSuperRecommendations(tax, protos,webDocs);
	List<GardeningRecommendation> hybridrec=Recommendation.hybridSuperRecommendations(tax, protos,webDocs);
	for (GardeningRecommendation a:hybridrec)
	System.out.println(a.toString());
	remoteSpace.close();
	}
	public static void main(String[] args){
			XMLWebservice_Example test = new XMLWebservice_Example();
			try {
				test.testSoboleoRemoteSpace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

