package de.fzi.ipe.soboleo.webdocument.officedocument;

import java.io.InputStream;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.officedocument.DownloadInformation;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentByUri;
import de.fzi.ipe.soboleo.queries.lucene.officedocument.GetOfficeDocumentInputStream;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;

@Component
public class DocumentLoader {

	private final Logger logger = Logger.getLogger(this.getClass());
	
	public DownloadInformation getDownloadInformation(User user, 
			EventSenderCredentials cred, Space space,
			String uri)
	{	
		// get space to use
		
		try
		{
			if (user != null)
			{
				// load document and return it
	
				logger.info("GetOfficeDocumentByUri ");
				GetOfficeDocumentByUri gu=new GetOfficeDocumentByUri(uri);
		
				LuceneResult result = (LuceneResult) space.getEventBus().executeQuery(gu, cred);
				logger.info("result size: " +result.length());
				
				IndexDocument id = (IndexDocument) result.doc(0);
				
				try {
					// load the inputstream 
					GetOfficeDocumentInputStream gis = new GetOfficeDocumentInputStream(
							id.getURL(), id.getURI());
					InputStream in = (InputStream) space.getEventBus()
							.executeQuery(gis, cred);
					DownloadInformation di = new DownloadInformation();
					di.setFileName(id.getTitle().replaceAll(" ", ""));
					di.setIs(in);
					return di;
				} catch (Exception e) {
					logger.error(e);
				}
				
			}
		}
		catch(Exception e)
		{
			logger.error(e);
		}
			
		return null;
	}
	
}
