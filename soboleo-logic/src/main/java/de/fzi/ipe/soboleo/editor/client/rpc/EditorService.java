package de.fzi.ipe.soboleo.editor.client.rpc;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public interface EditorService extends RemoteService{
	public Event[] getEvents(long lastEventId, String spaceName, EventSenderCredentials credentials) throws EventPermissionDeniedException;
	public ClientSideTaxonomy getClientSideTaxonomy(String spaceName, EventSenderCredentials credentials) throws EventPermissionDeniedException;
	public String sendCommand(CommandEvent event, String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException;
	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException;
	public List<GardeningRecommendation> getRecommendations(String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException;
	public List<GardeningRecommendation> getRecommendationsForConcept(String conceptURI,String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException;
	public List<? extends Event> getRecentHistory(String spaceName, EventSenderCredentials cred)throws EventPermissionDeniedException;
}
