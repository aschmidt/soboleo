package de.fzi.ipe.soboleo.gardening.eventbus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.gardening.GetGardeningRecommendations;
import de.fzi.ipe.soboleo.event.gardening.GetGardeningRecommendationsForConcept;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.gardening.heuristics.LabelIdenticallnessHeuristics;
import de.fzi.ipe.soboleo.gardening.heuristics.SingleConceptHeuristics;
import de.fzi.ipe.soboleo.gardening.recSys.Recommendation;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;
import de.fzi.ipe.soboleo.lucene.LuceneResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.lucene.document.ResultType;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetAllWebDocuments;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetWebDocument;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;

@Component
public class GardeningRecommendationEventBusAdaptor implements EventBusQueryProcessor, EventBusEventProcessor{

	private Space space;
	// private static Comparator<GardeningRecommendation> comparator = new RecommendationComparator();
	static Logger logger = Logger.getLogger(GardeningRecommendationEventBusAdaptor.class);
	
	private List<GardeningRecommendation> recommendations = null;
	
	//two variables to prevent too frequent recomputation of recommendations
	private boolean seenChangeSinceRecomputation = true;
	private long lastTimeRecomputation = -1;
	
	public GardeningRecommendationEventBusAdaptor(Space space) {
		this.space = space;
	}

	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}

	@Override
	public Object process(QueryEvent queryEvent) {
		if (queryEvent instanceof GetGardeningRecommendations) {
			return getGardeningRecommendations((GetGardeningRecommendations) queryEvent);
		}
		else if (queryEvent instanceof GetGardeningRecommendationsForConcept) {
			return getGardeningRecommendations((GetGardeningRecommendationsForConcept) queryEvent);
		}
		else return null;
	}

	private synchronized Object getGardeningRecommendations(GetGardeningRecommendationsForConcept get) {
		if (recommendations == null || get.isRefresh()) {
			computeRecommendations();
		}
		return recommendationsForConcept(get.getConceptURI());
	}

	private synchronized Object getGardeningRecommendations(GetGardeningRecommendations getRecommendations) {
		if (recommendations == null || getRecommendations.isRefresh()) { 
			computeRecommendations();
		}
		return recommendations;
	}

	private synchronized Object recommendationsForConcept(String conceptURI) {
		List<GardeningRecommendation> toReturn = new ArrayList<GardeningRecommendation>(5);
		for (GardeningRecommendation recommendation: recommendations) {
			if (recommendation.getConceptURI().equals(conceptURI)) toReturn.add(recommendation);
		}
		return toReturn;
	}

	private synchronized void computeRecommendations() {
		if (seenChangeSinceRecomputation && (System.currentTimeMillis()-lastTimeRecomputation > 1000)) {
			//refuses to do a recomputation if the last was less than 1s ago or it hasn't seen any command event. 
			ClientSideTaxonomy tax = space.getClientSideTaxonomy();
			recommendations = new ArrayList<GardeningRecommendation>();
			recommendations.addAll(SingleConceptHeuristics.getRecommendations(tax));
			recommendations.addAll(LabelIdenticallnessHeuristics.getRecommendations(tax));
			recommendations.addAll(getSuperRelatedConceptsRecommendations(tax));
			Collections.sort(recommendations);
			Collections.reverse(recommendations);
//			Collections.sort(recommendations,comparator);
			seenChangeSinceRecomputation = false;
			lastTimeRecomputation = System.currentTimeMillis(); 
		}
	}
	
	
	private List<GardeningRecommendation> getSuperRelatedConceptsRecommendations(ClientSideTaxonomy tax){
		List<GardeningRecommendation> recs = new ArrayList<GardeningRecommendation>();
		
		ClientSideConcept protoParent;
		try {
			protoParent = tax.getConceptForPrefLabel(new LocalizedString(space.getProperty(SpaceProperties.PROTOTYPICAL_CONCEPT_NAME), Language.en));
			
			if(protoParent != null){
				Set<ClientSideConcept> conceptsToPosition = protoParent.getConnectedAsConcepts(SKOS.HAS_NARROWER, space.getClientSideTaxonomy());
				if(!conceptsToPosition.isEmpty()){
					LuceneResult result = (LuceneResult) space.getEventBus().executeQuery(new GetAllWebDocuments(ResultType.WEB_DOCUMENT), EventSenderCredentials.SERVER);
					Iterator<IndexDocument> i =  result.iterator();
					Set<WebDocument> webDocs = new HashSet<WebDocument>();
					while(i.hasNext()){
						IndexDocument current = (IndexDocument) i.next();
						WebDocument doc = (WebDocument) space.getEventBus().executeQuery(new GetWebDocument(current.getURI()), EventSenderCredentials.SERVER);
						if(doc!= null) webDocs.add(doc);
					//	System.out.println("added");
						
					}
					recs = Recommendation.hybridSuperRecommendations(tax, conceptsToPosition, webDocs);
					recs.addAll(Recommendation.hybridRelatedRecommendations(tax, conceptsToPosition, webDocs));

				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return recs;
	}
	
	
	/**
	 * Class used of sorting gardening recommendations - first by priority and then by toString. 
	 */
	@SuppressWarnings("unused")
	private static class RecommendationComparator implements Comparator<GardeningRecommendation> {

		@Override
		public int compare(GardeningRecommendation o1, GardeningRecommendation o2) {
			String o1String = o1.getPriority()+o1.toString();
			String o2String = o2.getPriority()+o2.toString();
			return o1String.compareTo(o2String);
		}
	}

	@Override
	public void receiveEvent(Event event) {
		if (event instanceof CommandEvent) {
			seenChangeSinceRecomputation = true;
		}
	}

}
