/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.gardening.recommendations;

import java.util.HashSet;
import java.util.Set;
import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;


/**
 * A recommendation for labels that also appear at other locations. 
 */
public class ConceptLabelIdenticalProblem extends GardeningRecommendation implements IsSerializable{

	private LabelTupel thisLabel;
	
	private int priority = 9;
	
	private Set<LabelTupel> otherLabels = new HashSet<LabelTupel>();
	
	public ConceptLabelIdenticalProblem(ClientSideConcept concept, SKOS labelType, LocalizedString label) {
		thisLabel = new LabelTupel(concept,labelType,label);
	}

	@SuppressWarnings("unused")
	private ConceptLabelIdenticalProblem() {;}
	
	public void addProblem(ClientSideConcept concept, SKOS labelType, LocalizedString label, int priority) {
		otherLabels.add(new LabelTupel(concept,labelType,label));
		this.priority = Math.min(this.priority, priority);
	}
	
	public Set<LabelTupel> getProblems(){
		return otherLabels;
	}
	
	public LabelTupel getLabelTupel(){
		return thisLabel;
	}
	
	public String getConceptURI() {
		return thisLabel.concept.getURI();
	}
	
	public ClientSideConcept getConcept(){
		return thisLabel.concept;
	}
		
	@Override
	public int getPriority() {
		return priority;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Problem between label "+toString(thisLabel)+" and ");
		for (LabelTupel current:otherLabels) {
			builder.append("\n");
			builder.append("    "+toString(current)+" ");
		}
		return builder.toString();
	}
	
	private String toString(LabelTupel labelTupel) {
		return "("+labelTupel.concept.getURI()+","+labelTupel.labelType+","+labelTupel.label+")";
	}
	
	public static class LabelTupel implements IsSerializable{
		
		public ClientSideConcept concept;
		public LocalizedString label;
		public SKOS labelType;
		
		@SuppressWarnings("unused")
		private LabelTupel() {;}
		
		public LabelTupel(ClientSideConcept concept, SKOS labelType, LocalizedString label) {
			this.concept = concept;
			this.label = label;
			this.labelType = labelType;
		}
		
	}
	
	
}
