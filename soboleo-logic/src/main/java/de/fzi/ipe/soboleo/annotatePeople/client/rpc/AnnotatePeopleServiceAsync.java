/*
 * Ontoprise code for project ksi_underground
 * Created on 21.07.2006 by zach
 */
package de.fzi.ipe.soboleo.annotatePeople.client.rpc;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;

public interface AnnotatePeopleServiceAsync {

	public void getTaggedPersonByURL(String spaceName, EventSenderCredentials cred, Language userLanguage, String webURL, AsyncCallback<TaggedPerson> callback);
	public void getConceptRecommendation(TaggedPerson taggedPerson, String spaceName, Language userLanguage,EventSenderCredentials cred, AsyncCallback<List<ConceptStatistic>> callback);
	public void getAllLabels(String spaceName, EventSenderCredentials cred, Language userLanguage, AsyncCallback<Set<String>> callback);
	/**
	 * Deletes all Tags from a person for a special Space.
	 * @param spaceName
	 * @param cred
	 * @param taggedPersonURI
	 * @param callback
	 */
	public void removeTagsForUser(String spaceName, EventSenderCredentials cred, String taggedPersonURI, AsyncCallback<Void> callback);
	public void removeUser(String spaceName, EventSenderCredentials cred, String taggedPersonURI , AsyncCallback<Boolean> callback);
	/**
	 * Removes a named tag for a tagged person.
	 * Used in AnnotatePeopleMain/TopicLinkWithOptionalDelete to remove a tag.
	 * @param spaceName
	 * @param cred
	 * @param taggedPersonURI
	 * @param tagToRemove
	 * @param callback
	 */
	public void removeTag(String spaceName, EventSenderCredentials cred, TaggedPerson taggedPerson,String tagToRemove,AsyncCallback<Void> callback);
	public void saveAnnotationData(String spaceName, EventSenderCredentials cred, Language userLanguage, TaggedPerson taggedPerson, Map<String,String> conceptsMap, String webURL,AsyncCallback<Void> callback);
	public void getTaggedPersonByEmail(String spaceName, EventSenderCredentials cred, String email,AsyncCallback<TaggedPerson> callback);
	public void createTaggedPerson(String spaceName, EventSenderCredentials cred, String email, String name,AsyncCallback<TaggedPerson> callback);
	public void getConceptsForURIs(String spaceName, EventSenderCredentials cred,Set<String> myConceptURIs, AsyncCallback<Set<ClientSideConcept>> callback);
	public void getConceptForLabel(String spaceName, EventSenderCredentials cred,LocalizedString label, AsyncCallback<ClientSideConcept> callback);
	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred, AsyncCallback<Void> callback);
	public void getTaggedPersons (String spaceName, EventSenderCredentials cred, AsyncCallback<List<TaggedPerson>> callback);
	public void getConcepts(String spaceName, EventSenderCredentials cred, AsyncCallback<Collection<ClientSideConcept>> callback);
}
