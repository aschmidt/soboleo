package de.fzi.ipe.soboleo.server.xmlWebservice;

import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;



/**
 * Container class for an XMLWebservice request. Combines the
 * actual request with information about the space and the 
 * user credentials. 
 */
public class XMLWebserviceRequest {
	
	private EventSenderCredentials credentials;
	private String spaceID;
	private Object requestPayload; 
	private long lastEventID = Long.MAX_VALUE;
	
	
	public XMLWebserviceRequest(Object requestPayload, String spaceID, EventSenderCredentials credentials) {
		this.credentials = credentials;
		this.spaceID = spaceID;
		this.requestPayload = requestPayload;
	}
	
	public XMLWebserviceRequest(Object requestPayload, String spaceID, EventSenderCredentials credentials, long lastEventID) {
		this(requestPayload,spaceID, credentials);
		this.lastEventID = lastEventID;
	}
	
	public EventSenderCredentials getCredentials () {
		return credentials;
	}
	
	public String getSpaceID() {
		return spaceID;
	}
	
	public Object getRequestPayload() {
		return requestPayload;
	}

	public long getLastEventID() {
		return lastEventID;
	}
	
}
