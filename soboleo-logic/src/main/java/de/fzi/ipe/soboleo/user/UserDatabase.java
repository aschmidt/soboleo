package de.fzi.ipe.soboleo.user;

import java.io.IOException;
import java.util.Set;

public interface UserDatabase {

	public abstract String getTripleStoreDump() throws IOException;

	public abstract void close() throws IOException;

	public abstract User createNewUser() throws IOException;

	public abstract void deleteUser(User user) throws IOException;

	public abstract User getDefaultUser() throws IOException;

	public abstract User getUser(String uri) throws IOException;

	public abstract Set<User> getAllUsers() throws IOException;

	public abstract Set<User> getUserForSpace(String spaceIdentifier)
			throws IOException;

	public abstract User getUserForKey(String key) throws IOException;

	public abstract User getUserForEmail(String emailString) throws IOException;

}