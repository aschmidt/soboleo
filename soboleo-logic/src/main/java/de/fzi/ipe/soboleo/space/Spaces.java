/*
 * The class responsible for managing the Space objects. Use 
 * this class to get a Space object.
 * 
 * FZI - Information Process Engineering 
 * Created on 06.03.2009 by zach
 */
package de.fzi.ipe.soboleo.space;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.user.UserDatabase;


public class Spaces {
	
	public static final String DEFAULT_SPACE = "default";
	 
	protected Logger logger = Logger.getLogger(Spaces.class);
	
	private File spacesDirectory;
	private Map<String,Space> spaces = new HashMap<String,Space>();
	private UserDatabase userDatabase;
	private SpaceFactory factory = new DefaultSpaceFactory();
	
	public Spaces(File spacesDirectory, UserDatabase userDatabase) throws IOException {
		setupSpacesDirectory(spacesDirectory);
		this.userDatabase = userDatabase;
	}

	public synchronized void createNewSpace(String spaceName) throws IOException, EventPermissionDeniedException {
		if (isPossibleName(spaceName)!=null) throw new RuntimeException("Not a legal space name");
		else {
			Space newSpace = factory.createSpace(new File(spacesDirectory,spaceName),spaceName, userDatabase);
			spaces.put(spaceName, newSpace);
		}
	}

	public synchronized void closeSpace(String spaceName) throws IOException {
		Space toClose = spaces.get(spaceName);
		if (toClose != null) {
			toClose.close();
			spaces.remove(spaceName);
		}
	}
	
	public synchronized Space getSpace(String spaceName) throws IOException, EventPermissionDeniedException {
		Space toReturn = spaces.get(spaceName);
		if (toReturn == null) {
			File newSpaceDirectory = new File(spacesDirectory,spaceName);
			if (!newSpaceDirectory.exists()) 
			{
				logger.error("Space not found: " + spaceName + " in directory " + spacesDirectory.toString());
				return null;
			}
			else {
				toReturn = factory.createSpace(newSpaceDirectory, spaceName, userDatabase);
				// toReturn = new SpaceImpl(newSpaceDirectory,spaceName,userDatabase);
				spaces.put(spaceName, toReturn);
			}
		}
		return toReturn;
	}
	
	
	/**
	 * Checks whether name can be use as a name for a new space. Returns null if its ok, a
	 * string with a description of the problem otherwise
	 * @param name
	 * @return
	 */
	public String isPossibleName(String name) {
		if (!name.matches("[a-z0-9]+")) {
			return "Name contains illegal characters - it must consists of only lowercase letters and numbers";
		}
		else {
			File possibleSpaceDirectory = new File(spacesDirectory,name);
			if (possibleSpaceDirectory.exists()) {
				return "Space with this name exists already!";
			}
			else return null;
		}
	}
	
	private synchronized void setupSpacesDirectory(File spacesDirectory) throws IOException {
		this.spacesDirectory = spacesDirectory;
		if (!spacesDirectory.exists()) {
			logger.info("Creating new spaces directory at " + spacesDirectory.getAbsolutePath());
			if (!spacesDirectory.mkdirs()) throw new IOException("Failed to create spaces directory!");
		}
		else
			logger.info("Existing spaces directory with spaces #" + getSpaces().size());
		
		
	}
	
	public List<String> getSpaces() {
		List<String> toReturn = new ArrayList<String>();
		for (File f: spacesDirectory.listFiles()) {
			if (f.isDirectory()) toReturn.add(f.getName());
		}
		Collections.sort(toReturn);
		return toReturn;
	}
	
}
