package de.fzi.ipe.soboleo.gardening.recSys;

import java.util.HashMap;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;

public class AggregatedRec {
	public ClientSideConcept targetConcept;
	public HashMap<ClientSideConcept, Double> recConf = new HashMap<ClientSideConcept, Double>();
	
	public void update(ClientSideConcept toUpdate, Double conf){
		if(recConf.containsKey(toUpdate)) recConf.put(toUpdate, recConf.get(toUpdate) + conf);
		else recConf.put(toUpdate, conf);
	}
}