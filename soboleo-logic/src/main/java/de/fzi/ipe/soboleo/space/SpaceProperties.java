/*
 * FZI - Information Process Engineering 
 * Created on 17.08.2009 by zach
 */
package de.fzi.ipe.soboleo.space;


/** 
 * A class with constants identifying the space properties; 
 */
public enum SpaceProperties {
	
	SECURITY("security","write"),
	SYSOUT_LOGGING("sysoutLogging","nothing"),
	EVENT_HISTORY_LOGGING("eventHistoryLogging", "true"),
	EXTERNAL_USER_LOGGING("externalUserLogging", "false"),
	DEFAULT_LANGUAGE("defaultLanguage","en"), 
	COMMUNITY_ANNOTATION("communityAnnotation","true"),
	PEOPLE_SEARCH("peopleSearch","false"),
	DIALOG_SUPPORT("dialogSupport", "false"),
	PROTOTYPICAL_CONCEPT_NAME("prototypicalConcept", "prototypical concepts"),
	/**
	 * This SpaceProperty defines, if it is possible to delete other peoples topics.
	 * This is used in AnnotatePeople (Tag Person).
	 * If it is true and the person who should be tagged, is the same as the person logged on, the person can 
	 * delete the Topics,with which he was tagged by other persons.   
	 */
	ANNOTATEPEOPLE_DELETABLE_TOPIC("annotatePeopleDeletableTopic","false"),
	/**
	 * This SpaceProperty defines, if it should be possible to rate a web
	 * page.
	 */
	ANNOTATE_WEB_PAGE_RATING("annotateWebPageRating","false"),
	;
	
	
	private String key;
	private String defaultValue;
	
	SpaceProperties(String key, String defaultValue) {
		this.key = key;
		this.defaultValue = defaultValue;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
	
	
}
