/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.peopleTagging.eventBusAdaptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.AggregatedConceptTags;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.ManualPersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.PersonTag;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.peopletagging.AddPersonTag;
import de.fzi.ipe.soboleo.event.peopletagging.AddTaggedPerson;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePerson;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePersonTag;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePersonTagsByUser;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.execution.rdf.peopletagging.PeopleSearch;
import de.fzi.ipe.soboleo.execution.rdf.peopletagging.RDFManualPersonTag;
import de.fzi.ipe.soboleo.execution.rdf.peopletagging.RDFPersonTag;
import de.fzi.ipe.soboleo.execution.rdf.peopletagging.RDFTaggedPerson;
import de.fzi.ipe.soboleo.execution.rdf.peopletagging.RDFTaggedPersonsDatabase;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetAggregatedPersonTags;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetAllTaggedPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetPersonTag;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetPersonTags;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPerson;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPersonByURL;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPersonForEmail;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsByName;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsByNameAndTopic;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsForConcept;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchSimilarPersons;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;
import de.fzi.ipe.soboleo.user.UserDatabase;

@Component
public class PeopleTaggingEventBusAdaptor implements EventBusQueryProcessor,
		EventBusCommandProcessor {

	private RDFTaggedPersonsDatabase database;
	private Space space;
	private UserDatabase userDatabase;

	public PeopleTaggingEventBusAdaptor(TripleStore tripleStore, Space space, UserDatabase db) {
		database = new RDFTaggedPersonsDatabase(tripleStore);
		this.space = space;
		this.userDatabase = db;
	}

	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}

	@Override
	public Object process(QueryEvent query) {
		try {
			if (query instanceof GetTaggedPerson) {
				GetTaggedPerson getTagged = (GetTaggedPerson) query;
				RDFTaggedPerson rdfTaggedPerson = getRDFTaggedPerson(getTagged
						.getURI());
				return (rdfTaggedPerson != null) ? rdfTaggedPerson
						.getClientSideTaggedPerson() : null;
			}
			if (query instanceof GetTaggedPersonByURL) {
				GetTaggedPersonByURL getTaggedURL = (GetTaggedPersonByURL) query;
				RDFTaggedPerson taggedPerson = database
						.getTaggedPersonForURL(getTaggedURL.getURL());
				return (taggedPerson != null) ? taggedPerson
						.getClientSideTaggedPerson() : null;
			}
			if (query instanceof GetTaggedPersonForEmail) {
				GetTaggedPersonForEmail getTaggedByMail = (GetTaggedPersonForEmail) query;
				RDFTaggedPerson taggedPerson = database
						.getTaggedPersonForEmail(getTaggedByMail.getEmail());
				return (taggedPerson != null) ? taggedPerson
						.getClientSideTaggedPerson() : null;
			}
			if (query instanceof SearchPersons) {
				SearchPersons searchPersons = (SearchPersons) query;
				if (searchPersons.getAskerURI() != null) {
					RDFTaggedPerson asker = database
							.getTaggedPerson(searchPersons.getAskerURI());
					return PeopleSearch.findPersons(
							searchPersons.getSearchString(), asker, database,
							space.getClientSideTaxonomy());
				} else
					return PeopleSearch.findPersons(
							searchPersons.getSearchString(), database,
							space.getClientSideTaxonomy());
			}
			if (query instanceof SearchPersonsByName) // In this case, i will be
														// seared for Persons
														// only by the name
			{
				SearchPersonsByName searchPersonsByName = (SearchPersonsByName) query;
				if (searchPersonsByName.getAskerURI() != null) {
					RDFTaggedPerson asker = database
							.getTaggedPerson(searchPersonsByName.getAskerURI());
					return PeopleSearch.findPersonsByName(
							searchPersonsByName.getSearchString(), asker,
							database, space.getClientSideTaxonomy());
				} else
					return PeopleSearch.findPersonsByName(
							searchPersonsByName.getSearchString(), database,
							space.getClientSideTaxonomy());
			}
			if (query instanceof SearchPersonsByNameAndTopic) // In this case,
																// it will be
																// seared for
																// Persons by
																// the name and
																// topic
			{
				SearchPersonsByNameAndTopic searchPersonsByNameAndToppic = (SearchPersonsByNameAndTopic) query;
				if (searchPersonsByNameAndToppic.getAskerURI() != null) {
					RDFTaggedPerson asker = database
							.getTaggedPerson(searchPersonsByNameAndToppic
									.getAskerURI());
					return PeopleSearch.findPersonsByNameAndTopic(
							searchPersonsByNameAndToppic.getSearchString(),
							asker, database, space.getClientSideTaxonomy());
				} else
					return PeopleSearch.findPersonsByNameAndTopic(
							searchPersonsByNameAndToppic.getSearchString(),
							database, space.getClientSideTaxonomy());
			}
			if (query instanceof SearchPersonsForConcept) {
				SearchPersonsForConcept searchPersons = (SearchPersonsForConcept) query;
				if (searchPersons.getTaggedPersonURI() != null) {
					RDFTaggedPerson asker = database
							.getTaggedPerson(searchPersons.getTaggedPersonURI());
					return PeopleSearch.findPersonsForConcept(asker, database,
							space.getClientSideTaxonomy(),
							searchPersons.getConceptURIs());
				} else
					return PeopleSearch.findPersonsForConcept(database,
							space.getClientSideTaxonomy(),
							searchPersons.getConceptURIs());
			}
			if (query instanceof SearchSimilarPersons) {
				SearchSimilarPersons searchSimilarPersons = (SearchSimilarPersons) query;
				RDFTaggedPerson taggedPerson = database
						.getTaggedPerson(searchSimilarPersons
								.getTaggedPersonURI());
				return PeopleSearch.findPersons(taggedPerson, database,
						space.getClientSideTaxonomy());
			}
			if (query instanceof GetAllTaggedPersons) {
				User defaultUser = userDatabase.getDefaultUser();
				Set<RDFTaggedPerson> taggedPersons = database
						.getTaggedPersons();
				ArrayList<TaggedPerson> toReturn = new ArrayList<TaggedPerson>(
						taggedPersons.size());
				for (RDFTaggedPerson rdfTaggedPerson : taggedPersons) {
					TaggedPerson person = rdfTaggedPerson
							.getClientSideTaggedPerson();
					if (defaultUser == null
							|| !person.getEmail()
									.equals(defaultUser.getEmail()))
						toReturn.add(person);
				}
				Collections.sort(toReturn, new Comparator<TaggedPerson>() {
					public int compare(TaggedPerson one, TaggedPerson two) {
						return (one.getName().compareTo(two.getName()));
					}
				});
				return toReturn;
			}
			if (query instanceof GetPersonTags) {
				GetPersonTags getPersonTags = (GetPersonTags) query;
				String conceptURI = getPersonTags.getConceptURI();
				String makerURI = getPersonTags.getMakerURI();
				Set<PersonTag> toReturn = new HashSet<PersonTag>();
				if (getPersonTags.getTaggedPersonURI() != null) {
					RDFTaggedPerson rdfTaggedPerson = getRDFTaggedPerson(getPersonTags
							.getTaggedPersonURI());
					if (rdfTaggedPerson != null)
						toReturn.addAll(calculatePersonTags(conceptURI,
								makerURI,
								rdfTaggedPerson.getClientSideTaggedPerson()));
				} else {
					User defaultUser = userDatabase.getDefaultUser();
					Set<RDFTaggedPerson> taggedPersons = database
							.getTaggedPersons();
					for (RDFTaggedPerson rdfTaggedPerson : taggedPersons) {
						TaggedPerson person = rdfTaggedPerson
								.getClientSideTaggedPerson();
						if (defaultUser == null
								|| !person.getEmail().equals(
										defaultUser.getEmail()))
							toReturn.addAll(calculatePersonTags(conceptURI,
									makerURI, person));
					}
				}
				return toReturn;
			}
			if (query instanceof GetAggregatedPersonTags) {
				GetAggregatedPersonTags getAggregated = (GetAggregatedPersonTags) query;
				String makerURI = getAggregated.getMakerURI();
				String pageURL = getAggregated.getPageURL();
				Set<AggregatedConceptTags> toReturn = new HashSet<AggregatedConceptTags>();
				if (getAggregated.getTaggedPersonURI() != null) {
					RDFTaggedPerson rdfTaggedPerson = getRDFTaggedPerson(getAggregated
							.getTaggedPersonURI());
					if (rdfTaggedPerson != null)
						toReturn.addAll(calculateAggregatedConceptTags(
								makerURI,
								rdfTaggedPerson.getClientSideTaggedPerson()));
				} else if (pageURL != null) {
					RDFTaggedPerson rdfTaggedPerson = database
							.getTaggedPersonForURL(pageURL);
					if (rdfTaggedPerson != null)
						toReturn.addAll(calculateAggregatedConceptTags(
								makerURI,
								rdfTaggedPerson.getClientSideTaggedPerson()));
				} else {
					Set<RDFTaggedPerson> taggedPersons = database
							.getTaggedPersons();
					toReturn.addAll(calculateAggregatedConceptTags(makerURI,
							taggedPersons));
				}
				return toReturn;
			}
			if (query instanceof GetPersonTag) {
				GetPersonTag getTag = (GetPersonTag) query;
				RDFManualPersonTag rdfPersonTag = database
						.getManualPersonTag(getTag.getURI());
				return rdfPersonTag.getClientSideTag();
			} else
				return null;
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	private Set<? extends PersonTag> calculatePersonTags(String conceptURI,
			String makerURI, TaggedPerson person) throws IOException {
		Set<ManualPersonTag> tags = new HashSet<ManualPersonTag>();
		if (conceptURI != null && makerURI != null) {
			ManualPersonTag t;
			if ((t = person.hasUserTag(makerURI, conceptURI)) != null)
				tags.add(t);
			return tags;
		} else if (conceptURI != null)
			return (person.getTags(conceptURI) != null) ? person
					.getTags(conceptURI) : tags;
		else if (makerURI != null)
			return person.getUserTags(makerURI);
		else
			return (person.getTags() != null) ? person.getTags() : tags;
	}

	private Collection<AggregatedConceptTags> calculateAggregatedConceptTags(
			String makerURI, TaggedPerson person) {
		if (makerURI != null) {
			Map<String, AggregatedConceptTags> aggregated = new HashMap<String, AggregatedConceptTags>();
			for (PersonTag tag : person.getUserTags(makerURI)) {
				AggregatedConceptTags aggregatedTags = aggregated.get(tag
						.getConceptID());
				if (aggregatedTags == null) {
					aggregatedTags = new AggregatedConceptTags(
							tag.getConceptID());
					aggregated.put(tag.getConceptID(), aggregatedTags);
				}
				aggregatedTags.addTag(tag);
			}
			return aggregated.values();
		} else
			return person.getAggregatedTags();
	}

	private Collection<AggregatedConceptTags> calculateAggregatedConceptTags(
			String makerURI, Set<RDFTaggedPerson> taggedPersons)
			throws IOException {
		Map<String, AggregatedConceptTags> aggregated = new HashMap<String, AggregatedConceptTags>();
		User defaultUser = userDatabase.getDefaultUser();
		for (RDFTaggedPerson rdfTaggedPerson : taggedPersons) {
			TaggedPerson person = rdfTaggedPerson.getClientSideTaggedPerson();
			if (defaultUser == null
					|| !person.getEmail().equals(defaultUser.getEmail())) {
				if (makerURI != null) {
					for (PersonTag tag : person.getUserTags(makerURI)) {
						AggregatedConceptTags aggregatedTags = aggregated
								.get(tag.getConceptID());
						if (aggregatedTags == null) {
							aggregatedTags = new AggregatedConceptTags(
									tag.getConceptID());
							aggregated.put(tag.getConceptID(), aggregatedTags);
						}
						aggregatedTags.addTag(tag);
					}
				} else {
					for (String conceptURI : person.getTagConceptURIs()) {
						AggregatedConceptTags aggregatedTags = aggregated
								.get(conceptURI);
						if (aggregatedTags == null)
							aggregated.put(conceptURI,
									person.getAggregatedTags(conceptURI));
						else
							aggregatedTags.addAll(person
									.getAggregatedTags(conceptURI));
					}
				}
			}
		}
		return aggregated.values();
	}

	@Override
	public CommandEvent prepare(CommandEvent command) {
		return null;
	}

	@Override
	public Object process(CommandEvent command) {
		try {
			if (command instanceof AddTaggedPerson) {
				AddTaggedPerson addTaggedPerson = (AddTaggedPerson) command;
				RDFTaggedPerson createdRDFPerson = database.createTaggedPerson(
						addTaggedPerson.getEmail(), addTaggedPerson.getName());
				return createdRDFPerson.getClientSideTaggedPerson();
			}
			if (command instanceof AddPersonTag) {
				AddPersonTag addPersonTag = (AddPersonTag) command;
				RDFTaggedPerson taggedPerson = database
						.getTaggedPerson(addPersonTag.getTaggedPersonURI());
				RDFManualPersonTag manualTag = taggedPerson.addManualTag(
						addPersonTag.getUserID(), addPersonTag.getConceptID(),
						addPersonTag.getUrl());
				return manualTag.getClientSideTag();
			}
			if (command instanceof RemovePersonTag) {
				RemovePersonTag removePersonTagCmd = (RemovePersonTag) command;
				RDFPersonTag toRemove = database
						.getManualPersonTag(removePersonTagCmd
								.getPersonTagURI());
				toRemove.delete();
				return null;
			}
			
			if (command instanceof RemovePerson )
			{
				RemovePerson removePerson=(RemovePerson) command; 
				System.out.println("(PeopleTaggingEventBus) remove person : " +removePerson.getPersonTagURI());
				
				
				RDFTaggedPerson rdfTaggedPerson = database.getTaggedPerson(removePerson.getPersonTagURI());
				TaggedPerson taggedPerson = rdfTaggedPerson.getClientSideTaggedPerson();
				for(ManualPersonTag tag : taggedPerson.getUserTags(removePerson.getPersonTagURI())){
					System.out.println("remove tag of user : " +tag.getTagID());
					RDFPersonTag toRemove = database.getManualPersonTag(tag.getTagID());
					toRemove.delete();
				}
			
				// remove all tags of the person
				// rdfTaggedPerson.deleteAllTags();
				
				// remove the person itself
				System.out.println("((PeopleTaggingEventBus)) call removePerson");
				return rdfTaggedPerson.removePerson(rdfTaggedPerson.getURI());
			}
			
			
			
			if (command instanceof RemovePersonTagsByUser) {
				RemovePersonTagsByUser removeTags = (RemovePersonTagsByUser) command;
				RDFTaggedPerson rdfTaggedPerson = database
						.getTaggedPerson(removeTags.getTaggedPersonURI());
				TaggedPerson taggedPerson = rdfTaggedPerson
						.getClientSideTaggedPerson();
				for (ManualPersonTag tag : taggedPerson.getUserTags(removeTags
						.getUserURI())) {
					RDFPersonTag toRemove = database.getManualPersonTag(tag
							.getTagID());
					toRemove.delete();
				}
				return null;
			} else
				return null;
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	private RDFTaggedPerson getRDFTaggedPerson(String personURI)
			throws IOException {
		RDFTaggedPerson taggedPerson = null;
		if (personURI.toLowerCase().startsWith("mailto:"))
			taggedPerson = database
					.getTaggedPersonForEmail(skipMailto(personURI));
		else if (personURI.startsWith("http://"))
			taggedPerson = database.getTaggedPerson(personURI);
		return taggedPerson;
	}

	private String skipMailto(String mailto) {
		return mailto.toLowerCase().replace("mailto:", "");
	}

}
