package de.fzi.ipe.soboleo.server.xmlWebservice.client;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceException;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceRequest;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceResult;

public class SOBOLEOServerConnection {

	private URL url;
	private EventSenderCredentials credentials;

	private XStream xStream = new XStream(new DomDriver());
	
	public SOBOLEOServerConnection (String url, EventSenderCredentials credentials) throws XMLWebserviceException{
		try {
			this.credentials = credentials;
			this.url = new URL(url);
		} catch (IOException ioe) { throw new XMLWebserviceException(ioe); }
	}
	
	public synchronized XMLWebserviceResult send(String spaceID, Event event, long lastEventId) throws XMLWebserviceException{
		try {
			URLConnection urlCon = url.openConnection();
			urlCon.setDoInput(true);
			urlCon.setDoOutput(true);
			
			XMLWebserviceRequest request= new XMLWebserviceRequest(event,spaceID,credentials,lastEventId);
			xStream.toXML(request, new OutputStreamWriter(urlCon.getOutputStream()));
			//xStream.toXML(request, System.out); print xml request to system.out
			
			urlCon.getOutputStream().flush();
			urlCon.getOutputStream().close();
			
			return (XMLWebserviceResult) xStream.fromXML(new InputStreamReader(urlCon.getInputStream()));			
		} catch (IOException io) { 
			throw new XMLWebserviceException(io);
		}
	}
	
	
}
