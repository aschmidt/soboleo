package de.fzi.ipe.soboleo.tagcloud.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.AggregatedConceptTags;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.data.documentstorage.analyse.AnalyseSearchRequests;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.execution.lucene.webdocument.SearchResult;
import de.fzi.ipe.soboleo.lucene.document.IndexDocument;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetAllTaggedPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.GetTaggedPerson;
import de.fzi.ipe.soboleo.server.AutowiringRemoteServiceServlet;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.tagcloud.rpc.TagCloudService;

@Component
public class TagCloudServiceImpl extends AutowiringRemoteServiceServlet implements
		TagCloudService {
	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private Server server;
	
	@Autowired
	private File spacesPath;
	
	@Override
	public List<ConceptStatistic> getSearchRequestsOverall(String spaceId,
			int days, Language userLanguage) {


		AnalyseSearchRequests asr = new AnalyseSearchRequests();
		List<ConceptStatistic> statistic = asr.createAndReturnStatistic(spacesPath.getAbsolutePath(), spaceId, days);

		try {
			// fill clientside concepts in concept statistic objects
			logger.info("get space: " + spaceId);
			Space space = server.getSpaces().getSpace(spaceId);

			logger.info("get concepts from taxonomy");

			for (ConceptStatistic ci : statistic) {
//				logger.info("get concept : " + ci.getConceptId());
				ClientSideConcept concept = space.getClientSideTaxonomy().get(
						ci.getConceptId());
				if(concept != null)	ci.setConcept(concept);
				else statistic.remove(ci);
			}

		} catch (Exception e) {
			logger.error(e);
		}
		return statistic;
	}

	public List<ConceptStatistic> getUsedConceptsInPersonAnnotations(
			String spaceId, int days, Language userLanguage, EventSenderCredentials cred) {
		

		
		try {
			
//			logger.info("get space: " + spaceId);
			Space space = server.getSpaces().getSpace(spaceId);

			@SuppressWarnings("unchecked")
			List<TaggedPerson> persons = (List<TaggedPerson>) space
					.getEventBus()
					.executeQuery(new GetAllTaggedPersons(), cred);

			
			ArrayList<String> extractedConcepts = new ArrayList<String>();
			Hashtable<String, Integer> conceptsCount = new Hashtable<String, Integer>();

			
			for (TaggedPerson person:persons)
			{
				Set<String> conceptUris=person.getTagConceptURIs();
				
				for (String concept:conceptUris)
				{
					if(concept != null){
						if (conceptsCount.get(concept) == null) {
//							System.out.println("add new concept: " + concept);
							conceptsCount.put(concept, 1);
							extractedConcepts.add(concept);
						} else {
//							System.out.println("increase existing concept count: " + concept);
							int count = conceptsCount.get(concept);
							count++;
							conceptsCount.put(concept, count);
						}
					}
				}
				
			}
			
			// build the concepts
			List<ConceptStatistic> statistic=new ArrayList<ConceptStatistic>();
			for (String concept : extractedConcepts) {
				ConceptStatistic cs=new ConceptStatistic();
				cs.setConceptId(concept);
				cs.setCount(conceptsCount.get(concept));
//				logger.info("get concept : " + cs.getConceptId());
				ClientSideConcept csc = space.getClientSideTaxonomy().get(
						cs.getConceptId());
				if(csc != null){
					cs.setConcept(csc);
					statistic.add(cs);
				}
			}
			
			return statistic;
			
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}
	
	public List<ConceptStatistic> getAggregatedPersonTags(String taggedPersonURI, String spaceId, Language userLanguage, EventSenderCredentials cred){
		try {
//			logger.info("get space: " + spaceId);
			Space space = server.getSpaces().getSpace(spaceId);
//			logger.info("get tagged Person: " +taggedPersonURI);
			TaggedPerson taggedPerson = (TaggedPerson) space.getEventBus().executeQuery(new GetTaggedPerson(taggedPersonURI), cred);
//			logger.info("tagged person" +taggedPerson);
			List<ConceptStatistic> statistic=new ArrayList<ConceptStatistic>();
			for(AggregatedConceptTags aggregated : taggedPerson.getAggregatedTags()){
				ConceptStatistic cs=new ConceptStatistic();
				cs.setConceptId(aggregated.getConceptID());
				cs.setCount(aggregated.getWeight());
//				logger.info("get concept : " + cs.getConceptId());
				ClientSideConcept csc = space.getClientSideTaxonomy().get(
						aggregated.getConceptID());
				if(csc != null){
					cs.setConcept(csc);
					statistic.add(cs);
				}
			}
			return statistic;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	public List<ConceptStatistic> getUserAnnotations(String userkey, String spaceId, Language userLanguage,EventSenderCredentials cred)
	{

		try {
			logger.info("get space: " + spaceId);
			Space space = server.getSpaces().getSpace(spaceId);

			GetDocumentsSearchResult gu = new GetDocumentsSearchResult("");
			
			gu.setAnnotationOwner(userkey);
			
			SearchResult result = (SearchResult) space.getEventBus()
					.executeQuery(gu, cred);

			
			ArrayList<String> extractedConcepts = new ArrayList<String>();
			Hashtable<String, Integer> conceptsCount = new Hashtable<String, Integer>();
			
			logger.info("result size: " +result.getLuceneResult().length());			
			// count the annotations
			for (IndexDocument doc:result.getLuceneResult())
			{
//				logger.info("doc owner: " +doc.getDocumentOwner());
				
				// search the semantic annotations
				for (SemanticAnnotation sia:doc.getSemanticAnnotations())
				{
					if (sia.getUserId().equals(userkey))
					{
//						logger.info("> user is annotation owner for : " +sia.getUri());
						
						// add them to statistic
						if (conceptsCount.get(sia.getUri()) == null) {
//							logger.info("add new concept: " + sia.getUri());
							conceptsCount.put(sia.getUri(), 1);
							extractedConcepts.add(sia.getUri());
						} else {
//							logger.info("increase existing concept count: "	+ sia.getUri());
							int count = conceptsCount.get(sia.getUri());
							count++;
							conceptsCount.put(sia.getUri(), count);
						}
					}
				}
			}
			
			
			// build the concepts
			List<ConceptStatistic> statistic=new ArrayList<ConceptStatistic>();
			for (String concept : extractedConcepts) {
				ConceptStatistic cs=new ConceptStatistic();
				cs.setConceptId(concept);
				cs.setCount(conceptsCount.get(concept));
//				logger.info("get concept : " + cs.getConceptId());
				ClientSideConcept csc = space.getClientSideTaxonomy().get(
						cs.getConceptId());
				if(csc != null){
//					logger.info("csc: " +csc.getURI());
					cs.setConcept(csc);
					statistic.add(cs);
				}
			}
			return statistic;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	
}
