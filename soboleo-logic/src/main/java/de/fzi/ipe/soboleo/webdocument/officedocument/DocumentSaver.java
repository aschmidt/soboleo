package de.fzi.ipe.soboleo.webdocument.officedocument;

import java.io.InputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.document.DocumentType;
import de.fzi.ipe.soboleo.event.officedocument.AddOfficeDocument;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;

@Component
public class DocumentSaver {

	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired private Server server;

	
	public String getUploadResult(String key, String fileName, String fileType, InputStream is)
	{
		logger.info("get user" +key);
		// get space to use
		
		try
		{
			User user = server.getUserDatabase().getUserForKey(key);
			
			if (user != null)
			{
				
				logger.info("user is: " +user.getEmail());
				
				EventSenderCredentials cred = new EventSenderCredentials(user.getURI(), user.getName(), user.getKey());
				String spacename=null;
				
				if (spacename == null)spacename = user.getDefaultSpaceIdentifier();
				if (spacename.equals("")) { spacename="default"; }
				logger.info("use spacename: " +spacename);
				
				Space space=  server.getSpaces().getSpace(spacename);
		
			
				// save document
				
				DocumentType dc=null;
				if (fileType.startsWith("doc"))
				{
					dc=DocumentType.doc;
				}
				if (fileType.startsWith("pdf"))
				{
					dc=DocumentType.pdf;
				}
				if (fileType.startsWith("xls"))
				{
					dc=DocumentType.xls;
				}
				if (fileType.startsWith("ppt"))
				{
					dc=DocumentType.ppt;
				}
				
				logger.info("fis: " +is.available());
				
				AddOfficeDocument aod = new AddOfficeDocument(dc,
						is, fileName, cred.getKey());
				
				space.getLuceneEventBus().receiveEvent(aod);
				
				try
				{
					String docuri=space.getLuceneEventBus().getLastAddedDocumentKey();
					return docuri;
				}
				catch(Exception e)
				{
					return null;
				}
			}
		}
		catch(Exception e)
		{
			logger.error(e);
		}
			
		return null;
	}
	
}
