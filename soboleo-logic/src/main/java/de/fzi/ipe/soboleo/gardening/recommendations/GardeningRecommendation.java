/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.gardening.recommendations;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;


/**
 * quite empty superclass of all gardening recommendations
 */
public abstract class GardeningRecommendation implements IsSerializable, Comparable<GardeningRecommendation>{
	
	/**
	 * Priority (from 9-1), lower priorities come first.
	 * @return
	 */
	abstract public int getPriority();
	
	/**
	 * URI of the concept this gardening recommendation refers to 
	 */
	abstract public String getConceptURI();

	public double getConfidence() {
		return 0.5;
	}
	
	/**
	 * ClientSideConcept this gardening recommendation refers to
	 */
	abstract public ClientSideConcept getConcept();
	abstract public String toString();

	
	public int compareTo(GardeningRecommendation c) {
		return Double.compare(this.getConfidence(),c.getConfidence());
	}


}
