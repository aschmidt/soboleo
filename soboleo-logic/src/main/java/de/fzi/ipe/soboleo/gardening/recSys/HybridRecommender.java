package de.fzi.ipe.soboleo.gardening.recSys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptRecommendation;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public class HybridRecommender implements Recommender{
	
	 private ClientSideTaxonomy tax;
	 private Set<ClientSideConcept> targetConcept;
	 private Set<WebDocument> docs;
	 double beta=.5;
	
	static Logger logger = Logger.getLogger(StringRecommender.class);
	
	public HybridRecommender(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition, Set<WebDocument> docs){
		this.tax=tax;
		this.targetConcept=conceptsToPosition;
		this.docs=docs;	
	   }
			
	public List<GardeningRecommendation> SuperConceptRecommend() throws Exception{
		ContextualRecommender cr=new ContextualRecommender(tax,targetConcept,docs);
		StringRecommender sr=new StringRecommender(tax,targetConcept);
		List<GardeningRecommendation> cr_result=cr.SuperConceptRecommend();
		List<GardeningRecommendation> sr_result=sr.hybridSuperConceptRecommendusingtax();
		List<GardeningRecommendation> h_result=hybrid(cr_result,sr_result,beta, SKOS.HAS_BROADER);
		return h_result;}

	
	public List<GardeningRecommendation> RelatedConceptRecommend()
			throws Exception {
		ContextualRecommender cr=new ContextualRecommender(tax,targetConcept,docs);
		StringRecommender sr=new StringRecommender(tax,targetConcept);
		List<GardeningRecommendation> cr_result=cr.RelatedConceptRecommend();
		List<GardeningRecommendation> sr_result=sr.RelatedConceptRecommend();
		List<GardeningRecommendation> h_result=hybrid(cr_result,sr_result,beta, SKOS.RELATED);
		return h_result;
	}
	private List<GardeningRecommendation> hybrid(List<GardeningRecommendation> cr,List<GardeningRecommendation> sr,double beta, SKOS type){
		List <GardeningRecommendation> result=new ArrayList<GardeningRecommendation>();
		List<ConceptRecommendation> l=new ArrayList<ConceptRecommendation>();
		Map<ClientSideConcept,AggregatedRec> allrec=new HashMap<ClientSideConcept,AggregatedRec>();
		for(GardeningRecommendation a:cr){
			ConceptRecommendation c=(ConceptRecommendation)a;
			c.conf=beta*c.conf;
			l.add(c);	
		}
		for(GardeningRecommendation a:sr){
			ConceptRecommendation c=(ConceptRecommendation)a;
			c.conf=c.conf*(1-beta);
			l.add(c);	
		}
		for(ConceptRecommendation e:l){
			ClientSideConcept target=e.getConcept(); //target should be the same for all list
			logger.info("target concept: " + target.getURI());
			ClientSideConcept rec=e.getRecommendation();	
			if(allrec.containsKey(target)){
				AggregatedRec aggregated = allrec.get(target);
				aggregated.update(rec, e.conf);
				allrec.put(target, aggregated);
			}
			else{
				AggregatedRec aggregated = new AggregatedRec();
				aggregated.update(rec, e.conf);
				allrec.put(target, aggregated);
			}
		}
		l.clear();
		for(ClientSideConcept target:allrec.keySet()){
			AggregatedRec aggr = allrec.get(target);
			for(ClientSideConcept rec : aggr.recConf.keySet()){
				ConceptRecommendation g=new ConceptRecommendation(target,rec,aggr.recConf.get(rec));
				g.setType(type);
				l.add(g);
			}
		}
		Collections.sort(l);
		Collections.reverse(l);
		int i=1;
		for(ConceptRecommendation a:l){
			i++;
			result.add(a);
			if (i>10) break;
		}
		return result;
		
	}
}