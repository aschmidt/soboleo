package de.fzi.ipe.soboleo.space;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.SesameTripleStoreFactory;
import de.fzi.ipe.soboleo.dialog.eventBusAdaptor.DialogEventBusAdaptor;
import de.fzi.ipe.soboleo.dialog.widgetConnector.WidgetConnectorEventBusAdaptor;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.gardening.eventbus.GardeningRecommendationEventBusAdaptor;
import de.fzi.ipe.soboleo.lucene.SearchingLuceneWrapper;
import de.fzi.ipe.soboleo.lucene.eventubus.LuceneEventBusAdaptor;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStoreFactory;
import de.fzi.ipe.soboleo.peopleTagging.eventBusAdaptor.PeopleTaggingEventBusAdaptor;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetSearchingLuceneWrapper;
import de.fzi.ipe.soboleo.queries.rdf.skos.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.server.user.eventbus.EventPermitter;
import de.fzi.ipe.soboleo.skosTaxonomy.eventBusAdaptor.SkosTaxonomyEventBusAdaptor;
import de.fzi.ipe.soboleo.space.logging.EventHistoryLogger;
import de.fzi.ipe.soboleo.space.logging.ExternalUserLogging;
import de.fzi.ipe.soboleo.user.UserDatabase;
import de.fzi.ipe.soboleo.webdocument.eventBusAdaptor.WebDocumentEventBusAdaptor;


public class DefaultSpaceFactory implements SpaceFactory {

	protected Logger log = Logger.getLogger(DefaultSpaceFactory.class);
	
	protected TripleStoreFactory tripleStoreFactory = new SesameTripleStoreFactory();
	
	@Override
	public Space createSpace(File spaceDirectory, String spaceURI,
			UserDatabase userDatabase) throws IOException, EventPermissionDeniedException {

		log.debug("Creating space " + spaceURI);
		
		SpaceImpl s = new SpaceImpl(spaceDirectory, spaceURI, userDatabase);

		TripleStore ts = tripleStoreFactory.getTripleStore(new File(spaceDirectory,Space.TAXONOMY_DIRECTORY),"space-"+spaceURI);
		s.setTripleStore(ts);
		
		SkosTaxonomyEventBusAdaptor skosEventBusAdaptor = new SkosTaxonomyEventBusAdaptor(ts, s.getEventBus());
		s.getEventBus().addListener(skosEventBusAdaptor);
		ClientSideTaxonomy t = (ClientSideTaxonomy) s.getEventBus().executeQuery(new GetClientSideTaxonomy(true), EventSenderCredentials.SERVER);
		if (t == null)
			log.fatal("ClientSideTaxonomy from EventBus is null");
			
		s.setClientSideTaxonomy(t);

		// web document support
		WebDocumentEventBusAdaptor eventBusAdaptor = new WebDocumentEventBusAdaptor(ts);
		s.getEventBus().addListener(eventBusAdaptor);
		LuceneEventBusAdaptor luceneBusAdaptor = new LuceneEventBusAdaptor(new File(spaceDirectory,Space.INDEX_DIRECTORY), new File(spaceDirectory,Space.STORAGE_DIRECTORY),s.getEventBus(), s.getClientSideTaxonomy());
		s.getEventBus().addListener(luceneBusAdaptor);
		s.setLuceneEventBus(luceneBusAdaptor);
		s.setSearchingLuceneWrapper ((SearchingLuceneWrapper) s.getEventBus().executeQuery(new GetSearchingLuceneWrapper(), EventSenderCredentials.SERVER));
		
		s.getEventBus().addListener(new GardeningRecommendationEventBusAdaptor(s));
		s.getEventBus().addListener(new PeopleTaggingEventBusAdaptor(ts,s, userDatabase));
		s.getEventBus().addListener(new EventPermitter(s,userDatabase));
		
		if ("true".equals(s.getProperty(SpaceProperties.DIALOG_SUPPORT)))
		{
			s.getEventBus().addListener(new DialogEventBusAdaptor(ts));
			s.getEventBus().addListener(new WidgetConnectorEventBusAdaptor(s.getEventBus(),s));
		}
		
		s.getEventBus().addListener(new EventHistoryLogger(new File(spaceDirectory,Space.LOG_DIRECTORY), System.currentTimeMillis(), s));
		
		
		if ("true".equals(s.getProperty(SpaceProperties.EXTERNAL_USER_LOGGING)))
		{
			s.getEventBus().addListener(new ExternalUserLogging(s));
		}
		
		return s;
	}

}
