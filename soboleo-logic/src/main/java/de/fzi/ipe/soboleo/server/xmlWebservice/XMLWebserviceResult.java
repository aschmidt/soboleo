package de.fzi.ipe.soboleo.server.xmlWebservice;

import de.fzi.ipe.soboleo.event.Event;



/**
 * The object returned by the XMLWebservices that encabsulates any result. In addition to
 * an result object (or an exception) it also contains the most recent events. 
 */
public class XMLWebserviceResult {
	
	private Object result;
	private Exception exception;
	private Event[] events = new Event[0];
	
	public XMLWebserviceResult(Object result, Event[] events) {
		this.result = result;
		this.events = events;
	}
	
	public XMLWebserviceResult(Exception exception, Event[] events) {
		this.exception = exception;
		this.events = events;
	}
	
	public XMLWebserviceResult(Exception exception) {
		this.exception = exception;
	}
	
	public Object getResult() throws XMLWebserviceException {
		if (exception != null) throw new XMLWebserviceException(exception);
		else return result;
	}
	
	public Event[] getEvents() throws XMLWebserviceException {
		if (events == null && exception != null) throw new XMLWebserviceException(exception);
		else if (events == null) return new Event[0]; 
		else return events;
	}
	

}
