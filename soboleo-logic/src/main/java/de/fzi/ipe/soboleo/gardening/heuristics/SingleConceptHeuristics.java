/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.gardening.heuristics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptWithoutDescription;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

/**
 * A collection of gardening heuristics focussing only on single concepts. 
 */
public class SingleConceptHeuristics {

	public static Collection<GardeningRecommendation> getRecommendations(ClientSideTaxonomy taxonomy) {
		Collection<GardeningRecommendation> toReturn = new ArrayList<GardeningRecommendation>();
		
		toReturn.addAll(getConceptLabelRecommendations(taxonomy));
		return toReturn;
	}

	private static Collection<ConceptWithoutDescription> getConceptLabelRecommendations(ClientSideTaxonomy tax) {
		Set<ConceptWithoutDescription> toReturn = new HashSet<ConceptWithoutDescription>();
		for (ClientSideConcept concept: tax.getConcepts()) {
			if (concept.getText(SKOS.NOTE) == null) {
				toReturn.add(new ConceptWithoutDescription(concept));
			}
		}
		return toReturn;
	}
	
	
}
