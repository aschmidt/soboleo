/*
 * Class used to access information about an soboleo instance. It is used to access 
 * users, spaces etc. 
 * 
 * FZI - Information Process Engineering 
 * Created on 04.03.2009 by zach
 */
package de.fzi.ipe.soboleo.server;

import java.io.File;

import de.fzi.ipe.soboleo.space.Spaces;
import de.fzi.ipe.soboleo.user.UserDatabase;

public class Server {

	private File serverDirectory;
	private File spacesDirectory;
	private String serverRelativeURL;

	private String serverAbsoluteURL;
	private UserDatabase userDatabase;
	private Spaces spaces;

	private MessageConnector connector;

	public Server() { }
	
	public String getServerAbsoluteURL() {
		return serverAbsoluteURL;
	}

	public File getServerDirectory() {
		return serverDirectory;
	}

	public File getSpacesDirectory() {
		return spacesDirectory;
	}

	public String getServerRelativeURL() {
		return serverRelativeURL;
	}

	public UserDatabase getUserDatabase() {
		return userDatabase;
	}
	
	public void setServerDirectory(File f)
	{ serverDirectory = f; }
	
	public void setSpacesDirectory(File f)
	{ spacesDirectory = f; }
	
	public void setServerRelativeURL(String u)
	{ serverRelativeURL = u; }
	
	public void setUserDatabase(UserDatabase d)
	{ userDatabase = d; }
	
	public void setSpaces(Spaces s)
	{ spaces = s; }

	public void setServerAbsoluteURL(String u)
	{ serverAbsoluteURL = u; }
	
	/**
	 * Returns the existing spaces of the server
	 * 
	 * @return Spaces
	 */
	public Spaces getSpaces() {
		return spaces;
	}

	/*
	public Fetcher getFetcher() {
		return fetcher;
	}
	*/

	public MessageConnector getMessageConnector() {
		return connector;
	}
}
