package de.fzi.ipe.soboleo.dialog.widgetConnector;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.fzi.ipe.mature.XMLDialog;
import de.fzi.ipe.mature.XMLDialogRW;
import de.fzi.ipe.soboleo.beans.dialog.ConceptDialog;
import de.fzi.ipe.soboleo.beans.dialog.WebdocumentDialog;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.dialog.ContinueConceptDialog;
import de.fzi.ipe.soboleo.event.dialog.ContinueWebDocumentDialog;
import de.fzi.ipe.soboleo.event.dialog.StartConceptDialog;
import de.fzi.ipe.soboleo.event.dialog.StartWebDocumentDialog;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetConceptDialog;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetWebDocumentDialog;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetWebDocument;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.user.User;

/**
 * Event bus adaptor that makes watches for added documents & tags
 * adds these to the lucene index. Also answer queries
 * @author valentin
 *
 */
@Component
public class WidgetConnectorEventBusAdaptor implements EventBusEventProcessor, EventBusQueryProcessor{

	@Autowired
	private Server server;

	private EventBus eventBus;
	private Space space;
	private XMLDialogRW rw;
	
	public WidgetConnectorEventBusAdaptor(EventBus eventBus, Space space) throws IOException {
		this.eventBus = eventBus;
		this.space = space;
		rw = new XMLDialogRW();
	}

	@Override
	public void receiveEvent(Event event) {
		try {
			if (event instanceof StartConceptDialog) {
				StartConceptDialog startDialog = (StartConceptDialog) event;
				startConceptDialog(startDialog.getURI(), startDialog.getUserLanguage());
			}
			else if(event instanceof StartWebDocumentDialog){
				StartWebDocumentDialog startDialog = (StartWebDocumentDialog) event;
				startWebDocumentDialog(startDialog.getURI());
			}
			else if(event instanceof ContinueConceptDialog){
				ContinueConceptDialog continueDialog = (ContinueConceptDialog) event;
				startConceptDialog(continueDialog.getURI(), continueDialog.getUserLanguage());
			}
			else if(event instanceof ContinueWebDocumentDialog){
				ContinueWebDocumentDialog continueDialog = (ContinueWebDocumentDialog) event;
				startWebDocumentDialog(continueDialog.getURI());
			}
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void startWebDocumentDialog(String dialogURI)
			throws EventPermissionDeniedException, IOException {
		WebdocumentDialog dialog = (WebdocumentDialog) eventBus.executeQuery(new GetWebDocumentDialog(dialogURI), EventSenderCredentials.SERVER);
		WebDocument doc = (WebDocument) eventBus.executeQuery(new GetWebDocument(dialog.getAbout()), EventSenderCredentials.SERVER);
		User initiator = server.getUserDatabase().getUser(dialog.getInitiator());
		Set<User> participants = new HashSet<User>();
		for(String participantURI : dialog.getParticipants()){
			User participant = server.getUserDatabase().getUser(participantURI);
			participants.add(participant);
		}
		XMLDialog xmlWebdocumentDialog = rw.createXMLWebDocumentDialog(dialog, space.getURI(), doc.getURL(), initiator, participants);
		String xmlString = rw.toString(xmlWebdocumentDialog);
		if(server.getMessageConnector() != null) server.getMessageConnector().sendMessage(xmlString);
		else {
			//Todo ;
		}
	}

	private void startConceptDialog(String dialogURI, Language userLanguage) throws EventPermissionDeniedException, IOException {
		ConceptDialog dialog = (ConceptDialog) eventBus.executeQuery(new GetConceptDialog(dialogURI), EventSenderCredentials.SERVER);
		User initiator = server.getUserDatabase().getUser(dialog.getInitiator());
		Set<User> participants = new HashSet<User>();
		for(String participantURI : dialog.getParticipants()){
			User participant = server.getUserDatabase().getUser(participantURI);
			participants.add(participant);
		}
		ClientSideConcept csc = space.getClientSideTaxonomy().get(dialog.getAbout());
		LocalizedString prefLabel = new LocalizedString("", userLanguage);
		if(csc != null) prefLabel = csc.getBestFitText(SKOS.PREF_LABEL, userLanguage);
		XMLDialog xmlConceptDialog = rw.createXMLConceptDialog(dialog, space.getURI(), prefLabel, initiator, participants);
		String xmlString = rw.toString(xmlConceptDialog);
		if(server.getMessageConnector() != null) server.getMessageConnector().sendMessage(xmlString);
		else {
			//Todo ;
		}
	}
	
	
	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}


	@Override
	public Object process(QueryEvent query) {
		return null;
	}
}
