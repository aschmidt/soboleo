package de.fzi.ipe.soboleo.annotate.client.rpc;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.beans.rating.Rating;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;

public interface AnnotateService extends RemoteService{

	public Set<String> getAllLabels(String spaceName, EventSenderCredentials cred, Language userLanguage) throws EventPermissionDeniedException;
	
	public String[] getConceptRecommendation(String spaceName, String docURL) throws EventPermissionDeniedException;
	
	public void removeFromIndex(String spaceName, EventSenderCredentials cred, String docURL) throws EventPermissionDeniedException;
	
	public void saveAnnotationData(String spaceName, EventSenderCredentials cred, Language userLanguage, WebDocument webDoc, Map<String,String> conceptMap, String docTitle) throws EventPermissionDeniedException; 
	
	/**
	 * Saves the rating in the Annotation Web Page Pop up to ...
	 * 
	 * @param spaceName
	 * @param cred
	 * @param webDoc
	 * @param rating 
	 */
	public void saveAnnotationRating(String spaceName,EventSenderCredentials cred,WebDocument webDoc,double rating);
	/**
	 * Gets the total rating from the storage. 
	 * Used in Annotate Web Page.
	 * If the rating is -1 the rating is not supported.
	 * If the rating is 0, no rating has been made.
	 * If it is between 0 and 5, a rating was made.
	 * 
	 * @param spaceName
	 * @param cred
	 * @param webDoc
	 * @return Rating the rating object
	 */
	public Rating getAnnotationRating(String spaceName,EventSenderCredentials cred,WebDocument webDoc);
	

	public Set<ClientSideConcept> getConceptsForURIs(String spaceName, EventSenderCredentials cred,Set<String> myConceptURIs) throws EventPermissionDeniedException;
	
	public ClientSideConcept getConceptForLabel(String spaceName, EventSenderCredentials cred, LocalizedString label) throws EventPermissionDeniedException;
	
	public WebDocument getWebDocumentByURL(String spaceName, EventSenderCredentials cred, Language userLanguage, String docURL) throws EventPermissionDeniedException;
	
	public void startDialog(String docURL, String spaceName, Language userLanguage, EventSenderCredentials cred) throws EventPermissionDeniedException;
	
	public WebDocument createWebDocument(String spaceName, EventSenderCredentials cred, String docURL, String docTitle) throws EventPermissionDeniedException;
	
	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException;
	public Collection<ClientSideConcept> getConcepts(String spaceName, EventSenderCredentials eventSenderCredentials);
	/**
	 * Returns a space object for a given string if it exists and no
	 * error occurred. Else it will return null.
	 * 
	 * @param spaceName
	 * @param sourceFilePath  
	 * @return Space
	 * @throws IOException 
	 * @throws EventPermissionDeniedException 
	 */
	public String uploadDocToSpace(String spaceName,String sourceFilePath) throws IOException, EventPermissionDeniedException;
}
