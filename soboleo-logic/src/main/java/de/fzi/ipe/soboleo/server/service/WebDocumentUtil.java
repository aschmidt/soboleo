package de.fzi.ipe.soboleo.server.service;

import java.net.MalformedURLException;
import java.net.URL;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;

public class WebDocumentUtil {

	public static String checkURL(String docURL) throws EventPermissionDeniedException {
		if(!(docURL.startsWith("http:") || docURL.startsWith("https:") ||  docURL.startsWith("file:") ||  docURL.startsWith("ftp:") ||  docURL.startsWith("sftp:"))) docURL = "http://" +docURL;
		try {
			new URL(docURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new EventPermissionDeniedException("[error_300] URL not valid" , null);
		}
		return docURL;
	}
	
}
