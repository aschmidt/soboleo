package de.fzi.ipe.soboleo.server;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import de.fzi.ipe.soboleo.beans.client.ClientConfiguration;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.server.client.rpc.LoginService;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.User;
import de.fzi.ipe.soboleo.user.UserDatabase;

/**
 * @author FZI
 * 
 */
public class LoginServiceImpl extends AutowiringRemoteServiceServlet implements
		LoginService {

	private static final long serialVersionUID = 1L;

	protected static Logger logger = Logger.getLogger(LoginServiceImpl.class);
	
	@Autowired
	private Server server;
	
	public String login(String email, String password) {
		try {
			logger.info("login call for email : " + email);

			UserDatabase udb = server.getUserDatabase();
			User user = udb.getUserForEmail(email);
			if (user.isPassword(password)) {
				logger.info("return key: " + user.getKey());

				return user.getKey();
			} else {
				logger.info("found no user...");
				return null;
			}
		} catch (IOException e) {
			logger.error(e);
			return null;
		}
	}

	@Override
	public ClientConfiguration getConfig(String key, String spaceName) {
		try {
			UserDatabase udb = server.getUserDatabase();
			User user = udb.getUserForKey(key);
			if (user == null)
				user = udb.getDefaultUser();

			if (user != null) {
				ClientConfiguration config = new ClientConfiguration();
				config.setDefaultSpace(user.getDefaultSpaceIdentifier());
				Space space;
				if (spaceName != null)
					space = server.getSpaces().getSpace(spaceName);
				else
					space = server.getSpaces()
							.getSpace(user.getDefaultSpaceIdentifier());
				config.setDefaultSpaceLanguage(LocalizedString.Language
						.getLanguage(space
								.getProperty(SpaceProperties.DEFAULT_LANGUAGE)));
				config.setPrototypicalConceptsName(space
						.getProperty(SpaceProperties.PROTOTYPICAL_CONCEPT_NAME));
				config.setSpaceDialogSupport(Boolean.valueOf(
						space.getProperty(SpaceProperties.DIALOG_SUPPORT))
						.booleanValue());
				config.setDefaultUserLanguage(user.getDefaultLanguage());
				config.setUserName(user.getName());
				config.setUserID(user.getURI());
				config.setUserKey(user.getKey());
				config.setAnnotatePeopleDeleteTopic(Boolean.valueOf(space
						.getProperty(SpaceProperties.ANNOTATEPEOPLE_DELETABLE_TOPIC)));
				config.setRatingWebsides(Boolean.valueOf(space
						.getProperty(SpaceProperties.ANNOTATE_WEB_PAGE_RATING)));

				logger.info("user: " + config.getUserName());
				return config;
			} else
				return null;
		} catch (IOException e) {
			logger.error(e);
			return null;
		} catch (EventPermissionDeniedException epde) {
			logger.error(epde);
			return null;
		}
	}

}
