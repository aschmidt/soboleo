package de.fzi.ipe.soboleo.space.logging;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.dialog.ContinueConceptDialog;
import de.fzi.ipe.soboleo.event.dialog.ContinueWebDocumentDialog;
import de.fzi.ipe.soboleo.event.dialog.StartConceptDialog;
import de.fzi.ipe.soboleo.event.dialog.StartWebDocumentDialog;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.message.ChatCommand;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.AddTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ChangeTextCmd;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveTextCmd;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.queries.logging.GetRecentEvents;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetWebDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsByNameAndTopic;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;



public class EventHistoryLogger implements EventBusCommandProcessor,EventBusQueryProcessor,EventBusEventProcessor{
	private File logDir;
	private long runID;
	private XStream xStream;
	private int level = 0;

	@SuppressWarnings("unused")
	private Space space;
	private ClientSideTaxonomy tax;
	
	static Logger logger = Logger.getLogger(EventHistoryLogger.class);
	
	public EventHistoryLogger(File logDir, long runID, Space space){
		this.space = space;
		this.tax = space.getClientSideTaxonomy();
		String logLevel;
		try {
			logLevel = space.getProperty(SpaceProperties.EVENT_HISTORY_LOGGING);
			if (logLevel == null) logLevel = "false"; 
			if (logLevel.equals("false")) level = 0;
			else if (logLevel.equals("true")) level = 1;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		xStream = new XStream();
		if (!logDir.exists()) logDir.mkdirs();
		this.logDir = logDir;
		this.runID = runID;
	}

	
	@Override
	public void receiveEvent(Event event) {
		if (level > 0) { 
			write(event);
		}
	}
	
	@Override
	public CommandEvent prepare(CommandEvent command) {
		if(level > 0){
			if(command instanceof AddConnectionCmd){
				AddConnectionCmd addConnectionCmd = (AddConnectionCmd) command;
				ClientSideConcept from = tax.get(addConnectionCmd.getFromURI());
				ClientSideConcept to = tax.get(addConnectionCmd.getToURI());
				addConnectionCmd.setLogObjects(addConnectionCmd.getFromURI(), from);
				addConnectionCmd.setLogObjects(addConnectionCmd.getToURI(), to);
			} else if(command instanceof AddTextCmd){
				AddTextCmd addTextCmd = (AddTextCmd) command;
				ClientSideConcept from = tax.get(addTextCmd.getFromConceptURI());
				addTextCmd.setLogObjects(addTextCmd.getFromConceptURI(), from);
			} else if(command instanceof ChangeTextCmd){
				ChangeTextCmd changeTextCmd = (ChangeTextCmd) command;
				ClientSideConcept from = tax.get(changeTextCmd.getFromConceptURI());
				changeTextCmd.setLogObjects(changeTextCmd.getFromConceptURI(), from);
			} else if(command instanceof ChatCommand){
//				ChatCommand chatCommand = (ChatCommand) command;
//				ClientSideConcept from = tax.get(chatCommand.getSenderURI());
//				chatCommand.setLogObjects(chatCommand.getSenderURI(), from);
			} else if(command instanceof ContinueConceptDialog){
				ContinueConceptDialog continueConceptDialog = (ContinueConceptDialog) command;
//				ClientSideConcept from = tax.get(continueConceptDialog.getSenderURI());
				ClientSideConcept dialog = tax.get(continueConceptDialog.getURI());
//				continueConceptDialog.setLogObjects(continueConceptDialog.getSenderURI(), from);
				continueConceptDialog.setLogObjects(continueConceptDialog.getURI(), dialog);
			} else if(command instanceof ContinueWebDocumentDialog){
				ContinueWebDocumentDialog continueWSDialog = (ContinueWebDocumentDialog) command;
//				ClientSideConcept from = tax.get(continueWSDialog.getSenderURI());
				ClientSideConcept dialog = tax.get(continueWSDialog.getURI());
//				continueWSDialog.setLogObjects(continueWSDialog.getSenderURI(), from);
				continueWSDialog.setLogObjects(continueWSDialog.getURI(), dialog);
			} else if(command instanceof CreateConceptCmd){
//				CreateConceptCmd createConceptCmd = (CreateConceptCmd) command;
//				ClientSideConcept from = tax.get(createConceptCmd.getSenderURI());
//				createConceptCmd.setLogObjects(createConceptCmd.getSenderURI(), from);
			} else if(command instanceof RemoveConceptCmd){
				RemoveConceptCmd removeConceptCmd = (RemoveConceptCmd) command;
//				ClientSideConcept from = tax.get(removeConceptCmd.getSenderURI());
				ClientSideConcept concept = tax.get(removeConceptCmd.getConceptURI());
//				removeConceptCmd.setLogObjects(removeConceptCmd.getSenderURI(), from);
				removeConceptCmd.setLogObjects(removeConceptCmd.getConceptURI(), concept);
			} else if(command instanceof RemoveConnectionCmd){
				RemoveConnectionCmd removeConnectionCmd = (RemoveConnectionCmd) command;
				ClientSideConcept from = tax.get(removeConnectionCmd.getFromConceptURI());
				ClientSideConcept to = tax.get(removeConnectionCmd.getToConceptURI());
				removeConnectionCmd.setLogObjects(removeConnectionCmd.getFromConceptURI(), from);
				removeConnectionCmd.setLogObjects(removeConnectionCmd.getToConceptURI(), to);
			} else if (command instanceof RemoveTextCmd){
				RemoveTextCmd removeTextCmd = (RemoveTextCmd) command;
				ClientSideConcept from = tax.get(removeTextCmd.getFromConceptURI());
				removeTextCmd.setLogObjects(removeTextCmd.getFromConceptURI(), from);
			} else if(command instanceof StartConceptDialog){
				StartConceptDialog startConceptDialog = (StartConceptDialog) command;
				ClientSideConcept about = tax.get(startConceptDialog.getAboutURI());
				ClientSideConcept concept = tax.get(startConceptDialog.getURI());
//				ClientSideConcept from = tax.get(startConceptDialog.getSenderURI());
				startConceptDialog.setLogObjects(startConceptDialog.getAboutURI(), about);
				startConceptDialog.setLogObjects(startConceptDialog.getURI(), concept);
//				startConceptDialog.setLogObjects(startConceptDialog.getSenderURI(), from);
			} else if(command instanceof StartWebDocumentDialog){
				StartWebDocumentDialog startWebDocumentDialog = (StartWebDocumentDialog) command;
				ClientSideConcept about = tax.get(startWebDocumentDialog.getAboutURI());
				ClientSideConcept concept = tax.get(startWebDocumentDialog.getURI());
//				ClientSideConcept from = tax.get(startWebDocumentDialog.getSenderURI());
				startWebDocumentDialog.setLogObjects(startWebDocumentDialog.getAboutURI(), about);
				startWebDocumentDialog.setLogObjects(startWebDocumentDialog.getURI(), concept);
//				startWebDocumentDialog.setLogObjects(startWebDocumentDialog.getSenderURI(), from);
			}
		}
		return null;
	}

	@Override
	public Object process(CommandEvent command) {
		if(level > 0) write(command);
		command.resetLogObjects();
		return null;
	}

	@Override
	public QueryEvent prepare(QueryEvent query) {
		if(level > 0){
			if(query instanceof GetWebDocumentsSearchResult){
				write(query);
			}
			if(query instanceof GetDocumentsSearchResult){
				write(query);
			}
			if (query instanceof SearchPersonsByNameAndTopic){
				write(query);
			}
			if (query instanceof SearchPersons){
				write(query);
			}		
		}
		return null;
	}

	@Override
	public Object process(QueryEvent query) {
		if (query instanceof GetRecentEvents) {
			GetRecentEvents recentEventsQuery = (GetRecentEvents) query;
			return processGetEvents(recentEventsQuery);
		}
		return null;
	}


	private List<Event> processGetEvents(GetRecentEvents query) {
		File[] files = logDir.listFiles();
		if (files != null) {
			Arrays.sort(files, new FileComparator() {
				});
			List<Event> toReturn = new ArrayList<Event>(20);
			int offset = query.getOffset();
			for (int i=0;(i<files.length && toReturn.size()<20);i++) {
				try {
					File currentFile = files[i];
					InputStream in = new FileInputStream(currentFile);
					InputStreamReader reader = new InputStreamReader(in,Charset.forName("UTF-8"));
					Event currentEvent = (Event) xStream.fromXML(reader);
					if (currentEvent != null && hasCorrectClass(currentEvent,query)) {
						if (offset > 0) offset --;
						else toReturn.add(currentEvent);
					}
					in.close();

				} catch (Exception ioe) {
					ioe.printStackTrace();
				}
			}
			return toReturn;
		}
		else {
			return new ArrayList<Event>(0);
		}
	}

	/**
	 * Helper method that checks whether the current events has a class that 
	 * is of interest to the GetRecentEvents query. 
	 * @param event
	 * @param query
	 */
	private boolean hasCorrectClass(Event event, GetRecentEvents query) {
		if (query.getClasses() == null) return true;
		else {
			for (Class<? extends Event> c: query.getClasses()) {
				if (c.isInstance(event)) return true;
			}
			return false;
		}
	}
	
	public void write(Event event){
		try {
			File location = new File(logDir, runID+"-"+event.getId()+".xml");
			OutputStream outStream = new FileOutputStream(location);
			OutputStreamWriter out = new OutputStreamWriter(outStream,Charset.forName("UTF-8"));
			xStream.toXML(event, out);
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Comparator used for sorting the event files in chronological order
	 */
	private class FileComparator implements Comparator<File> {

		@Override
		public int compare(File arg0, File arg1) {
			return getEventID(arg1)-getEventID(arg0);
		}

		private int getEventID(File arg) {
			String fileName = arg.getName();
			String number = fileName.substring(fileName.indexOf("-")+1,fileName.indexOf("."));
			return Integer.parseInt(number);
		}
		
		
	}
	
	
}
