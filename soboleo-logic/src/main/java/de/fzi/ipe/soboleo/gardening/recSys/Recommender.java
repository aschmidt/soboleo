package de.fzi.ipe.soboleo.gardening.recSys;


import java.util.List;

import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public interface Recommender {
	
	
	public List<GardeningRecommendation> SuperConceptRecommend() throws Exception;
	public List<GardeningRecommendation> RelatedConceptRecommend() throws Exception;

/*	public Set<String> getAllconceptURI(){
		Set<String> allURI=new HashSet<String>();
		String uri=new String();
		
		Set<ClientSideConcept> roots = tax.getRootConcepts();
		for (ClientSideConcept c:roots)
		uri=c.getURI();
		Set<String> s=tax.getAllSubconceptURIs(uri);
		for(String a:s)
			allURI.add(a);
					
		return allURI;*/		
		
		//}
}
