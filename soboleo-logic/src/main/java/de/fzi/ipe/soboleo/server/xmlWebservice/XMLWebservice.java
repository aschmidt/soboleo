package de.fzi.ipe.soboleo.server.xmlWebservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationContextUtils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.Spaces;
import de.fzi.ipe.soboleo.space.commandHistory.QueryPastEvents;

public class XMLWebservice extends HttpServlet {

	private Spaces spaces;

	private static final long serialVersionUID = 1L;

	private XStream xStream;

	public XMLWebservice() {
		xStream = new XStream(new DomDriver());
	}

	@Override
	public void init() throws ServletException {
		ServletConfig config = getServletConfig();
		spaces = (Spaces) WebApplicationContextUtils
				.getRequiredWebApplicationContext(config.getServletContext())
				.getBean("spaces");
	}

	public void doGet(HttpServletRequest rq, HttpServletResponse rs)
			throws IOException, ServletException {
		doPost(rq, rs);
	}

	public void doPost(HttpServletRequest rq, HttpServletResponse rs)
			throws IOException, ServletException {
		XMLWebserviceResult toSend = processXMLRequest(rq.getInputStream());
		xStream.toXML(toSend, new OutputStreamWriter(rs.getOutputStream()));
	}

	private XMLWebserviceResult processXMLRequest(InputStream inputStream) {
		XMLWebserviceRequest requestObject = (XMLWebserviceRequest) xStream
				.fromXML(new InputStreamReader(inputStream));
		try {
			Object result = null;
			Space space = spaces.getSpace(requestObject.getSpaceID());
			if (requestObject.getCredentials() == EventSenderCredentials.SERVER) {
				// comes from outside but claims to be SERVER
				throw new EventPermissionDeniedException(
						"Server event credentials must not be used for remote calls",
						null);
			}
			if (requestObject.getRequestPayload() instanceof QueryEvent) {
				QueryEvent query = (QueryEvent) requestObject
						.getRequestPayload();
				result = space.getEventBus().executeQuery(query,
						requestObject.getCredentials());
			} else if (requestObject.getRequestPayload() instanceof CommandEvent) {
				CommandEvent command = (CommandEvent) requestObject
						.getRequestPayload();
				result = space.getEventBus().executeCommandNow(command,
						requestObject.getCredentials());
			} else if (requestObject.getRequestPayload() instanceof Event) {
				Event event = (Event) requestObject.getRequestPayload();
				space.getEventBus().sendEvent(event,
						requestObject.getCredentials());
			} else
				throw new NullPointerException(
						"No recognized payload object for webservice request");

			Event[] pastEvents = new Event[0];
			if (requestObject.getLastEventID() < Long.MAX_VALUE) {
				pastEvents = (Event[]) space.getEventBus().executeQuery(
						new QueryPastEvents(requestObject.getLastEventID()),
						requestObject.getCredentials());
			}
			return new XMLWebserviceResult(result, pastEvents);
		} catch (Exception e) {
			return new XMLWebserviceResult(e);
		}
	}

}