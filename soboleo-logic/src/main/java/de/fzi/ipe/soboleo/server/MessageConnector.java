package de.fzi.ipe.soboleo.server;

import java.io.IOException;

public interface MessageConnector {
	public int sendMessage(String content) throws IOException;
	public void processResponse(String content);
}
