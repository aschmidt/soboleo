package de.fzi.ipe.soboleo.server.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.fzi.ipe.soboleo.beans.client.ClientConfiguration;


/**
 * @author FZI
 *
 */
public interface LoginServiceAsync {

	public void getConfig(String key,String space, AsyncCallback<ClientConfiguration> callback);

	public void login(String email, String password, AsyncCallback<String> callback);
	
}