/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.dialog.eventBusAdaptor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import de.fzi.ipe.soboleo.beans.dialog.Dialog;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFTag;
import de.fzi.ipe.soboleo.event.dialog.AddDialogTag;
import de.fzi.ipe.soboleo.event.dialog.ContinueDialog;
import de.fzi.ipe.soboleo.event.dialog.RemoveDialog;
import de.fzi.ipe.soboleo.event.dialog.RemoveDialogTag;
import de.fzi.ipe.soboleo.event.dialog.SetDialogContent;
import de.fzi.ipe.soboleo.event.dialog.SetDialogTitle;
import de.fzi.ipe.soboleo.event.dialog.StartConceptDialog;
import de.fzi.ipe.soboleo.event.dialog.StartWebDocumentDialog;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.execution.rdf.dialog.RDFConceptDialog;
import de.fzi.ipe.soboleo.execution.rdf.dialog.RDFDialog;
import de.fzi.ipe.soboleo.execution.rdf.dialog.RDFDialogDatabase;
import de.fzi.ipe.soboleo.execution.rdf.dialog.RDFWebDocumentDialog;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetConceptDialog;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetConceptDialogsForConcept;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetDialog;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetDialogsForWebDocument;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetWebDocumentDialog;

@Component
public class DialogEventBusAdaptor implements EventBusCommandProcessor, EventBusQueryProcessor{

	private RDFDialogDatabase database;
	
	
	public DialogEventBusAdaptor(TripleStore tripleStore) throws IOException {
		this.database = new RDFDialogDatabase(tripleStore);
	}
	
	
	@Override
	public CommandEvent prepare(CommandEvent command) {
		return null;
	}

	@Override
	public Object process(CommandEvent command) {
		try {
			if(command instanceof StartConceptDialog){
				StartConceptDialog startDialog = (StartConceptDialog) command;
				RDFDialog dialog = database.createDocument(SoboleoNS.DIALOG_CONCEPT.toString());
				dialog.setTitle(startDialog.getTitle());
				dialog.setAbout(startDialog.getAboutURI());
				dialog.setInitiator(startDialog.getSenderURI());
				dialog.addParticipant(startDialog.getSenderURI());
				startDialog.setURI(dialog.getURI().toString());
				return startDialog.getURI();
			}
			else if (command instanceof StartWebDocumentDialog) {
				StartWebDocumentDialog addDialog = (StartWebDocumentDialog) command;
				RDFDialog dialog = database.createDocument(SoboleoNS.DIALOG_WEBDOCUMENT.toString());
				dialog.setTitle(addDialog.getTitle());
				dialog.setAbout(addDialog.getAboutURI());
				dialog.setInitiator(addDialog.getSenderURI());
				dialog.addParticipant(addDialog.getSenderURI());
				addDialog.setURI(dialog.getURI().toString());
				return addDialog.getURI();
			}
			else if(command instanceof ContinueDialog){
				ContinueDialog continueDialog = (ContinueDialog) command;
				RDFDialog document = database.getDocument(continueDialog.getURI());
				if(document != null && !document.getInitiator().equals(continueDialog.getSenderURI())){
					document.setInitiator(continueDialog.getSenderURI());
					document.addParticipant(continueDialog.getSenderURI());
				}
			}
			else if (command instanceof AddDialogTag) {
				AddDialogTag addTag = (AddDialogTag) command;
				RDFDialog document = database.getDocument(addTag.getDocumentURI());
				if (document != null){
					RDFTag tag = document.addTag(addTag.getSenderURI(), addTag.getSia().getUri());
					addTag.setTagURI(tag.getURI().toString());
					return addTag.getTagURI();
				}
			}
			else if (command instanceof RemoveDialog) {
				RemoveDialog removeDialog = (RemoveDialog) command;
				database.deleteDocument(removeDialog.getDocURI());
			}
			else if (command instanceof RemoveDialogTag) {
				RemoveDialogTag removeTag = (RemoveDialogTag) command;
				RDFTag tag = database.getAnnotation(removeTag.getTagID());
				tag.delete();
			}
			else if (command instanceof SetDialogTitle) {
				SetDialogTitle setDialogTitle = (SetDialogTitle) command;
				RDFDialog doc = database.getDocument(setDialogTitle.getDocURI());
				if(doc != null) doc.setTitle(setDialogTitle.getTitle());
			}
			else if(command instanceof SetDialogContent){
				SetDialogContent setDialogContent = (SetDialogContent) command;
				RDFDialog doc = database.getDocument(setDialogContent.getDocURI());
				if(doc != null) doc.setContent(setDialogContent.getContent());
			}
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
		return null;
	}


	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}


	@Override
	public Object process(QueryEvent query) {
		try {
			if (query instanceof GetConceptDialog) {
				GetConceptDialog getConceptDialog = (GetConceptDialog) query;
				RDFDialog rdfDialog = database.getDocument(getConceptDialog.getURI());
				return (rdfDialog == null || !(rdfDialog instanceof RDFConceptDialog)) ? null: rdfDialog.getClientSideDocument();
			}
			else if(query instanceof GetWebDocumentDialog){
				GetWebDocumentDialog getWebDocumentDialog = (GetWebDocumentDialog) query;
				RDFDialog rdfDialog = database.getDocument(getWebDocumentDialog.getURI());
				return (rdfDialog == null || !(rdfDialog instanceof RDFWebDocumentDialog)) ? null: rdfDialog.getClientSideDocument();
			}
			else if (query instanceof GetDialog){
				GetDialog getDialog = (GetDialog) query;
				RDFDialog rdfDialog = database.getDocument(getDialog.getURI());
				return (rdfDialog == null) ? null : rdfDialog.getClientSideDocument();
			}
			else if(query instanceof GetConceptDialogsForConcept){
				GetConceptDialogsForConcept getDialogs = (GetConceptDialogsForConcept) query;
				Set<RDFDialog> conceptDialogs = database.getDialogsByAbout(getDialogs.getConceptURI(), SoboleoNS.DIALOG_CONCEPT.toString());
				List<Dialog> toReturn = new ArrayList<Dialog>();
				for(RDFDialog d : conceptDialogs){
					if(d.getContent() != null) toReturn.add(d.getClientSideDocument());
				}
				return toReturn;
			}
			else if(query instanceof GetDialogsForWebDocument){
				GetDialogsForWebDocument getDialogs = (GetDialogsForWebDocument) query;
				Set<RDFDialog> webDocumentDialogs = database.getDialogsByAbout(getDialogs.getWebDocumentURI(), SoboleoNS.DIALOG_WEBDOCUMENT.toString());
				List<Dialog> toReturn = new ArrayList<Dialog>();
				for(RDFDialog d : webDocumentDialogs){
					if(d.getContent() != null) toReturn.add(d.getClientSideDocument());
				}
				return toReturn;
			}
			return null;
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	
}
