package de.fzi.ipe.soboleo.annotateDocuments.client.rpc;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.rating.Rating;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.lucene.document.LuceneIndexDocument;

public interface AnnotateDocumentsServiceAsync {

	
	public void testService(AsyncCallback<String> callback);

	public void getOfficeDocumentByURI(String spaceName, EventSenderCredentials cred, Language userLanguage, String docURI, AsyncCallback<LuceneIndexDocument> callback);
	
	public void getConceptRecommendation(String spaceName, String uri, AsyncCallback<String[]> callback);
	public void getAllLabels(String spaceName, EventSenderCredentials cred, Language userLanguage, AsyncCallback<Set<String>> callback);
	public void removeFromIndex(String spaceName, EventSenderCredentials cred, String docURL, AsyncCallback<Void> callback);
	public void saveAnnotationData(String spaceName, EventSenderCredentials cred, Language userLanguage, LuceneIndexDocument officeDoc, Map<String,String> conceptMap, AsyncCallback<Void> callback); 
	/**
	 * Saves the rating in the Annotation Web Page Pop up to ...
	 * 
	 * @param spaceName
	 * @param cred
	 * @param webDoc
	 * @param rating 
	 * @param callback 
	 */
	public void saveAnnotationRating(String spaceName,EventSenderCredentials cred,LuceneIndexDocument officeDoc,double rating, AsyncCallback<Void> callback);
	/**
	 * Gets the total rating from the storage. 
	 * Used in Annotate Web Page.
	 * If the rating is -1 the rating is not supported.
	 * If the rating is 0, no rating has been made.
	 * If it is between 0 and 5, a rating was made.
	 * 
	 * @param spaceName
	 * @param cred
	 * @param webDoc
	 * @param callback 
	 * @return double Rating -1 not supported 0 not yet made 1..5 for made ratings
	 */
	public void getAnnotationRating(String spaceName,EventSenderCredentials cred,LuceneIndexDocument officeDoc,AsyncCallback<Rating> callback);

	public void startDialog(String docURL, String spaceName, Language userLanguage, EventSenderCredentials cred, AsyncCallback<Void> callback);
	public void getConceptsForURIs(String spaceName, EventSenderCredentials cred,Set<SemanticAnnotation> myConceptURIs, AsyncCallback<Set<ClientSideConcept>> callback);
	public void getConceptForLabel(String spaceName, EventSenderCredentials cred,LocalizedString label, AsyncCallback<ClientSideConcept> callback);


	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred, AsyncCallback<Void> callback);
	public void getConcepts(String spaceName, EventSenderCredentials eventSenderCredentials, AsyncCallback<Collection<ClientSideConcept>> asyncCallback);
	/**
	 * Returns a space object for a given string if it exists and no
	 * error occurred. Else it will return null.
	 * 
	 * @param spaceName
	 * @param sourceFilePath 
	 * @param asyncCallback 
	 * @throws IOException 
	 * @throws EventPermissionDeniedException 
	 */
	public void uploadDocToSpace(String spaceName,String sourceFilePath,AsyncCallback<String> asyncCallback) throws IOException, EventPermissionDeniedException;
	
}
