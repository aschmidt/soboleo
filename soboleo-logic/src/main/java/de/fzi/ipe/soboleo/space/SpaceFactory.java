package de.fzi.ipe.soboleo.space;

import java.io.File;
import java.io.IOException;

import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.user.UserDatabase;

public interface SpaceFactory {
	public Space createSpace(File spaceDirectory, String spaceURI, UserDatabase userDatabase) throws IOException, EventPermissionDeniedException;
}
