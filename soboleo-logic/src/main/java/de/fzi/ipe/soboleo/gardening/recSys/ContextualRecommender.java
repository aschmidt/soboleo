package de.fzi.ipe.soboleo.gardening.recSys;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.HashSet;

import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptRecommendation;


public class ContextualRecommender implements Recommender{
	
	 private ClientSideTaxonomy tax;
	 private Set<ClientSideConcept> targetConcept;
	 private Set<WebDocument> docs;
	
	
	
	public ContextualRecommender(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition, Set<WebDocument> docs){
		this.tax=tax;
		this.targetConcept=conceptsToPosition;
		this.docs=docs;	
	   }
			
	public List<GardeningRecommendation> RelatedConceptRecommend() throws Exception{
		
	List<ConceptRecommendation> l=new ArrayList<ConceptRecommendation>();
	List<GardeningRecommendation> result=new ArrayList<GardeningRecommendation>();
	AnnotationSet data=new AnnotationSet(tax,docs);
	Collection<ClientSideConcept> allconcepts=tax.getConcepts();
	intPair[][] tagProfiles=data.buildTagProfileAsResourceCount();
	for(ClientSideConcept target:targetConcept){
		
		if (data.tagMap.containsKey(target.getURI())){
	//	System.out.println(target.getText(SKOS.PREF_LABEL).getString());
		int conceptId= data.tagMap.get(target.getURI());
	//	System.out.println(conceptId);
	    intPair[] tagVector=tagProfiles[conceptId];
			
		for (ClientSideConcept other:allconcepts){
			if (!other.equals(target)){
		if (data.tagMap.containsKey(other.getURI())){
		int id=data.tagMap.get(other.getURI());
		intPair[] otherTagVector=tagProfiles[id];
		double sim=CosineSimilarity.calcCosineSimilarity(tagVector, otherTagVector);
		if (sim!=0){
		ConceptRecommendation g=new ConceptRecommendation(target,other,sim);
		g.setType(SKOS.RELATED);
		l.add(g);}
		}
		}
		}	
		
	}
}
	Collections.sort(l);
	Collections.reverse(l);
	int i=1;
	for(ConceptRecommendation a:l){
	i++;
	result.add(a);
	if (i>10) break;
	}
	return result;
	}

	//Super concepts are recommended based on the related concepts, the supers of related concepts are recommended
	public List<GardeningRecommendation> SuperConceptRecommend()
			throws Exception {
		List<GardeningRecommendation> l= RelatedConceptRecommend();
		List<ConceptRecommendation> superlist= new ArrayList<ConceptRecommendation>();
		List<GardeningRecommendation> result=new ArrayList<GardeningRecommendation>();
		Map<ClientSideConcept,Integer> allsupers=new HashMap<ClientSideConcept,Integer>();
		ClientSideConcept target=new ClientSideConcept(); //target is the concept we want to make recommendation for so it will be the same for all values of l
		int max=1;
		for(GardeningRecommendation a:l){
			ConceptRecommendation c=(ConceptRecommendation)a;
			target=c.getConcept();
			ClientSideConcept relatedrec=c.getRecommendation();
			System.out.println("relatedconcept="+relatedrec.getText(SKOS.PREF_LABEL));
			Set<ClientSideConcept> sn=relatedrec.getConnectedAsConcepts(SKOS.HAS_BROADER,tax);
			Set<ClientSideConcept> supersuper=new HashSet<ClientSideConcept>();
			for(ClientSideConcept s:sn){
			Set<ClientSideConcept> snbr=s.getConnectedAsConcepts(SKOS.HAS_BROADER,tax);
			supersuper.addAll(snbr);}
			sn.addAll(supersuper);
		
			if(sn!=null){
				for(ClientSideConcept s:sn){
					System.out.println("broder="+s.getText(SKOS.PREF_LABEL));
				if (allsupers.containsKey(s)){
					if (allsupers.get(s)+1>max)
					max=allsupers.get(s)+1;
					allsupers.put(s, allsupers.get(s)+1);
				
				}
				else{
					allsupers.put(s,1);
					}
				}
				
			}
		}
		for(ClientSideConcept cl:allsupers.keySet())	{
			//	System.out.println(allsupers.get(cl));
				double conf=100*allsupers.get(cl)/max; // max is used to normalize the confidence
			ConceptRecommendation g=new ConceptRecommendation(target,cl,conf/100);
			g.setType(SKOS.HAS_BROADER);
			superlist.add(g);
		}
		Collections.sort(superlist);
		Collections.reverse(superlist);
		int i=1;
		for(ConceptRecommendation crc:superlist){
			i++;
			result.add(crc);
			if (i>10) break;
		}
		return result;
		}
	}
		
		
			
	
	
