package de.fzi.ipe.soboleo.server;

/**
 * Make GWT servlets configurable by Spring
 * 
 * @author Andreas Schmidt
 */

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public abstract class AutowiringRemoteServiceServlet extends RemoteServiceServlet {
	private static final long serialVersionUID = 1L;

	@Override
    public void init() throws ServletException {
        super.init();
 
        final WebApplicationContext ctx =
            WebApplicationContextUtils.getWebApplicationContext(getServletContext());
 
        if (ctx != null) 
        {
            ctx.getAutowireCapableBeanFactory().autowireBeanProperties(this,
                    AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, true);
        } 
    }
}