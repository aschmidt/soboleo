package de.fzi.ipe.soboleo.gardening.recSys;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.gardening.recommendations.ConceptRecommendation;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public class StringRecommender implements Recommender{
	static Logger logger = Logger.getLogger(StringRecommender.class);
	
	 ClientSideTaxonomy tax;
	 Set<ClientSideConcept> targetConcept;
	public StringRecommender(ClientSideTaxonomy tax){
		this.tax=tax;
	
	}
	
	public StringRecommender(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition ){
		this.tax=tax;
		this.targetConcept=conceptsToPosition;
	
	}
			
	public List<GardeningRecommendation> SuperConceptRecommend() throws Exception{
		
		List<ConceptRecommendation> l=new ArrayList<ConceptRecommendation>();
		List<GardeningRecommendation> result=new ArrayList<GardeningRecommendation>();
		
		LocalizedString targetString=new LocalizedString(); String targetLabel=new String();
		LocalizedString existingString=new LocalizedString();String existingLabel=new String();
		
		Collection<ClientSideConcept> allconcepts=tax.getConcepts();
		for(ClientSideConcept target:targetConcept){
		targetString=target.getText(SKOS.PREF_LABEL);
		targetLabel=targetString.getString();
		
		for (ClientSideConcept other:allconcepts){
		existingString=other.getText(SKOS.PREF_LABEL);
		existingLabel=existingString.getString();
		double sim=StringSimilarity.jaccardSS( existingLabel,targetLabel);
		if (sim!=0){
		//	int p=(int) Math.round((1-sim)*10);
		ConceptRecommendation g=new ConceptRecommendation(target,other,sim);
		//System.out.println(existingLabel+" "+sim);
		l.add(g);
		}
		}
		}
		Collections.sort(l);
		Collections.reverse(l);
		int i=1;
		for(ConceptRecommendation a:l){
		i++;
		result.add(a);
		if (i>10) break;
		}
		return result;
		}


	public List<GardeningRecommendation> RelatedConceptRecommend()
			throws Exception {
		List<ConceptRecommendation> l=new ArrayList<ConceptRecommendation>();
		List<GardeningRecommendation> result=new ArrayList<GardeningRecommendation>();
		
		LocalizedString targetString=new LocalizedString(); String targetLabel=new String();
		LocalizedString existingString=new LocalizedString();String existingLabel=new String();
		
		Collection<ClientSideConcept> allconcepts=tax.getConcepts();
		for(ClientSideConcept target:targetConcept){
		targetString=target.getText(SKOS.PREF_LABEL);
		targetLabel=targetString.getString();
		
		for (ClientSideConcept other:allconcepts){
		existingString=other.getText(SKOS.PREF_LABEL);
		existingLabel=existingString.getString();
		double sim=StringSimilarity.jaccardSim( existingLabel,targetLabel);
		if (sim!=0 && sim!=1){
		//	int p=(int) Math.round((1-sim)*10);
		ConceptRecommendation g=new ConceptRecommendation(target,other,sim);
		//System.out.println(existingLabel+" "+sim);
		l.add(g);
		}
		}
		}
		Collections.sort(l);
		Collections.reverse(l);
		int i=1;
		for(ConceptRecommendation a:l){
		i++;
		result.add(a);
		if (i>10) break;
		}
		return result;
	}
	public List<GardeningRecommendation> SuperConceptRecommendusingtax() throws Exception{
	
		List<GardeningRecommendation> l= RelatedConceptRecommend();
		List<ConceptRecommendation> superlist= new ArrayList<ConceptRecommendation>();
		List<GardeningRecommendation> result=new ArrayList<GardeningRecommendation>();
		Map<ClientSideConcept,Integer> allsupers=new HashMap<ClientSideConcept,Integer>();
		ClientSideConcept target=new ClientSideConcept(); //target is the concept we want to make recommendation for so it will be the same for all values of l
		for(GardeningRecommendation a:l){
			ConceptRecommendation c=(ConceptRecommendation)a;
			target=c.getConcept();
			ClientSideConcept relatedrec=c.getRecommendation();
			System.out.println("relatedconcept="+relatedrec.getText(SKOS.PREF_LABEL));
			Set<ClientSideConcept> sn=relatedrec.getConnectedAsConcepts(SKOS.HAS_BROADER,tax);
			Set<ClientSideConcept> supersuper=new HashSet<ClientSideConcept>();
			for(ClientSideConcept s:sn){
			Set<ClientSideConcept> snbr=s.getConnectedAsConcepts(SKOS.HAS_BROADER,tax);
			supersuper.addAll(snbr);}
			sn.addAll(supersuper);
		
			if(sn!=null){
				for(ClientSideConcept s:sn){
					System.out.println("braoder="+s.getText(SKOS.PREF_LABEL));
				if (allsupers.containsKey(s))
					allsupers.put(s, allsupers.get(s)+1);
				else
					allsupers.put(s,1);
				}
				
			}
		}
		for(ClientSideConcept cl:allsupers.keySet())	{
		//	System.out.println(allsupers.get(cl));
		ConceptRecommendation g=new ConceptRecommendation(target,cl,allsupers.get(cl));	
		superlist.add(g);
		}
		Collections.sort(superlist);
		Collections.reverse(superlist);
		int i=1;
		for(ConceptRecommendation crc:superlist){
		i++;
		result.add(crc);
		if (i>10) break;
		}
		return result;
		}
	public List<GardeningRecommendation> hybridSuperConceptRecommendusingtax() throws Exception{
		double alpha=.5;
		List<GardeningRecommendation> cr_result=SuperConceptRecommendusingtax();
		List<GardeningRecommendation> sr_result=SuperConceptRecommend();
		List<GardeningRecommendation> h_result=hybrid(cr_result,sr_result,alpha);
		return h_result;
	}
	private List<GardeningRecommendation> hybrid(List<GardeningRecommendation> cr,List<GardeningRecommendation> sr,double alpha){
		List <GardeningRecommendation> result=new ArrayList<GardeningRecommendation>();
		List<ConceptRecommendation> l=new ArrayList<ConceptRecommendation>();
		Map<ClientSideConcept,AggregatedRec> allrec=new HashMap<ClientSideConcept,AggregatedRec>();
		for(GardeningRecommendation a:cr){
			ConceptRecommendation c=(ConceptRecommendation)a;
			c.conf=alpha*c.conf;
			l.add(c);	
		}
		for(GardeningRecommendation a:sr){
			ConceptRecommendation c=(ConceptRecommendation)a;
			c.conf=c.conf*(1-alpha);
			l.add(c);	
		}
		for(ConceptRecommendation e:l){
			ClientSideConcept target=e.getConcept(); //target should be the same for all list
			logger.info("target concept: " + target.getURI());
			ClientSideConcept rec=e.getRecommendation();	
			if(allrec.containsKey(target)){
				AggregatedRec aggregated = allrec.get(target);
				aggregated.update(rec, e.conf);
				allrec.put(target, aggregated);
			}
			else{
				AggregatedRec aggregated = new AggregatedRec();
				aggregated.update(rec, e.conf);
				allrec.put(target, aggregated);
			}
		}
		l.clear();
		for(ClientSideConcept target:allrec.keySet()){
			AggregatedRec aggr = allrec.get(target);
			for(ClientSideConcept rec : aggr.recConf.keySet()){
				ConceptRecommendation g=new ConceptRecommendation(target,rec,aggr.recConf.get(rec));
				l.add(g);
			}
		}
		Collections.sort(l);
		Collections.reverse(l);
		int i=1;
		for(ConceptRecommendation a:l){
		i++;
		result.add(a);
		if (i>10) break;
		}
		return result;
		
	}
	
}
