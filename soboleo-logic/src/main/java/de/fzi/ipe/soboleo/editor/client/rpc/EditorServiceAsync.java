package de.fzi.ipe.soboleo.editor.client.rpc;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;

public interface EditorServiceAsync {

	public void getEvents(long lastEventId, String spaceName, EventSenderCredentials credentials, AsyncCallback<Event[]> callback);
	public void getClientSideTaxonomy(String spaceName, EventSenderCredentials credentials, AsyncCallback<ClientSideTaxonomy> callback);
	public void sendCommand(CommandEvent event, String spaceName, EventSenderCredentials cred, AsyncCallback<String> callback);
	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred, AsyncCallback<Void> callback);
	public void getRecommendations(String spaceName, EventSenderCredentials cred, AsyncCallback<List<GardeningRecommendation>> callback);
	public void getRecommendationsForConcept(String conceptURI, String spaceName, EventSenderCredentials cred, AsyncCallback<List<GardeningRecommendation>> callback);
	public void getRecentHistory(String spaceName, EventSenderCredentials credentials, AsyncCallback<List<? extends Event>> callback);
}
