package de.fzi.ipe.soboleo.space.logging;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.document.AddTag;
import de.fzi.ipe.soboleo.event.document.RemoveTag;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.logging.BrowseConcept;
import de.fzi.ipe.soboleo.event.logging.BrowseRoots;
import de.fzi.ipe.soboleo.event.logging.SubscribeConcept;
import de.fzi.ipe.soboleo.event.logging.SubscribeRoots;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.AddTextCmd;
import de.fzi.ipe.soboleo.event.ontology.ChangeTextCmd;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConceptCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.RemoveTextCmd;
import de.fzi.ipe.soboleo.event.peopletagging.AddPersonTag;
import de.fzi.ipe.soboleo.event.peopletagging.RemovePersonTag;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.queries.lucene.documents.GetDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.lucene.webdocument.GetWebDocumentsSearchResult;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersons;
import de.fzi.ipe.soboleo.queries.rdf.peopletagging.SearchPersonsByNameAndTopic;
import de.fzi.ipe.soboleo.space.Space;

public class ExternalUserLogging implements EventBusCommandProcessor,EventBusQueryProcessor, EventBusEventProcessor {

	private Space space;
	private EventLogger externalLogger;
	private final Logger logger = Logger.getLogger(this.getClass());
	
	public ExternalUserLogging(Space space){		
		this.space = space;
		this.externalLogger = new ExternalEventLoggerImpl();
	}
	
	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}

	@Override
	public Object process(QueryEvent query) {
		if(query instanceof GetWebDocumentsSearchResult){
			externalLogger.log(query, space);
		}
		else if(query instanceof GetDocumentsSearchResult){
			externalLogger.log(query, space);
		}
		else if (query instanceof SearchPersonsByNameAndTopic){
			logger.info(query.getClass());
			externalLogger.log(query, space);
		}
		else if (query instanceof SearchPersons){
			externalLogger.log(query, space);
		}
		return null;
	}

	@Override
	public CommandEvent prepare(CommandEvent command) {
		return null;
	}

	@Override
	public Object process(CommandEvent command) {
		if(command instanceof AddConnectionCmd){
			externalLogger.log(command, space);
		} else if(command instanceof AddTextCmd){
			externalLogger.log(command, space);
		} else if(command instanceof ChangeTextCmd){
			externalLogger.log(command, space);
		} else if(command instanceof CreateConceptCmd){
			externalLogger.log(command, space);
		} else if(command instanceof RemoveConceptCmd){
			externalLogger.log(command, space);
		} else if(command instanceof RemoveConnectionCmd){
			externalLogger.log(command, space);
		} else if (command instanceof RemoveTextCmd){
			externalLogger.log(command, space);
		} else if(command instanceof AddTag){
			externalLogger.log(command, space);
		} else if (command instanceof RemoveTag){
			externalLogger.log(command, space);
		} else if(command instanceof AddPersonTag){
			externalLogger.log(command, space);
		} else if (command instanceof RemovePersonTag){
			externalLogger.log(command, space);
		}
		return null;
	}

	@Override
	public void receiveEvent(Event event) {
		if(event instanceof BrowseRoots){
			externalLogger.log(event, space);
		} else if(event instanceof BrowseConcept){
			externalLogger.log(event, space);
		} else if(event instanceof SubscribeRoots){
			externalLogger.log(event, space);
		} else if(event instanceof SubscribeConcept){
			externalLogger.log(event, space);
		}
	}
	
}
