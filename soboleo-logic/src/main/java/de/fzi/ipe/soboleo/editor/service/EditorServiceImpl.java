package de.fzi.ipe.soboleo.editor.service;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.editor.client.rpc.EditorService;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.gardening.GetGardeningRecommendations;
import de.fzi.ipe.soboleo.event.gardening.GetGardeningRecommendationsForConcept;
import de.fzi.ipe.soboleo.event.message.ReadableEditorEvent;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;
import de.fzi.ipe.soboleo.queries.logging.GetRecentEvents;
import de.fzi.ipe.soboleo.queries.rdf.skos.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.server.AutowiringRemoteServiceServlet;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.commandHistory.QueryPastEvents;

public class EditorServiceImpl extends AutowiringRemoteServiceServlet  implements EditorService {
  
	private static final long serialVersionUID = 1L;
	protected static Logger logger = Logger.getLogger(EditorServiceImpl.class);
	
	@Autowired
	private Server server;
	
	@Override
	public Event[] getEvents(long lastEventId, String spaceName, EventSenderCredentials credentials) throws EventPermissionDeniedException{
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			Event[] events = (Event[]) space.getEventBus().executeQuery(new QueryPastEvents(lastEventId), credentials);
			return events;
		} catch (IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		}
	}
		
	@Override
	public de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy getClientSideTaxonomy(String spaceName, EventSenderCredentials credentials) throws EventPermissionDeniedException {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			ClientSideTaxonomy  clientSideTaxonomy = (ClientSideTaxonomy) space.getEventBus().executeQuery(new GetClientSideTaxonomy(false), credentials);
			logger.info("Getting client side taxonomy " + (clientSideTaxonomy == null ? " (is null) " : ""));
			
			return clientSideTaxonomy;
		} catch (IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		} 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GardeningRecommendation> getRecommendations(String spaceName, EventSenderCredentials credentials) throws EventPermissionDeniedException{
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			List<GardeningRecommendation> recs = (List<GardeningRecommendation>) space.getEventBus().executeQuery(new GetGardeningRecommendations(true), credentials);
			return recs;
		} catch(IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GardeningRecommendation> getRecommendationsForConcept(String conceptURI, String spaceName, EventSenderCredentials credentials) throws EventPermissionDeniedException{
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			List<GardeningRecommendation> recs = (List<GardeningRecommendation>) space.getEventBus().executeQuery(new GetGardeningRecommendationsForConcept(conceptURI, true), credentials);
			return recs;
		} catch(IOException e) {
			logger.error(e);
			return null; //TODO real error handling
		}
	}

	
	
	@Override
	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			space.getEventBus().sendEvent(event, cred);
		} catch (IOException e) {
			logger.error(e); //TODO realExceptionHandlings
		}
	}

	@Override
	public String sendCommand(CommandEvent event, String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			Object result = space.getEventBus().executeCommandNow(event, cred);
			if(result != null) return result.toString();
			else return null;
		} catch (IOException e) {
			logger.error(e); //TODO realExceptionHandlings
			return null;
		}
	}

	@Override
	public List<? extends Event> getRecentHistory(String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			@SuppressWarnings("unchecked")
			List<? extends Event> eventList = (List<? extends Event>) space.getEventBus().executeQuery(new GetRecentEvents(0, ReadableEditorEvent.class), cred);
			return eventList;
		} catch (IOException e) {
			logger.error(e); //TODO realExceptionHandlings
			return null;
		}
	}	
		
}

