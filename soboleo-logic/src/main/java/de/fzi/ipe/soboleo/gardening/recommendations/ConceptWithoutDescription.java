/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.gardening.recommendations;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;


/**
 * A gardening recommendations, it indicates a concept without description
 */
public class ConceptWithoutDescription extends GardeningRecommendation implements IsSerializable{

	private ClientSideConcept concept;

	public ConceptWithoutDescription(ClientSideConcept concept) {
		this.concept = concept;
	}
	
	public int getPriority() {
		return 9;
	}
	
	@SuppressWarnings("unused")
	private ConceptWithoutDescription() {; }
	
	@Override
	public double getConfidence() {
		return 0.1;
	}
	
	public String getConceptURI() {
		return concept.getURI();
	}
	
	public ClientSideConcept getConcept(){
		return concept;
	}
	
	@Override
	public String toString() {
		return "Concept "+concept.getURI()+" has no description.";
	}
}
