/*
 * Ontoprise code for project ksi_underground
 * Created on 21.07.2006 by zach
 */
package de.fzi.ipe.soboleo.annotatePeople.client.rpc;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.peopletag.TaggedPerson;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;

public interface AnnotatePeopleService extends RemoteService{

	public TaggedPerson getTaggedPersonByURL(String spaceName, EventSenderCredentials cred, Language userLanguage, String webURL) throws EventPermissionDeniedException;
	
	public Set<String> getAllLabels(String spaceName, EventSenderCredentials cred, Language userLanguage)throws EventPermissionDeniedException;
	
	public List<ConceptStatistic> getConceptRecommendation(TaggedPerson taggedPerson, String spaceName, Language userLanguage,EventSenderCredentials cred)throws EventPermissionDeniedException;
	
	public void removeTagsForUser(String spaceName, EventSenderCredentials cred, String taggedPersonURI)throws EventPermissionDeniedException;
	
	public Boolean removeUser(String spaceName, EventSenderCredentials cred, String taggedPersonURI)throws EventPermissionDeniedException;

	/**
	 * Removes a named tag for a tagged person.
	 * Used in AnnotatePeopleMain/TopicLinkWithOptionalDelete to remove a tag.
	 * @param spaceName
	 * @param cred
	 * @param taggedPersonURI
	 * @param tagToRemove
	 */
	public void removeTag(String spaceName, EventSenderCredentials cred, TaggedPerson taggedPerson,String tagToRemove);
	
	public void saveAnnotationData(String spaceName, EventSenderCredentials cred, Language userLanguage, TaggedPerson taggedPerson, Map<String,String> conceptsMap, String webURL)throws EventPermissionDeniedException;
	
	public TaggedPerson getTaggedPersonByEmail(String spaceName, EventSenderCredentials cred, String email) throws EventPermissionDeniedException;
	
	public TaggedPerson createTaggedPerson(String spaceName, EventSenderCredentials cred, String email, String name) throws EventPermissionDeniedException;
	
	public Set<ClientSideConcept> getConceptsForURIs(String spaceName, EventSenderCredentials cred,Set<String> myConceptURIs) throws EventPermissionDeniedException;
	
	public ClientSideConcept getConceptForLabel(String spaceName, EventSenderCredentials cred, LocalizedString label) throws EventPermissionDeniedException;
	
	public void sendEvent(Event event, String spaceName, EventSenderCredentials cred) throws EventPermissionDeniedException;
	
	public List<TaggedPerson> getTaggedPersons (String spaceName, EventSenderCredentials cred);
	public Collection<ClientSideConcept> getConcepts (String spaceName, EventSenderCredentials cred);
	
}
