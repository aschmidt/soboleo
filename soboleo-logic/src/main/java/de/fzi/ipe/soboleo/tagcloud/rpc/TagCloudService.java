package de.fzi.ipe.soboleo.tagcloud.rpc;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;

public interface TagCloudService extends RemoteService {

	public List<ConceptStatistic> getSearchRequestsOverall(String spaceId,
			int days, Language userLanguage);

	public List<ConceptStatistic> getUsedConceptsInPersonAnnotations(
			String spaceId, int days, Language userLanguage,
			EventSenderCredentials cred);

	public List<ConceptStatistic> getUserAnnotations(String userkey,
			String spaceId, Language userLanguage, EventSenderCredentials cred);

	public List<ConceptStatistic> getAggregatedPersonTags(
			String taggedPersonURI, String spaceId, Language userLanguage,
			EventSenderCredentials cred);

}
