/*
 * FZI - Information Process Engineering 
 * Created on 24.07.2009 by zach
 */
package de.fzi.ipe.soboleo.webdocument.eventBusAdaptor;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.document.AggregatedDocumentTags;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFTag;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFWebDocument;
import de.fzi.ipe.soboleo.data.tripleStore.sesame.store.RDFWebdocumentDatabase;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.event.webdocument.AddWebDocument;
import de.fzi.ipe.soboleo.event.webdocument.AddWebDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.RemoveWebDocument;
import de.fzi.ipe.soboleo.event.webdocument.RemoveWebDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.SetWebDocumentTitle;
import de.fzi.ipe.soboleo.event.webdocument.SetWebDocumentURL;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetAggregatedWebDocumentTags;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetDocumentByURLQuery;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetWebDocument;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetWebDocumentTags;

public class WebDocumentEventBusAdaptor implements EventBusCommandProcessor, EventBusQueryProcessor{

	private RDFWebdocumentDatabase database;
	
	
	public WebDocumentEventBusAdaptor(TripleStore tripleStore) throws IOException {
		this.database = new RDFWebdocumentDatabase(tripleStore);
	}
	
	
	@Override
	public CommandEvent prepare(CommandEvent command) {
		return null;
	}

	@Override
	public Object process(CommandEvent command) {
		try {
			if (command instanceof AddWebDocument) {
				AddWebDocument addDocument = (AddWebDocument) command;
				RDFWebDocument documentByURL = database.getDocumentByURL(addDocument.getURL(),true);
				documentByURL.setTitle(addDocument.getTitle());
				addDocument.setURI(documentByURL.getURI().toString());
				return documentByURL.getClientSideDocument();
			}
			else if (command instanceof AddWebDocumentTag) {
				AddWebDocumentTag addTag = (AddWebDocumentTag) command;
				RDFWebDocument document = database.getDocument(addTag.getDocumentURI());
				if (document != null){
					RDFTag tag = document.addTag(addTag.getSenderURI(), addTag.getSia().getUri());
					addTag.setTagURI(tag.getURI().toString());
					return addTag.getTagURI();
				}
			}
			else if (command instanceof RemoveWebDocument) {
				RemoveWebDocument removeDoc = (RemoveWebDocument) command;
				database.deleteDocument(removeDoc.getDocURI());
			}
			else if (command instanceof RemoveWebDocumentTag) {
				//TODO is done twice, in WebDocumentEventBus and DialogEventBus
				RemoveWebDocumentTag removeTag = (RemoveWebDocumentTag) command;
				RDFTag tag = database.getAnnotation(removeTag.getTagID());
				tag.delete();
			}
			else if (command instanceof SetWebDocumentTitle) {
				SetWebDocumentTitle setDocumentTitle = (SetWebDocumentTitle) command;
				RDFWebDocument doc = database.getDocument(setDocumentTitle.getDocURI());
				if(doc != null) doc.setTitle(setDocumentTitle.getTitle());
			}
			else if (command instanceof SetWebDocumentURL) {
				SetWebDocumentURL setDocumentURL = (SetWebDocumentURL) command;
				RDFWebDocument doc = database.getDocument(setDocumentURL.getDocURI());
				if(doc != null) doc.setURL(setDocumentURL.getURL());
			}
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
		return null;
	}


	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}


	@Override
	public Object process(QueryEvent query) {
		try {
			if (query instanceof GetDocumentByURLQuery) {
				GetDocumentByURLQuery getDocumentQuery = (GetDocumentByURLQuery) query;
				RDFWebDocument rdfDoc = database.getDocumentByURL(getDocumentQuery.getURL(),false);
				return (rdfDoc == null) ? null: rdfDoc.getClientSideDocument();
			}
			else if(query instanceof GetWebDocument){
				GetWebDocument getWebDocument = (GetWebDocument) query;
				RDFWebDocument rdfDoc  = getRDFWebDocument(getWebDocument.getURI());
				return (rdfDoc == null) ? null : rdfDoc.getClientSideDocument();
			}
			else if(query instanceof GetWebDocumentTags){
				GetWebDocumentTags getWebDocumentTags = (GetWebDocumentTags) query;
				String conceptURI = getWebDocumentTags.getConceptURI();
				String makerURI = getWebDocumentTags.getMakerURI();
				Set<Tag> toReturn = new HashSet<Tag>();
				if(getWebDocumentTags.getWebDocumentURI() != null){
					RDFWebDocument rdfWebDoc = getRDFWebDocument(getWebDocumentTags.getWebDocumentURI());
					if(rdfWebDoc != null)	toReturn.addAll(calculateWebDocumentTags(conceptURI, makerURI, rdfWebDoc.getClientSideDocument()));
				} 
				else{
					Set<RDFWebDocument> webDocs = database.getAllDocuments();
					for (RDFWebDocument rdfWebDoc : webDocs) toReturn.addAll(calculateWebDocumentTags(conceptURI, makerURI, rdfWebDoc.getClientSideDocument()));
				}
				return toReturn;
			}
			else if(query instanceof GetAggregatedWebDocumentTags){
				GetAggregatedWebDocumentTags getAggregated = (GetAggregatedWebDocumentTags) query;
				String makerURI = getAggregated.getMakerURI();
				String pageURL = getAggregated.getPageURL();
				Set<AggregatedDocumentTags> toReturn = new HashSet<AggregatedDocumentTags>();
				Set<RDFWebDocument> rdfWebDocs = new HashSet<RDFWebDocument>();
				if(getAggregated.getWebDocumentURI() != null){
					RDFWebDocument rdfWebDoc = getRDFWebDocument(getAggregated.getWebDocumentURI());
					if(rdfWebDoc != null)	rdfWebDocs.add(rdfWebDoc);
				}
				else if(pageURL != null){
					RDFWebDocument rdfWebDoc = database.getDocumentByURL(pageURL, false);
					if(rdfWebDoc != null) rdfWebDocs.add(rdfWebDoc);
				}
				else rdfWebDocs = database.getAllDocuments();
				toReturn.addAll(calculateAggregatedDocumentTags(makerURI, rdfWebDocs));
				return toReturn;
			}
			return null;
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}
	
	private Collection<AggregatedDocumentTags> calculateAggregatedDocumentTags(String makerURI, Set<RDFWebDocument> rdfWebDocs) throws IOException {
		Map<String,AggregatedDocumentTags> aggregated = new HashMap<String, AggregatedDocumentTags>();
		for (RDFWebDocument rdfWebDoc: rdfWebDocs) {
			WebDocument webDoc = rdfWebDoc.getClientSideDocument();
			if (makerURI != null) {
				for (Tag tag : webDoc.getUserTags(makerURI)) {
					AggregatedDocumentTags aggregatedTags = aggregated.get(tag.getConceptID());
					if (aggregatedTags == null) {
						aggregatedTags = new AggregatedDocumentTags(tag.getConceptID());
						aggregated.put(tag.getConceptID(), aggregatedTags);
					}
					aggregatedTags.addTag(tag);
				}
			} else {
				for (Tag tag : webDoc.getAllTags()) {
					AggregatedDocumentTags aggregatedTags = aggregated.get(tag.getConceptID());
					if (aggregatedTags == null) {
						aggregatedTags = new AggregatedDocumentTags(tag.getConceptID());
						aggregated.put(tag.getConceptID(), aggregatedTags);
					}
					aggregatedTags.addTag(tag);
				}
			}
		}
		return aggregated.values();
	}

	private Set<? extends Tag> calculateWebDocumentTags(String conceptURI, String makerURI, WebDocument doc) throws IOException {
		Set<Tag> tags = new HashSet<Tag>();
		if(conceptURI != null && makerURI != null){
			Tag t;
			if((t = doc.hasAnnotation(makerURI, conceptURI)) != null) tags.add(t);
			return tags;
		}
		else if(conceptURI != null) return (doc.getConceptTags(conceptURI) != null) ? doc.getConceptTags(conceptURI) : tags;
		else if(makerURI != null)return doc.getUserTags(makerURI);
		else return (doc.getAllTags() != null) ? doc.getAllTags() : tags;
	}
	
	private RDFWebDocument getRDFWebDocument(String url) throws IOException{
		RDFWebDocument rdfWebDoc = null;
		if(url.contains(TripleStore.SOBOLEO_NS)) rdfWebDoc = database.getDocument(url);
		else rdfWebDoc = database.getDocumentByURL(url, false);
		return rdfWebDoc;
	}
	
}
