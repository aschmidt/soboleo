package de.fzi.ipe.soboleo.gardening.recSys;

import java.util.List;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.gardening.recommendations.GardeningRecommendation;;



public class Recommendation {
	
	public static List<GardeningRecommendation> contextualSuperRecommendations(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition, Set<WebDocument> docs) throws Exception
	{
		Recommender r=new ContextualRecommender(tax,conceptsToPosition,docs);
		return r.SuperConceptRecommend();
		
	}
	public static List<GardeningRecommendation> stringBasedSuperRecommendations(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition) throws Exception
	{
		Recommender r=new StringRecommender(tax,conceptsToPosition);
		return r.SuperConceptRecommend();
	}
	
	//hybrid Super is implemented as hybrid 2 in the paper (the hybridSuperConceptRecommendusingtax() is used) 
	//Thus this method has a combination of all implemented methods.
	public static List<GardeningRecommendation> hybridSuperRecommendations(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition, Set<WebDocument> docs) throws Exception
	{
		Recommender r=new HybridRecommender(tax,conceptsToPosition,docs);
		return r.SuperConceptRecommend();
		}
	public static List<GardeningRecommendation> contextualRelatedRecommendations(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition, Set<WebDocument> docs) throws Exception
	{
		Recommender r=new ContextualRecommender(tax,conceptsToPosition,docs);
		return r.RelatedConceptRecommend();
		
	}
	public static List<GardeningRecommendation> stringBasedRelatedRecommendations(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition) throws Exception
	{
		Recommender r=new StringRecommender(tax,conceptsToPosition);
		return r.RelatedConceptRecommend();
	}
	public static List<GardeningRecommendation> hybridRelatedRecommendations(ClientSideTaxonomy tax, Set<ClientSideConcept> conceptsToPosition, Set<WebDocument> docs) throws Exception
	{
		Recommender r=new HybridRecommender(tax,conceptsToPosition,docs);
		return r.RelatedConceptRecommend();
		}




}
