package de.fzi.ipe.soboleo.tagcloud.rpc;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.tagcloud.ConceptStatistic;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;

public interface TagCloudServiceAsync extends RemoteService{

	
	public void getSearchRequestsOverall(String spaceId, int days,Language userLanguage, AsyncCallback<List<ConceptStatistic>> callback);
	
	public void getUsedConceptsInPersonAnnotations(String spaceId, int days,Language userLanguage,EventSenderCredentials cred, AsyncCallback<List<ConceptStatistic>> callback);


	public void getUserAnnotations(String userkey, String spaceId, Language userLanguage,EventSenderCredentials cred, AsyncCallback<List<ConceptStatistic>> callback);
	
	public void getAggregatedPersonTags(String taggedPersonURI, String spaceId, Language userLanguage, EventSenderCredentials cred, AsyncCallback<List<ConceptStatistic>> callback);

}
