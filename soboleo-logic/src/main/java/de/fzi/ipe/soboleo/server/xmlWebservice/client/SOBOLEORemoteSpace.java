package de.fzi.ipe.soboleo.server.xmlWebservice.client;

import java.util.HashSet;
import java.util.Set;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBusEventProcessor;
import de.fzi.ipe.soboleo.queries.rdf.skos.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceException;
import de.fzi.ipe.soboleo.server.xmlWebservice.XMLWebserviceResult;
import de.fzi.ipe.soboleo.skosTaxonomy.eventBusAdaptor.TaxonomyUpdater;
import de.fzi.ipe.soboleo.space.commandHistory.QueryPastEvents;



public class SOBOLEORemoteSpace extends Thread{
	
	private SOBOLEOServerConnection serverConnection;
	private String spaceURI;
	private ClientSideTaxonomy clientSideTaxonomy;
	private int pollingBreakMiliSeconds;
	
	private Set<EventBusEventProcessor> listeners = new HashSet<EventBusEventProcessor>();
	private boolean stopped = false;
	
	
	public SOBOLEORemoteSpace(String url) throws XMLWebserviceException{
		this(url,"default",5,EventSenderCredentials.ANONYM);
	}
	
	public SOBOLEORemoteSpace(String url,String spaceURI, int pollingInSeconds, EventSenderCredentials credentials) throws XMLWebserviceException {
		serverConnection = new SOBOLEOServerConnection(url,credentials);
		this.spaceURI = spaceURI;
		this.pollingBreakMiliSeconds = pollingInSeconds*1000;
		clientSideTaxonomy = (ClientSideTaxonomy) sendEvent(new GetClientSideTaxonomy(true), Long.MAX_VALUE);
		addListener(new TaxonomyUpdater(clientSideTaxonomy)); 
		this.start();
	}
	
	public ClientSideTaxonomy getTaxonomy() {
		return clientSideTaxonomy;
	}
	
	public synchronized void addListener(EventBusEventProcessor listener) {
		listeners.add(listener);
	}
	
	public synchronized void removeListener(EventBusEventProcessor listener) {
		listeners.remove(listener);
	}
	
	public synchronized Object sendEvent(Event event) throws XMLWebserviceException {
		return sendEvent(event,clientSideTaxonomy.getVersion());
	}
	
	private synchronized Object sendEvent(Event event, long lastEventID) throws XMLWebserviceException {
		XMLWebserviceResult result = serverConnection.send(spaceURI, event, lastEventID);
		distributeEvents(result.getEvents());
		return result.getResult();
	}
	
	private synchronized void distributeEvents(Event[] events) {
		for (Event currentEvent: events) {
			for (EventBusEventProcessor currentListener: listeners) {
				currentListener.receiveEvent(currentEvent);
			}
		}
	}
	
	public synchronized void close() {
		stopped = true;
		this.interrupt();
		try {
			this.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void  run() {
		while (!stopped) {
			try {
				Thread.sleep(pollingBreakMiliSeconds);
				synchronized (this) {
					Event[] events = (Event[]) sendEvent(new QueryPastEvents(clientSideTaxonomy.getVersion()), Long.MAX_VALUE);
					if (events != null) {
						distributeEvents(events);					
					}
				}
			} catch(InterruptedException ioe) {
				;
			} catch(XMLWebserviceException e) {
				e.printStackTrace();
			}
		}
	}

}
