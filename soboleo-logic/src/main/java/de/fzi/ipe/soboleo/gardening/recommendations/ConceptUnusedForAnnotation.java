/*
 * FZI - Information Process Engineering 
 * Created on 02.02.2010 by zach
 */
package de.fzi.ipe.soboleo.gardening.recommendations;

import com.google.gwt.user.client.rpc.IsSerializable;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;


/**
 * A gardening recommendations, it indicates a concept not used for annotation
 */
public class ConceptUnusedForAnnotation extends GardeningRecommendation implements IsSerializable{

	private ClientSideConcept concept;

	public ConceptUnusedForAnnotation(ClientSideConcept concept) {
		this.concept = concept;
	}
	
	public int getPriority() {
		return 7;
	}
	
	@SuppressWarnings("unused")
	private ConceptUnusedForAnnotation() {; }
	
	
	public String getConceptURI() {
		return concept.getURI();
	}
	
	public ClientSideConcept getConcept(){
		return concept;
	}
	
	@Override
	public String toString() {
		return "Concept "+concept.getURI()+" is not used for annotation.";
	}
}
