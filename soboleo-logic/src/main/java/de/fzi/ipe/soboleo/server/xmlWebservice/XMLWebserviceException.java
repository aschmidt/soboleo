package de.fzi.ipe.soboleo.server.xmlWebservice;

/**
 * Generic exception class that containing information about why a 
 * call to the XMLWebservice failed. Use "getReason()" for more 
 * detailed information on what went wrong.  
 */
public class XMLWebserviceException extends Exception {

	private static final long serialVersionUID = 1L;

	public XMLWebserviceException(Exception exception) {
		super(exception);
	}
	
}
