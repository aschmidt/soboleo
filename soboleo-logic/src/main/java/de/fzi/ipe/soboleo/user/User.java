/*
 * FZI - Information Process Engineering 
 * Created on 04.03.2009 by zach
 */
package de.fzi.ipe.soboleo.user;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.RDF;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.space.Spaces;

/**
 * @author FZI - Information Process Engineering 
 * Created on 04.03.2009 by zach
 *
 */
public class User {

	private static Random random = new Random();
	
	private URI uri;
	private TripleStore tripleStore; 
	
	protected User(URI uri, TripleStore tripleStore) throws IOException {
		this.uri = uri;
		this.tripleStore = tripleStore; 
		
	}
	
	protected void createUser() throws IOException {
		this.tripleStore.addTriple(this.uri.toString(), RDF.TYPE.toString(), SoboleoNS.USER_TYPE.toString(),null);
		createSecretKey();
	}
	
	protected void deleteUser() throws IOException {
		this.tripleStore.removeTriples(this.uri, null, null, null);
	}
	
	/**
	 * Returns the URI of this user
	 * @return String
	 */ 
	public String getURI() {
		return this.uri.toString();
	}
	
	private void createSecretKey() throws IOException {
		byte[] randomBytes = new byte[20];		
		random.nextBytes(randomBytes);
    	StringBuilder builder = new StringBuilder();
    	for (byte b:randomBytes) builder.append(Integer.toString(b-Byte.MIN_VALUE,34));
    	String secretKey = builder.toString().toUpperCase();
		
    	ValueFactory vf = tripleStore.getValueFactory();
    	URI userKey = vf.createURI(SoboleoNS.USER_KEY.toString());
    	Literal key = vf.createLiteral(secretKey);
		tripleStore.addTriple(this.uri, userKey,key,null);
	}
	
	/**
	 * Returns the key of an user
	 * @return String
	 * @throws IOException
	 */
	public String getKey() throws IOException {
		return this.getObjectString(SoboleoNS.USER_KEY.toString());
	}
	
	/**
	 * Returns all spaceIdentifiers of a person is a Set of Strings.
	 * @return Set<String>
	 * @throws IOException
	 */
	public Set<String> getSpaceIdentifier() throws IOException {
		Set<String> toReturn = new HashSet<String>();
		List<Statement> statementList = this.tripleStore.getStatementList(this.uri.toString(), SoboleoNS.USER_TAXONOMY.toString(),null);
		for (Statement stmt: statementList) {
			toReturn.add(((Literal)stmt.getObject()).getLabel());
		}
		return toReturn;
	}
	
	/**
	 * Adds a new space with the given spaceIdentifier. If the person did not have a default
	 * space, the new space will be his default space.
	 * @param spaceIdentifier
	 * @throws IOException
	 */
	public void addSpaceIdentifier(String spaceIdentifier) throws IOException {
		ValueFactory vf = this.tripleStore.getValueFactory();
		URI p = vf.createURI(SoboleoNS.USER_TAXONOMY.toString());
		Literal o = vf.createLiteral(spaceIdentifier);
		this.tripleStore.addTriple(this.uri, p, o, null);
		if (this.getObjectString(SoboleoNS.USER_DEFAULT_SPACE.toString())== null) 
		{
			this.setDefaultSpaceIdentifier(spaceIdentifier);
		}
	}
	
	/**
	 * Removes the space given by the spaceIdentifier.
	 * @param spaceIdentifier
	 * @throws IOException
	 */
	public void removeSpaceIdentifier(String spaceIdentifier) throws IOException 
	{
		ValueFactory vf = this.tripleStore.getValueFactory();
		URI p = vf.createURI(SoboleoNS.USER_TAXONOMY.toString());
		Literal o = vf.createLiteral(spaceIdentifier);
		this.tripleStore.removeTriples(this.uri, p, o, null);
		if (spaceIdentifier.equals(this.getDefaultSpaceIdentifier())) {
			p = vf.createURI(SoboleoNS.USER_DEFAULT_SPACE.toString());
			this.tripleStore.removeTriples(this.uri, p, null, null);
		}
	}
	
	/**
	 * Sets the password hash for a password. Used to set a password see method setPassword(String password).
	 * @param password
	 * @throws IOException
	 */
	public void setPasswordHash(String password) throws IOException 
	{
		this.setObjectString(SoboleoNS.USER_PASSWORD.toString(), password);
	}
	
	/**
	 * Sets the password of a person
	 * @param password
	 * @throws IOException
	 */
	public void setPassword(String password) throws IOException 
	{
	    setPasswordHash(computePasswordHash(password));
	}

	/**
	 * Checks if a given password is the correct password of the person.
	 * It is true, if it is OK. 
	 * @param password
	 * @return boolean
	 * @throws IOException
	 */
	public boolean isPassword(String password) throws IOException 
	{
	    String realPasswordHash = this.getObjectString(SoboleoNS.USER_PASSWORD.toString());
	    String enteredPasswordHash = computePasswordHash(password);
	    boolean isPassword = realPasswordHash.equals(enteredPasswordHash); 
	    /*
	     logging in externally
	    if (isPassword) {
	    	ExternalUserRegistrationService userLoginService = new ExternalUserRegistrationServiceImpl();
	    	userLoginService.login(this.getEmail(), this.getKey());
	    }
	    */
	    return isPassword;
	}

	/**
	 * Sets the user name of a person
	 * @param nameString
	 * @throws IOException
	 */
	public void setName(String nameString) throws IOException 
	{
	    setObjectString(SoboleoNS.USER_NAME.toString(),nameString.trim());
	}

	/**
	 * Returns the name of a person
	 * @return String
	 * @throws IOException
	 */
	public String getName() throws IOException 
	{
	    return getObjectString(SoboleoNS.USER_NAME.toString());
	}
	
	/**
	 * Sets the Link of a person
	 * @param link
	 * @throws IOException
	 */
	public void setLink(String link) throws IOException {
		setObjectString(SoboleoNS.USER_LINK.toString(),link.trim());
	}
	
	/**
	 * Returns the link of a person
	 * @return String
	 * @throws IOException
	 */
	public String getLink() throws IOException 
	{
		return getObjectString(SoboleoNS.USER_LINK.toString());
	}
	
	/**
	 * Sets the default space of a person
	 * @param defaultSpace
	 * @throws IOException
	 */
	public void setDefaultSpaceIdentifier(String defaultSpace) throws IOException {
		setObjectString(SoboleoNS.USER_DEFAULT_SPACE.toString(),defaultSpace);
	}
	
	/**
	 * Returns the default Space of a person
	 * @return String
	 * @throws IOException
	 */
	public String getDefaultSpaceIdentifier() throws IOException {
		String defaultSpaceIdentifier = getObjectString(SoboleoNS.USER_DEFAULT_SPACE.toString());
		if (defaultSpaceIdentifier == null) return Spaces.DEFAULT_SPACE;
		return defaultSpaceIdentifier;
	}
	
	/**
	 * Sets the email address of a person
	 * @param email
	 * @throws IOException
	 */
	public void setEmail(String email) throws IOException 
	{
	    setObjectString(SoboleoNS.USER_EMAIL.toString(), email.toLowerCase().trim());
	}
	
	/**
	 * Returns the email address of a person
	 * @return String
	 * @throws IOException
	 */
	public String getEmail() throws IOException
	{
	    return this.getObjectString(SoboleoNS.USER_EMAIL.toString());
	}

	/**
	 * Sets the default language for a person
	 * @param lan
	 * @throws IOException
	 */
	public void setDefaultLanguage(Language lan) throws IOException {
		this.setObjectString(SoboleoNS.USER_DEFAULT_LANGUAGE.toString(),lan.getAcronym());
	}
	
	/**
	 * Returns the default language of a person.
	 * @return Language
	 * @throws IOException
	 */
	public Language getDefaultLanguage() throws IOException {
		String defaultLanguageIdentifier = this.getObjectString(SoboleoNS.USER_DEFAULT_LANGUAGE.toString());
		if (defaultLanguageIdentifier == null) return Language.en;
		else return Language.getLanguage(defaultLanguageIdentifier);
	}
		
	private String getObjectString(String predicate) throws IOException {
		ValueFactory vf = this.tripleStore.getValueFactory();
		URI p = vf.createURI(predicate);
		List<Statement> statementList = this.tripleStore.getStatementList(this.uri,p, null);
		if (statementList.size()>0) {
			Literal name = (Literal) statementList.get(0).getObject();
			return name.getLabel();
		}
		else return null;
	}
	
	private void setObjectString(String predicate, String object) throws IOException {
		ValueFactory vf = this.tripleStore.getValueFactory();
		URI p = vf.createURI(predicate);
		this.tripleStore.removeTriples(this.uri, p, null, null);
		Literal name = vf.createLiteral(object);
		this.tripleStore.addTriple(this.uri, p, name, null);
	}
	
	/**
	 * Computes the hash of a given password of the user and returns it as String.
	 * It uses the Message-Digest Algorithm 5.
	 * @param password
	 * @return String
	 */
	public static String computePasswordHash(String password) {
	    try {
	    	MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
	    	digest.update(password.getBytes());
	    	byte[] hash = digest.digest();
	    	StringBuilder builder = new StringBuilder();
	    	for (byte b:hash) builder.append(Integer.toString(b-Byte.MIN_VALUE,16));
	    	return builder.toString().toUpperCase();
	    } catch (NoSuchAlgorithmException nsae) {
	    	//MD5 should always be present.
	    	throw new AssertionError(nsae);
	    }
	}
	

}
