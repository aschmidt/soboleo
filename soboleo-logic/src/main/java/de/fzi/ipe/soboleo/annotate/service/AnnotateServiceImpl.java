package de.fzi.ipe.soboleo.annotate.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import de.fzi.ipe.soboleo.annotate.client.rpc.AnnotateService;
import de.fzi.ipe.soboleo.beans.dialog.Dialog;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString;
import de.fzi.ipe.soboleo.beans.ontology.LocalizedString.Language;
import de.fzi.ipe.soboleo.beans.ontology.SKOS;
import de.fzi.ipe.soboleo.beans.ontology.SemanticAnnotation;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideConcept;
import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.Tag;
import de.fzi.ipe.soboleo.beans.ontology.skos.document.WebDocument;
import de.fzi.ipe.soboleo.beans.rating.Rating;
import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.dialog.ContinueWebDocumentDialog;
import de.fzi.ipe.soboleo.event.dialog.StartWebDocumentDialog;
import de.fzi.ipe.soboleo.event.ontology.AddConnectionCmd;
import de.fzi.ipe.soboleo.event.ontology.CreateConceptCmd;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.event.webdocument.AddWebDocument;
import de.fzi.ipe.soboleo.event.webdocument.AddWebDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.RemoveWebDocument;
import de.fzi.ipe.soboleo.event.webdocument.RemoveWebDocumentTag;
import de.fzi.ipe.soboleo.event.webdocument.SetWebDocumentTitle;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.queries.rdf.dialog.GetDialogsForWebDocument;
import de.fzi.ipe.soboleo.queries.rdf.webdocument.GetDocumentByURLQuery;
import de.fzi.ipe.soboleo.rating.client.RatingService;
import de.fzi.ipe.soboleo.rating.client.RatingServiceImpl;
import de.fzi.ipe.soboleo.server.AutowiringRemoteServiceServlet;
import de.fzi.ipe.soboleo.server.Server;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.User;

/**
 * This class provides the annotate Main class with services. It is needed to
 * access the data on the server.
 * 
 * @author FZI
 * 
 */
public class AnnotateServiceImpl extends AutowiringRemoteServiceServlet 
	implements AnnotateService {

	private final static Logger logger = Logger.getLogger(AnnotateServiceImpl.class);

	private static final long serialVersionUID = 1L;

	@Autowired
	private Server server;
	
	public String[] getConceptRecommendation(String spaceName, String docURL) {
		String[] recommendation = new String[0];
		try {
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return recommendation;
	}

	@Override
	public void sendEvent(Event event, String spaceName,
			EventSenderCredentials cred) throws EventPermissionDeniedException {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			space.getEventBus().sendEvent(event, cred);
		} catch (IOException e) {
			logger.error(e); // TODO realExceptionHandlings
		}
	}

	public Set<String> getAllLabels(String spaceName,
			EventSenderCredentials cred, Language userLanguage)
			throws EventPermissionDeniedException {
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			Language spaceLanguage = LocalizedString.Language
					.getLanguage(currentSpace
							.getProperty(SpaceProperties.DEFAULT_LANGUAGE));
			ClientSideTaxonomy clientSideTaxonomy = currentSpace
					.getClientSideTaxonomy();
			Collection<ClientSideConcept> allConcepts = clientSideTaxonomy
					.getConcepts();

			Set<LocalizedString> altLabels = new HashSet<LocalizedString>();
			Set<String> prefsWithAlts = new HashSet<String>();
			for (ClientSideConcept current : allConcepts) {
				String nameOfCpt = current.getBestFitText(SKOS.PREF_LABEL,
						userLanguage, spaceLanguage, Language.en).getString();
				prefsWithAlts.add(nameOfCpt);
				// TODO which language to consider for alt labels?
				for (LocalizedString related : current.getTexts(SKOS.ALT_LABEL,
						userLanguage)) {
					String name = related.getString() + " " + "(" + nameOfCpt
							+ ")";
					prefsWithAlts.add(name);
				}
				altLabels.clear();
			}
			return prefsWithAlts;
		} catch (IOException e) {
			logger.error(e);
			return null; // TODO real error handling
		}
	}

	public ClientSideConcept getConceptForLabel(String spaceName,
			EventSenderCredentials cred, LocalizedString label)
			throws EventPermissionDeniedException {
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			ClientSideTaxonomy clientSideTaxonomy = currentSpace
					.getClientSideTaxonomy();
			// TODO language stuff; --> suggestBox mit Konzepten
			ClientSideConcept concept = clientSideTaxonomy
					.getConceptForPrefLabel(label);
			if (concept == null) {
				Set<ClientSideConcept> concepts = clientSideTaxonomy
						.getConceptsForPrefLabel(label.getString());
				Iterator<ClientSideConcept> it = concepts.iterator();
				if (it.hasNext())
					return it.next();
				else
					return null;
			}
			return concept;
		} catch (IOException e) {
			logger.error(e);
			return null; // TODO real error handling
		}
	}

	public void saveAnnotationData(String spaceName,
			EventSenderCredentials cred, Language userLanguage,
			WebDocument webDoc, Map<String, String> conceptMap, String docTitle)
			throws EventPermissionDeniedException {
		try {

			logger.info("save annotation data for " + webDoc.getURI());

			Space currentSpace = server.getSpaces().getSpace(spaceName);
			EventBus eb = currentSpace.getEventBus();
			ClientSideTaxonomy clientSideTaxonomy = currentSpace
					.getClientSideTaxonomy();

			if (!docTitle.equals(webDoc.getTitle()))
				eb.executeCommandNow(new SetWebDocumentTitle(webDoc.getURI(),
						docTitle), cred);

			Set<String> conceptURIsToRemove = webDoc.getTagConceptIDs();
			Set<String> usedConceptURIs = new HashSet<String>();
			for (Entry<String, String> entry : conceptMap.entrySet()) {

				String conceptURI = entry.getValue();

				logger.info("concept uri : " + conceptURI);

				// in case, it is a not yet existing concept, create a new
				// concept
				if (conceptURI.equals("")) {
					conceptURI = createNewConcept(cred, userLanguage,
							currentSpace, clientSideTaxonomy, entry.getKey());
				}
				// check if it is an existing annotation by the user
				Tag currentTag = webDoc.hasAnnotation(cred.getSenderURI(),
						conceptURI);
				// if there is not yet an annotation for the user and if the
				// conceptURI is not yet used, create a new annotation for the
				if (currentTag == null && !usedConceptURIs.contains(conceptURI)) {

					logger.info("this is a new annotation");

					SemanticAnnotation sia = new SemanticAnnotation();
					sia.setUri(conceptURI);
					sia.setUserId(cred.getKey());
					eb.executeCommandNow(new AddWebDocumentTag(webDoc.getURI(),
							sia), cred);
				} else {
					logger.info("this is an annotation to remove");

					// it's an previously existing concept that should be kept
					// for any user -> remove it from the list of concepts to
					// remove their tags
					conceptURIsToRemove.remove(conceptURI);
				}
				usedConceptURIs.add(conceptURI);
			}
			for (String conceptURIToRemove : conceptURIsToRemove) {
				for (Tag annotation : webDoc.getConceptTags(conceptURIToRemove))
					eb.executeCommandNow(
							new RemoveWebDocumentTag(webDoc.getURI(),
									annotation.getTagID(), conceptURIToRemove),
							cred);
			}

		} catch (IOException e) {
			logger.error(e); // TODO real error handling
		}
	}

	private String createNewConcept(EventSenderCredentials cred,
			Language userLanguage, Space space,
			ClientSideTaxonomy clientSideTaxonomy, String conceptLabel)
			throws EventPermissionDeniedException {
		String conceptURI;
		conceptURI = (String) space.getEventBus().executeCommandNow(
				new CreateConceptCmd(new LocalizedString(conceptLabel,
						userLanguage)), cred);
		// put new concept under "prototypical concepts" concept
		ClientSideConcept proto;
		try {
			proto = clientSideTaxonomy
					.getConceptForPrefLabel(new LocalizedString(
							space.getProperty(SpaceProperties.PROTOTYPICAL_CONCEPT_NAME),
							Language.en));
			// TODO how to deal with "prototypical concepts" container and
			// multilinguality?
			String protoURI;
			// create "prototypical concepts" concept if does not exist
			if (proto == null) {
				protoURI = (String) space
						.getEventBus()
						.executeCommandNow(
								new CreateConceptCmd(
										new LocalizedString(
												space.getProperty(SpaceProperties.PROTOTYPICAL_CONCEPT_NAME))),
								cred);
			} else
				protoURI = proto.getURI();
			space.getEventBus()
					.executeCommandNow(
							new AddConnectionCmd(conceptURI, SKOS.HAS_BROADER,
									protoURI), cred);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return conceptURI;
	}

	public WebDocument createWebDocument(String spaceName,
			EventSenderCredentials cred, String docURL, String docTitle)
			throws EventPermissionDeniedException {
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			docURL = checkURL(docURL);
			EventBus eb = currentSpace.getEventBus();
			WebDocument doc = (WebDocument) eb.executeQuery(
					new GetDocumentByURLQuery(docURL), cred);
			if (doc == null)
				doc = (WebDocument) eb.executeCommandNow(new AddWebDocument(
						docURL, docTitle, cred.getKey()), cred);
			return doc;
		} catch (IOException e) {
			logger.error(e);
			return null; // TODO real error handling
		}
	}

	public void removeFromIndex(String spaceName, EventSenderCredentials cred,
			String webURL) throws EventPermissionDeniedException {
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			WebDocument doc = (WebDocument) currentSpace.getEventBus()
					.executeQuery(new GetDocumentByURLQuery(webURL), cred);
			if (doc != null)
				currentSpace.getEventBus().executeCommandNow(
						new RemoveWebDocument(doc.getURI()), cred);
		} catch (IOException e) {
			logger.error(e);
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
	}

	public WebDocument getWebDocumentByURL(String spaceName,
			EventSenderCredentials cred, Language userLanguage, String docURL)
			throws EventPermissionDeniedException {
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			WebDocument doc = (WebDocument) currentSpace.getEventBus()
					.executeQuery(new GetDocumentByURLQuery(docURL), cred);
			return doc;
		} catch (IOException e) {
			logger.error(e);
			return null; // TODO real error handling
		}
	}

	public Set<ClientSideConcept> getConceptsForURIs(String spaceName,
			EventSenderCredentials cred, Set<String> conceptURIs)
			throws EventPermissionDeniedException {
		Set<ClientSideConcept> concepts = new HashSet<ClientSideConcept>();
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			ClientSideTaxonomy clientSideTaxonomy = currentSpace
					.getClientSideTaxonomy();
			for (String currentURI : conceptURIs) {
				logger.info("concept uri: " + currentURI);
				if (clientSideTaxonomy.get(currentURI) != null)
					concepts.add(clientSideTaxonomy.get(currentURI));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return concepts;
	}

	@Override
	public void startDialog(String docURL, String spaceName,
			Language userLanguage, EventSenderCredentials cred)
			throws EventPermissionDeniedException {
		try {
			Space space = server.getSpaces().getSpace(spaceName);
			WebDocument doc = (WebDocument) space.getEventBus().executeQuery(
					new GetDocumentByURLQuery(docURL), cred);
			if (doc != null) {
				@SuppressWarnings("unchecked")
				List<Dialog> dialogs = (List<Dialog>) space.getEventBus()
						.executeQuery(
								new GetDialogsForWebDocument(doc.getURI()),
								cred);
				if (dialogs.size() > 0) {
					space.getEventBus().executeCommandNow(
							new ContinueWebDocumentDialog(dialogs.get(0)
									.getURI(), userLanguage), cred);
				} else {
					space.getEventBus().executeCommandNow(
							new StartWebDocumentDialog(doc.getURI(),
									doc.getTitle(), userLanguage), cred);
				}
			}
		} catch (IOException e) {
			logger.error(e); // TODO realExceptionHandlings
		}
	}

	@Override
	public Collection<ClientSideConcept> getConcepts(String spaceName,
			EventSenderCredentials eventSenderCredentials) {
		// TODO Auto-generated method stub
		try {
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			ClientSideTaxonomy clientSideTaxonomy = currentSpace
					.getClientSideTaxonomy();
			Collection<ClientSideConcept> allConcepts = clientSideTaxonomy
					.getConcepts();
			List<ClientSideConcept> result = new ArrayList<ClientSideConcept>();
			for (ClientSideConcept c : allConcepts) {
				result.add(c);
			}
			return result;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		} catch (EventPermissionDeniedException e) {
			// TODO Auto-generated catch block
			logger.error(e);
		}
		return null;
	}

	/**
	 * Checks the URL if it is correct. If the http:// is missing it will be
	 * added automatically.
	 * 
	 * @param docURL
	 * @return String
	 * @throws EventPermissionDeniedException
	 */
	public static String checkURL(String docURL)
			throws EventPermissionDeniedException {
		if (!(docURL.startsWith("http:") || docURL.startsWith("https:")
				|| docURL.startsWith("file:") || docURL.startsWith("ftp:") || docURL
					.startsWith("sftp:")))
			docURL = "http://" + docURL;
		try {
			new URL(docURL);
		} catch (MalformedURLException e) {
			logger.error(e);
			throw new EventPermissionDeniedException(
					"[error_300] URL not valid", null);
		}
		return docURL;
	}

	@Override
	public void saveAnnotationRating(String spaceName,
			EventSenderCredentials cred, WebDocument webDoc, double rating) {

		logger.info("saveAnnotationRating");
		// invoke the rating web service
		// need the email address
		User user;
		try {
			user = server.getUserDatabase()
					.getUser(cred.getSenderURI());

			RatingService rs = new RatingServiceImpl();
			rs.setUserRating(cred.getSenderURI(), "mailto:" + user.getEmail(),
					spaceName, webDoc.getURL(), (int) rating);

		} catch (IOException e) {
			logger.error("error saving annotation : " + e);
		}

	}

	// important: the rating of the user is requestred

	@Override
	public Rating getAnnotationRating(String spaceName,
			EventSenderCredentials cred, WebDocument webDoc) {
		// need the email address
		User user;
		try {
			user = server.getUserDatabase()
					.getUser(cred.getSenderURI());

			RatingService rs = new RatingServiceImpl();
			return rs.getUserRating(user.getKey(), "mailto:" + user.getEmail(),
					spaceName, "mailto:" + user.getEmail(), webDoc.getURL());

		} catch (Exception e) {
			logger.error("error loading annotation: " + e);
			return new Rating(3, null);
		}

	}

	/**
	 * This gets the space for a space name as String
	 * 
	 * @param spaceName
	 * @param sourceFilePath
	 * @return Space the space for the given String
	 * @throws IOException
	 * @throws EventPermissionDeniedException
	 */
	public String uploadDocToSpace(String spaceName, String sourceFilePath)
			throws IOException, EventPermissionDeniedException {
		try {
			File desFile = null;
			Space currentSpace = server.getSpaces().getSpace(spaceName);
			// Setup destination path for file upload
			File destinationPath = currentSpace.getSpaceDirectory();
			// System.out.println("Destination path :" + destinationPath);
			// System.out.println("Destination path :" + destinationPath+
			// "\\docs\\");
			boolean notfound = true;
			for (File file : destinationPath.listFiles()) {
				// System.out.println("File List :" + file.getAbsolutePath());
				if (file.getName().contentEquals("docs")) {

					// System.out.println("found docs file");
					// System.out.println("Is Directory?:" +
					// file.isDirectory());
					if (!file.isDirectory()) {
						if (!file.mkdir()) {
							throw new IOException(
									"Could not create path docs for documents!");
						}
						notfound = false;
						desFile = file;
						break;
					} else {
						notfound = false;
						desFile = file;
						break;
					}
				}

			}
			if (notfound) {
				desFile = new File(destinationPath.getAbsolutePath()
						+ "\\docs\\");
				if (!desFile.mkdir()) {
					throw new IOException(
							"Could not create path docs for documents!");
				}
			}
			String filename = sourceFilePath;
			boolean found = false;
			while (!found) {

				if (filename.indexOf("\\") == -1) {
					found = true;
					break;
				} else {
					filename = filename.substring(filename.indexOf("\\") + 1);
				}
				// System.out.println("filename: " + filename);
			}
			// TODO: remove comment if events are used to update progress bar.
			// File sourcefile = new File(sourceFilePath);
			// long fileSize = sourcefile.length();
			FileInputStream in = new FileInputStream(sourceFilePath);
			FileOutputStream out = new FileOutputStream(
					desFile.getAbsolutePath() + "\\" + filename);
			byte[] buffer = new byte[4096];
			int len;
			// TODO: remove comment if events are used to update progress bar.
			// long bytesRead = 0;
			while ((len = in.read(buffer)) > 0) {
				out.write(buffer, 0, len);
				// TODO: remove comment if events are used to update progress
				// bar.
				// bytesRead += len;
				// int progress = (int) (len / fileSize);
				// TODO: Send event to progress bar
				// progressSender.newProgress(progress);
			}
			out.close();
			in.close();
			System.out.println("File uploaded: " + filename);
			return desFile.getAbsolutePath() + "\\" + filename;
		} catch (IOException e) {
			logger.error(e);
			return ""; // upload failed
		}
	}
}
