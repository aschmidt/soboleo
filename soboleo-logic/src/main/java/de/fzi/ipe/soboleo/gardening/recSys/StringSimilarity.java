package de.fzi.ipe.soboleo.gardening.recSys;

import java.util.ArrayList;
import java.util.List;

public class StringSimilarity {
	
	
	public static double jaccardSS(String s1,String s2) throws Exception{
		
		String[] c1 = s1.split(" ");
		String[] c2 = s2.split(" ");
		
		List<String> str1=removeStopword (c1);
		List <String> str2=removeStopword(c2);
		//return findSimJacard(str1,str2);
		return findSuperSubJacard(str1,str2);
	}
public static double jaccardSim(String s1,String s2) throws Exception{
		
		String[] c1 = s1.split(" ");
		String[] c2 = s2.split(" ");
		
		List<String> str1=removeStopword (c1);
		List <String> str2=removeStopword(c2);
		//return findSimJacard(str1,str2);
		return findSimJacard(str1,str2);
	}
	private static List<String> removeStopword(String[] s){
		List<String> result=new ArrayList<String>();
		
		for (String a1:s){
			if ( a1.contentEquals("of") || a1.contentEquals("in") || a1.contentEquals("the")){
				
				}
				else
			{
				result.add(a1);
			}
			
		}	
		
		return result;
	}
	private static double findSuperSubJacard(List<String> s1,List<String> s2) throws Exception
	{
		
		double sim=0;
		int count=0;
		if(s1.size()<s2.size())
		{
			
			for(String a1:s1){
				//System.out.println(a1);
				for (String a2:s2){
					//System.out.println(a2);
					if (a1.contentEquals(a2))
					count++;
					}
				}
			//System.out.println(count);
			 int d=s1.size()+s2.size()-count;
			 if (d!=0)
			 sim=(count*100)/(s1.size()+s2.size()-count); // number of matching string/number of non-mathing strings
			}
		//System.out.println(sim);
		return sim/100;
			
	}
	
	private static double findSimJacard(List<String> s1,List<String> s2) throws Exception
	{
		
		double sim=0;
		int count=0;
		
			
			for(String a1:s1){
				//System.out.println(a1);
				for (String a2:s2){
					
					if (a1.contentEquals(a2))
					count++;
					}
				}
			//System.out.println(count);
			 int d=s1.size()+s2.size()-count;
			 if (d!=0)
			 sim=(count*100)/(s1.size()+s2.size()-count); // number of matching string/number of non-mathing strings
			
		//System.out.println(sim);
		return sim/100;
			
	}
	

}
