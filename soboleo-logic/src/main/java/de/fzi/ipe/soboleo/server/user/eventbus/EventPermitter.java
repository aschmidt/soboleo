/*
 * FZI - Information Process Engineering 
 * Created on 20.08.2009 by zach
 */
package de.fzi.ipe.soboleo.server.user.eventbus;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBusPermitter;
import de.fzi.ipe.soboleo.space.Space;
import de.fzi.ipe.soboleo.space.SpaceProperties;
import de.fzi.ipe.soboleo.user.UserDatabase;
import de.fzi.ipe.soboleo.user.User;

/** 
 * EventBus adapter that checks events, commands and queries 
 * against the user database and against the space properties. 
 */
public class EventPermitter implements EventBusPermitter{

	private boolean readAll = false;
	private boolean writeAll = false;

	private Set<String> allowedKeys; 
	
	
	public EventPermitter(Space space, UserDatabase userDatabase) throws IOException {
		String property = space.getProperty(SpaceProperties.SECURITY);
		if (property.equals("open")) {
			readAll = true;
			writeAll = true;
		}
		else if (property.equals("write")) {
			readAll = true;
			writeAll = false;
		}
		else if (property.equals("readwrite")) {
			readAll = false;
			writeAll = false;
		}
		if (!readAll || !writeAll) {
			allowedKeys = new HashSet<String>();
			if (userDatabase != null) {
				Set<User> userForSpace = userDatabase.getUserForSpace(space.getURI());
				for (User user: userForSpace) allowedKeys.add(user.getKey());
			}
		}
	}
	
	@Override
	public String permitCommand(CommandEvent event, EventSenderCredentials credentials) {
		if (writeAll) return null;
		else if (credentials == EventSenderCredentials.SERVER) return null;
		else if (allowedKeys.contains(credentials.getKey())) return null;
		else return "[error_200] You are not allowed to change this space! - Please login or ask for permission for this space";
	}
	
	@Override
	public String permitEvent(Event event, EventSenderCredentials credentials) {
		if (readAll) return null;
		else if (credentials == EventSenderCredentials.SERVER) return null;
		else if (allowedKeys.contains(credentials.getKey())) return null;
		else return "[error_201] You are not allowed to view this space! Please login or ask for permission for this space";
	}
	
}
