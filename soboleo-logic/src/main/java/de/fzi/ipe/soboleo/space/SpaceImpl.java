/*
 * FZI - Information Process Engineering 
 * Created on 21.08.2009 by zach
 */
package de.fzi.ipe.soboleo.space;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import de.fzi.ipe.soboleo.beans.ontology.skos.ClientSideTaxonomy;
import de.fzi.ipe.soboleo.event.common.ShutdownCommand;
import de.fzi.ipe.soboleo.event.user.EventPermissionDeniedException;
import de.fzi.ipe.soboleo.event.user.EventSenderCredentials;
import de.fzi.ipe.soboleo.eventbus.EventBus;
import de.fzi.ipe.soboleo.lucene.SearchingLuceneWrapper;
import de.fzi.ipe.soboleo.lucene.eventubus.LuceneEventBusAdaptor;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.queries.rdf.skos.GetClientSideTaxonomy;
import de.fzi.ipe.soboleo.space.commandHistory.CommandHistory;
import de.fzi.ipe.soboleo.space.logging.SystemOutLogger;
import de.fzi.ipe.soboleo.user.UserDatabase;
 
public class SpaceImpl implements Space{

	protected Logger logger = Logger.getLogger(SpaceImpl.class);
	
	private final File  spaceDirectory;
	private final String spaceURI;
	private Properties properties; 
	private EventBus eventBus = null;
	private TripleStore tripleStore;
	private ClientSideTaxonomy clientSideTaxonomy;
	private SearchingLuceneWrapper searchingLuceneWrapper;
	
	/**
	 * Creates a new space object. Only one space object should exist at any given time for each directory. 
	 * @param spaceDirectory Directory where the data of this space is stored. Is created if it didn't exist. 
	 * @param spaceURI Arbitrary name used to identify this space.
	 * @param defaultProperties Properties object with default values for the space properties. Can be null. 
	 * @throws IOException 
	 * @throws EventPermissionDeniedException 
	 */
	public SpaceImpl(File spaceDirectory, String spaceURI, UserDatabase userDatabase) throws IOException, EventPermissionDeniedException {
		this.spaceDirectory = spaceDirectory; 
		this.spaceURI = spaceURI;
		setupSpaceDirectory(spaceDirectory);
		setupProperties();
		setupEventBus();
		setupClientSideTaxonomy();
	}
	

	private void setupClientSideTaxonomy() throws IOException, EventPermissionDeniedException
	{
		logger.debug("Initializing client-side taxonomy");
		ClientSideTaxonomy t = (ClientSideTaxonomy) getEventBus().executeQuery(new GetClientSideTaxonomy(true), EventSenderCredentials.SERVER);
		setClientSideTaxonomy(t);
		
		if (t==null)
			logger.error("EventBus returned null client side taxonomy");
	}
	
		
	private LuceneEventBusAdaptor luceneBusAdaptor=null;
	
	public  LuceneEventBusAdaptor getLuceneEventBus()	{
		return luceneBusAdaptor;
	}
	
	public void setLuceneEventBus(LuceneEventBusAdaptor a) {
		luceneBusAdaptor = a;
	}

	public void setTripleStore(TripleStore s)
	{
		tripleStore = s;
	}
		
	private void setupProperties() throws IOException {
		Properties defaultProperties = new Properties();
		for (SpaceProperties currentProp: SpaceProperties.values()) {
			defaultProperties.setProperty(currentProp.getKey(), currentProp.getDefaultValue());
		}
		this.properties = new Properties(defaultProperties);
		File propertyFile = new File(spaceDirectory, PROPERTY_FILE_NAME);
		if (propertyFile.exists()) {
			InputStream io = new BufferedInputStream(new FileInputStream(propertyFile));
			properties.load(io);
		}
	}
		
	//create the initial layout for the eventbus.
	private void setupEventBus() {
		eventBus = new EventBus();
		eventBus.addListener(new CommandHistory());
		eventBus.addListener(new SystemOutLogger(properties));
	}

	//ensure directory exists and is well
	private void setupSpaceDirectory(File spaceDirectory) throws IOException {
		if (spaceDirectory.exists()) {
			if (!spaceDirectory.isDirectory()) throw new IOException("spaceDirectory needs to be a directory!");
		}
		else if (!spaceDirectory.mkdirs()) throw new IOException("Failed creating spaceDirectory");
	}

	/**
	 * Closes the space and saves any possible changes. After calling this operation the space object should 
	 * be discarded; won't work anymore.
	 */
	@Override
	public void close() throws IOException {
		try {
			eventBus.executeCommandNow(new ShutdownCommand(), EventSenderCredentials.SERVER);
		} catch (EventPermissionDeniedException e) {
			throw new AssertionError(e); //must not happen
		}
		eventBus.stop();
		savePropertyFile();
		
		tripleStore.close();
	}

	private void savePropertyFile() throws FileNotFoundException, IOException {
		File propertyFile = new File(spaceDirectory, PROPERTY_FILE_NAME);
		OutputStream out = new BufferedOutputStream(new FileOutputStream(propertyFile));
		properties.store(out, "Space Properties");
		out.close();
	}
	
	@Override
	public EventBus getEventBus() {
		return eventBus;
	}
	
	@Override
	public String getURI() {
		return spaceURI;
	}
	
	@Override
	public String getProperty(SpaceProperties propertyName) {
		return properties.getProperty(propertyName.getKey());
	}
	
	@Override
	public void setProperty(SpaceProperties propertyName, String value) throws IOException {
		properties.setProperty(propertyName.getKey(), value);
		savePropertyFile();
	}
	
	@Override
	public String getTripleStoreDump() throws IOException {
		return tripleStore.getContentDump();
	}
	
	@Override
	public String getTaxonomyRDFDump() throws IOException {
		return tripleStore.getSKOSTaxonomyAsXML();
	}
	
	@Override
	public String getCompleteRDFDump() throws IOException {
		return tripleStore.getCompleteRDFDump();
	}
	
	@Override
	public String getTripleStoreURI(){
		return tripleStore.getDatastoreURI().toString();
	}
	
	@Override
	public ClientSideTaxonomy getClientSideTaxonomy() {
		return clientSideTaxonomy;
	}

	public void setClientSideTaxonomy(ClientSideTaxonomy t)
	{
		this.clientSideTaxonomy = t;
	}

	@Override
	public SearchingLuceneWrapper getSearchingLuceneWrapper(){
		return searchingLuceneWrapper;
	}

	public void setSearchingLuceneWrapper(SearchingLuceneWrapper w)
	{
		searchingLuceneWrapper = w;
	}
	
	@Override
	public File getSpaceDirectory()
	{
	    return this.spaceDirectory;
	}
	
}
