/*
 * Contains the information about the users. On startup it demands a 
 * directory where it will create (or open) the user database. 
 * 
 * FZI - Information Process Engineering 
 * Created on 04.03.2009 by zach
 */
package de.fzi.ipe.soboleo.user;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;

import de.fzi.ipe.soboleo.beans.ontology.RDF;
import de.fzi.ipe.soboleo.beans.ontology.SoboleoNS;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStore;
import de.fzi.ipe.soboleo.ontology.triplestore.TripleStoreFactory;


public class UserDatabaseImpl implements UserDatabase {

	public static final String USER_DIRECTORY = "users-db";
	
	
	private TripleStore tripleStore;
	private Map<String,User> userCache = new HashMap<String,User>();
	private Map<String,User> userKeyCache = new WeakHashMap<String, User>();

	private String defaultUserURI = null;
	
	public UserDatabaseImpl(File directory, TripleStoreFactory factory) throws IOException {
		tripleStore = factory.getTripleStore(new File(directory,USER_DIRECTORY),USER_DIRECTORY);
		ensureDefaultUser();
	}
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#getTripleStoreDump()
	 */
	@Override
	public String getTripleStoreDump() throws IOException {
		return tripleStore.getContentDump();
	}
	
	private void ensureDefaultUser() throws IOException {
		User user = null;
		ValueFactory vf = tripleStore.getValueFactory();
		URI propertyURI = vf.createURI(RDF.TYPE.toString());
		URI objectURI = vf.createURI(SoboleoNS.USER_TYPE_DEFAULT_USER.toString());
		List<Statement> statements = tripleStore.getStatementList(null, propertyURI, objectURI);
		if (statements.size()!= 0) {
			Statement statement = statements.get(0);
			String uri = statement.getSubject().toString();
			user = getUser(uri);
		}
		else {
			user = createNewUser();
			user.setName("Anonymous");
			user.setEmail("noemail available");
			URI userURI = vf.createURI(user.getURI());
			URI type = vf.createURI(RDF.TYPE.toString());
			URI typeDefaultUser  = vf.createURI(SoboleoNS.USER_TYPE_DEFAULT_USER.toString());
			tripleStore.addTriple(userURI, type, typeDefaultUser, null);
		}
		defaultUserURI = user.getURI();
	}

	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#close()
	 */
	@Override
	public synchronized void close() throws IOException {
		tripleStore.close();
	}
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#createNewUser()
	 */
	@Override
	public synchronized User createNewUser() throws IOException {
		User newUser = new User(tripleStore.getUniqueURI(),tripleStore);
		newUser.createUser();
		userCache.put(newUser.getURI(),newUser);
		return newUser;
	}

	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#deleteUser(de.fzi.ipe.soboleo.user.User)
	 */
	@Override
	public synchronized void deleteUser(User user) throws IOException {
		if (user.getURI().equals(defaultUserURI)) return; //never delete default user
		else {
			userCache.remove(user.getURI());
			userKeyCache.remove(user.getKey());
			user.deleteUser();
 		}
	}
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#getDefaultUser()
	 */
	@Override
	public synchronized User getDefaultUser() throws IOException {
		return getUser(defaultUserURI);
	}
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#getUser(java.lang.String)
	 */
	@Override
	public synchronized User getUser(String uri) throws IOException {
		User user = userCache.get(uri);
		if (user == null) {
			ValueFactory vf = tripleStore.getValueFactory();
			URI userURI = vf.createURI(uri);
			user = new User(userURI,tripleStore);
			userCache.put(user.getURI(),user);
		}
		return user;
	}
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#getAllUsers()
	 */
	@Override
	public synchronized Set<User> getAllUsers() throws IOException {
		List<Statement> userStatements = tripleStore.getStatementList(null, RDF.TYPE.toString(),SoboleoNS.USER_TYPE.toString());
		Set<User> toReturn = new HashSet<User>();
		for (Statement s:userStatements) {
			toReturn.add(getUser(s.getSubject().toString()));
		}
		return toReturn;
	}

	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#getUserForSpace(java.lang.String)
	 */
	@Override
	public synchronized Set<User> getUserForSpace(String spaceIdentifier) throws IOException {
		Set<User> toReturn = new HashSet<User>();
		ValueFactory vf = tripleStore.getValueFactory();
		URI p = vf.createURI(SoboleoNS.USER_TAXONOMY.toString());
		Literal o = vf.createLiteral(spaceIdentifier);
		List<Statement> statementList = tripleStore.getStatementList(null, p, o);
		for (Statement stmt: statementList) {
			toReturn.add(getUser(stmt.getSubject().toString()));
		}
		return toReturn;
	}
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#getUserForKey(java.lang.String)
	 */
	@Override
	public synchronized User getUserForKey(String key) throws IOException {
		User toReturn = userKeyCache.get(key);
		if (toReturn == null) {
			toReturn = getUserForPropertyValue(SoboleoNS.USER_KEY.toString(), key);
			userKeyCache.put(key, toReturn);
		}
		return toReturn;
	}
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.soboleo.user.IUserDatabase#getUserForEmail(java.lang.String)
	 */
	@Override
	public synchronized User getUserForEmail(String emailString) throws IOException {
		return getUserForPropertyValue(SoboleoNS.USER_EMAIL.toString(), emailString.toLowerCase());
	}

	private User getUserForPropertyValue(String property, String object) throws IOException {
		if (property == null | object == null) return null;
		ValueFactory vf = tripleStore.getValueFactory();
		URI propertyURI = vf.createURI(property);
		Literal objectLiteral = vf.createLiteral(object);
		List<Statement> statements = tripleStore.getStatementList(null, propertyURI, objectLiteral);
		if (statements.size()== 0) return null;
		else {
			Statement statement = statements.get(0);
			String uri = statement.getSubject().toString();
			return getUser(uri);
		}
	}
}
