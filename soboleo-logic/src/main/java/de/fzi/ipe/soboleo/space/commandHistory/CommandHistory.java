/*
 * Stores and makes accessible the history of commands. This class is important to allow 
 * Ajax clients that can then query for the events they 'missed'. 
 * FZI - Information Process Engineering 
 * Created on 27.09.2008 by zach
 */
package de.fzi.ipe.soboleo.space.commandHistory;

import java.util.ArrayList;
import java.util.List;

import de.fzi.ipe.soboleo.event.Event;
import de.fzi.ipe.soboleo.event.execution.CommandEvent;
import de.fzi.ipe.soboleo.event.execution.QueryEvent;
import de.fzi.ipe.soboleo.eventbus.EventBusCommandProcessor;
import de.fzi.ipe.soboleo.eventbus.EventBusQueryProcessor;

public class CommandHistory implements EventBusCommandProcessor,EventBusQueryProcessor{

	private static final int BUFFER_CAPACITY = 5000;
	
	private List<CommandEvent> history = new ArrayList<CommandEvent>(BUFFER_CAPACITY);
	private long highestEventIDSeen = -1;

	
	@Override
	public CommandEvent prepare(CommandEvent command) {
		return null;
	}

	@Override
	public Object process(CommandEvent command) {
		if ((history.size()) == BUFFER_CAPACITY) history.remove(0);
		history.add(command);
		highestEventIDSeen = command.getId();
		return null;
	}

	@Override
	public QueryEvent prepare(QueryEvent query) {
		return null;
	}

	@Override
	public Event[] process(QueryEvent query) {
		if (query instanceof QueryPastEvents) {
			QueryPastEvents passedEvents = (QueryPastEvents) query;
			if (passedEvents.getEventIdQueried() >= highestEventIDSeen) return null;
			else {
				int beginIndex = getBeginIndex(passedEvents.getEventIdQueried());
				Event[] toReturn = new Event[history.size()-beginIndex];
				for (int i=0;i<toReturn.length;i++) toReturn[i] = history.get(beginIndex+i);
				return toReturn;
			}
		}
		return null;
	}

	private int getBeginIndex(long eventIdQueried) {
		int i = history.size()-1;
		for (;i>=0;i--) {
			if (history.get(i).getId()<=eventIdQueried) {
				i++;
				break;
			}
		}
		return Math.max(i,0); //to catch queries using -1 as query id
	}

	
	
	
}
